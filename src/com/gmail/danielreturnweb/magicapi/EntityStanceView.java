package com.gmail.danielreturnweb.magicapi;

import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Entity;

/**
 * This interface defends a standard way to check the stance of two entities. <p>
 * This is a service that can be overridden by other plugin's to provide better
 * stance setting.
 * <p> Example, a factions plugin can set the service to use a custom EntityStanceView that sets
 * all faction members to friendly while setting hostile faction members to Hostile.
 * @author daniel
 *
 */
public interface EntityStanceView
{
	/**
	 * This is the stance between two entities. This states if they are friends, enemies, hostile, etc.
	 * <p>
	 * Stance's have a number value associated with them. The higher the value the more
	 * friendly the stance is. The only exception is UNKNOWN.
	 * @author daniel
	 *
	 */
	 public enum EntityStance
	 {
		 /** Represents a Hostile State. Value: 0*/
		HOSTILE(0,ChatColor.RED + "HOSTILE" + ChatColor.RESET),
		/** Represents a Hostile State. Value: 1*/
		NEUTRAL(1,ChatColor.LIGHT_PURPLE + "NEUTRAL" + ChatColor.RESET),
		/** Represents a Hostile State. Value: 2*/
		FRIENDLY(2,ChatColor.GREEN + "FRIENDLY" + ChatColor.RESET),
		/** Represents a Hostile State. Value: 100*/
		UNKNOWN(100,ChatColor.BLACK + "UNKNOWN" + ChatColor.RESET);
		
		
		int val = 0;
		String coloredString = "";
		
		EntityStance(int val, String coloredString)
		{
			this.val = val;
			this.coloredString = coloredString;
		}
		
		public int getValue()
		{
			return val;
		}
		
		/**
		 * Gets the EntityStance as a string formatted with ChatColor colors. 
		 * @return
		 */
		public String coloredString()
		{
			return coloredString;
		}
	 }
	 
	 
	 public EntityStance getStance(Entity entity1, Entity entity2);
	 public EntityStance getStance(OfflinePlayer player1, OfflinePlayer player2);
	 public EntityStance getStance (OfflinePlayer player, Entity entity);
	 
	 
	 public void setStance (OfflinePlayer player1, OfflinePlayer player2, EntityStance stance);
	 //TODO setStance(Player player, LivingEntity entity) ?? This could allow for better integration of making hostile monsters not hostile to the player
	 //TODO would I also want a setStance for LivingEntity LivingEntity?
}
