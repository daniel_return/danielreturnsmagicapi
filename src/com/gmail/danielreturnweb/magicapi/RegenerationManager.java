package com.gmail.danielreturnweb.magicapi;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;

import com.gmail.danielreturnweb.magicapi.events.other.HealthRegenerationEvent;


/**
 * Allows the API user to add or subtract health regeneration from an entity. This regeneration
 * occurs every second and is separate from vanilla regeneration. <p>
 * Regeneration has RegenerationType which dictates what type of regeneration this is.
 * Spells and Effects may interact with regeneration in differing ways based upon the
 * regeneration type
 * @author daniel
 *
 */
public class RegenerationManager implements Runnable
{
	/**
	 * The type of Regeneration this represents. <p>
	 * This aids in separation of spells and effects when doing effects that 
	 * provide regeneration or reduces it.
	 * <p> Regeneration should never be < 0 while degeneration should never be > 0 <p>
	 * This aids in creation of effects that boost HP regeneration while
	 * preventing it from boosting degeneration effects.
	 * @author daniel
	 *
	 */
	public enum RegenerationType
	{
		/**This represents health regeneration that is considered natural */
		NATURAL_REGENERATION,
		
		/**This represents health regeneration that is considered magical */
		MAGIC_REGENERATION,
		
		
		
		/** This represents health degeneration that is considered natural*/
		NATURAL_DEGENERATION,
		
		/**This represents health degeneration that is considered magical */
		MAGIC_DEGENERATION;
		
	}
	
	private static boolean hasRunnable = false; //If a second regen manager is created we do not want to spawn an additional runnable for it, though this shoudl not occure.
	protected Map<UUID,Map<String,RegenerationContainer>> tracker;
	
	double baseRegenerationPlayer = 0;
	double baseRegenerationMonster = 0;
	double baseRegenerationBoss = 0;
	
	boolean negativeNaturalHarms = false;
	boolean negativeMagicalHarms = true;
	boolean negativeCanKill = false;
	
	public RegenerationManager(MagicAPI api)
	{
		tracker = new HashMap<UUID,Map<String,RegenerationContainer>>();
		
		if(!hasRunnable) Bukkit.getScheduler().scheduleSyncRepeatingTask(MagicAPI.getInstance(), this,60, 20);
		
		baseRegenerationPlayer = api.getConfig().getDouble("Regeneration.base_regen.players");
		baseRegenerationMonster = api.getConfig().getDouble("Regeneration.base_regen.monsters");
		baseRegenerationBoss = api.getConfig().getDouble("Regeneration.base_regen.boss");
		
		negativeNaturalHarms = api.getConfig().getBoolean("Regeneration.negative_harms.natural");
		negativeMagicalHarms = api.getConfig().getBoolean("Regeneration.negative_harms.magical");
		negativeCanKill = api.getConfig().getBoolean("Regeneration.negative_harms.canKill");
	}
	
	/**
	 * Adds regeneration to the entity
	 * @param entity
	 * @param label
	 * @param value
	 * @param type
	 */
	public void addRegeneration(UUID entity, String label, double value, RegenerationType type)
	{
		if(!tracker.containsKey(entity)) tracker.put(entity, new HashMap<String,RegenerationContainer>());
	
		tracker.get(entity).put(label, new RegenerationContainer(value,type));
		return;
	}
	
	/**
	 * Removes regeneration from the entity
	 * @param entity
	 * @param label
	 */
	public void removeRegeneration(UUID entity, String label)
	{
		if(!tracker.containsKey(entity)) tracker.put(entity, new HashMap<String,RegenerationContainer>());
		
		tracker.get(entity).remove(label);
		return;
	}
	
	/**
	 * <b>Gets the total regeneration on this entity. </b> <p>
	 * This is all regeneration and degeneration values placed upon it.
	 * To get specific Regeneration Types use getRegenerationByType()
	 * @param entity
	 * @return
	 * @see #getRegenerationByType(UUID, RegenerationType)
	 */
	public double getTotalgeneration(UUID entity)
	{
		if(!tracker.containsKey(entity)) return 0;
		
		double regen = 0;
		for(Entry<String,RegenerationContainer> rg : tracker.get(entity).entrySet())
		{
			regen += rg.getValue().getValue();
		}
		return regen;
	}
	
	/**
	 * Gets the regeneration by type on this entity
	 * @param entity
	 * @param type
	 * @return
	 */
	public double getRegenerationByType(UUID entity, RegenerationType type)
	{
		if(!tracker.containsKey(entity)) return 0;
		
		double regen = 0;
		for(Entry<String,RegenerationContainer> rg : tracker.get(entity).entrySet())
		{
			if(rg.getValue().getType() == type) regen += rg.getValue().getValue();
		}
		return regen;
	}
	
	
	/**
	 * Sees if the entity has any form of regeneration
	 * @param entity
	 * @return
	 */
	public boolean hasRegeneration(UUID entity)
	{
		if(!tracker.containsKey(entity)) return false;
		
		for(Entry<String,RegenerationContainer> rg : tracker.get(entity).entrySet())
		{
			if(rg.getValue().getType() == RegenerationType.NATURAL_REGENERATION || rg.getValue().getType() == RegenerationType.MAGIC_REGENERATION)
			{
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Sees if the entity has any form of health degeneration
	 * @param entity
	 * @return
	 */
	public boolean hasDegeneration(UUID entity)
	{
		if(!tracker.containsKey(entity)) return false;
		
		for(Entry<String,RegenerationContainer> rg : tracker.get(entity).entrySet())
		{
			if(rg.getValue().getType() == RegenerationType.NATURAL_DEGENERATION || rg.getValue().getType() == RegenerationType.MAGIC_DEGENERATION)
			{
				return true;
			}
		}
		return false;
		
	}
	
	/**
	 * Sees if the entity has any regeneration/degeneration of the provided type
	 * @param entity
	 * @param type
	 * @return
	 */
	public boolean hasRegenerationOfType(UUID entity, RegenerationType type)
	{
		if(!tracker.containsKey(entity)) return false;
		
		for(Entry<String,RegenerationContainer> rg : tracker.get(entity).entrySet())
		{
			if(rg.getValue().getType() == type) return true;
		}
		return false;
	}
	
	
	/**
	 * See if the entity has regeneration/degeneration that matches the provided label
	 * @param entity
	 * @param label
	 * @return
	 */
	public boolean hasRegenerationByLabel(UUID entity, String label)
	{
		if(!tracker.containsKey(entity)) return false;
		
		for(Entry<String,RegenerationContainer> rg : tracker.get(entity).entrySet())
		{
			if(rg.getKey().equals(label)) return true;
		}
		return false;
	}
	
	
	private void removeDead()
	{
		for(Entry<UUID,Map<String,RegenerationContainer>> obj : this.tracker.entrySet())
		{
			Entity entity = Bukkit.getEntity(obj.getKey()); //This returns players and entities :)
			if(entity == null) this.tracker.remove(obj.getKey());
//			else if(entity.isEmpty()) this.tracker.remove(obj.getKey()); //TODO is this causing no regen to occure???
			else if(entity.isDead()) this.tracker.remove(obj.getKey());			
		}
	}
	
	/**
	 * Places the instant heal upon the entity
	 * @param entity
	 * @param heal
	 */
	private void applyHeal(LivingEntity entity, double heal)
	{
		double max = entity.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue();
		double current = entity.getHealth();
		double hp = heal + current;
		
		if(hp > max) hp = max;
		if(hp <= 0) hp = 1;
//		hp = DoubleTruncator.truncateDouble(hp, 4);
		
		entity.setHealth(hp);
	}
	
	private void dealDamage(LivingEntity entity, double damage)
	{
		double hp = entity.getHealth() - damage;
		if(hp <=0)
		{
			if(this.negativeCanKill) hp = 0;
			else hp = 1;
		}
		if(!entity.isDead()) entity.setHealth(hp);
	}
	

	

	@Override
	public void run()
	{
		this.removeDead();
		
		for(Entry<UUID,Map<String,RegenerationContainer>> obj : this.tracker.entrySet())
		{
			double regen = this.getTotalgeneration(obj.getKey());
			Entity entity = Bukkit.getEntity(obj.getKey()); //This returns players and entities :)
			
			if(!entity.isDead())
			{
				if(entity instanceof LivingEntity)
				{
					LivingEntity liv = (LivingEntity)entity;
					
					double nr = this.getRegenerationByType(entity.getUniqueId(), RegenerationType.NATURAL_REGENERATION);
					double mr = this.getRegenerationByType(entity.getUniqueId(), RegenerationType.MAGIC_REGENERATION);
					double nd = this.getRegenerationByType(entity.getUniqueId(), RegenerationType.NATURAL_DEGENERATION);
					double md = this.getRegenerationByType(entity.getUniqueId(), RegenerationType.MAGIC_DEGENERATION);
										
					
//					TODO Should Magic Degeneration be reduced by Magic Resistance?
					
					HealthRegenerationEvent event = new HealthRegenerationEvent((LivingEntity)entity,nr,nd,mr,md);
					Bukkit.getServer().getPluginManager().callEvent(event);				
					
					if(!event.isCancelled())
					{
						double natReg = event.getTotalNaturalRegeneration();
						double magReg = event.getTotalMagicalRegeneration();
						
						if(natReg < 0 && !negativeNaturalHarms) natReg = 0;
						if(magReg < 0 && !negativeMagicalHarms) magReg = 0;
						
						regen = natReg+magReg;
						
						if(regen >= 0) this.applyHeal(liv, regen);
						else dealDamage(liv,0-regen);
					}
				}
			}
		}
	}
	
	
	
	//********************************************
	//TODO place base natural regeneration on Players, Monsters, and bosses
	//TODO possibly have a config value stating if this is natural or magical instead of it allways being natural
	//********************************************
	
	//Place base regeneration on players
	@EventHandler
	public void onLogin(PlayerJoinEvent event)
	{
		Player player = event.getPlayer();
		if(this.baseRegenerationPlayer > 0)
		{
			this.addRegeneration(player.getUniqueId(), "MAGIC_API_BASE", this.baseRegenerationPlayer, RegenerationType.NATURAL_REGENERATION);
		}
		else if(this.baseRegenerationPlayer < 0)
		{
			this.addRegeneration(player.getUniqueId(), "MAGIC_API_BASE", this.baseRegenerationPlayer, RegenerationType.NATURAL_DEGENERATION);
		}
	}
	
	//TODO on spawn event for boss and monster regeneration
}
