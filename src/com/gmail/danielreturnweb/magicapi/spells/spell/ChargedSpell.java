package com.gmail.danielreturnweb.magicapi.spells.spell;

/**
 * For use with SpellItem. <p>
 * This marks a SpellItem as using the charge system.
 * @author daniel
 */
public interface ChargedSpell
{
	
	/**
	 * Gets the charges this item has.<br> 
	 * @return
	 */
	public int getSpellCharges();
	
	
	/**
	 * When a item reaches/has 0 charges is it destroyed? <br>
	 * If false, the item will not break when it uses up its last charge.
	 * Furthermore future uses of the item will not cast. <br>
	 * If true, the item is destroyed.  <br>
	 * Only functions if getSpellCharges() returns a number >= 0.
	 * 
	 * @return true - item is destroyed, false - not destroyed 
	 */
	public boolean itemDestoryedOnLastChargeCast();
}
