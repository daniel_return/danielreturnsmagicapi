package com.gmail.danielreturnweb.magicapi.spells.spell;

/**
 * For use with SpellItem. <p>
 * This marks a SpellItem as a spell that costs vanilla durability to cast.
 * Upon the item performing its spell cast, durability will be consumed.
 * If the item does not have enough durability, then the spell is unable to be casted.
 * @author daniel
 * @deprecated Currently not supported, will not work. May support in the future
 */
public interface DurabilitySpell
{
	/**
	 * How much durability will be removed from the held item upon casting.
	 * If the remaining durability is less then this, it will not cast.
	 * @return
	 */
	public int getCastDurabilityCost();
	
	/**
	 * If the item runs out of durability upon cast will the item break.
	 * If set to false, the item will have its durability set to 1.
	 * 
	 * @return true - item breaks, false - does not break.
	 */
	public boolean noDurabilityBreaksItem();
}
