package com.gmail.danielreturnweb.magicapi.spells.spell;

import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffect;

/**
 * For use with SpellItem. <p>
 * This marks a SpellItem as providing a passive ability.
 * Any active ability the item has will still function. <p>
 * Passive abilities have no cooldown, mana cost, durability cost, etc.
 * For simplicity a passive spell 
 * @author daniel
 *
 */
public interface PassiveSpell
{
	/**
	 * Represents a slot that a passive spell is active within.
	 * @author daniel
	 *
	 */
	public enum PassiveSlot
	{
		/** Represents the players main inventory not including the Hot Bar or armor slots.*/
		Inventory,
		
		/** The 9 Inventory Slots on the Hot Bar */
		HotBar, 
		
		/** The inventory slot selected by the players main hand */
		MainHand, 
		
		/** The Off Hand Slot */
		OffHand, 
		
		/** Any of the 4 armor slots */
		ArmorSlot, 
		
		HeadSlot, 
		
		ChestSlot, 
		
		LeggingsSlot, 
		
		BootsSlot;
	}
	
	
	/**
	 * Gets the Custom Effect that is placed upon the entity when the passive spell is active.
	 * This ability is occasionally refreshed. When refreshed it's removal type will be EffectRemovalType.Stack.
	 * @return
	 */
	public CustomEffect getPassiveAbility();
	
	
	/**Gets the slot that the passive is active in
	 * If the item SpellItem using this interface is not in
	 * one of the provided slots, then it becomes inactive.  
	 */
	public PassiveSlot[] getPassiveSlots();
	
}
