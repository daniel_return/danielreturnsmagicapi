package com.gmail.danielreturnweb.magicapi.spells.spell;


/**
 * Represents a spell that is casted with a command.
 * @author daniel
 *
 */
public interface SpellCommand extends Spell
{	
	/** 
	 * The permission to cast this spell with the /spellcast command <p>
	 * 
	 * If set to empty "" or null it requires no permission to cast. 
	 */
	public String getPermissionToCast();
	
	/**
	 * Gets the commands Alias. This should be a short mnemonic to allow quicker casting
	 * of the spell
	 * @return
	 */
	public String getAlias();
}
