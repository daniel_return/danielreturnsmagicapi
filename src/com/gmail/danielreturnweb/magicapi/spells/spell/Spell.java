package com.gmail.danielreturnweb.magicapi.spells.spell;

import com.gmail.danielreturnweb.magicapi.spell.SpellCastData;
import com.gmail.danielreturnweb.magicapi.spell.SpellCooldownType;
import com.gmail.danielreturnweb.magicapi.spell.SpellTargeting;


/**
 * Represents a basic spell. This is not useful on its own, one should implement SpellItem or/and SpellCommand.
 * 
 * @author daniel
 *
 */
public interface Spell 
{
	/**
	 * Gets the xp that this spells costs to cast.
	 * @return
	 */
	public int getSpellXPCost();
	
	/**
	 * Gets the cooldown in seconds of this spell
	 * @return
	 */
	public int getSpellCooldown();
	
	/**
	 * Gets the cooldown type of the spell
	 * @return
	 */
	public SpellCooldownType getSpellCooldownType();
	
	/**
	 * Gets the spells ID. <p>
	 * A id should not contain any spaces. 
	 * They can be camel case or snake case.
	 * return The spells ID
	 */
	public String getSpellID();
	
	
	/**
	 * Gets the name of a spell
	 * @return
	 */
	public String getSpellName();
	
	
	/**
	 * Gets the spells entry for the Grimoire command. <p>
	 * Can use ChatColor codes
	 * @return
	 */
	public String[] getGrimoireEntry();
	
	

	/**
	 * Does the actions the spell takes when casted. If it returns true it means the spell was
	 * casted and the cooldown must be set and xp consumed. Returning false does not
	 * consume xp or set cooldown.
	 * @param data
	 * @param targeting
	 * @return True if the spell was casted, false if the spell was not casted for any reasons
	 */
	public boolean onSpellCast(SpellCastData data, SpellTargeting targeting);
}
