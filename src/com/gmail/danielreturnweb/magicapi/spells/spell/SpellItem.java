package com.gmail.danielreturnweb.magicapi.spells.spell;

import org.bukkit.event.block.Action;

/**
 * Represents a spell that can be bound and casted with a item.
 * @author daniel
 *
 */
public interface SpellItem extends Spell
{
	public enum CastHand
	{
		MainHand("Main Hand"), OffHand("Off Hand"), Both("Either Hand"), None("None");
		
		private String text = "";
		CastHand(String s)
		{
			text = s;
		}
		public String getText()
		{
			return text;
		}
	}
	
	
	
	/**
	 * The actions that cause the spell to be casted when bound to a item. <p>
	 * Only works if the spell is bound to a item, else it is ignored.
	 * @return
	 */
	public Action[] getCastActions();
	//TODO does any Action trigger when attacking? For spells that inflict an effect upon attack.
	
	
	public CastHand getCastHand();
	
	
	/**
	 * Gets the lore description of this spell.
	 * 
	 * @return The description or a empty String[]. Do not return NULL.
	 */
	public String[] getLoreDescription();
	
	
	//TODO Passive Spell? is this a separate interface like DurabilityCastable? it effectively returns a
	//Effect placed upon the player when the item is held. The effect would be removed upon no longer being held
	//May have other triggers such as entering the inventory, being held in off hand, or worn as armor.
	//Triggers: HELD_MAIN_HAND, HELD_OFF_HAND, HELD (either hand), WORN (armor), INVENTORY (anywere in the inventory). 
}
