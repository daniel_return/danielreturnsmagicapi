package com.gmail.danielreturnweb.magicapi.summons;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.LivingEntity;

/**
 * 
 * @author daniel
 *
 */
public class SummonTracker
{
	List<LivingEntity> summons;
	List<Double> pointCosts;
	

	public SummonTracker()
	{
		summons = new ArrayList<LivingEntity>();
		pointCosts = new ArrayList<Double>();
	}
	
	/**
	 * Gets the number of summons this player has
	 * @return
	 */
	public int getNumberOfSummons()
	{
		return summons.size();
	}
	
	/**
	 * Gets how many points are being used by active summons
	 * @return
	 */
	public double getPointUsage()
	{
		double use = 0;
		for(double d: pointCosts)
		{
			use+= d;
		}
		return use;
	}
	
	/**
	 * Adds an entity with its cost to the players SummonTracker
	 * @param e
	 * @param cost
	 */
	public void addEntity(LivingEntity e, double cost)
	{
		summons.add(e);
		pointCosts.add(cost);
	}
	
	/**
	 * Removes the entity from the players summon tracker. Will also remove its cost.
	 * @param entity
	 */
	public void removeEntity(LivingEntity entity)
	{
		for(int i = 0; i < summons.size(); i++)
		{
			if(summons.get(i).getUniqueId().equals(entity.getUniqueId()))
			{
				summons.remove(i);
				pointCosts.remove(i);
				return;
			}
		}
	}
	
	

}
