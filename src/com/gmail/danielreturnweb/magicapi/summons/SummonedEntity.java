package com.gmail.danielreturnweb.magicapi.summons;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;

/**
 * Represents the basic data of a summoned entity
 * @author daniel
 *
 */
public interface SummonedEntity
{
	/** The point cost of this summon */
	public double summonPointCost();
	
	/** The vanilla type of this entity */
	public EntityType summonType();
	
	/** Duration in seconds */
	public int summonedDuration();
	
	/** 
	 * The summon duration is increased by this value per summon level above level 1.
	 * Set to 0 to disable this functionality
	 * @return
	 */
	public int summonLevelBonusDuration();
	
	
	/** The maximum level this summon can be summoned at */
	public int summonMaxLevel();
	
	
	/**
	 * Called when the entity is summoned
	 * @param entity
	 */
	public void onEntitySummon(LivingEntity entity, int level);
}
