package com.gmail.danielreturnweb.magicapi.summons;

import org.bukkit.Bukkit;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.gmail.danielreturnweb.magicapi.MagicAPI;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffect;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffectBundle;
import com.gmail.danielreturnweb.magicapi.effects.types.BuffType;
import com.gmail.danielreturnweb.magicapi.effects.types.EffectRemovalType;
import com.gmail.danielreturnweb.magicapi.effects.types.SelfStackType;

/**
 * <b> Effect ID: <b> <i> MagicAPI_Summoned </i>
 * <p>
 * This effect is placed upon a summoned entity marking it as a summon.
 * This will despawn the entity when the effect ends. The effect duration
 * is set to the entities summon duration
 * 
 * @see #EFFECT_ID
 * @author daniel
 *
 */
public class SummonedEntityEffect implements CustomEffect
{
	/** The ID of this effect */
	public static final String EFFECT_ID = "MagicAPI_Summoned";
	

	public SummonedEntityEffect()
	{}

	@Override
	public String getEffectID()
	{
		return EFFECT_ID;
	}

	@Override
	public boolean isDisplayed()
	{
		return false;
	}

	@Override
	public int getEffectDuration()
	{
		return 20*60;
	}

	@Override
	public boolean persistsAfterDeath()
	{
		return false;
	}

	@Override
	public BuffType getBuffType()
	{
		return BuffType.Positive;
	}

	@Override
	public int getPurgeResistence()
	{
		return 0;
	}

	@Override
	public SelfStackType getSelfStackType()
	{
		return SelfStackType.REPLACES;
	}

	@Override
	public void onInflict(LivingEntity effectHolder, LivingEntity inflicter, CustomEffectBundle bundle)
	{
		//Cannot be used on a player
		if(effectHolder instanceof Player)
		{
			bundle.setEnded(true);
			bundle.setRemainingDuration(0);
		}
	}

	@Override
	public void onRemoval(LivingEntity effectHolder, CustomEffectBundle bundle, EffectRemovalType type)
	{
		//Upon end, remove the entity unless its a player
		if(!(effectHolder instanceof Player))
		{
			if(bundle.getDataUUID("Summoner") != null)
			{
				Player player = Bukkit.getPlayer(bundle.getDataUUID("Summoner"));
				MagicAPI.getInstance().getSummonAPI().removeEntityFromTracker(player, effectHolder);
			}
			
			effectHolder.remove();
			
			//TODO send summoner a msg when it desummons
			//will have 2 messages, summoned died, and summon desummoned
		}
	}

	@Override
	public void onJoin(Player effectHolder, CustomEffectBundle bundle, PlayerJoinEvent event)
	{
		//If this effect is on a player, remove it as it should not exist on a player
		bundle.setEnded(true);
		bundle.setRemainingDuration(0);
	}

	@Override
	public void onQuit(Player effectHolder, CustomEffectBundle bundle, PlayerQuitEvent event)
	{
		//If this effect is on a player, remove it as it should not exist on a player
		bundle.setEnded(true);
		bundle.setRemainingDuration(0);
	}

	@Override
	public String[] getOpusEntry()
	{
		// TODO Auto-generated method stub
		return null;
	}

}
