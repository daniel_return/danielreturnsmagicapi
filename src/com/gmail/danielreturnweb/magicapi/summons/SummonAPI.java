package com.gmail.danielreturnweb.magicapi.summons;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang.NullArgumentException;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import com.gmail.danielreturnweb.magicapi.MagicAPI;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffectBundle;

/**
 * <i>
 * This API allows for a simple entity summoning system. This has methods to summon a creature on the under control
 * of the chosen player. <p>
 * This will track how many summoned entities the player has as well as place the necessary effects and data upon the summoned
 * creature. <p>
 * 
 * This system does not allow for spawning of summons under non player entities. <p>
 * </i>
 * The Summon System works using a point based system. Each Summon Costs a certain number of points.
 * When a player reaches the maximum points in use they cannot summon any more creatures. This allows
 * for weaker summons to cost less points and stronger summons to cost more.
 * @author daniel
 *
 */
public class SummonAPI implements Listener
{
	//Config Fields
	private double maxSummonPoints = 2.0;
	
	
	Map<UUID,SummonTracker> trackers;
	
	public SummonAPI(FileConfiguration config)
	{
		trackers = new HashMap<UUID,SummonTracker>();
		
		maxSummonPoints = config.getInt("SummonAPI.baseMaxSummonPoints");
		if(maxSummonPoints <=0) maxSummonPoints = 0;
	}
	
	/**
	 * Gets the tracker associated with the player
	 * @param player
	 * @return
	 */
	protected SummonTracker getTracker(Player player)
	{
		if(trackers.containsKey(player.getUniqueId()))
		{
			return trackers.get(player.getUniqueId());
		}
		SummonTracker tr = new SummonTracker();
		trackers.put(player.getUniqueId(), tr);
		return tr;
	}
	
	
	/**
	 * If a player is at the maximum number of active summons, this will return false
	 * @param player
	 * @return
	 */
	public boolean atMaxSummons(Player player)
	{
		if(this.getTracker(player).getPointUsage() <= this.maxSummonPoints) return false;
		return true;
	}
	
	/**
	 * Sees if the player can summon a monster with the associated point cost.
	 * @param player the player doing the summoning
	 * @param cost The point cost of the summon
	 * @return
	 */
	public boolean canSummon(Player player, double cost)
	{
		if(this.getTracker(player).getPointUsage() + cost <= this.maxSummonPoints) return true;
		return false;
	}
	
	
	/**
	 * Removes the entity from the players tracker
	 * @param player
	 * @param entity
	 */
	public void removeEntityFromTracker(Player player, LivingEntity entity)
	{
		SummonTracker tracker = this.getTracker(player);
		tracker.removeEntity(entity);
	}
	
	
	
	/**
	 * Summons the chosen entity for the player. This <b> DOES NOT do canSummon() checks </b> and will
	 * summon the entity even if the player is at the max summon point usage. Use canSummon() first to
	 * check to see if the player can summon a monster of the chosen cost.
	 * <p>
	 * @see #canSummon
	 * @param player
	 * @param data
	 */
	public void summonEntity(Player player, SummonedEntity data, int level)
	{
		int lev = level;
		
		//Do field validity checks
		if(player == null) throw new NullArgumentException("SummonAPI.summonEntity(...) Player cannot be null");
		if(data == null) throw new NullArgumentException("SummonAPI.summonEntity(...) data cannot be null");
		if(lev <= 0) lev = 1; 
		if(lev > data.summonMaxLevel()) lev = data.summonMaxLevel();
		
		//Spawn Entity around player
		Entity entity = player.getWorld().spawnEntity(player.getLocation().add(1,1,0), data.summonType());
		//TODO make the location around the player be random location within 1 to 3 blocks
		
		if(!(entity instanceof LivingEntity))
		{
			entity.remove();
			throw new IllegalArgumentException("SummonAPI.summonEntity(...) SummonedEntity data, data summonType() must be a LivingEntity");
		}
		
		//Get entity and pass it into data.onEntitySummon()
		data.onEntitySummon((LivingEntity)entity, lev);
		
		//Get Summon Effect bundle.
		CustomEffectBundle bundle = MagicAPI.getInstance().getCustomEffectAPI().createCustomEffectBundle(SummonedEntityEffect.EFFECT_ID);
		
		//Add summoner and set duration
		int duration = (data.summonedDuration() + data.summonLevelBonusDuration()*(level-1));
		duration = duration*20;
		bundle.setRemainingDuration(duration);
		
		bundle.setDataUUID("Summoner", player.getUniqueId());
		bundle.setLevel(lev);
		
		
		//Place summoned Effect upon the entity
		MagicAPI.getInstance().getCustomEffectAPI().placeEffect((LivingEntity)entity, SummonedEntityEffect.EFFECT_ID,bundle);
		
		//TODO Add entity point cost to the player tracker
		this.getTracker(player).addEntity((LivingEntity)entity, data.summonPointCost());
	}
	
	
	/**
	 * Summons the chosen entity for the player. Unlike summonEntity(...) this will check to see if the player
	 * has enough free Summon Points to summon this entity.
	 * <p>
	 * @see #summonEntity
	 * @see #canSummon
	 * 
	 * @param player
	 * @param data
	 * @param level
	 * @return True if entity was summoned, False if it was not
	 */
	public boolean summonSummon(Player player, SummonedEntity data, int level)
	{
		if(atMaxSummons(player))
		{
			return false;
		}
		else
		{
			this.summonEntity(player, data, level);
			return true;
			
		}
	}
	
	
	/**
	 * Summons a summoned entity that is not bound to any player
	 * @param data
	 * @param level
	 * @param location
	 */
	public void summonUnboundEntity(SummonedEntity data, int level, Location location)
	{
		//Spawn Entity around player
		Entity entity = location.getWorld().spawnEntity(location, data.summonType());
		
		int lev = level;
		if(lev <= 0) lev = 1; 
		if(lev > data.summonMaxLevel()) lev = data.summonMaxLevel();
		
		//Get entity and pass it into data.onEntitySummon()
		data.onEntitySummon((LivingEntity)entity, lev);
		
		//Get Summon Effect bundle.
		CustomEffectBundle bundle = MagicAPI.getInstance().getCustomEffectAPI().createCustomEffectBundle(SummonedEntityEffect.EFFECT_ID);
		
		//Add summoner and set duration
		int duration = (data.summonedDuration() + data.summonLevelBonusDuration()*(level-1));
		duration = duration*20;
		bundle.setRemainingDuration(duration);
		bundle.setDataUUID("Summoner", null);
		bundle.setLevel(lev);
				
		//Place summoned Effect upon the entity
		MagicAPI.getInstance().getCustomEffectAPI().placeEffect((LivingEntity)entity, SummonedEntityEffect.EFFECT_ID,bundle);

	}
	
//	//TODO implement this method and make it public
//	/**
//	 * This method would convert an already existing LivingEntity to a Summoned Monster <p>
//	 * <b> NOT YET IMPLEMENTED </b>
//	 * @param player
//	 * @param data
//	 * @param level
//	 */
//	private void convertEntity(Player player, SummonedEntity data, int level)
//	{
//		
//	}
//	
	

}
