package com.gmail.danielreturnweb.magicapi;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

/**
 * A class that manages the hit chance of a player or entity along with managing evasion. <p>
 * Upon a vanilla attack (EntityDamageByEntityEvent) the attacking entity will have a chance
 * to miss its attack. This chance is calculated from the attacking entities HitChance and the 
 * targets evasion. <p>
 * 
 * All entities start with a default of 100% hit chance and 5% evasion. Can be changed in the config
 * 
 * <p>
 * <b> Evasion Calculation </b>
 * Total Evasion Chance = Evasion + (1-[Attacker Hit Chance]) <br>
 * Minimum Total Evasion Chance = 0% <br>
 * Maximum Total Evasion Chance = 100% <p>
 * 
 * <p>
 * If the attacking entity is a player they will receive a message when they miss a attack. <br>
 * If the evading entity is a player they will receive a message when they evade the attack
 * Displaying a missed chat message if attacker is entity
 * @author daniel
 *
 */
public class HitEvasionManager implements Listener
{
	Map<UUID,Map<String,Double>> hitChance;
	Map<UUID,Map<String,Double>> evasion;
	
	FileConfiguration config;
	
	double baseHitChance = 1;
	double baseEvasion = 0.05;
	
	boolean luckEnabeld = false;
	double luckEvasion = 0;
	double luckHitChance = 0;
	
	boolean unluckEnabeld = false;
	double unluckEvasion = 0;
	double unluckHitChance = 0;
	
	String missMessage = "";
	String evadeMessage = "";
	
	//TODO split the base/default hit and evasion chances in config into monster and player groups.
	//This would allow the API user more freedom in designing how monster and player hit chances
	//and evasion compare to each other. 
	
	public HitEvasionManager(FileConfiguration config)
	{
		hitChance = new HashMap<UUID,Map<String,Double>>();
		evasion = new HashMap<UUID,Map<String,Double>>();
		this.config = config;
		
		baseHitChance = this.config.getDouble("AttackMissing.defaults.hitChance");
		baseEvasion = this.config.getDouble("AttackMissing.defaults.evadeChance");
		
		luckEnabeld = this.config.getBoolean("AttackMissing.other.luck.enabeld");
		luckEvasion = this.config.getDouble("AttackMissing.other.luck.evasion");
		luckHitChance = this.config.getDouble("AttackMissing.other.luck.hitChance");
		
		unluckEnabeld = this.config.getBoolean("AttackMissing.other.unluck.enabeld");
		unluckEvasion = this.config.getDouble("AttackMissing.other.unluck.evasion");
		unluckHitChance = this.config.getDouble("AttackMissing.other.unluck.hitChance");
		
		missMessage = this.config.getString("AttackMissing.messages.miss");
		evadeMessage = this.config.getString("AttackMissing.messages.evade");
		missMessage = ChatColor.translateAlternateColorCodes('&', missMessage);
		evadeMessage= ChatColor.translateAlternateColorCodes('&', evadeMessage);
		
		
		//TODO remove this test code
		
//		baseEvasion = 0.5;
		
		//END of test code
		
	}
	
	
	//***************************
	//Evasion
	//***************************
	
	/**
	 * Adds a Hit Chance modifier to the selected entity. This value can be positive
	 * or negative. 
	 * @param entity
	 * @param label
	 * @param chance A + or - number
	 */
	public void addHitChance(UUID entity, String label, double chance)
	{
		if(hitChance.containsKey(entity))
		{
			hitChance.get(entity).put(label, chance);
			return;
		}
		Map<String,Double> map = new HashMap<String,Double>();
		map.put(label, chance);
		hitChance.put(entity, map);
	}
	
	
	public void removeHitChance(UUID entity, String label)
	{
		
		if(hitChance.containsKey(entity))
		{
			if(hitChance.get(entity).containsKey(label)) hitChance.get(entity).remove(label);
		}
	}
	
	public double getHitChance(UUID entity)
	{
		double val = this.baseHitChance;
		if(hitChance.containsKey(entity))
		{			
			for(Entry<String,Double> v: hitChance.get(entity).entrySet())
			{
				val += v.getValue();
			}
		}
		return val;
	}
	
	public Map<String,Double> getAllHitChances(UUID entity)
	{
		if(hitChance.containsKey(entity))
		{
			Map<String, Double> chances = hitChance.get(entity);
			if(chances == null)
			{
				chances = new HashMap<String,Double>();
			}
			return chances;
		}
		return new HashMap<String,Double>();
	}
	

	//***************************
	//Evasion
	//***************************
	
	
	public void addEvasionChance(UUID entity, String label, double chance)
	{
		if(evasion.containsKey(entity))
		{
			evasion.get(entity).put(label, chance);
			return;
		}
		Map<String,Double> map = new HashMap<String,Double>();
		map.put(label, chance);
		evasion.put(entity, map);
	}
	
	
	public void removeEvasionChance(UUID entity, String label)
	{
		if(evasion.containsKey(entity))
		{
			if(evasion.get(entity).containsKey(label)) evasion.get(entity).remove(label);
		}
	}
	
	public double getEvasionChance(UUID entity)
	{
		double val = this.baseEvasion;
		if(evasion.containsKey(entity))
		{
			for(Entry<String,Double> v: evasion.get(entity).entrySet())
			{
				val += v.getValue();
			}
		}
		return val;
	}
	
	public Map<String,Double> getAllEvasionChances(UUID entity)
	{
		if(evasion.containsKey(entity))
		{
			Map<String, Double> chances = evasion.get(entity);
			if(chances == null)
			{
				chances = new HashMap<String,Double>();
			}
			return chances;
		}
		return new HashMap<String,Double>();
	}
	
	
	//***************************
	//Calculations
	//***************************
	
	/**
	 * Calculates a defenders evasion from the evasion they have and the attackers hit chance
	 * @param defender
	 * @param attacker
	 * @return The "true" evasion the defender has at this instance
	 */
	public double calculateEvasion(UUID defender, UUID attacker)
	{
		double maxEvasion = 1;
		double minEvasion = 0;
		
		double total = this.getEvasionChance(defender) + (1-this.getHitChance(attacker));
		
		if(total < minEvasion) total = 0;
		if(total > maxEvasion) total = 1;
		
		return total;
	}
	
	
	/**
	 * Performs the calculation to see if the attacking entity hits the defending entity. <p>
	 * Throws an IllegalArgumentException if useEvasion and useHitChance are both false.
	 * One must always be true
	 * 
	 * @param defender
	 * @param attacker
	 * @param useEvasion
	 * @param useHitChance
	 * @return True if attack hits, false if it does not hit
	 */
	public boolean doesAttackHit(UUID defender, UUID attacker, boolean useEvasion, boolean useHitChance)
	{
		if(useEvasion && useHitChance)
		{
			if(Math.random() <= this.calculateEvasion(defender, attacker)) return false;
		}
		else if (useEvasion)
		{
			if(Math.random() <= this.getEvasionChance(defender)) return false;
		}
		else if(useHitChance)
		{
			if(Math.random() <= this.getHitChance(attacker)) return false;
		}
		else if(!useEvasion && !useHitChance)
		{
			throw new IllegalArgumentException("HitEvasionManager.doesAttackHit(...) useEvasion and useHitChance are both false. One must always be true! \n");
		}
		return true;
	}
	
	private void sendMessage(LivingEntity entity, String message)
	{
		if(entity instanceof Player)
		{
			entity.sendMessage(ChatColor.AQUA + "[Evasion] " + ChatColor.RESET + message);
		}
	}

	//***************************
	//Listeners
	//***************************
	
	//To do evasion/hit chance
	@EventHandler
	public void onEntityDamage(EntityDamageByEntityEvent event)
	{
//		System.out.println("Evasion entity damage event");
		if(!(event.getEntity() instanceof LivingEntity)) return;
		if(!(event.getDamager() instanceof LivingEntity)) return;
				
		LivingEntity attacker = (LivingEntity) event.getDamager();
		LivingEntity defender = (LivingEntity) event.getEntity();
		
//		System.out.println("Evasion living entities");
		if(event.getCause() == DamageCause.ENTITY_ATTACK)
		{
//			System.out.println("Evasion entity attack");
			if(!this.doesAttackHit(defender.getUniqueId(), attacker.getUniqueId(), true, true))
			{
				//Evade
				event.setCancelled(true);
				this.sendMessage(attacker, this.missMessage);
				this.sendMessage(defender, this.evadeMessage);
			}
		}
		else if(event.getCause() == DamageCause.ENTITY_SWEEP_ATTACK)
		{
//			System.out.println("Evasion Sweeping");
			//Can evade but its a lesser value
			double evasion = this.calculateEvasion(defender.getUniqueId(), attacker.getUniqueId());
			evasion /=2; //TODO entity's hit by sweeping edge should have a higher evasion chance then a directly hit entity.
			if(Math.random() <= evasion)
			{
				//Evade
				event.setCancelled(true);
//				this.sendMessage(attacker, this.missMessage); Do not have Sweeping attack damage do messages as it can be very spammy
//				this.sendMessage(defender, this.evadeMessage);
			}
		}
	}
	
	
	
	
	//DO projectile hit event (can this be canceld?)
	//If not just use damage event and see if entity is projectile
	//If it is then do hit chance
	
}
