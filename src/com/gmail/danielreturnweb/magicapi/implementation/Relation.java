package com.gmail.danielreturnweb.magicapi.implementation;

import java.util.UUID;

import com.gmail.danielreturnweb.magicapi.EntityStanceView.EntityStance;

/**
 * The class/object that EntityStanceViewDefault utilized for the default implementation of the EntityStanceView provided by this Library
 * @author daniel
 *
 */
public class Relation
{
	public UUID player1;
	public UUID player2;
	public EntityStance stance;
	
	/**
	 * Creates a Relation object with the EntityStance between the two players set to UNKNOWN
	 * @param player1
	 * @param player2
	 */
	public Relation(UUID player1, UUID player2)
	{
		this.player1 = player1;
		this.player2 = player2;
		this.stance = EntityStance.UNKNOWN;
	}
	
	public Relation(UUID player1, UUID player2, EntityStance stance)
	{
		this.player1 = player1;
		this.player2 = player2;
		this.stance = stance;
	}
	
	/**
	 * Sees if the associated players in this Relation matches the associated players in the provided Relation object.
	 * Does not check the stance.
	 * @param re
	 * @return
	 */
	public boolean matches(Relation re)
	{
		if(player1.equals(re.player1) && player2.equals(re.player2)) return true;
		if(player1.equals(re.player2) && player2.equals(re.player1)) return true;
		return false;
	}
}
