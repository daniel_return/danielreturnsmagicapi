package com.gmail.danielreturnweb.magicapi.implementation;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Animals;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Golem;
import org.bukkit.entity.IronGolem;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.NPC;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Tameable;
import org.bukkit.entity.WaterMob;
import org.bukkit.entity.Wolf;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.projectiles.ProjectileSource;

import com.gmail.danielreturnweb.magicapi.EntityStanceView;
import com.gmail.danielreturnweb.magicapi.MagicAPI;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffectBundle;
import com.gmail.danielreturnweb.magicapi.summons.SummonedEntityEffect;
import com.gmail.danielreturnweb.magicapi.utils.CustomizedConfiguration;

/**
 * The default EntityStanceView that DanielReturnsMagicAPI utilizes. <p>
 * This is a basic EntityStanceView implementation. <p>
 * 
 * <b>Defaults:</b> <br>
 * <i> Player relation to </i> <br>
 * Monsters: Hostile <br>
 * Animals: Friendly <br>
 * Players: Neutral <br>
 * Wolf: Friendly (if player is owner), Hostile (if wild/not tamed), Else stance of the wolf owner to the player. <br>
 * Golem: neutral <br>
 * IronGolem: Hostile if NOT player built, neutral if player built. <br>
 * Water Mobs: Hostile, excluding dolphin's which are neutral <b>
 * Projectile: Based upon the shooters state with the hit entity or UNKNOWN
 * 
 * <p>
 * <i> Monster relation to </i> <br>
 * Monsters: Friendly <br>
 * Animals: Friendly <br>
 * Wolf: Hostile
 * Projectile: State is based upon shooters state to the monster, or UNKNOWN.
 * 
 * @author daniel
 *
 */
public class EntityStanceViewDefault implements EntityStanceView, Listener
{
	/**
	 * The list of player relations
	 */
	List<Relation> playerRelations;
	
	CustomizedConfiguration cConfig;
	FileConfiguration config;
	

	public EntityStanceViewDefault()
	{
		playerRelations = new ArrayList<Relation>();
		cConfig = new CustomizedConfiguration(MagicAPI.getInstance(),"Stances.yml");
		cConfig.saveDefaultConfig();
		
		config = cConfig.getCustomConfig();
	}

	@Override
	public EntityStance getStance(Entity entity1, Entity entity2)
	{
		if(entity1.getType() == EntityType.PLAYER && entity2.getType() != EntityType.PLAYER)
		{
			return this.getStance((OfflinePlayer)entity1,entity2);
		}
		if(entity1.getType() != EntityType.PLAYER && entity2.getType() == EntityType.PLAYER)
		{
			return this.getStance((OfflinePlayer)entity2,entity1);
		}
		if(entity1.getType() == EntityType.PLAYER && entity2.getType() == EntityType.PLAYER)
		{
			return this.getStance((OfflinePlayer)entity1,(OfflinePlayer)entity2);
		}
		
		
		if(entity1 instanceof Monster && entity2 instanceof Monster) return EntityStance.FRIENDLY;
		if((entity1 instanceof Monster && entity2 instanceof Animals) ||
				(entity2 instanceof Monster && entity1 instanceof Animals)) return EntityStance.FRIENDLY;
		
		if((entity1 instanceof Monster && entity2 instanceof Wolf) || (entity2 instanceof Monster && entity1 instanceof Wolf))
		{
			return EntityStance.HOSTILE;
		}
		
		//Projectiles
		if(entity1 instanceof Projectile || entity2 instanceof Projectile) return this.projectileOwnership(entity1, entity2);
		
		
		return EntityStance.UNKNOWN;
	}
	
	private EntityStance projectileOwnership(Entity entity1, Entity entity2)
	{
		Entity t1 = entity1;
		Entity t2 = entity2;
		
		if(entity1 instanceof Projectile)
		{
			ProjectileSource ps = ((Projectile)entity1).getShooter();
			if(ps != null)
			{
				if(ps instanceof LivingEntity)
				{
					t1 = (LivingEntity)ps;
				}
			}
		}
		if(entity2 instanceof Projectile)
		{
			ProjectileSource ps = ((Projectile)entity2).getShooter();
			if(ps != null)
			{
				if(ps instanceof LivingEntity)
				{
					t2 = (LivingEntity)ps;
				}
			}
		}
		
		
		if(t1 instanceof Player && t2 instanceof Player)
		{
			return this.getStance((OfflinePlayer)t1, (OfflinePlayer)t2);
		}
		else if(t1 instanceof Player)
		{
			return this.getStance((OfflinePlayer)t1,t2);
		}
		else if(t2 instanceof Player)
		{
			return this.getStance((OfflinePlayer)t2, t1);
		}
		else
		{
			return this.getStance(t1, t2);
		}
	}

	@Override
	public EntityStance getStance(OfflinePlayer player1, OfflinePlayer player2)
	{
		return this.getPlayerStance(player1, player2);
	}

	@Override
	public EntityStance getStance(OfflinePlayer player, Entity entity)
	{
		//TODO see if entity has SummonedEntityEffect if so get associated data and see if the OfflinePlayer is the summoner of this entity.
		//TODO then return stance as friendly.
		//Will need to implement a method on the onTarget event to prevent summons from attacking there summoner is this due to stance
		//or due to it being a summoned (and therefore implemented in the SummonAPI)
		//If not the summoner, then get the stance between the summoner and the target player
		if(entity instanceof LivingEntity)
		{
			Map<String, CustomEffectBundle> effs = MagicAPI.getInstance().getCustomEffectAPI().getAllEffectData((LivingEntity) entity);
			for(Entry<String,CustomEffectBundle> entry: effs.entrySet())
			{
				if(entry.getKey().equals(SummonedEntityEffect.EFFECT_ID))
				{
					//Get the summoner
						//If player is summoner return friendly
					//If player is not summoner
						//get player and summoners stance and return that
					UUID id = entry.getValue().getDataUUID("Summoner");
					if(id != null)
					{
						OfflinePlayer summoner = Bukkit.getOfflinePlayer(id);
						if(summoner != null)
						{
							if(summoner.getUniqueId().equals(player.getUniqueId()))
							{
								return EntityStance.FRIENDLY;
							}
							else
							{
								EntityStance stance = this.getStance(summoner, player);
								return stance;
							}
						}
					}
				}
			}
		}
		
		
		if(entity instanceof Animals) return EntityStance.FRIENDLY;
		if(entity instanceof Player) return getPlayerStance(player, (Player)entity);
		if(entity instanceof Monster) return EntityStance.HOSTILE;
		
		if(entity.getType() == EntityType.WOLF)
		{
			Wolf wolf = (Wolf) entity;
			
			//Wild Wolf
			if(!wolf.isTamed())
			{
				return EntityStance.HOSTILE;
			}
			else //Tame Wolf
			{
				//Player is wolf owner
				if(wolf.getOwner().getUniqueId().equals(player.getUniqueId()))
				{
					return EntityStance.FRIENDLY;
				}
				else //If player is not owner, return the player' and wolf owners stance
				{
					OfflinePlayer targ = Bukkit.getOfflinePlayer(wolf.getOwner().getUniqueId());
					if(targ == null) return EntityStance.UNKNOWN;
					return this.getPlayerStance(player, targ);
				}
			}
		}
		//Note, this Tameable does not include wolves since it comes after there check
		if(entity instanceof Tameable) return EntityStance.NEUTRAL;
		if(entity instanceof Golem)
		{
			if(entity instanceof IronGolem)
			{
				IronGolem g = (IronGolem) entity;
				if(!g.isPlayerCreated()) return EntityStance.HOSTILE;
			}
			return EntityStance.NEUTRAL;
		}
		if(entity instanceof WaterMob)
		{
			if(entity.getType() == EntityType.DOLPHIN) return EntityStance.NEUTRAL;
			return EntityStance.HOSTILE;
		}
		if(entity instanceof NPC) return EntityStance.FRIENDLY;
		return EntityStance.UNKNOWN;
		
		//TODO projectile check?
	}
	
	
	public void setStance (OfflinePlayer player1, OfflinePlayer player2, EntityStance stance)
	{
		if(player1 == null || player2 == null || stance == null) return;
		Relation re = new Relation(player1.getUniqueId(),player2.getUniqueId(),stance);
		this.setRelation(re);
	}
	
	
	
	
	
	protected void setRelation(Relation re)
	{
		for(int i = 0; i < this.playerRelations.size(); i++)
		{
			if(playerRelations.get(i).matches(re))
			{
				playerRelations.set(i, re);
				return;
			}
		}
		playerRelations.add(re);
	}
	
	
	protected boolean hasRelation(UUID p1, UUID p2)
	{
		Relation re = new Relation(p1,p2,EntityStance.UNKNOWN);
		
		for(int i = 0; i < this.playerRelations.size(); i++)
		{
			if(playerRelations.get(i).matches(re))
			{
				return true;
			}
		}
		return false;
	}
	
	protected Relation getRelation(UUID p1, UUID p2)
	{
		Relation re = new Relation(p1,p2,EntityStance.UNKNOWN);
		
		for(int i = 0; i < this.playerRelations.size(); i++)
		{
			if(playerRelations.get(i).matches(re))
			{
				return playerRelations.get(i);
			}
		}
		return null;
	}
		
	
	protected EntityStance getPlayerStance(OfflinePlayer p1, OfflinePlayer p2)
	{		
		if(!this.hasRelation(p1.getUniqueId(), p2.getUniqueId()))
		{
			return EntityStance.NEUTRAL;
		}
		return this.getRelation(p1.getUniqueId(), p2.getUniqueId()).stance;
	}	
	
	
	
	
	public void savePlayerRelations()
	{
		System.out.println("[MagicAPI] Stances | Saving Player Stances");
		List<String> relations = new ArrayList<String>();
		
		for(Relation relation : playerRelations)
		{
			String relat = relation.player1.toString() + "," + relation.player2.toString() + "," + relation.stance.toString();
			relations.add(relat);
		}
		config.set("Relations", relations);
		cConfig.saveCustomConfig();
		System.out.println("[MagicAPI] Stances | Save compleat");
	}
	
	public void loadPlayerRelations()
	{
		System.out.println("[MagicAPI] Stances | Loading Player Stances");
		
		if(!config.contains("Relations")) return;
		
		@SuppressWarnings("unchecked")
		List<String> list = (List<String>) config.getList("Relations");
		if(list == null || list.size() < 1) return;
		
		for(String val : list)
		{
			String[] split = val.split(","); 
//			for(String s: split) //DEBUG
//			{
//				System.out.println("Split =" + s + "=");
//			}
			
			UUID uuid1 = UUID.fromString(split[0]);
			UUID uuid2 = UUID.fromString(split[1]);
			EntityStance stance = EntityStance.valueOf(split[2]);
			
			Relation relat = new Relation(uuid1,uuid2,stance);
//			playerRelations.add(relat);
			this.setRelation(relat);
		}
	}
	
	@EventHandler
	public void onTarget(EntityTargetEvent event)
	{
		Entity entity = event.getEntity();
		Entity target = event.getTarget();
		
		if(!(entity instanceof LivingEntity)) return;
		
//		List<CustomEffectBundle> k = MagicAPI.getInstance().getCustomEffectAPI().getEffectData((LivingEntity)entity, SummonedEntityEffect.EFFECT_ID);
		
		if(target instanceof Player)
		{
			OfflinePlayer player = (OfflinePlayer)target;
			EntityStance stance = MagicAPI.getInstance().getEntityStanceView().getStance(player, entity);
			if(stance == EntityStance.FRIENDLY)
			{
				event.setCancelled(true);
			}
		}
	}
}
