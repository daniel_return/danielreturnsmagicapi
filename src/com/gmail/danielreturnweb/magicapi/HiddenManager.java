package com.gmail.danielreturnweb.magicapi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.gmail.danielreturnweb.magicapi.utils.Distance;

/**
 * <b> THIS MANAGER IS UNTESTED </b>
 * <p>
 * A hidden player cannot be seen by players unless they have a admin permission (WIP) or have true sight.
 * <p> True sight is based upon distance from the user of the true sight. I.e. if a player has a 10 block true sight
 * they can see all hidden players within 10 blocks of them. Admin's have global true sight w/ the permission.
 * <p> True sight does not stack additively. The highest true sight value is used. 
 * <br> though this can be made changeable in the config and have a method that allows API users to get the stack type of true sight distance.
 * @author daniel
 *
 */
public class HiddenManager implements Runnable
{
	Map<UUID,List<String>> hiddenPlayers; //UUID, [List of hidden sources]
	Map<UUID,Map<String,Double>> trueSightPlayers; //UUID, [sight distance]
	
	public HiddenManager(MagicAPI api)
	{
		this.hiddenPlayers = new HashMap<UUID,List<String>>();
		this.trueSightPlayers = new HashMap<UUID,Map<String,Double>>();
		
		Bukkit.getScheduler().scheduleSyncRepeatingTask(MagicAPI.getInstance(), this,100, 40); //Runs every 2 seconds (for testing, may reduce it later)
		
		//TODO make config have fields for this. (see HiddenManager java doc at top of page)
	}

	
	//*************************************
	//			HIDDEN
	//*************************************
	
	/**
	 * Hides the player.
	 * @param player the target player
	 * @param tag the tag/id of your specific Hide.
	 */
	public void hidePlayer(UUID player, String tag)
	{		
		if(!this.hiddenPlayers.containsKey(player))
		{
			List<String> ar = new ArrayList<String>();
			ar.add(tag);
			this.hiddenPlayers.put(player, ar);
			return;
		}
		
		List<String> list = this.hiddenPlayers.get(player);
		list.add(tag);
		this.hiddenPlayers.put(player, list);
	
		Player p = Bukkit.getPlayer(player);
		if(p != null) this.hidePlayer(p);
	}
	
	/**
	 * Removes the hidden status from the player by your specific tag.
	 * @param player The player to target
	 * @param tag the tag/id of your specific Hide
	 */
	public void unhidePlayer(UUID player, String tag)
	{		
		if(!this.hiddenPlayers.containsKey(player)) return; //If player has no entry, they are not hidden so cannot be unhidden
		
		//Find the tag and remove it
		List<String> list = this.hiddenPlayers.get(player);
		for(int i = 0; i < list.size(); i++)
		{
			if(list.get(i).equals(tag))
			{
				list.remove(i);
				break;
			}
		}
		this.hiddenPlayers.put(player, list);
		
		//See if the player is hidden, if not unhide them
		if(!this.isHidden(player))
		{
			Player p = Bukkit.getPlayer(player);
			if(p != null) this.hidePlayer(p);
		}
	}
	
	/**
	 * Removes the hidden status from the player by removing all tags.
	 * @param player
	 */
	public void unhidePlayer(UUID player)
	{
		if(!this.hiddenPlayers.containsKey(player)) return; //If player has no entry, they are not hidden so cannot be unhidden
		
		this.hiddenPlayers.remove(player);
		
		Player p = Bukkit.getPlayer(player);
		if(p != null) this.hidePlayer(p);
	}
	
	/**
	 * See's if the player is in a hidden state
	 * @param player
	 * @return
	 */
	public boolean isHidden(UUID player)
	{
		return hiddenPlayers.containsKey(player);
	}
	
	
	//*************************************
	//			TRUE SIGHT
	//*************************************
	
	/**
	 * Grants a player true sight within distance blocks from them.
	 * @param player the player who is given true sight
	 * @param tag the tag of this true sight
	 * @param distance the distance the player is able to see hidden targets within
	 */
	public void giveTrueSight(UUID player, String tag, double distance)
	{
		if(this.trueSightPlayers.containsKey(player))
		{
			Map<String,Double> map = this.trueSightPlayers.get(player);
			map.put(tag, distance);
			this.trueSightPlayers.put(player, map);
			return;
		}
		Map<String,Double> map = new HashMap<String,Double>();
		map.put(tag, distance);
		this.trueSightPlayers.put(player, map);
	}
	
	/**
	 * Removes true sight of a specific tag from the player
	 * @param player
	 * @param tag
	 */
	public void removeTrueSight(UUID player, String tag)
	{
		if(this.trueSightPlayers.containsKey(player))
		{
			Map<String,Double> map = this.trueSightPlayers.get(player);
			map.remove(tag);
			this.trueSightPlayers.put(player, map);
			return;
		}
	}
	
	/**
	 * See's if the provided player has true sight
	 * @param player
	 * @return
	 */
	public boolean hasTrueSight(UUID player)
	{
		return trueSightPlayers.containsKey(player);
	}
	
	/**
	 * Gets the distance a player with true sight can see hidden players
	 * @param player
	 * @return
	 */
	public double getTrueSightDistance(UUID player)
	{
		if(this.trueSightPlayers.containsKey(player))
		{
			Map<String,Double> map = this.trueSightPlayers.get(player);
			double sight = 0;
			for(Entry<String,Double> entry : map.entrySet())
			{
				if(entry.getValue() > sight) sight = entry.getValue();
			}
		}
		return 0;
	}
	
	
	
	//*************************************
	//		   FUNCTIONALITY
	//*************************************
	
	
	/**
	 * The method that does the player.hidePlayer() for all applicable players on the server
	 * @param player
	 */
	private void hidePlayer(Player player)
	{
		//TODO loop though all online players hiding this player from them unless
		//They have the admin permission OR the hidden player is within range of
		//a true sight player.
		
		for(Player t : Bukkit.getOnlinePlayers())
		{
			if(!t.hasPermission("WIP"))
			{
				if(this.hasTrueSight(t.getUniqueId()))
				{
					if(Distance.getDistence3D(player.getLocation(), t.getLocation()) > this.getTrueSightDistance(player.getUniqueId()))
					{
						t.hidePlayer(MagicAPI.getInstance(), player);
					}
				}
				else
				{
					t.hidePlayer(MagicAPI.getInstance(), player);
				}
			}
		}
	}
	
	
	/**
	 * 
	 * @param player
	 */
	private void unhidePlayer(Player player)
	{
		for(Player t : Bukkit.getOnlinePlayers())
		{
			t.showPlayer(MagicAPI.getInstance(), player);
		}
	}
	
	
	
	/*
		TODO Runnable that updates the hiddenness of players. 
		This takes into any players that have true sight and are 
	 	within distance of the hidden player. (along with players with admin permission)
	 */
	@Override
	public void run() //TODO should this run every 10 ticks? or every 20?
	{		
		for(Player player : Bukkit.getOnlinePlayers())
		{
			if(this.isHidden(player.getUniqueId()))
			{
				this.hidePlayer(player); //TODO This primarily needs to update for true sight as a hidden player comes within range. 
				//A method can be made specifically for to do this instead of doing all other hiding saving CPU usage.
			}
		}
	}
}
