package com.gmail.danielreturnweb.magicapi;

import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;

import com.gmail.danielreturnweb.magicapi.effect.CustomEffectManager;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffect;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffectAPI;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffectBundle;
import com.gmail.danielreturnweb.magicapi.effectcomponents.Rooted;
import com.gmail.danielreturnweb.magicapi.effectcomponents.Stunned;
import com.gmail.danielreturnweb.magicapi.effectcomponents.WeakRoot;

/**
 * Manages the stun effects and rooted effects for all effects that implement
 * the appropriate interfaces.
 * @author daniel
 *
 */
public class StunManager implements Listener
{
	public StunManager() {}
	
	private CustomEffectAPI getCeapi()
	{
		return MagicAPI.getInstance().getCustomEffectAPI();
	}
	
	
	/*
	 * Blocked by Stun, Rooted and WeakRoot
	 */
	@EventHandler
	public void onMove(PlayerMoveEvent e)
	{
		Player player = e.getPlayer();
					
		//See if entity has this effect.
		Map<String,CustomEffectBundle> b = getCeapi().getAllEffectData(player);
		if(b == null ||b.size() <= 0) return;
		
		for(Entry<String,CustomEffectBundle> ent : b.entrySet())
		{
			CustomEffect effect = CustomEffectManager.getInstance().getRegisteredEffect(ent.getKey());
			if(effect instanceof Stunned || effect instanceof Rooted || effect instanceof WeakRoot)
			{
				if(ent.getValue().getRemainingDuration() > 0) e.setCancelled(true);
			}
		}
	}
	
	/*
	 * Blocked by Stun
	 */
	@EventHandler
	public void onAttack(EntityDamageByEntityEvent e)
	{
		if(!(e.getDamager() instanceof LivingEntity)) return;
		
		LivingEntity entity = (LivingEntity) e.getDamager();
					
		//See if entity has this effect.
		Map<String,CustomEffectBundle> b = getCeapi().getAllEffectData(entity);
		if(b == null ||b.size() <= 0) return;
		
		for(Entry<String,CustomEffectBundle> ent : b.entrySet())
		{
			CustomEffect effect = CustomEffectManager.getInstance().getRegisteredEffect(ent.getKey());
			if(effect instanceof Stunned)
			{
				if(ent.getValue().getRemainingDuration() > 0) e.setCancelled(true);
			}
		}
	}

	
	/*
	 * Blocked by Stun
	 */
	@EventHandler
	public void onConsume(PlayerItemConsumeEvent e)
	{
		Player player = e.getPlayer();
					
		//See if entity has this effect.
		Map<String,CustomEffectBundle> b = getCeapi().getAllEffectData(player);
		if(b == null ||b.size() <= 0) return;
		
		for(Entry<String,CustomEffectBundle> ent : b.entrySet())
		{
			CustomEffect effect = CustomEffectManager.getInstance().getRegisteredEffect(ent.getKey());
			if(effect instanceof Stunned)
			{
				e.setCancelled(true);
			}
		}
	}
	
	/*
	 * Blocked by Stun
	 */
	@EventHandler
	public void onInteraction(PlayerInteractEvent e)
	{
		Player player = e.getPlayer();
		
		//See if entity has this effect.
		Map<String,CustomEffectBundle> b = getCeapi().getAllEffectData(player);
		if(b == null ||b.size() <= 0) return;
		
		for(Entry<String,CustomEffectBundle> ent : b.entrySet())
		{
			CustomEffect effect = CustomEffectManager.getInstance().getRegisteredEffect(ent.getKey());
			if(effect instanceof Stunned)
			{
				e.setCancelled(true);
			}
		}
	}
	
	
	/*
	 * Blocks Stunned and Rooted.
	 * WeakRoot is broken and removed by teleport events.
	 */
	@EventHandler
	public void onTeleport(PlayerTeleportEvent event)
	{
		Player player = event.getPlayer();
		
		
		//The teleports that are blocked
		if(
				event.getCause() != TeleportCause.CHORUS_FRUIT ||
				event.getCause() != TeleportCause.ENDER_PEARL ||
				event.getCause() != TeleportCause.COMMAND ||
				event.getCause() != TeleportCause.PLUGIN ||
				event.getCause() != TeleportCause.UNKNOWN
		  ) return;
		
		
		//See if entity is Stunned, Rooted or WeakRooted
		Map<String,CustomEffectBundle> b = getCeapi().getAllEffectData(player);
		if(b == null ||b.size() <= 0) return;
		
		for(Entry<String,CustomEffectBundle> ent : b.entrySet())
		{
			CustomEffect effect = CustomEffectManager.getInstance().getRegisteredEffect(ent.getKey());
			if(effect instanceof Stunned || effect instanceof Rooted)
			{
				if(ent.getValue().getRemainingDuration() > 0) event.setCancelled(true);
			}
			else if(effect instanceof WeakRoot)
			{
				ent.getValue().setEnded(true);
				ent.getValue().setRemainingDuration(0);
			}
		}
	}
}
