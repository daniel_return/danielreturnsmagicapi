package com.gmail.danielreturnweb.magicapi.DamageSystem.DamageTypes;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.potion.PotionEffectType;

import com.gmail.danielreturnweb.magicapi.MagicAPI;
import com.gmail.danielreturnweb.magicapi.DamageSystem.CustomDamageType;
import com.gmail.danielreturnweb.magicapi.DamageSystem.DamageManager;

/**
 * This represents a custom Fire Damage Type. Damage reduction values that effect this type can be found in
 * {@code Damages.Fire.reduction}<p>
 * 
 * The values found in {@code Damages.Fire} only effect this Custom Fire Damage Type and not vanilla
 * fire damage unless {@code Damages.Fire.valuesOverridesVanillaBehavior} is set to true.
 * @author daniel
 *
 */
public class FireDamageType implements CustomDamageType, Listener
{
	FileConfiguration config;
	
	protected double damageBurning;
	protected double damageFire;
	protected double damageLava;
	protected double damageHotFloor;
	
	protected double reductionFireResistance;
	protected double reductionResistance;
	protected double reductionProtection;
	protected double reductionFireProtection;
	
//	protected double reductionNetheriteHelmet; //TODO not needed yet, but will need for 1.16.x
	
	protected double reductionMaximum;
	
	
	
	
	public FireDamageType(FileConfiguration config)
	{
		this.config = config;
		
		damageBurning = config.getDouble("Fire.damage.burning");
		damageFire = config.getDouble("Fire.damage.fire");
		damageLava = config.getDouble("Fire.damage.lava");
		damageHotFloor = config.getDouble("Fire.damage.hot_floor");
		
		
		reductionFireResistance = config.getDouble("Fire.reduction.fire_resistance");
		reductionResistance = config.getDouble("Fire.reduction.resistance");
		reductionProtection = config.getDouble("Fire.reduction.protection");
		reductionFireProtection = config.getDouble("Fire.reduction.fire_protection");
		
		reductionMaximum = config.getDouble("Fire.reduction.max_reduction");
	}
	
	@Override
	public String typeName()
	{
		return "Fire";
	}
	
	public boolean canDealFatalDamage()
	{
		return true;
	}

	@Override
	public CustomDamageType parentType()
	{
		return null;
	}
	
	public String[] getDamageTypeDescription()
	{
		return new String[]
				{
				"Fire damage bypasses armor and is only reduced by Protection, Fire Protection, Resistance and Fire Resistance."
				};
	}

	@Override
	public double calculateDamage(LivingEntity entity, double damage)
	{
		/*
		 * TODO
		 * Get Armor [✓]
		 * Get Netherite Armor Protection [held for future]
		 * Fire Protection Enchantment [✓]
		 * Get protection Enchantment [✓]
		 * Get Fire Resistance Potion effect [✓]
		 * Get Resistance Effect [✓]
		 */
		
		double reduction = 0;
		
		
		EntityEquipment eq = entity.getEquipment();

		//Netherite Armor Reduction TODO when this is updated to 1.16
		
		
		//Fire Protection Enchantment
		reduction += getFireProtectionReduction(entity);
		
		//Protection Enchantment
		reduction += getProtectionReduction(entity);

		//Fire Resistance
		reduction += getFireResistanceReduction(entity);
		
		//Resistance
		reduction += getResistanceReduction(entity);
		
		//Calculate and check damage resistance value
		if(reduction < 0) reduction = 0;
		if(reduction > reductionMaximum) reduction = reductionMaximum;
		
		//Calculate damage reduction
		double dmg = damage*(1-reduction);
		if(dmg < 0) dmg = 0;
		
		return dmg;
	}
	
	
	/**
	 * The Event listener that overrides vanilla damage if config is set to true
	 * @param event
	 */
	@EventHandler
	public void onVanillaFireDamage(EntityDamageEvent event)
	{
		if(event.getEntity() instanceof LivingEntity == false) return;
		if(config.getBoolean("OverrideVanilla.fire") == false) return;
		
		DamageManager dMan = MagicAPI.getInstance().getDamageManager();
		
		if(event.getCause() == DamageCause.FIRE_TICK)
		{
			event.setDamage(0);
			dMan.dealDamage(dMan.TYPE_FIRE, (LivingEntity)event.getEntity(), this.damageBurning , false);
		}
		else if(event.getCause() == DamageCause.FIRE)
		{
			event.setDamage(0);
			dMan.dealDamage(dMan.TYPE_FIRE, (LivingEntity)event.getEntity(), this.damageFire, false);
		}
		else if(event.getCause() == DamageCause.LAVA)
		{
			event.setDamage(0);
			dMan.dealDamage(dMan.TYPE_FIRE, (LivingEntity)event.getEntity(), this.damageLava , false);
		}
		else if(event.getCause() == DamageCause.HOT_FLOOR)
		{
			event.setDamage(0);
			dMan.dealDamage(dMan.TYPE_FIRE, (LivingEntity)event.getEntity(), this.damageHotFloor , false);
		}
	}
	
	//TODO uncomment code and get working and add values to config
//	@EventHandler 
//	public void onVanillaFireDamageTwo(EntityDamageByEntityEvent event)
//	{
//		if(event.getEntity() instanceof LivingEntity == false) return;
//		if(config.getBoolean("Damages.Fire.valuesOverridesVanillaBehavior") == false) return;
//		
//		DamageManager dMan = MagicAPI.getInstance().getDamageManager();
//		
//		if(event.getCause() == DamageCause.PROJECTILE)
//		{
//			if(event.getDamager().getType() == EntityType.FIREBALL)
//			{
//				event.setCancelled(true);
//				dMan.dealDamage(dMan.TYPE_FIRE, (LivingEntity)event.getEntity(), event.getDamage() , false);
//			}
//			else if(event.getDamager().getType() == EntityType.SMALL_FIREBALL)
//			{
//				event.setCancelled(true);
//				dMan.dealDamage(dMan.TYPE_FIRE, (LivingEntity)event.getEntity(), event.getDamage() , false);
//			}
////			else if(event.getDamager().getType() == EntityType.BLAZE)
////			{
////				
////			}
////			else if(event.getDamager().getType() == EntityType.GHAST)
////			{
////				
////			}	
//		}
//	}
	
	private double getFireProtectionReduction(LivingEntity entity)
	{
		int fireProtectionLevel = 0;
		EntityEquipment eq = entity.getEquipment();
		if(eq.getHelmet() != null && eq.getHelmet().containsEnchantment(Enchantment.PROTECTION_FIRE))
		{
			fireProtectionLevel += eq.getHelmet().getEnchantmentLevel(Enchantment.PROTECTION_FIRE);
		}
		if(eq.getChestplate() != null && eq.getChestplate().containsEnchantment(Enchantment.PROTECTION_FIRE))
		{
			fireProtectionLevel += eq.getChestplate().getEnchantmentLevel(Enchantment.PROTECTION_FIRE);
		}
		if(eq.getLeggings() != null && eq.getLeggings().containsEnchantment(Enchantment.PROTECTION_FIRE))
		{
			fireProtectionLevel += eq.getLeggings().getEnchantmentLevel(Enchantment.PROTECTION_FIRE);
		}
		if(eq.getBoots() != null && eq.getBoots().containsEnchantment(Enchantment.PROTECTION_FIRE))
		{
			fireProtectionLevel += eq.getBoots().getEnchantmentLevel(Enchantment.PROTECTION_FIRE);
		}
		return reductionFireProtection*fireProtectionLevel;
	}
	
	private double getProtectionReduction(LivingEntity entity)
	{
		int protectionLevel = 0;
		EntityEquipment eq = entity.getEquipment();
		if(eq.getHelmet() != null && eq.getHelmet().containsEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL))
		{
			protectionLevel += eq.getHelmet().getEnchantmentLevel(Enchantment.PROTECTION_ENVIRONMENTAL);
		}
		if(eq.getChestplate() != null && eq.getChestplate().containsEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL))
		{
			protectionLevel += eq.getChestplate().getEnchantmentLevel(Enchantment.PROTECTION_ENVIRONMENTAL);
		}
		if(eq.getLeggings() != null && eq.getLeggings().containsEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL))
		{
			protectionLevel += eq.getLeggings().getEnchantmentLevel(Enchantment.PROTECTION_ENVIRONMENTAL);
		}
		if(eq.getBoots() != null && eq.getBoots().containsEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL))
		{
			protectionLevel += eq.getBoots().getEnchantmentLevel(Enchantment.PROTECTION_ENVIRONMENTAL);
		}
		return reductionProtection*protectionLevel;
	}
	
	
	private double getFireResistanceReduction(LivingEntity entity)
	{
		if(entity.hasPotionEffect(PotionEffectType.FIRE_RESISTANCE))
		{
			int amp = entity.getPotionEffect(PotionEffectType.FIRE_RESISTANCE).getAmplifier()+1;
			return reductionFireResistance*amp;
		}
		return 0;
	}
	
	private double getResistanceReduction(LivingEntity entity)
	{
		if(entity.hasPotionEffect(PotionEffectType.DAMAGE_RESISTANCE))
		{
			int amp = entity.getPotionEffect(PotionEffectType.DAMAGE_RESISTANCE).getAmplifier()+1;
			return reductionResistance*amp;
		}
		return 0;
	}
}
