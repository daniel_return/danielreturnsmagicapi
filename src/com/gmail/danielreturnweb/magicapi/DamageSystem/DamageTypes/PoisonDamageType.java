package com.gmail.danielreturnweb.magicapi.DamageSystem.DamageTypes;

import org.bukkit.entity.LivingEntity;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.gmail.danielreturnweb.magicapi.DamageSystem.CustomDamageType;

/**
 * A custom Poison Damage Type. This Damage type bypasses armor and enchantments but not
 * vanilla Resistance Effect. Furthermore it does not deal fatal damage.
 * @author daniel
 *
 */
public class PoisonDamageType implements CustomDamageType
{
	public PoisonDamageType()
	{
		
	}
	
	@Override
	public String typeName()
	{
		return "Poison";
	}

	@Override
	public CustomDamageType parentType()
	{
		return null;
	}
	
	
	public boolean canDealFatalDamage()
	{
		return false;
	}
	
	public String[] getDamageTypeDescription()
	{
		return new String[]
				{
				"Poison bypasses all armor and enchantments. Is sreduced by the Resistance Effect"
				};
	}

	@Override
	public double calculateDamage(LivingEntity entity, double damage)
	{
		double dmg = damage;
		
		if(entity.hasPotionEffect(PotionEffectType.DAMAGE_RESISTANCE))
		{
			PotionEffect pot = entity.getPotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
			int amp = pot.getAmplifier() + 1;
			dmg = dmg*(1-0.15*amp); //Each level provides a 15% poison reduction TODO make this configurable
		}
		
		return dmg;
	}
}
