package com.gmail.danielreturnweb.magicapi.DamageSystem.DamageTypes;

import org.bukkit.entity.LivingEntity;

import com.gmail.danielreturnweb.magicapi.DamageSystem.CustomDamageType;

/**
 * Damage that is not reduced by any vanilla source.
 * Custom sources may listen for the CustomDamageEvent to modify this damage Type
 * @see com.gmail.danielreturnweb.magicapi.events.damage.CustomEntityDamageEvent
 * @author daniel
 *
 */
public class PureDamageType implements CustomDamageType
{

	@Override
	public String typeName()
	{
		return "Pure";
	}

	@Override
	public CustomDamageType parentType()
	{
		return null;
	}
	
	public boolean canDealFatalDamage()
	{
		return true;
	}
	
	public PureDamageType()
	{
		
	}
	
	public String[] getDamageTypeDescription()
	{
		return new String[]
				{
				"Pure damage bypasses all armor, enchantments and effects."
				}
		;
	}

	@Override
	public double calculateDamage(LivingEntity entity, double damage)
	{
		//TODO how does this work? I think it just calls the event, and the event listeners add the block/reduce it.
		//else I need some sort of damgeblock interface that this can call on CustomEffects on the living entity.
		
		return damage;
	}
}
