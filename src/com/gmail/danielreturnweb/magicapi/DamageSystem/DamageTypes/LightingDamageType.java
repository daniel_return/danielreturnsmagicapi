package com.gmail.danielreturnweb.magicapi.DamageSystem.DamageTypes;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.LivingEntity;

import com.gmail.danielreturnweb.magicapi.MagicAPI;
import com.gmail.danielreturnweb.magicapi.DamageSystem.CustomDamageType;

/**
 * Lightning damage is a sub type of PURE damage and therefore has PureDamage as its parentType.
 * Anything that 
 * @author daniel
 * @deprecated This is WIP and currently returns 0 for the damage dealt.
 */
public class LightingDamageType implements CustomDamageType
{	
	/**
	 * The counters for lighting damage on an entity.
	 * Each counter is its own item in the list where the maps UUID field
	 * is the player or living entity.
	 */
	Map<UUID,List<Long>> LightingDamageCounters;
	
	FileConfiguration config;
	
	//Configuration value fields
	CustomDamageType baseDamageType;
	double vanillaLightingStrike;
	double armorMetalBonusDamage;
	
	boolean stackingEnabled;
	double stackingBonusPercent;
	int stackingMaxCharges;
	double stackingMaximumBonusDamage;
	int stackingMaxDuration; //In seconds
	
	
	//TODO should all custom damage types take in the damage manager instead of using MagicAPI.getInstance().getDamageManager()?
	public LightingDamageType(FileConfiguration config) 
	{
		LightingDamageCounters = new HashMap<UUID,List<Long>>();
		this.config = config;
		
		//TODO Load configuration values
		String base = config.getString("Lightning.baseType");
		if(base.equalsIgnoreCase("PURE")) baseDamageType = MagicAPI.getInstance().getDamageManager().TYPE_PURE;
		else if(base.equalsIgnoreCase("MAGICAL")) baseDamageType = MagicAPI.getInstance().getDamageManager().TYPE_MAGIC;
		
		
	}
	
	@Override
	public String typeName()
	{
		return "Lighting";
	}
	
	public boolean canDealFatalDamage()
	{
		return true;
	}

	@Override
	public CustomDamageType parentType()
	{
		return new PureDamageType();
	}
	
	public String[] getDamageTypeDescription()
	{
		return new String[]{};
	}


	@Override
	public double calculateDamage(LivingEntity entity, double damage)
	{
		return 0;
	}
	
	
	
	protected void addCounter(UUID entity)
	{
		//TODO get entity's map, get current systemMills. Add appropriate config time value to it and add it to the map list
	}
	
	protected void removeExpiredCounters(UUID entity)
	{
		//TODO get entities map, loop backwards though list removing all expired counters
	}
	
	
}
