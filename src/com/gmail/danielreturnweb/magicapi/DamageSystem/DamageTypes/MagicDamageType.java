package com.gmail.danielreturnweb.magicapi.DamageSystem.DamageTypes;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.LivingEntity;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.potion.PotionEffectType;

import com.gmail.danielreturnweb.magicapi.DamageSystem.CustomDamageType;

/**
 * A Damage Type Representing Magic <p>
 * Is not reduced by armor or toughness but is reduced by 
 * Resistance and the protection enchantment. Is further modified by event listeners
 * 
 * @author daniel
 * @see com.gmail.danielreturnweb.magicapi.events.damage.CustomEntityDamageEvent
 */
public class MagicDamageType implements CustomDamageType
{
	/**
	 * <p> <b> Interesting Values </b><br>
	 * 0.025  would make a full armor set of protection 4 provide a 40% magic damage reduction <br>
	 * 0.03125 would make a full armor set of protection 4 provide a 50% magic damage reduction <br>
	 * 
	 * <p>
	 */
	protected double protectionDamageReduction;
	protected double resistanceDamageReduction;
	
	protected double naturalReductionPlayer;
	protected double naturalReductionEntity;
	protected double naturalReductionBoss;

	public MagicDamageType(FileConfiguration config)
	{
		this.protectionDamageReduction = config.getDouble("Magical.reductions.protection");
		this.resistanceDamageReduction = config.getDouble("Magical.reductions.resistance"); 
		
		naturalReductionPlayer = config.getDouble("Magical.reductions.natural.player");
		naturalReductionEntity = config.getDouble("Magical.reductions.natural.entity");
		naturalReductionBoss = config.getDouble("Magical.reductions.natural.boss");
	}
	
	@Override
	public String typeName()
	{
		return "Magic";
	}

	@Override
	public CustomDamageType parentType()
	{
		return null;
	}
	
	public boolean canDealFatalDamage()
	{
		return true;
	}
	
	public String[] getDamageTypeDescription()
	{
		return new String[]
				{
				"Magic Damage bypasses armor and is only reduced by the Protection Enchantment and Reistance Effect"
				};
	}
	

	@Override
	public double calculateDamage(LivingEntity entity, double damage)
	{
		//Do magic resistance calculations
		double magRed = this.getProtectionMagicDamageModifier(entity);
		double resistRed = this.getResistenceEffectMagicDamageReduction(entity);
		double totalResistance = magRed + resistRed;
			
//		return -(damage*(1-totalResistance));
		return (damage*(1-totalResistance));
	}
	
	/**
	 * Gets the magic damage reduction amount that Protection Provides per level.
	 * @return
	 */
	public double getProtectionDamageReduction()
	{
		return protectionDamageReduction;
	}
	
	
	
	/**
	 * Returns a damage reduction for the protection enchantment.
	 * @param target
	 * @return
	 */
	public final double getProtectionMagicDamageModifier(LivingEntity target)
	{
		if(target == null) return 0;
		EntityEquipment ee =  target.getEquipment();
		if(ee == null) return 0;
		
		double amt = 0;
		
		if(ee.getHelmet() != null) amt += ee.getHelmet().getEnchantmentLevel(Enchantment.PROTECTION_ENVIRONMENTAL);
		if(ee.getChestplate() != null) amt += ee.getChestplate().getEnchantmentLevel(Enchantment.PROTECTION_ENVIRONMENTAL);
		if(ee.getLeggings() != null) amt += ee.getLeggings().getEnchantmentLevel(Enchantment.PROTECTION_ENVIRONMENTAL);
		if(ee.getBoots() != null) amt += ee.getBoots().getEnchantmentLevel(Enchantment.PROTECTION_ENVIRONMENTAL);
		
//		System.out.println("[MagicAPI] Debug - MagicDamageModifier " + amt*getProtectionDamageReduction());
		
		return amt*getProtectionDamageReduction();
	}
	
	/**
	 * Gets the reduction that the vanilla Resistance effect provides from the entity.
	 * @param target
	 * @return The percentage the damage should be reduced by.
	 */
	public final double getResistenceEffectMagicDamageReduction(LivingEntity target)
	{
		double k = 0.0;
		
		if(target.hasPotionEffect(PotionEffectType.DAMAGE_RESISTANCE))
		{
			k = (1+target.getPotionEffect(PotionEffectType.DAMAGE_RESISTANCE).getAmplifier()) * this.resistanceDamageReduction;
			//Has a +1 as level 1 is a value of 0
		}
		return k;
	}


}
