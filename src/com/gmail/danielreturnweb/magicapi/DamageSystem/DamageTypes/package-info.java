/**
 * 
 */
/**
 * A package of the few Damage Types that the MagicAPI supports. More can be created by the user using the API
 * @author daniel
 *
 */
package com.gmail.danielreturnweb.magicapi.DamageSystem.DamageTypes;