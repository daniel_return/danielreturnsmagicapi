package com.gmail.danielreturnweb.magicapi.DamageSystem;

import org.bukkit.entity.LivingEntity;

/**
 * This represents a customizable damage type used by the damage manager.
 * A API user can implement this to define there own damage type/calculations.
 * 
 * When the DamageManager is used to deal damage a CustomDamageType is passed in and its
 * calculateDamage method is called to calculate the damage.
 * @author daniel
 *
 */
public interface CustomDamageType
{
	public String typeName();
	
	public boolean canDealFatalDamage();
	
	public CustomDamageType parentType();
	
	public String[] getDamageTypeDescription();
	
	public double calculateDamage(LivingEntity entity, double damage);
}
