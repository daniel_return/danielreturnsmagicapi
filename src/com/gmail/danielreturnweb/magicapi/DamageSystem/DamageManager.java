package com.gmail.danielreturnweb.magicapi.DamageSystem;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.attribute.Attribute;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import com.gmail.danielreturnweb.magicapi.MagicAPI;
import com.gmail.danielreturnweb.magicapi.DamageSystem.DamageTypes.FireDamageType;
import com.gmail.danielreturnweb.magicapi.DamageSystem.DamageTypes.MagicDamageType;
import com.gmail.danielreturnweb.magicapi.DamageSystem.DamageTypes.PoisonDamageType;
import com.gmail.danielreturnweb.magicapi.DamageSystem.DamageTypes.PureDamageType;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffect;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffectBundle;
import com.gmail.danielreturnweb.magicapi.events.damage.CustomEntityDamageByEntityEvent;
import com.gmail.danielreturnweb.magicapi.events.damage.CustomEntityDamageEvent;
import com.gmail.danielreturnweb.magicapi.utils.CustomizedConfiguration;
/**
 * The DamageManager allows for damaging of entities using various damage types.
 * Most damage is dealt by reducing the targets health and then dealing 0 damage (for the hit noise/red flash).
 * If the damage would kill the entity, normal damage is applied instead but at very high values.
 * <p>
 * Unlike vanilla minecraft damage, all forms of damage here stack with themselves and each other even in the same damage
 * tick. (They also ignore iFrames and invincibility ticks)
 * <p>
 * <b><i>Types of Damage</i></b>
 * <br><br> <b>Physical</b>: |<i> Not yet implemented </i>| This will be similar to vanilla damage utilizing its formula (found on WIKI).
 * 							Is mainly for completion of this API.
 *  
 * <br> <br> <b>Magic</b>: Magic damage. Is not reduced by armor or toughness but is reduced by
 * 					  Resistance and the protection enchantment. Is further modified by the players Magic Resistance.
 * 
 * <br> <br> <b>Pure</b>: Damage that is not reduced by any source except for some custom effects.
 * 
 * <br> <br> <b>Lightning</b> This is a variation of Pure damage. The damage dealt is increased by metallic items and consecutive damages.
 * 		 				 Metallic items are Iron sword/tool, Gold sword/tools, Chain armor, etc.
 * 						 It is only reduced by effects that specifically reduce lighting damage.
 * 						 Consecutive Damage works by placing a hidden non-purgable effect on the target, that stacks w/ level.
 * 						 Each level provides a percent bonus to the next lightnings damage. The effect lasts for 20 seconds.
 *                       Damage bonus and max level can be changed in the configuration file.
 *                       Default Bonus Damage: 5%, Default max charges 10.
 * 
 * <br> <br> <b>Fire</b> This represents a custom Fire Damage Type. {@link FireDamageType} for more infomation
 * 
 * <br> <br> <b>Poison</b> A custom Poison Damage Type. This Damage type bypasses armor and enchantments but not vanilla Resistance Effect. \
 * 						   Furthermore it does not deal fatal damage. 
 * 
 * <br> <br> <b>OTHER</b> API users are able to define there own type of damage by implementing the {@link CustomDamageType} interface.
 * 
 * <p>
 * <p> The above are general rules for how damage should be reduced, if at all. However differing plugins
 * may break these rules.
 * @author daniel
 *
 */


//TODO make my own damage events for my 4 types of custom damage.


//TODO Make damage dealt wit the DamageManager deal durability damage to armor's

public class DamageManager 
{

	/*
	 * The "Standard" damage types provided by the DamageManager and the Magic API
	 */
	public final MagicDamageType TYPE_MAGIC;
	public final PureDamageType TYPE_PURE = new PureDamageType();
	public final FireDamageType TYPE_FIRE;
	public final PoisonDamageType TYPE_POISON = new PoisonDamageType();
	

	CustomizedConfiguration customConfig;
	FileConfiguration damageConfig;
	
	Map<String,CustomDamageType> damageTypes;
	
	
	public DamageManager(MagicAPI magicAPI, FileConfiguration fileConfiguration)
	{
		customConfig = new CustomizedConfiguration(MagicAPI.getInstance(),"Damages.yml");
		
		customConfig.saveDefaultConfig();
		try
		{
			customConfig.reloadCustomConfig();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		damageConfig = customConfig.getCustomConfig();
		
		
		TYPE_MAGIC = new MagicDamageType(damageConfig);
		TYPE_FIRE = new FireDamageType(damageConfig);
		Bukkit.getPluginManager().registerEvents(TYPE_FIRE, magicAPI);
		
		damageTypes = new HashMap<String,CustomDamageType>();
		
		this.registerDamageType(TYPE_MAGIC, false);
		this.registerDamageType(TYPE_PURE, false);
		this.registerDamageType(TYPE_FIRE, false);
		this.registerDamageType(TYPE_POISON, false);
		
	}

	
	/**
	 * Removes the HP directly from the entity ignoring all forms of damage reduction.
	 * @param entity the entity to have health removed
	 * @param health the health to remove (should be positive, negative values are converted to positive)
	 * @param ignoreAbsorption if true only main HP will be removed, if false absorption health will be removed before main health
	 */
	public final void removeHealth(LivingEntity entity, double health, boolean ignoreAbsorption) //TODO add a damager field so faital dmg can be dealt by a killer
	{
		double remainingDamage = Math.abs(health);
		remainingDamage = Math.abs(remainingDamage);
		if(remainingDamage <= 0) return;
		
		if(ignoreAbsorption == false)
		{
			if(entity.getAbsorptionAmount() > 0)
			{
				remainingDamage = health - entity.getAbsorptionAmount();
				
				if(remainingDamage <= 0 ) //The full damage was blocked by absorption
				{
					entity.setAbsorptionAmount(entity.getAbsorptionAmount() - health);

					entity.damage(0.02);
					return;
				}
				else
				{
					entity.setAbsorptionAmount(0);				
				}
			}
		}

				
		if(entity.getHealth() - remainingDamage <= 0)
		{
			entity.setNoDamageTicks(0);
			entity.setHealth(0);
			return;
		}
		else if(entity.getHealth() - remainingDamage > entity.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue())
		{
			System.err.println("[MagicAPI] Damage Manager - removeHealth() | entity health - remaining damage greater then max HP");
		}
		else
		{
			entity.setHealth(entity.getHealth() - remainingDamage);
			entity.setNoDamageTicks(0);
			entity.damage(0.05);
			if(entity.getHealth() <= 0) entity.setHealth(0);
		}		
	}

		
	
	/**
	 * Deals damage to the target
	 * @param type the damage type this damage will deal
	 * @param target
	 * @param damage
	 */
	public void dealDamage(CustomDamageType type, LivingEntity target, double damage, boolean ignoreAbsorption)
	{
		if(damage <= 0) return;
		
//		System.out.println("[MagicAPI] DEBUG: DamageMangager - Damage (before) " + damage);
		
		if(target == null || type == null) return;
		
		if(target.isInvulnerable()) return;
		
		if(target.getType() == EntityType.PLAYER)
		{
			if(((Player)target).getGameMode() == GameMode.CREATIVE || ((Player)target).getGameMode() == GameMode.SPECTATOR)
			{
				return;
			}
		}
		
		
		
		//Do Custom Damage Event
		CustomEntityDamageEvent dEvent = new CustomEntityDamageEvent(target, damage, type, ignoreAbsorption);
		Bukkit.getServer().getPluginManager().callEvent(dEvent);
		
//		System.out.println("[MagicAPI] DEBUG: DamageMangager - Damage (raw) " + dEvent.getRawDamage());
//		System.out.println("[MagicAPI] DEBUG: DamageMangager - Damage (after) " + dEvent.getFinalDamage());
		
		if(dEvent.isCancelled())
		{
//			System.out.println("[MagicAPI] DEBUG: DamageMangager - Cancelled");
			return;
		}
		
		if(dEvent.isFatalDamage() && !type.canDealFatalDamage())
		{
			System.out.println("[MagicAPI] DEBUG: DamageMangager - is fatal and type cannot deal fatal damage");
			return;
		}
		//Remove final damage from the entity
		this.removeHealth(dEvent.getEntity(),dEvent.getFinalDamage(),dEvent.isIgnoreAbsorption());	
	}
	
	
	/**
	 * Deals damage to the target with a entity being the source of the damage.
	 * @param type the damage type this damage will deal
	 * @param target The entity to take damage
	 * @param damager the entity causing the damage
	 * @param damage the damage to take
	 */
	public void dealDamage(CustomDamageType type, LivingEntity target, LivingEntity damager, double damage, boolean ignoreAbsorption)
	{
		if(damage <= 0) return;
		
//		System.out.println("[MagicAPI] DEBUG: DamageMangager - Damage (before) " + damage);
		
		if(target == null || type == null) return;
		
		if(target.isInvulnerable()) return;
		
		if(target.getType() == EntityType.PLAYER)
		{
			if(((Player)target).getGameMode() == GameMode.CREATIVE || ((Player)target).getGameMode() == GameMode.SPECTATOR)
			{
				return;
			}
		}
		
		
		//Do Custom Damage Event
		CustomEntityDamageByEntityEvent dEvent = new CustomEntityDamageByEntityEvent(target, damager, damage, type, ignoreAbsorption);
		Bukkit.getServer().getPluginManager().callEvent(dEvent);
		
//		System.out.println("[MagicAPI] DEBUG: DamageMangager - Damage (raw) " + dEvent.getRawDamage());
//		System.out.println("[MagicAPI] DEBUG: DamageMangager - Damage (after) " + dEvent.getFinalDamage());
		
		if(dEvent.isCancelled())
		{
//			System.out.println("[MagicAPI] DEBUG: DamageMangager - Cancelled");
			return;
		}
		
		if(dEvent.isFatalDamage() && !type.canDealFatalDamage())
		{
//			System.out.println("[MagicAPI] DEBUG: DamageMangager - is fatal and type cannot deal fatal damage");
			return;
		}
		//Remove final damage from the entity
		this.removeHealth(dEvent.getEntity(),dEvent.getFinalDamage(),dEvent.isIgnoreAbsorption());	
	}
	
	
	/**
	 * Deals damage with the source being a CustomEffect
	 * @param type the damage type this damage will deal
	 * @param target the target taking the damage
	 * @param effect the effect causing the damage
	 * @param bundle the effect's data
	 * @param damage the damage to take
	 */
	private void dealDamage(CustomDamageType type, LivingEntity target, CustomEffect effect, CustomEffectBundle bundle, double damage)
	{
		if(damage <= 0) return;
		//TODO
	}
	
	
//	public void dealDamage(DamageType type, LivingEntity target, CustomSpell spell, double damage)
//	{
//		
//	}
//	
//	public void dealDamage(DamageType type, LivingEntity target, LivingEntity spellCaster, CustomSpell spell, double damage)
//	{
//		
//	}	
	
	
	
	/**
	 * Registers a CustomDamageType with the DamageManager
	 * @param type
	 * @param overrideExisting
	 */
	public void registerDamageType(CustomDamageType type, boolean overrideExisting)
	{
		if(overrideExisting)
		{
			this.damageTypes.put(type.typeName(), type);
		}
		else
		{
			if(!this.damageTypes.containsKey(type.typeName()))
			{
				this.damageTypes.put(type.typeName(), type);
			}
		}
	}
	
	/**
	 * Sees if a custom damage type exists
	 * @param typeName
	 * @return
	 */
	public boolean damageTypeExists(String typeName)
	{
		return this.damageTypes.containsKey(typeName);
	}
	
	/**
	 * Gets a custom damage type by its nameType
	 * @param s
	 * @return Null if the damage type does not exist, otherwise the damage type
	 */
	public CustomDamageType getDamageType(String s)
	{
		if(this.damageTypes.containsKey(s)) return this.damageTypes.get(s);
		return null;
	}

}
