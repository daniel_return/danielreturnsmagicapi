package com.gmail.danielreturnweb.magicapi;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

/**
 * This class is a manager for placing and removing a entities absorption.
 * When absorption is removed current absorption is appropriately reduced down to the new maximum.
 * If the current is equal to or less then the new maximum, current absorption will stay the same.
 * 
 * 
 * 
 * @author daniel
 *
 */
public class AbsorptionManager 
{
	public static AbsorptionManager instance;
	
	public static AbsorptionManager getInstance() {return AbsorptionManager.instance;}
	
	Map<UUID,Map<String,Integer>> entityTracker;
	Map<UUID,Map<String,Integer>> playerTracker;
	
	public AbsorptionManager() 
	{
		entityTracker = new HashMap<UUID,Map<String,Integer>>();
		playerTracker = new HashMap<UUID,Map<String,Integer>>();
		if(AbsorptionManager.instance == null) AbsorptionManager.instance = this;
	}
	
	
	/**
	 * Places the magic boost onto the player. The key allows finding and indexing of the magic boost.
	 * @param key the name of this boost
	 * @param player the player being effected
	 * @param amount the value of absorption being added
	 */
	public void addAbsorption(String key, Player player, int amount)
	{
		//If tracker does not exist, put one in.
		if(!playerTracker.containsKey(player.getUniqueId()))
		{
			Map<String,Integer> effectMap = new HashMap<String,Integer>();
			effectMap.put(key, amount);
			playerTracker.put(player.getUniqueId(), effectMap);
			
			player.setAbsorptionAmount(player.getAbsorptionAmount() + amount);
			return;
		}
		//If it exists...
		Map<String,Integer> effectMap = playerTracker.get(player.getUniqueId());
		if(effectMap == null) //and null create a new one then
		{
			effectMap = new HashMap<String,Integer>();
		}
		//put the magic boost onto the player.
		effectMap.put(key, amount);
		playerTracker.put(player.getUniqueId(), effectMap);
		player.setAbsorptionAmount(player.getAbsorptionAmount() + amount);
	}
	
	
	
	/**
	 * Places the magic boost onto the entity. The key allows finding and indexing of the magic boost.
	 * @param key the name of this boost
	 * @param entity the entity being effected
	 * @param amount the value of absorption to add
	 */
	public void addAbsorption(String key, LivingEntity entity, int amount)
	{
		//If tracker does not exist, put one in.
		if(!entityTracker.containsKey(entity.getUniqueId()))
		{
			Map<String,Integer> effectMap = new HashMap<String,Integer>();
			effectMap.put(key, amount);
			entityTracker.put(entity.getUniqueId(), effectMap);
			
			entity.setAbsorptionAmount(entity.getAbsorptionAmount() + amount);
			return;
		}
		//If it exists...
		Map<String,Integer> effectMap = entityTracker.get(entity.getUniqueId());
		if(effectMap == null) //and null create a new one then
		{
			effectMap = new HashMap<String,Integer>();
		}
		//put the magic boost onto the entity.
		effectMap.put(key, amount);
		entityTracker.put(entity.getUniqueId(), effectMap);
		entity.setAbsorptionAmount(entity.getAbsorptionAmount() + amount);
	}
	
	
	
	/**
	 * Removes a magic boost from the player by its key.
	 * @param key
	 * @param player
	 */
	public void removeAbsorption(String key, Player player)
	{
		if(!playerTracker.containsKey(player.getUniqueId())) return;
		
		Map<String,Integer> effectMap = playerTracker.get(player.getUniqueId());
		if(effectMap == null)
		{
			effectMap = new HashMap<String,Integer>();
			return;
		}
		//If current absorption is greater then the new total, set current to total
		if(effectMap.containsKey(key))
		{			
			effectMap.remove(key);
			playerTracker.put(player.getUniqueId(), effectMap);
			
			int total = this.getTotalAbsorption(player);
//			System.out.println("player abs total = " + total);
			if(total < player.getAbsorptionAmount())
			{
				player.setAbsorptionAmount(total);
			}
		}
	}
	
	
	
	
	public void removeAbsorption(String key, LivingEntity entity)
	{
		if(!entityTracker.containsKey(entity.getUniqueId())) return;
		
		Map<String,Integer> effectMap = entityTracker.get(entity.getUniqueId());
		if(effectMap == null)
		{
			effectMap = new HashMap<String,Integer>();
			return;
		}
		
		if(effectMap.containsKey(key))
		{
			effectMap.remove(key);
			entityTracker.put(entity.getUniqueId(), effectMap);
			
			//If current absorption is greater then the new total, set current to total
			int total = this.getTotalAbsorption(entity);
			if(total > entity.getAbsorptionAmount())
			{
				entity.setAbsorptionAmount(total);
			}
		}
	}
	
	
	
	
	
	
	public int getTotalAbsorption(LivingEntity entity)
	{
		if(!entityTracker.containsKey(entity.getUniqueId())) return 0;
		Map<String,Integer> effectMap = entityTracker.get(entity.getUniqueId());
		if(effectMap == null) return 0;
		
		int val = 0;
		for(Entry<String,Integer> mb: effectMap.entrySet())
		{
			val +=mb.getValue();
		}
		return val;
	}
	
	public int getTotalAbsorption(Player player)
	{
		if(!playerTracker.containsKey(player.getUniqueId())) return 0;
		Map<String,Integer> effectMap = playerTracker.get(player.getUniqueId());
		if(effectMap == null) return 0;
		
		int val = 0;
		for(Entry<String,Integer> mb: effectMap.entrySet())
		{
			val +=mb.getValue();
		}
		return val;
	}
	
	
	/**
	 * Gets a map of all magic boosts on the Player
	 * @param player
	 * @return a map of magic boosts. Can be null.
	 */
	public Map<String,Integer> getAbsorptionData(Player player)
	{
		if(!playerTracker.containsKey(player.getUniqueId())) return null;
		return playerTracker.get(player.getUniqueId());
		
	}
	
	/**
	 * Gets a map of all magic boosts on the entity
	 * @param entity
	 * @return a map of magic boosts. Can be null.
	 */
	public Map<String,Integer> getAbsorptionData(LivingEntity entity)
	{
		if(!entityTracker.containsKey(entity.getUniqueId())) return null;
		return entityTracker.get(entity.getUniqueId());
	}
	
}

