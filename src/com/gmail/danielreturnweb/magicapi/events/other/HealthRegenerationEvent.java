package com.gmail.danielreturnweb.magicapi.events.other;

import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;


/**
 * This event triggers when a living entity is healed by a custom regeneration effect utilizing the
 * RegenerationMangaer.
 * @author daniel
 *
 */
public class HealthRegenerationEvent extends Event implements Cancellable 
{
	/*
	 * Event basic fields and methods
	 */
	private static final HandlerList handles = new HandlerList();
	private boolean cancelled = false;
	
	
    public boolean isCancelled() 
    {
        return cancelled;
    }
 
    public void setCancelled(boolean bln) 
    {
        this.cancelled = bln;
    }
    
    @Override
    public HandlerList getHandlers()
    {
    	return handles;
    }
    
    public static HandlerList getHandlerList() 
    {
        return handles;
    }
	
    
	/*
	 * HealthRegenerationEvent fields and methods
	 */
    
    
    /** The entity being healed*/
	protected LivingEntity entity;
	
	double naturalRegeneration = 0;
	double naturalDegeneration = 0;
	double magicRegeneration = 0;
	double magicDegeneration = 0;


	public HealthRegenerationEvent(LivingEntity entity, double naturalRegeneration, double naturalDegeneration,
			double magicRegeneration, double magicDegeneration)
	{
		super();
		this.entity = entity;
		this.naturalRegeneration = naturalRegeneration;
		this.naturalDegeneration = naturalDegeneration;
		this.magicRegeneration = magicRegeneration;
		this.magicDegeneration = magicDegeneration;
	}

	public double getNaturalRegeneration()
	{
		return naturalRegeneration;
	}

	/**
	 * Sets the natural regeneration occurring from this event.
	 * Cannot be set below 0.
	 * @param naturalRegeneration
	 */
	public void setNaturalRegeneration(double naturalRegeneration)
	{
		this.naturalRegeneration = naturalRegeneration;
		if(this.naturalRegeneration < 0) this.naturalRegeneration = 0;
	}

	public double getNaturalDegeneration()
	{
		return naturalDegeneration;
	}

	/**
	 * Sets the natural degeneration occurring from this event.
	 * Cannot be set above 0.
	 * @param naturalDegeneration
	 */
	public void setNaturalDegeneration(double naturalDegeneration)
	{
		this.naturalDegeneration = naturalDegeneration;
		if(this.naturalDegeneration > 0) this.naturalDegeneration = 0;
	}

	public double getMagicRegeneration()
	{
		return magicRegeneration;
	}

	/**
	 * Sets the magic regeneration occurring from this event.
	 * Cannot be set below 0.
	 * @param magicRegeneration
	 */
	public void setMagicRegeneration(double magicRegeneration)
	{
		this.magicRegeneration = magicRegeneration;
		if(this.magicRegeneration < 0) this.magicRegeneration = 0;
	}

	public double getMagicDegeneration()
	{
		return magicDegeneration;
	}

	/**
	 * Sets the natural degeneration occurring from this event.
	 * Cannot be set above 0.
	 * @param magicDegeneration
	 */
	public void setMagicDegeneration(double magicDegeneration)
	{
		this.magicDegeneration = magicDegeneration;
		if(this.magicDegeneration > 0) this.magicDegeneration = 0;
	}

	public LivingEntity getEntity()
	{
		return entity;
	}
	
	
	/** If this is true, the entity is being healed, if
	 * false then the totalRegeneration is negative and
	 * the entity will taking damage from this regeneration event.
	 *  (Assuming Regeneration.negative_harms is true)
	 */
	public boolean isHeal()
	{
		double val = this.naturalDegeneration + this.naturalRegeneration + this.magicDegeneration + this.magicRegeneration;
		if(val >= 0 ) return true;
		else return false;
	}
	
	/**
	 * Gets the total Natural regeneration represented by this event. 
	 * <br> this is Natural Regeneration + Natural Degeneration
	 * @return
	 */
	public double getTotalNaturalRegeneration()
	{
		return this.naturalDegeneration + this.naturalRegeneration;
	}
	
	/**
	 * Gets the total Magical regeneration represented by this event. 
	 * <br> this is Magical Regeneration + Magical Degeneration
	 * @return
	 */
	public double getTotalMagicalRegeneration()
	{
		return  this.magicDegeneration + this.magicRegeneration;
	}
	
	/**
	 * Gets the total regeneration represented by this event
	 * <br> this is all regeneration + all degeneration
	 * @return
	 */
	public double getTotalRegeneration()
	{
		return this.naturalDegeneration + this.naturalRegeneration + this.magicDegeneration + this.magicRegeneration;
	}	
}
