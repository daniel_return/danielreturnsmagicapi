package com.gmail.danielreturnweb.magicapi.events.damage;


import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import com.gmail.danielreturnweb.magicapi.DamageSystem.CustomDamageType;

/**
 * This event triggers when ever a living entity takes custom damage.
 * @author daniel
 *
 */
public class CustomEntityDamageByEntityEvent extends  Event implements Cancellable 
{
	/*
	 * Event basic fields and methods
	 */
	private static final HandlerList handles = new HandlerList();
	private boolean cancelled = false;
	
	
    public boolean isCancelled() 
    {
        return cancelled;
    }
 
    public void setCancelled(boolean isCancelled) 
    {
        this.cancelled = isCancelled;
    }
    
    @Override
    public HandlerList getHandlers()
    {
    	return handles;
    }
    
    public static HandlerList getHandlerList() 
    {
        return handles;
    }
	
	/*
	 * EntityCustomDamageEvent fields and methods
	 */
    
    
    /** The entity taking the damage*/
	protected LivingEntity entity;
	
	/** The entity dealing the damage */
	protected LivingEntity damager;
	
	protected double rawDamage = 0;
	protected CustomDamageType damageType;
	
	protected boolean ignoreAbsorption;
		
	
	//TODO Create the following damage events that extend this one
	//One that is damage no source
	//One for entity as the source
	//One for effect as the source
	//One for damage with no source but is from a spell [allows blocking of spell damage]
	//One for damage from a entity as the source but from a spell [allows blocking of spell damage]
	//All will take in a DamageType and the getFinalDamage method will use the type to
	//calculate the final damage? or for right now do not provide a final damage method.
	
	

	public CustomEntityDamageByEntityEvent(LivingEntity entity, LivingEntity damager, double rawDamage, CustomDamageType damageType, boolean ignoreAbsorption)
	{
		this.entity = entity;
		this.damager = damager;
		this.rawDamage = rawDamage;
		this.damageType = damageType;
		this.ignoreAbsorption = ignoreAbsorption;
	}
	
	public LivingEntity getEntity()
	{
		return this.entity;
	}
	
	public LivingEntity getDamager()
	{
		return this.damager;
	}

	/**
	 * Is the damage being dealt fatal? Will it kill
	 * the entity.
	 * @return
	 */
	public final boolean isFatalDamage()
	{
		if(this.isIgnoreAbsorption())
		{
			return (entity.getHealth()) - this.getFinalDamage() <= 0;
		}
		else
		{
			return (entity.getHealth() + entity.getAbsorptionAmount()) - this.getFinalDamage() <= 0;
		}
	}

	public double getRawDamage() {
		return rawDamage;
	}

	public void setRawDamage(double rawDamage) {
		this.rawDamage = rawDamage;
	}

	public CustomDamageType getDamageType() {
		return damageType;
	}

	public void setDamageType(CustomDamageType damageType) {
		this.damageType = damageType;
	}
	
	public double getFinalDamage()
	{
		//TODO if the damage type has a parent, its calculateDamage must be ran
		return this.damageType.calculateDamage(this.entity, this.getRawDamage());
	}

	public boolean isIgnoreAbsorption()
	{
		return ignoreAbsorption;
	}

	public void setIgnoreAbsorption(boolean ignoreAbsorption)
	{
		this.ignoreAbsorption = ignoreAbsorption;
	}
}
