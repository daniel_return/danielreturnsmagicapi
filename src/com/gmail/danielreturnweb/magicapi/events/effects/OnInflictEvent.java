package com.gmail.danielreturnweb.magicapi.events.effects;

import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffect;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffectBundle;


/**
 * This events occurs when a effect is inflicted upon an entity.
 * The object must be created and called in the onInflicted method of the custom effect.
 * @author daniel
 *
 */
public class OnInflictEvent extends Event implements Cancellable
{
	CustomEffect effect;
	CustomEffectBundle bundle;
	
	public OnInflictEvent(CustomEffect effect, CustomEffectBundle bundle) 
	{
		this.effect = effect;
		this.bundle = bundle;
	}

	public CustomEffect getEffect() 
	{
		return effect;
	}


	public CustomEffectBundle getBundle() 
	{
		return bundle;
	}
    
	
	
	
	
	/*
	 * Event fields and methods
	 */
	
	private static final HandlerList handles = new HandlerList();
	private boolean cancelled = false;
	
	
    public boolean isCancelled() 
    {
        return cancelled;
    }
 
    public void setCancelled(boolean bln) 
    {
        this.cancelled = bln;
    }
    
    @Override
    public HandlerList getHandlers()
    {
    	return handles;
    }
    
    public static HandlerList getHandlerList() 
    {
        return handles;
    }
    
}
