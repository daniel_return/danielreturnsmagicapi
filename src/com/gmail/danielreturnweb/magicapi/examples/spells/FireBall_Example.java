package com.gmail.danielreturnweb.magicapi.examples.spells;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.AreaEffectCloud;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.Vector;

import com.gmail.danielreturnweb.magicapi.EntityStanceView.EntityStance;
import com.gmail.danielreturnweb.magicapi.MagicAPI;
import com.gmail.danielreturnweb.magicapi.DamageSystem.DamageManager;
import com.gmail.danielreturnweb.magicapi.spell.SpellCastData;
import com.gmail.danielreturnweb.magicapi.spell.SpellCooldownType;
import com.gmail.danielreturnweb.magicapi.spell.SpellTargeting;
import com.gmail.danielreturnweb.magicapi.spell.SpellTargeting.TargetType;
import com.gmail.danielreturnweb.magicapi.spells.spell.ChargedSpell;
import com.gmail.danielreturnweb.magicapi.spells.spell.SpellCommand;
import com.gmail.danielreturnweb.magicapi.spells.spell.SpellItem;

/**
 * A fire ball that flies though the air. Upon impact it damages all Monsters and players within 5 blocks and
 * sets them on fire. Fire will be added to other fire on the target.
 * <p> Deals 5 magic damage.
 * <p> Will ignite creepers
 * @author daniel
 *
 */
public class FireBall_Example implements Listener, SpellItem, ChargedSpell, SpellCommand
{
	public static List<Fireball> fireballs;
	
	
	double damage = 5;
	double magicBoostDamage = 1d/20d;
	int burnTime = 20*12; //In Ticks
	int magicBoostBurnTime = 1; //In ticks
	
	int maxBurnTime = 20*30;
	
	int size = 5;
	
	public FireBall_Example() 
	{
		if(fireballs == null) fireballs = new ArrayList<Fireball>();
	}

	@Override
	public int getSpellXPCost() 
	{
		return 7; // "1 level"
	}

	@Override
	public int getSpellCooldown() 
	{
		return 12;
	}
	
	@Override
	public SpellCooldownType getSpellCooldownType() 
	{
		return SpellCooldownType.SPELL;
	}

	@Override
	public String getSpellID() 
	{
		return "FireBall_Example";
	}

	@Override
	public String getSpellName() 
	{
		return "Fire Ball Example";
	}

	@Override
	public Action[] getCastActions() 
	{
		return new Action[]{Action.RIGHT_CLICK_AIR, Action.RIGHT_CLICK_BLOCK};
	}

	
	@Override
	public CastHand getCastHand() 
	{
		return CastHand.MainHand;
	}

	@Override
	public int getSpellCharges() 
	{
		return 8;
	}

	@Override
	public boolean itemDestoryedOnLastChargeCast() 
	{
		return true;
	}
	
	@Override
	public String[] getLoreDescription()
	{
		return new String[]
				{
				"Launches a fire ball in the",
				"direction your facing. Deals " + damage,
				"magic damage in a "+size+"b area. Set's",
				"entities on fire and ignites creepers",
				"Targets Hostile/Neutral Players/Monsters"
				};
	}
	
	@Override
	public boolean onSpellCast(SpellCastData data,  SpellTargeting targeting) 
	{
		if(data == null) throw new NullPointerException("[MagicAPI] FireBall Example SpellCastData is null");
		if(data.getCaster() == null) throw new NullPointerException("[MagicAPI] FireBall Example Caster[LivingEntity] is null");
		
//		System.out.println("Fireball spell magic boost is " + data.getMagicBoost());
		
		
		Location eye = data.getCaster().getEyeLocation();
		
//		double pitch = ((eye.getPitch() + 90) * Math.PI) / 180;
//		double yaw  = ((eye.getYaw() + 90)  * Math.PI) / 180;
		
//		double pitch = ((eye.getPitch()) * Math.PI) / 180;
//		double yaw  = ((eye.getYaw())  * Math.PI) / 180;
//		
//		Vector v = new Vector();
//		v.setX(Math.sin(pitch) * Math.cos(yaw));
//		v.setY(Math.sin(pitch) * Math.sin(yaw));
//		v.setZ(Math.cos(pitch));
//		v.normalize();
//		v.multiply(2);
		
		Vector v = eye.getDirection();
		v.multiply(3);
		
//		Vector dir = location.toVector().subtract(attacker.getLocation().toVector()).normalize();
//		attacker.setVelocity(dir.multiply(speed));
		
		
		Fireball fb = data.getCaster().launchProjectile(Fireball.class,v); //TODO take in a vector for higher speed?
//		fb.setVelocity(fb.getVelocity().multiply(5.0)); //Note, they only have a direction and do not take setVelocity
		fb.setCustomName(this.getSpellID() + "_entity");
		fb.setGlowing(true);
		fb.setIsIncendiary(false);
		fb.setYield(0f);
		
		fb.setMetadata("FireBall_MagicBoost", new FixedMetadataValue(MagicAPI.getInstance(),data.getMagicBoost()));
		
//		System.out.println("Fireball entity meta size is " + fb.getMetadata("FireBall_MagicBoost").size());
//		System.out.println("Fireball entity magic boost is " + fb.getMetadata("FireBall_MagicBoost").get(0));
//		System.out.println("Fireball entity magic boost is " + fb.getMetadata("FireBall_MagicBoost").get(0).asInt());
		
//		FireBall_Example.fireballs.add(fb); //TODO remove the fireballs list
		
		//TODO can its attributes be set, and if so do they function, (like attack damage, speed, etc)
		
		return true;
	}
	
	
	@EventHandler
	public void onHit(ProjectileHitEvent event)
	{
//		System.out.println("Projectile hit event - " + event.getEntityType());
		
		if(event.getEntity().getCustomName() == null) return;
		if(!event.getEntity().getCustomName().equals(this.getSpellID() + "_entity")) return;
		
		Location location = event.getEntity().getLocation();
		
		SpellTargeting st = new SpellTargeting(event.getEntity());
		
		//What it targets
		st.setTargetType(new TargetType[]{TargetType.MONSTER, TargetType.PLAYER,});
		st.setTargetStance(new EntityStance[]{EntityStance.HOSTILE,EntityStance.NEUTRAL});
		
		List<LivingEntity> targets = st.target_AOE_getNearbyEntities(this.size);
		for(LivingEntity target : targets)
		{			
			int magicBoost =  event.getEntity().getMetadata("FireBall_MagicBoost").get(0).asInt();
			
			//Do fire ticks
			//Doing this before damage should allow instantly killed animals to drop cooked food (Though once the target Class is compleat
			//This will not target animals
			target.setFireTicks(this.getBurnDuration(magicBoost) + target.getFireTicks());
			if(target.getFireTicks() > this.maxBurnTime) target.setFireTicks(this.maxBurnTime);
			
			//Do damage
			DamageManager dMan = MagicAPI.getInstance().getDamageManager();
			dMan.dealDamage(dMan.TYPE_FIRE, target, this.getDamage(magicBoost),false);
			
			if(target.getType() == EntityType.CREEPER)
			{
				Creeper c = (Creeper)target;
				c.ignite();
			}
		}
		
		this.cosmetic(location);
	}
	
	private void cosmetic(Location loc)
	{
		AreaEffectCloud aec1 = (AreaEffectCloud) loc.getWorld().spawnEntity(loc, EntityType.AREA_EFFECT_CLOUD);
		
//		aec1.setColor(Color.RED);
		aec1.setParticle(Particle.FLAME);
		aec1.setRadius(this.size);
		aec1.setDuration(3);
		
		aec1.setDurationOnUse(0);
		aec1.setRadiusOnUse(0);
//		aec1.setRadiusPerTick((float)(-1.0/20.0));
		
		
//		AreaEffectCloud aec2 = (AreaEffectCloud) loc.getWorld().spawnEntity(loc.add(0, 0.5, 0), EntityType.AREA_EFFECT_CLOUD);
		
//		aec2.setParticle(Particle.CAMPFIRE_COSY_SMOKE);
//		aec2.setRadius(this.size);
//		aec2.setDuration(3);
//		
//		aec2.setDurationOnUse(0);
//		aec2.setRadiusOnUse(0);
//		aec2.setRadiusPerTick((float)(1.0/20.0));
	}
	
	
	@EventHandler
	public void onExpload(EntityExplodeEvent event)
	{
//		System.out.println("Fireball explosion");
		if(!event.getEntity().getName().equals(this.getSpellID() + "_entity")) return;
		
		event.setCancelled(true);
//		System.out.println("Fireball explosion - correct name");
		
//		//Set Fireball name to the spell ID
//		event.getEntity().setCustomName(this.getSpellID());
//		
//		//If instance of projectile add sources name onto the name of this fire ball
//		if(event.getEntity() instanceof Projectile)
//		{
//			ProjectileSource source = ((Projectile) event.getEntity()).getShooter();
//			if(source != null)
//			{
//				if(source instanceof Player)
//				{
//					event.getEntity().setCustomName(((Player)source).getDisplayName() + "'s " + this.getSpellID());
//				}
//				else if(source instanceof LivingEntity)
//				{
//					LivingEntity eSource = (LivingEntity)source;
//					if(eSource.getCustomName() != null)
//					{
//						event.getEntity().setCustomName(eSource.getCustomName() + "'s " + this.getSpellID());
//					}
//				}
//			}
//		}
//		
//		
//		//Create an explosion
//		event.getEntity().getWorld().createExplosion(event.getLocation(), 2f,true,false,event.getEntity());
//		
	}
	
	
//	@EventHandler
//	public void onDamage(EntityDamageEvent event)
//	{
//		if(event.getCause() != DamageCause.ENTITY_EXPLOSION) return;
//		System.out.println("EntityDamage Explosion Event - " + event.getEntityType());
////		if(event.get)
//	}
//	
	
	protected double getDamage(int magicBoost)
	{
		return this.damage + this.magicBoostDamage*((double)magicBoost);
	}
	
	protected int getBurnDuration(int magicBoost)
	{
		//casted to int, incase I need to weaken magic boost duration
		//by making magicBoostBurnTime a double.
		//Right now as a Int every 20 points is 1 second.
		return this.burnTime + (int)(this.magicBoostBurnTime*magicBoost);
	}

	@Override
	public String getPermissionToCast() 
	{
		return null;
	}

	@Override
	public String getAlias() 
	{
		return "fbe";
	}

	@Override
	public String[] getGrimoireEntry()
	{
		return new String[]
				{
				"Launches a fire ball in the",
				"direction your facing. Deals " + damage,
				"magic damage in a "+size+"b area. Set's",
				"entities on fire and ignites creepers",
				"Targets Hostile/Neutral Players/Monsters"
				};
	}
}
