package com.gmail.danielreturnweb.magicapi.examples.spells;

import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;

import com.gmail.danielreturnweb.magicapi.MagicAPI;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffectBundle;
import com.gmail.danielreturnweb.magicapi.examples.effects.DiskEffect_Example;
import com.gmail.danielreturnweb.magicapi.spell.SpellCastData;
import com.gmail.danielreturnweb.magicapi.spell.SpellCooldownType;
import com.gmail.danielreturnweb.magicapi.spell.SpellTargeting;
import com.gmail.danielreturnweb.magicapi.spells.spell.ChargedSpell;
import com.gmail.danielreturnweb.magicapi.spells.spell.SpellCommand;
import com.gmail.danielreturnweb.magicapi.spells.spell.SpellItem;

/**
 * TODO compleat this example effect
 * @author daniel
 * @deprecated Effect not complete and does not work
 *
 */
public class Disk_Example implements Listener, SpellItem, ChargedSpell, SpellCommand
{
	
	public static final int diskSize = 3;
	
	double damage = 6;
	
	int diskRotationSpeed = 30;
	
	public Disk_Example() 
	{
		
	}

	@Override
	public int getSpellXPCost() 
	{
		return 16;
	}

	@Override
	public int getSpellCooldown() 
	{
		return 30;
	}
	
	@Override
	public SpellCooldownType getSpellCooldownType() 
	{
		return SpellCooldownType.PLAYER;
	}

	@Override
	public String getSpellID() 
	{
		return "Disk_Example";
	}

	@Override
	public String getSpellName() 
	{
		return "Disk Example";
	}

	@Override
	public Action[] getCastActions() 
	{
		return new Action[]{Action.RIGHT_CLICK_AIR, Action.RIGHT_CLICK_BLOCK};
	}

	
	@Override
	public CastHand getCastHand() 
	{
		return CastHand.Both;
	}

	@Override
	public int getSpellCharges() 
	{
		return 4;
	}

	@Override
	public boolean itemDestoryedOnLastChargeCast() 
	{
		return true;
	}
	
	@Override
	public String[] getLoreDescription()
	{
		return new String[]
				{
				"Creates 3 disks that circle",
				"the caster. Upon colliding ",
				"with a entity, the disk",
				"dissapates dealing " + damage + " Magic",
				"Damage.",
				"Not effected by magic boost",
				
				"Targets Hostile/Neutral"
				};
	}
	
	@Override
	public boolean onSpellCast(SpellCastData data,  SpellTargeting targeting) 
	{
		if(data == null) throw new NullPointerException("[MagicAPI] Disk Example SpellCastData is null");
		if(data.getCaster() == null) throw new NullPointerException("[MagicAPI] Disk Example Caster[LivingEntity] is null");
		
		CustomEffectBundle bundle = MagicAPI.getInstance().getCustomEffectAPI().createCustomEffectBundle(new DiskEffect_Example());
		bundle.setDataDouble("damage", this.damage);
		bundle.setDataInt("rotation_speed", this.diskRotationSpeed);
		
		bundle.setDataInt("disk1", 0);
		bundle.setDataInt("disk2", 0);
		bundle.setDataInt("disk3", 0);
		
		MagicAPI.getInstance().getCustomEffectAPI().placeEffect(data.getCaster(), new DiskEffect_Example(),bundle); //TODO make effect
		return true;
		
	}
	
	
	
	

	
	@Override
	public String getPermissionToCast() 
	{
		return null;
	}

	@Override
	public String getAlias() 
	{
		return "diske";
	}

	@Override
	public String[] getGrimoireEntry()
	{
		return new String[]
				{
				"Creates 3 disks that circle the caster. Upon colliding ",
				"with a entity, the disk dissapates dealing " + damage + " Magic",
				"Damage. Not effected by magic boost",
				"Targets Hostile/Neutral"
				};
	}
}
