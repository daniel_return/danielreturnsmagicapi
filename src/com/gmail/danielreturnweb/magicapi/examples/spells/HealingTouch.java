package com.gmail.danielreturnweb.magicapi.examples.spells;

import org.bukkit.Color;
import org.bukkit.Particle;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;

import com.gmail.danielreturnweb.magicapi.EntityStanceView.EntityStance;
import com.gmail.danielreturnweb.magicapi.MagicAPI;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffectAPI;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffectBundle;
import com.gmail.danielreturnweb.magicapi.examples.effects.HealingTouchEffect_Example;
import com.gmail.danielreturnweb.magicapi.spell.SpellCastData;
import com.gmail.danielreturnweb.magicapi.spell.SpellCooldownType;
import com.gmail.danielreturnweb.magicapi.spell.SpellTargeting;
import com.gmail.danielreturnweb.magicapi.spells.spell.ChargedSpell;
import com.gmail.danielreturnweb.magicapi.spells.spell.SpellItem;
import com.gmail.danielreturnweb.magicapi.utils.DoubleTruncator;

/**
 * Heals a single target and places a natural life regeneration upon them <br>
 * If cast on self healing is halved and regeneration's duration is halved.
 * @author daniel
 *
 */
public class HealingTouch implements SpellItem, ChargedSpell, Listener
{
	double heal = 8;
	double healBoost = 1/12;
	
	public HealingTouch()
	{}

	@Override
	public int getSpellXPCost()
	{
		return 10;
	}

	@Override
	public int getSpellCooldown()
	{
		return 16;
	}

	@Override
	public SpellCooldownType getSpellCooldownType()
	{
		return SpellCooldownType.PLAYER;
	}

	@Override
	public String getSpellID()
	{
		return "HealingTouch_Example";
	}

	@Override
	public String getSpellName()
	{
		return "Healing Touch";
	}
	
	@Override
	public boolean onSpellCast(SpellCastData data, SpellTargeting targeting)
	{			
		HealingTouchEffect_Example hte = new HealingTouchEffect_Example();
		CustomEffectAPI ceapi = MagicAPI.getInstance().getCustomEffectAPI();
		
		//Self Cast
		if(data.isPlayer() && data.isSneaking())
		{			
			CustomEffectBundle bundle = ceapi.createCustomEffectBundle(hte);
			bundle.setRemainingDuration(bundle.getRemainingDuration()/2); //Half the duration
			bundle.setBoost(data.getMagicBoost());
			
			ceapi.placeEffect(data.getCaster(), data.getCaster(), hte,bundle);
			
			double amt = (heal + data.getMagicBoost()*healBoost)/2; //Half the heal
			
			this.applyHeal(data.getCaster(), amt);
			data.getCaster().getWorld().spawnParticle(Particle.REDSTONE, data.getCaster().getEyeLocation(), 6, new Particle.DustOptions(Color.RED,1.5f));
			return true;
		}
		
		
		//Target Cast
		targeting.setTargetStance(new EntityStance[]{EntityStance.FRIENDLY});
		targeting.setTargetType(null);
		
		LivingEntity target = targeting.target_Single_getEntityBeingLookedAt(4);
		if(target == null) return false;
		if(target.getUniqueId() == data.getCaster().getUniqueId())
		{
			System.out.println("Caster and target are the same");
			return false;
		}
		
		ceapi.placeEffect(target, data.getCaster(), hte);
		
		double amt = heal + data.getMagicBoost()*healBoost;
		this.applyHeal(target, amt);
		
		target.getWorld().spawnParticle(Particle.REDSTONE, target.getEyeLocation(), 4, new Particle.DustOptions(Color.RED,1f));
		return true;
	}
	
	/**
	 * Places the instant heal upon the entity
	 * @param entity
	 * @param heal
	 */
	private void applyHeal(LivingEntity entity, double heal)
	{
		double max = entity.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue();
		double current = entity.getHealth();
		double hp = heal + current;
		
		if(hp > max) hp = max;
		if(hp <= 0) hp = 1;
		
		if(!entity.isDead()) entity.setHealth(hp);
	}
	

	@Override
	public int getSpellCharges()
	{
		return 10;
	}

	@Override
	public boolean itemDestoryedOnLastChargeCast()
	{
		return true;
	}

	@Override
	public Action[] getCastActions()
	{
		return new Action[]{Action.RIGHT_CLICK_AIR};
	}

	@Override
	public CastHand getCastHand()
	{
		return CastHand.MainHand;
	}

	@Override
	public String[] getLoreDescription()
	{
		HealingTouchEffect_Example ef = new HealingTouchEffect_Example();
		
		double hp1 = DoubleTruncator.truncateDouble(heal, 2);
		double hp2 = DoubleTruncator.truncateDouble(heal/2, 2);
		int dur1 = ef.getEffectDuration()/20;
		int dur2 = ef.getEffectDuration()/2/20;
		
		//Note: 28 seems to be a good character cap per line
		return new String[]
				{
				"Heals a target and places",
				"a minor regeneration effect",
				"upon them. Targeting ",
				"yourself reduces heal and",
				"regen duration by half",
				"Right click entity to",
				"target.",
				"Shift right click to",
				"cast upon self",
				"Heal: " + hp1 + " Self: " + hp2,
				"Dur: " + dur1 + " Self: " + dur2
				};
	}
	
	/*
	 * 			"Heals a target your",
				"looking at for " + hp1 + " health",
				"and places a natural",
				"regeneration effect on them",
				"for " + dur1 + " seconds.",
				
				"Shift Cast to heal",
				"yourself for " + hp2 + " health",
				"and places a natural",
				"regeneration effect on you",
				"for " + dur2 + " seconds."
	 */

	@Override
	public String[] getGrimoireEntry()
	{
		HealingTouchEffect_Example ef = new HealingTouchEffect_Example();
		
		double hp1 = DoubleTruncator.truncateDouble(heal, 2);
		double hp2 = DoubleTruncator.truncateDouble(heal/2, 2);
		int dur1 = ef.getEffectDuration()/20;
		int dur2 = ef.getEffectDuration()/2/20;
		
		return new String[]{
		"Heals a target and places a minor regeneration effect",
		"upon them. Targeting yourself reduces heal and",
		"regen duration by half Right click entity to",
		"target.",
		"Shift right click to cast upon self",
		"Heal: " + hp1 + " Self: " + hp2,
		"Dur: " + dur1 + " Self: " + dur2
		};
	}
	
	
	


}
