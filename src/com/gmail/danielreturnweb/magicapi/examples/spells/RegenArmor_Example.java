package com.gmail.danielreturnweb.magicapi.examples.spells;

import org.bukkit.event.block.Action;

import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffect;
import com.gmail.danielreturnweb.magicapi.examples.effects.RegenArmorEffect_Example;
import com.gmail.danielreturnweb.magicapi.spell.SpellCastData;
import com.gmail.danielreturnweb.magicapi.spell.SpellCooldownType;
import com.gmail.danielreturnweb.magicapi.spell.SpellTargeting;
import com.gmail.danielreturnweb.magicapi.spells.spell.PassiveSpell;
import com.gmail.danielreturnweb.magicapi.spells.spell.SpellItem;

/**
 * A passive spell that provides 1.5 Health regeneration per second when out side of battle.
 * Upon attacking or taking entity damage, the regen falls to 0.
 * It will recover by 0.1 every second until it reaches its normal regen value. <p>
 * 
 * This is meant to be bound to armor 
 * @author daniel
 *
 */
public class RegenArmor_Example implements SpellItem, PassiveSpell
{

	RegenArmorEffect_Example raee;
	
	public RegenArmor_Example()
	{
		raee = new RegenArmorEffect_Example();
	}

	@Override
	public int getSpellXPCost()
	{
		return 0;
	}

	@Override
	public int getSpellCooldown()
	{
		return 0;
	}

	@Override
	public SpellCooldownType getSpellCooldownType()
	{
		return SpellCooldownType.SPELL;
	}

	@Override
	public String getSpellID()
	{
		return "RegenArmor_Example";
	}

	@Override
	public String getSpellName()
	{
		return "Regen Armor";
	}

	@Override
	public boolean onSpellCast(SpellCastData data, SpellTargeting targeting)
	{
		return false;
	}

	@Override
	public Action[] getCastActions()
	{
		return new Action[]{};
	}

	@Override
	public CastHand getCastHand()
	{
		return CastHand.Both;
	}

	@Override
	public String[] getLoreDescription()
	{
		return new String[]
				{ //~26 chars
				"Passivly provides " + raee.regen + " regen",
				"per second. Upon entering",
				"combat the regen is",
				"reduced to " + raee.regenMin + ". Regen will",
				"recover by" + raee.recover + " per second",				
				};
	}

	@Override
	public CustomEffect getPassiveAbility()
	{
		return new RegenArmorEffect_Example();
	}

	@Override
	public PassiveSlot[] getPassiveSlots()
	{
		return new PassiveSlot[]{PassiveSlot.ArmorSlot};
	}

	@Override
	public String[] getGrimoireEntry()
	{
		// TODO Auto-generated method stub
		return new String[]{
				"Passivly provides " + raee.regen + " health regeneration",
				"per second. UPon entering combat the regeneration is",
				"reduced to " + raee.regenMin + " per second.",
				"The regeneration will recover by " + raee.recover + " per second after battle exit"
		};
	}

}
