package com.gmail.danielreturnweb.magicapi.examples.spells;

import java.util.List;

import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Listener;

import com.gmail.danielreturnweb.magicapi.MagicAPI;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffectAPI;
import com.gmail.danielreturnweb.magicapi.examples.effects.AcidEffect_Example;
import com.gmail.danielreturnweb.magicapi.spell.SpellCastData;
import com.gmail.danielreturnweb.magicapi.spell.SpellCooldownType;
import com.gmail.danielreturnweb.magicapi.spell.SpellTargeting;
import com.gmail.danielreturnweb.magicapi.spells.spell.SpellCommand;

/**
 * A Cone breath attack that deals minor damage to targets within the cone
 * and places AcidEffect_example onto the target
 * @author daniel
 *
 */
public class BreathOfAcid_Example implements SpellCommand, Listener
{

	public BreathOfAcid_Example()
	{
		
	}

	@Override
	public int getSpellXPCost()
	{
		return 12;
	}

	@Override
	public int getSpellCooldown()
	{
		return 24;
	}

	@Override
	public SpellCooldownType getSpellCooldownType()
	{
		return SpellCooldownType.PLAYER;
	}

	@Override
	public String getSpellID()
	{
		return "BreathOfAcid_Example";
	}

	@Override
	public String getSpellName()
	{
		return "Breath of Acid";
	}
	
	@Override
	public boolean onSpellCast(SpellCastData data, SpellTargeting targeting)
	{
		double distance = 7;
		double degree = 80;
		
		
		LivingEntity source = data.getCaster();
		Location loc = source.getLocation();
		
		SpellTargeting st = new SpellTargeting(source);
		
		List<LivingEntity> ents =  st.target_AOE_getWedge(distance,degree);		
		
		AcidEffect_Example ae = new AcidEffect_Example();
		CustomEffectAPI ceapi = MagicAPI.getInstance().getCustomEffectAPI();
		
		for(LivingEntity e : ents)
		{
			//Place Effect
			ceapi.placeEffect(e, ae);
//			entityCloud(e);
		}
		
		//Do Cosmetic - Acid Cone
		doCosmetic(source,distance-0.5,degree);
		
		return true;
	}
	
	private void doCosmetic(LivingEntity e, double distance, double degree)
	{
		for(int i = 0; i < degree; i+=2)
		{
			double deg = i - (degree/2);
			deg += getVariation();
			cosmeticLine(e,distance,deg);
		}
	}
	
	/**
	 * Makes a single breath line
	 * @param e
	 * @param distance
	 * @param degree
	 */
	private void cosmeticLine(LivingEntity e, double distance, double degree)
	{
		int amount = 3;
		float size = 0.8f;
		SpellTargeting targ = new SpellTargeting(e);
		
		for(double i = 0; i < distance; i+=0.4)
		{
			Location loc = targ.target_Location_InDirectionFacing(e, i+getVariation(), degree);
			loc = loc.subtract(0,1+getVariation(),0);
			loc.getWorld().spawnParticle(Particle.REDSTONE, loc, amount, new Particle.DustOptions(Color.LIME,size));
		}
	}
	
	public double getVariation()
	{
		double variation = Math.random()/2.4;
		if(Math.random() <= .50) variation = 0 - variation;
		return variation;
	}

	@Override
	public String getPermissionToCast()
	{
		return null;
	}

	@Override
	public String getAlias()
	{
		return "boa";
	}

	@Override
	public String[] getGrimoireEntry()
	{
		return new String[]{
				"Launches a cone of acid in the direction the",
				"caster is facing. This deals damage over time",
				"and reduces effected targets armor and toughness."
		};
	}

}
