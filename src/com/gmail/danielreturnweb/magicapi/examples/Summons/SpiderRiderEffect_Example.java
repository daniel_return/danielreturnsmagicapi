package com.gmail.danielreturnweb.magicapi.examples.Summons;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.util.Vector;

import com.gmail.danielreturnweb.magicapi.MagicAPI;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffect;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffectBundle;
import com.gmail.danielreturnweb.magicapi.effect.api.Tickable;
import com.gmail.danielreturnweb.magicapi.effects.types.BuffType;
import com.gmail.danielreturnweb.magicapi.effects.types.EffectRemovalType;
import com.gmail.danielreturnweb.magicapi.effects.types.SelfStackType;
import com.gmail.danielreturnweb.magicapi.spell.SpellTargeting;

public class SpiderRiderEffect_Example implements Listener, CustomEffect, Tickable
{
	
	public final Material[] nonClimableBlocks = new Material[]{
			Material.AIR, Material.WATER, Material.LAVA, Material.BARRIER, 
			Material.SUGAR_CANE, Material.CACTUS, Material.BAMBOO, Material.BAMBOO_SAPLING, Material.VINE,
			Material.CHORUS_FLOWER, Material.CHORUS_PLANT
			
			};

	public SpiderRiderEffect_Example()
	{
	}

	@Override
	public String getEffectID()
	{
		return "SpiderRiderEffect";
	}

	@Override
	public boolean isDisplayed()
	{
		return false;
	}

	@Override
	public int getEffectDuration()
	{
		return 20*60;
	}

	@Override
	public boolean persistsAfterDeath()
	{
		return false;
	}

	@Override
	public BuffType getBuffType()
	{
		return BuffType.Positive;
	}

	@Override
	public int getPurgeResistence()
	{
		return 0;
	}

	@Override
	public SelfStackType getSelfStackType()
	{
		return SelfStackType.REPLACES;
	}

	@Override
	public void onInflict(LivingEntity effectHolder, LivingEntity inflicter, CustomEffectBundle bundle)
	{
		
	}

	@Override
	public void onRemoval(LivingEntity effectHolder, CustomEffectBundle bundle,
			EffectRemovalType type)
	{

	}

	@Override
	public void onJoin(Player effectHolder, CustomEffectBundle bundle,
			PlayerJoinEvent event)
	{

	}

	@Override
	public void onQuit(Player effectHolder, CustomEffectBundle bundle,
			PlayerQuitEvent event)
	{

	}

	@Override
	public void onTick(LivingEntity effectHolder, CustomEffectBundle bundle)
	{
		int tickMax = 3;
		if(!bundle.hasDataInt("tick"))
		{
			bundle.setDataInt("tick", tickMax);
		}
		int tick = bundle.getDataInt("tick");
		tick--;
		
		if(tick > 0)
		{
			bundle.setDataInt("tick", tick);
			return;
		}
		bundle.setDataInt("tick", tickMax);
		
		bundle.setRemainingDuration(this.getEffectDuration());
		
		SpellTargeting st = new SpellTargeting(effectHolder);
		
		Location loc = st.target_Location_InDirectionFacing(effectHolder, 1, 0);
//		loc = loc.subtract(0,1,0); //Adjust from eye height to feet height, Note this make the spider more "Jumpy" and shaky. 
		//May just remove it as it will still work without this adjustment, but 1 block high "walls" will sometimes not 
		//move over
		
		LivingEntity spider = (LivingEntity) Bukkit.getEntity(bundle.getDataUUID("Spider"));
		if(spider == null)
		{
			bundle.setEnded(true);
			return;
		}
		moveEntity(spider,this.getAdjustedLocation(loc));
		
		//If player not passenger, remove effect?
		List<Entity> pass = spider.getPassengers();
		boolean riding = false;
		for(Entity ent : pass)
		{
			if(ent.getUniqueId().equals(effectHolder.getUniqueId()))
			{
				riding = true;
				break;
			}
		}
		if(riding == false)
		{
			bundle.setEnded(true);
		}
	}
	
	/**
	 * If the provided location is a "wall" the spider will try and move up.
	 * If the provided location is air, then the spider will try to move down.
	 */
	protected Location getAdjustedLocation(Location loc)
	{
		if(loc.subtract(0,1,0).getBlock().getType() == Material.AIR)
		{
			return loc.subtract(0,2,0);
		}
		else
		{
			return climb(loc);
		}
//		else if(loc.add(0,1,0).getBlock().getType() != Material.AIR)
//		{
//			return loc.add(0,2,0);
//		}
//		return loc;
	}
	
	protected Location climb(Location loc)
	{		
		int height = 250-loc.getBlockY();
		if(height < 0) height = 0;
		
		Location climb = loc.subtract(0,1,0);
		for(int i = 0; i < height ; i++)
		{
//			if(climb.getBlock().getType() == Material.AIR)
			for(Material mat : nonClimableBlocks)
			{
				if(climb.getBlock().getType() == mat)
				{
//					climb = climb.add(0,1,0);
					return climb;
				}
			}
			climb = loc.add(0,i,0);
		}
		return loc;
	}
	
	protected void moveEntity(LivingEntity entity, Location dest)
	{
		float pot = 300.0f;
		int potLev = 0;
		//TODO get speed on entity. multiple level by pot and sub that from the speed value
		
		float speed = -1.0f/(4000.0f - pot*potLev);
		
		Vector velocity = entity.getLocation().toVector().subtract(dest.toVector()).normalize();
		
		velocity.normalize();
//		velocity.setY(0);
		velocity.multiply(speed);
		
		entity.setVelocity(velocity.normalize());
	}

	@Override
	public String[] getOpusEntry()
	{
		// TODO Auto-generated method stub
		return null;
	}

	//The owning player has the effect, not the entity...
//	@EventHandler
//	public void onDamage(EntityDamageEvent event)
//	{
//		if(!(event.getEntity() instanceof LivingEntity)) return;
//		
//		if(event.getCause() == DamageCause.FALL)
//		{
//			System.out.println("Fall Damage SpiderRiderEffect_Example damage taken");
//			List<CustomEffectBundle> k = MagicAPI.getInstance().getCustomEffectAPI().getEffectData((LivingEntity) event.getEntity(), this.getEffectID());
//			if(k.size() > 0)
//			{
//				double damage = event.getDamage();
//				damage -= 6;
//				if(damage < 0 ) damage = 0;
//				event.setDamage(damage);
//				System.out.println("Adjusting SpiderRiderEffect_Example damage taken");
//			}
//		}
//	}
//	
}
