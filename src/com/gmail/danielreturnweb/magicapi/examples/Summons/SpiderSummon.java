package com.gmail.danielreturnweb.magicapi.examples.Summons;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Spider;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;

import com.gmail.danielreturnweb.magicapi.MagicAPI;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffectBundle;
import com.gmail.danielreturnweb.magicapi.summons.SummonedEntity;

public class SpiderSummon implements SummonedEntity, Listener
{
	
	public SpiderSummon()
	{
		
	}

	@Override
	public double summonPointCost()
	{
		return 1;
	}

	@Override
	public EntityType summonType()
	{
		return EntityType.SPIDER;
	}

	@Override
	public int summonedDuration()
	{
		return 60*5;
	}

	@Override
	public int summonLevelBonusDuration()
	{
		return 30;
	}

	@Override
	public int summonMaxLevel()
	{
		return 2;
	}

	@Override
	public void onEntitySummon(LivingEntity entity, int level)
	{
		Spider spider = (Spider) entity;
		spider.setCustomName("Summoned Spider");
		spider.setCustomNameVisible(true);
	}
	
	@EventHandler
	public void onAttack(EntityDamageByEntityEvent event)
	{
//		if(event.getDamager().getType() != EntityType.PLAYER) return;
//		if(event.getEntity().getType() != EntityType.SPIDER) return;
//		
//		if(event.getDamage() <= 5)
//		{
//			event.getDamager().sendMessage(ChatColor.YELLOW + "Calling all Spider Riders!");
//			event.setCancelled(true);
//			event.getEntity().addPassenger(event.getDamager());
//		}
	}
	
	@EventHandler
	public void doRide(PlayerInteractAtEntityEvent event)
	{
		if(event.getRightClicked() != null)
		{
			if(event.getRightClicked().getType() == EntityType.SPIDER)
			{
				event.getPlayer().sendMessage(ChatColor.YELLOW + "Calling all Spider Riders!");
				Spider spider = (Spider) event.getRightClicked();
				//TODO Check if spider is summoned and owned by right click player
				
				spider.addPassenger(event.getPlayer());
				
				CustomEffectBundle bundle = MagicAPI.getInstance().getCustomEffectAPI().createCustomEffectBundle(new SpiderRiderEffect_Example());
				
				bundle.setDataUUID("Spider", spider.getUniqueId());
				
				MagicAPI.getInstance().getCustomEffectAPI().placeEffect(event.getPlayer(), new SpiderRiderEffect_Example(), bundle);
			}
		}
	}
	
	//TODO see if a dismount event exists. If it does when spider dismounted remove the Spider Rider Effect
}
