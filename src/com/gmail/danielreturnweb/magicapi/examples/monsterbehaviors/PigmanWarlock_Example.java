package com.gmail.danielreturnweb.magicapi.examples.monsterbehaviors;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntitySpawnEvent;

import com.gmail.danielreturnweb.magicapi.MagicAPI;
import com.gmail.danielreturnweb.magicapi.examples.spells.FireBall_Example;
import com.gmail.danielreturnweb.magicapi.spell.monstercasting.MagicMonsterBehavior;

/**
 * This is an example of a <i> very </i> simple MagicMonsterBehavior, so simple in fact that
 * it even defines the PigmanWarlock entity (though all I do is set its name)
 * This Example makes 40% of zombie pigmen that spawn into a Pigman Warlock. <br>
 * They can cast Fire ball. 
 * <p>
 * @author daniel
 *
 */
public class PigmanWarlock_Example implements Listener, MagicMonsterBehavior
{
	private static PigmanWarlock_Example instance;
	
	
	protected FireBall_Example fireBall;

	
	/*
	 * Basic code to define pigman and capture spawning
	 */
	
	public PigmanWarlock_Example()
	{
		 fireBall = new FireBall_Example();
		 if(PigmanWarlock_Example.instance == null) PigmanWarlock_Example.instance = this;
	}
	
	@EventHandler
	public void onPigmanSpawn(EntitySpawnEvent event)
	{
		if(event.getEntityType() != EntityType.PIG_ZOMBIE) return;
		if(Math.random() > 0.40) return;
		
		event.getEntity().setCustomName("Warlock Example");
		
		//Register the pigman with the magic monster manager so it updates.
		MagicAPI.getInstance().getMagicMonsterManager().registerLivingEntity((LivingEntity) event.getEntity(),PigmanWarlock_Example.instance);
	}
	
	/*
	 * Behavior Example
	 */
	
	
	@Override
	public String getBehaviorID()
	{
		return "PigmanWarlockBehavior_Example";
	}

	@Override
	public void doBehavor(LivingEntity entity)
	{		
		//Could do timing logic here
		//Could store the cooldown
		//Other AI/Behavior logic.
		//However in this example I just spam the fire ball every update.
		//But the Warlock only does an update every 10 updates as per the reduceUpdates() method
		//which reduces how often PigmenWarlocks are updated

		//Will have a better example in the future with a better spell casting behavior 
		
		//Magic Boost test
		MagicAPI.getInstance().getMagicBoostManager().addMagicBoost("test", entity, 20*10);
		
		MagicAPI.getInstance().getSpellManager().castSpellFromEntity(entity, fireBall);
	}

	@Override
	public int reducedUpdates()
	{
		return 10;
	}

	@Override
	public double activeDistance()
	{
		return 16;
	}

	@Override
	public boolean persistentWithChunks()
	{
		return false;
	}
}
