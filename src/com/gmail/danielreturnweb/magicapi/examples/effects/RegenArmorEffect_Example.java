package com.gmail.danielreturnweb.magicapi.examples.effects;

import java.util.List;

import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.gmail.danielreturnweb.magicapi.MagicAPI;
import com.gmail.danielreturnweb.magicapi.RegenerationManager;
import com.gmail.danielreturnweb.magicapi.RegenerationManager.RegenerationType;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffect;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffectAPI;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffectBundle;
import com.gmail.danielreturnweb.magicapi.effect.api.Updatable;
import com.gmail.danielreturnweb.magicapi.effects.types.BuffType;
import com.gmail.danielreturnweb.magicapi.effects.types.EffectRemovalType;
import com.gmail.danielreturnweb.magicapi.effects.types.SelfStackType;

/**
 * The effect of RegenArmor_Example spell. 
 * <p> 
 * Provides 1.5 Health regeneration per second when out side of battle.
 * Upon attacking or taking entity damage, the regen falls to 0.
 * It will recover by 0.05 every second until it reaches its normal regen value.
 * <br> Takes 30 seconds to get the full 1.5 regeneration back. 
 * <p>
 * @author daniel
 *
 */
public class RegenArmorEffect_Example implements CustomEffect, Updatable, Listener
{
	public final double regen = 1.5;
	public final double regenMin = 0;
	public final double recover = 0.05;
	
	private final String keyBN = "regen";
	private final String keyRM = "RegenArmor_Example";
	
	public RegenArmorEffect_Example(){}

	@Override
	public String getEffectID()
	{
		return "RegenArmorEffect_Example";
	}

	@Override
	public boolean isDisplayed()
	{
		return false;
	}

	@Override
	public int getEffectDuration()
	{
		return 20*5;
	}

	@Override
	public boolean persistsAfterDeath()
	{
		return true;
	}

	@Override
	public BuffType getBuffType()
	{
		return BuffType.Positive;
	}

	@Override
	public int getPurgeResistence()
	{
		return -1;
	}

	@Override
	public SelfStackType getSelfStackType()
	{
		return SelfStackType.REFRESH_DURATION;
	}

	@Override
	public void onUpdate(LivingEntity effectHolder, CustomEffectBundle bundle)
	{
		if(!bundle.hasDataDouble(keyBN)) bundle.setDataDouble(keyBN, this.regen);
		
		RegenerationManager rm = MagicAPI.getInstance().getRegenerationManager();
		
		if(!rm.hasRegenerationByLabel(effectHolder.getUniqueId(), keyRM))
		{
			rm.addRegeneration(effectHolder.getUniqueId(), keyRM, this.regen, RegenerationType.MAGIC_REGENERATION);
			return;
		}
		
		double reg = bundle.getDataDouble(keyBN); 
//		System.out.println("Regen " + (reg < this.regen) );
		if(reg < this.regen)
		{
			rm.removeRegeneration(effectHolder.getUniqueId(), keyRM);
			reg += this.recover;
			if(reg > this.regen) reg = this.regen;
			bundle.setDataDouble(keyBN,reg);
			
			rm.addRegeneration(effectHolder.getUniqueId(), keyRM, reg, RegenerationType.MAGIC_REGENERATION);
		}
		
//		System.out.println("has test " + bundle.hasDataInt("test"));
	}

	@Override
	public void onInflict(LivingEntity effectHolder, LivingEntity inflicter, CustomEffectBundle bundle)
	{
		
	}

	@Override
	public void onRemoval(LivingEntity effectHolder, CustomEffectBundle bundle, EffectRemovalType type)
	{
		if(type != EffectRemovalType.Stack)
		{
			RegenerationManager rm = MagicAPI.getInstance().getRegenerationManager();
			rm.removeRegeneration(effectHolder.getUniqueId(), keyRM);
		}	
	}

	@Override
	public void onJoin(Player effectHolder, CustomEffectBundle bundle, PlayerJoinEvent event)
	{}

	@Override
	public void onQuit(Player effectHolder, CustomEffectBundle bundle, PlayerQuitEvent event)
	{}
	
	@EventHandler
	public void onAttack(EntityDamageByEntityEvent event)
	{
		Entity e = event.getEntity();
		Entity ee = event.getDamager();
		
		CustomEffectAPI ceapi = MagicAPI.getInstance().getCustomEffectAPI();
		
		if(e instanceof LivingEntity)
		{
			List<CustomEffectBundle> b = ceapi.getEffectData((LivingEntity)e, this);
//			System.out.println("size " + b.size());
			if(b != null && b.size() > 0 )
			{
//				b.get(0).setDataDouble(keyBN, 0);
//				b.get(0).setDataInt("test", 4);
				this.newEff((LivingEntity)e);
			}
		}
		if(ee instanceof LivingEntity)
		{
			List<CustomEffectBundle> b = ceapi.getEffectData((LivingEntity)ee, this);
//			System.out.println("size2 " + b.size());
			if(b != null && b.size() > 0 )
			{
//				b.get(0).setDataDouble("keyBN", 0);
				this.newEff((LivingEntity)ee);
			}
		}
//		System.out.println("======");
	}
	
	private void newEff(LivingEntity entity)
	{
		CustomEffectAPI ceapi = MagicAPI.getInstance().getCustomEffectAPI();
		ceapi.removeEffect(entity, this);
		
		CustomEffectBundle bundle = ceapi.createCustomEffectBundle(this);
		bundle.setDataDouble(keyBN, 0);
		bundle.setDataInt("test", 4);
		
		ceapi.placeEffect(entity, this, bundle);
	}

	@Override
	public String[] getOpusEntry()
	{
		return new String[]
			{
			"Provides 1.5 Health Regeneration when outside of battle",
			"Upon entering battle the regeneration falls to 0.",
			"The regeneration will recover by 0.05 every second outside of battle",
			};
	}
}
