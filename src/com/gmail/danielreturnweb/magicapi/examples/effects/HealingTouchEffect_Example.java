package com.gmail.danielreturnweb.magicapi.examples.effects;

import org.bukkit.Bukkit;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.RegisteredServiceProvider;

import com.gmail.danielreturnweb.magicapi.MagicAPI;
import com.gmail.danielreturnweb.magicapi.RegenerationManager;
import com.gmail.danielreturnweb.magicapi.RegenerationManager.RegenerationType;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffect;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffectAPI;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffectBundle;
import com.gmail.danielreturnweb.magicapi.effect.api.Updatable;
import com.gmail.danielreturnweb.magicapi.effects.types.BuffType;
import com.gmail.danielreturnweb.magicapi.effects.types.EffectRemovalType;
import com.gmail.danielreturnweb.magicapi.effects.types.SelfStackType;

/**
 * A Example Effect
 * <p>
 * Provides a 0.5 natural regeneration per second<br>
 * Lasts for 30 seconds <br>
 * Healing Duration Stacks <p>
 * Each magic boost point grants 0.01 regeneration
 * 
 * @author daniel
 *
 */
public class HealingTouchEffect_Example implements CustomEffect, Updatable
{
//	static Map<UUID,>
	
	CustomEffectAPI ceapi = null;
	
	double regen = 0.5;
	double boostRegen = 0.01;
	
	public HealingTouchEffect_Example() 
	{
		RegisteredServiceProvider<CustomEffectAPI> rsp = Bukkit.getServicesManager().getRegistration(CustomEffectAPI.class);
		if(rsp != null)
		{
			ceapi = rsp.getProvider();
		}
	}

	@Override
	public String getEffectID() 
	{
		return "HealingTouchEffect_Example";
	}

	@Override
	public int getEffectDuration() 
	{
		return 20*30;
	}

	@Override
	public boolean persistsAfterDeath() 
	{
		return false;
	}

	@Override
	public boolean isDisplayed()
	{
		return true;
	}
	
	public BuffType getBuffType()
	{
		return BuffType.Positive;
	}
	
	public int getPurgeResistence()
	{
		return 1;
	}

	public SelfStackType getSelfStackType()
	{
		return SelfStackType.COMBIND_DURATION;
	}
	
	
	@Override
	public void onUpdate(LivingEntity effectHolder, CustomEffectBundle bundle)
	{}

	@Override
	public void onInflict(LivingEntity entity, LivingEntity inflicter, CustomEffectBundle bundle) 
	{
		double boost = bundle.getBoost();
		
		//Add Regeneration
		RegenerationManager regen = MagicAPI.getInstance().getRegenerationManager();
		regen.addRegeneration(entity.getUniqueId(), "HealingTouchEffect", (this.regen + boost*this.boostRegen), RegenerationType.NATURAL_REGENERATION);
	}

	@Override
	public void onRemoval(LivingEntity effectHolder, CustomEffectBundle bundle, EffectRemovalType type) 
	{
		RegenerationManager regen = MagicAPI.getInstance().getRegenerationManager();
		regen.removeRegeneration(effectHolder.getUniqueId(), "HealingTouchEffect");
	}

	@Override
	public void onJoin(Player effectHolder, CustomEffectBundle bundle,
			PlayerJoinEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onQuit(Player effectHolder, CustomEffectBundle bundle,
			PlayerQuitEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String[] getOpusEntry()
	{
		return new String[]
				{
				"Provides a natural regeneration per second to the effected",
				"Lasts for 30 seconds.",
				"Multiple casts on the same target will stack the duration"
				};
	}
}
