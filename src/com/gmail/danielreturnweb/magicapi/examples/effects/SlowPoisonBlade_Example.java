package com.gmail.danielreturnweb.magicapi.examples.effects;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.RegisteredServiceProvider;

import com.gmail.danielreturnweb.magicapi.MagicAPI;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffect;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffectAPI;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffectBundle;
import com.gmail.danielreturnweb.magicapi.effect.api.Updatable;
import com.gmail.danielreturnweb.magicapi.effects.types.BuffType;
import com.gmail.danielreturnweb.magicapi.effects.types.EffectRemovalType;
import com.gmail.danielreturnweb.magicapi.effects.types.SelfStackType;

/**
 * A Example Effect
 * <p>
 * The entity with this ability will place SlowPoison onto any entity they attack.
 * Lasts for 120s
 * 
 * @author daniel
 *
 */
public class SlowPoisonBlade_Example implements CustomEffect, Updatable, Listener
{	
	CustomEffectAPI ceapi = null;
	
	
	public SlowPoisonBlade_Example() 
	{
		RegisteredServiceProvider<CustomEffectAPI> rsp = Bukkit.getServicesManager().getRegistration(CustomEffectAPI.class);
		if(rsp != null)
		{
			ceapi = rsp.getProvider();
		}
	}

	@Override
	public String getEffectID() 
	{
		return "SlowPoisonBlade_Example";
	}

	@Override
	public int getEffectDuration() 
	{
		return 20*120;
	}

	@Override
	public boolean persistsAfterDeath() 
	{
		return true;
	}

	@Override
	public boolean isDisplayed()
	{
		return true;
	}
	
	public BuffType getBuffType()
	{
		return BuffType.Positive;
	}
	
	public int getPurgeResistence()
	{
		return 1;
	}

	public SelfStackType getSelfStackType()
	{
		return SelfStackType.REPLACES;
	}
	
	
	@Override
	public void onUpdate(LivingEntity effectHolder, CustomEffectBundle bundle)
	{
		
	}

	@Override
	public void onInflict(LivingEntity entity, LivingEntity inflicter, CustomEffectBundle bundle) 
	{
		
	}

	@Override
	public void onRemoval(LivingEntity effectHolder, CustomEffectBundle bundle, EffectRemovalType type) 
	{

	}
	
	@EventHandler
	public void onAttack(EntityDamageByEntityEvent e)
	{
		if(!(e.getEntity() instanceof LivingEntity)) return;
		if(!(e.getDamager() instanceof LivingEntity)) return;
		
		LivingEntity inflictor = (LivingEntity) e.getDamager();
		LivingEntity target = (LivingEntity) e.getEntity();
					
		//See if entity has this effect.
		List<CustomEffectBundle> b = ceapi.getEffectData(inflictor, this);
		if(b == null ||b.size() <= 0) return;
		
		CustomEffectBundle bundle = MagicAPI.getInstance().getCustomEffectAPI().createCustomEffectBundle(new SlowPoison_Example());
		if(inflictor instanceof Player)	bundle.setBoost(MagicAPI.getInstance().getMagicBoostManager().getMagicBoost((Player)inflictor));
		ceapi.placeEffect(target, inflictor, new SlowPoison_Example(),bundle);
	}

	@Override
	public void onJoin(Player effectHolder, CustomEffectBundle bundle,
			PlayerJoinEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onQuit(Player effectHolder, CustomEffectBundle bundle,
			PlayerQuitEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String[] getOpusEntry()
	{
		return new String[]
				{
				"Places the Slow Poison effect onto any entity the effected attacks",
				};
	}
}
