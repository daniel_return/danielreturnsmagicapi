package com.gmail.danielreturnweb.magicapi.examples.effects;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.entity.AreaEffectCloud;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.AreaEffectCloudApplyEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.plugin.RegisteredServiceProvider;

import com.gmail.danielreturnweb.magicapi.EntityStanceView;
import com.gmail.danielreturnweb.magicapi.EntityStanceView.EntityStance;
import com.gmail.danielreturnweb.magicapi.MagicAPI;
import com.gmail.danielreturnweb.magicapi.DamageSystem.DamageManager;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffect;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffectAPI;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffectBundle;
import com.gmail.danielreturnweb.magicapi.effect.api.Updatable;
import com.gmail.danielreturnweb.magicapi.effects.types.BuffType;
import com.gmail.danielreturnweb.magicapi.effects.types.EffectRemovalType;
import com.gmail.danielreturnweb.magicapi.effects.types.SelfStackType;
import com.gmail.danielreturnweb.magicapi.examples.spells.Disk_Example;
import com.gmail.danielreturnweb.magicapi.spell.SpellTargeting;

/**
 * Controls 3 disks around the player. Upon colliding with an entity, the disk
 * deal damage to it and dissipate.
 * 
 * @author daniel
 * @deprecated not compleat, buggy
 */
public class DiskEffect_Example implements CustomEffect, Updatable, Listener, Runnable
{
//	static Map<UUID,>
	
	CustomEffectAPI ceapi = null;
	
	protected static Map<UUID,DiskTrack> tracking; 
	private static boolean runCreated = false;
	private static final int updateSpeed = 5;
	
	
	public DiskEffect_Example() 
	{
		RegisteredServiceProvider<CustomEffectAPI> rsp = Bukkit.getServicesManager().getRegistration(CustomEffectAPI.class);
		if(rsp != null)
		{
			ceapi = rsp.getProvider();
		}
		
		if(DiskEffect_Example.tracking == null)  DiskEffect_Example.tracking = new HashMap<UUID,DiskTrack>();
		if(DiskEffect_Example.runCreated == false) Bukkit.getScheduler().scheduleSyncRepeatingTask(MagicAPI.getInstance(), 
				this, DiskEffect_Example.updateSpeed , DiskEffect_Example.updateSpeed);
	}

	@Override
	public String getEffectID() 
	{
		return "DiskEffect_Example";
	}

	@Override
	public int getEffectDuration() 
	{
		return 20*30;
	}

	@Override
	public boolean persistsAfterDeath() 
	{
		return false;
	}

	@Override
	public boolean isDisplayed()
	{
		return true;
	}
	
	public BuffType getBuffType()
	{
		return BuffType.Positive;
	}
	
	public int getPurgeResistence()
	{
		return 2;
	}

	public SelfStackType getSelfStackType()
	{
		return SelfStackType.REPLACES;
	}
	
	
	@Override
	public void onUpdate(LivingEntity effectHolder, CustomEffectBundle bundle)
	{
		
	}

	@Override
	public void onInflict(LivingEntity entity, LivingEntity inflicter, CustomEffectBundle bundle) 
	{
		AreaEffectCloud c1 = spawnDisk(entity,bundle);
		AreaEffectCloud c2 = spawnDisk(entity,bundle);
		AreaEffectCloud c3 = spawnDisk(entity,bundle);
		
		DiskTrack track = new DiskTrack(entity,c1,c2,c3,bundle.getDataInt("rotation_speed"),bundle.getDataDouble("damage"));
		track.update();
		DiskEffect_Example.tracking.put(entity.getUniqueId(), track);
	}

	@Override
	public void onRemoval(LivingEntity effectHolder, CustomEffectBundle bundle, EffectRemovalType type) 
	{
		
	}

	@Override
	public void onJoin(Player effectHolder, CustomEffectBundle bundle,
			PlayerJoinEvent event) {
		
	}

	@Override
	public void onQuit(Player effectHolder, CustomEffectBundle bundle,
			PlayerQuitEvent event) {
		
	}
	
	private AreaEffectCloud spawnDisk(LivingEntity entity, CustomEffectBundle bundle)
	{
		AreaEffectCloud aec1 = (AreaEffectCloud) entity.getWorld().spawnEntity(entity.getLocation(), EntityType.AREA_EFFECT_CLOUD);
		
		
		aec1.setColor(Color.BLUE);
		aec1.setRadius(Disk_Example.diskSize);
		aec1.setDuration(this.getEffectDuration()-1);
		aec1.setCustomName(this.getEffectID() + "_Disk");
//		aec1.setSource()
		aec1.setReapplicationDelay(10);
		
		aec1.setMetadata("Owner", new FixedMetadataValue(MagicAPI.getInstance(),entity.getUniqueId().toString()));
		
		aec1.setDurationOnUse(0);
		aec1.setRadiusOnUse(0);
		return aec1;
	}
	
	
	
	@EventHandler
	public void onEffect(AreaEffectCloudApplyEvent event)
	{		
//		System.out.println("DiskEffect_Example cloud event");
		AreaEffectCloud aec = event.getEntity();
		
		//See if its the this cloud
		if(aec.getCustomName() == null || !aec.getCustomName().equals(this.getEffectID() + "_Disk")) return;
		System.out.println("DiskEffect_Example Cloud is Disk");
		
		event.setCancelled(true);
		DiskTrack track = null;
		
		if(aec.hasMetadata("Owner"))
		{
			UUID own = UUID.fromString(aec.getMetadata("Owner").get(0).toString());
			if(!DiskEffect_Example.tracking.containsKey(own))
			{
				System.err.println("Error - DiskEffect_Example AreaEffectCloud does not have a owner in the tracking list");
				System.err.println("Effected UUID: " + own.toString());
				return;
			}	
			track = DiskEffect_Example.tracking.get(own);
		}
		
		for(LivingEntity target : event.getAffectedEntities())
		{
			if(!target.getUniqueId().equals(track.owner))
			{
				EntityStanceView esv = MagicAPI.getInstance().getEntityStanceView();
				if(esv.getStance(track.owner, target) == EntityStance.HOSTILE || esv.getStance(track.owner, target) == EntityStance.NEUTRAL)
				{
					//Do damage
					DamageManager dMan = MagicAPI.getInstance().getDamageManager();
					dMan.dealDamage(dMan.TYPE_MAGIC, target, track.damage,false);
					
//					MagicAPI.getInstance().getDamageManager().dealDamage(DamageType.Magic, target, track.damage);
					System.out.println("DiskEffect_Example Dealing Damage");
					
					//Update tracker
					removeCloudFromTracker(event.getEntity());
					event.getEntity().remove();					
					
					return;
				}
			}
		}
	}
	
	
	public void removeCloudFromTracker(AreaEffectCloud cloud)
	{
		for(Entry<UUID,DiskTrack> tracks : DiskEffect_Example.tracking.entrySet())
		{
			DiskTrack track = tracks.getValue();
			
			//Disk 1
			if(track.disk1 != null && track.disk1.getUniqueId().equals(cloud.getUniqueId()))
			{
				track.disk1 = null;
				return;
			}
			if(track.disk2 != null && track.disk2.getUniqueId().equals(cloud.getUniqueId()))
			{
				track.disk2 = null;
				return;
			}
			if(track.disk2 != null && track.disk2.getUniqueId().equals(cloud.getUniqueId()))
			{
				track.disk2 = null;
				return;
			}
		}
	}
	
	
	
	@Override
	public void run()
	{
		for(Entry<UUID,DiskTrack> tracks : DiskEffect_Example.tracking.entrySet())
		{
			DiskTrack track = tracks.getValue();
			
			if(track != null) track.update();
			
			if(track.empty()) DiskEffect_Example.tracking.remove(track.owner.getUniqueId());
		}
	}
	
	
	
	
	
	
	class DiskTrack
	{
		LivingEntity owner;
		AreaEffectCloud disk1;
		AreaEffectCloud disk2;
		AreaEffectCloud disk3;
		int rotationSpeed = 30;
		double distance = 4.5;
		
		double damage = 0;
		
		int rot1 = 0;
		int rot2 = 120;
		int rot3 = 240;
		
		public DiskTrack(LivingEntity owner, 
				AreaEffectCloud disk1, AreaEffectCloud disk2, AreaEffectCloud disk3,
				int rotationSpeed,  double damage)
		{
			this.owner = owner;
			this.disk1 = disk1;
			this.disk2 = disk2;
			this.disk3 = disk3;
			this.rotationSpeed = rotationSpeed;
			this.damage = damage;
		}

		protected void update()
		{			
			SpellTargeting targ = new SpellTargeting(owner);		
			
			if(disk1 != null)
			{
				rot1 = validateRotate(rot1 + rotationSpeed);
				disk1.teleport(targ.target_Location_AroundEntity(distance, rot1));
			}
			if(disk2 != null)
			{
				rot2 = validateRotate(rot2 + rotationSpeed);
				disk2.teleport(targ.target_Location_AroundEntity(distance, rot2));
			}
			if(disk3 != null)
			{
				rot3 = validateRotate(rot3 + rotationSpeed);
				disk3.teleport(targ.target_Location_AroundEntity(distance, rot3));
			}
		}
		
		protected boolean empty()
		{
			return (disk1 == null) && (disk2 == null) && (disk3 == null);
		}
		
		
		private int validateRotate(int rot)
		{
			if(rot <0) return 0;
			if(rot > 360)
			{
				return rot-360;
			}
			return rot;
		}		
	}






	@Override
	public String[] getOpusEntry()
	{
		// TODO Auto-generated method stub
		return null;
	}	
}
