package com.gmail.danielreturnweb.magicapi.examples.effects;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Particle;
import org.bukkit.entity.AreaEffectCloud;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.RegisteredServiceProvider;

import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffect;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffectAPI;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffectBundle;
import com.gmail.danielreturnweb.magicapi.effect.api.Updatable;
import com.gmail.danielreturnweb.magicapi.effects.types.BuffType;
import com.gmail.danielreturnweb.magicapi.effects.types.EffectRemovalType;
import com.gmail.danielreturnweb.magicapi.effects.types.SelfStackType;

/**
 * A Example Effect
 * <p>
 * Prevents the player from moving, attacking, eating/drinking, and interacting for a short duration.
 * duration is increased with magic boost.
 * 
 * Cannot be purged [resistance 12]
 * 
 * @author daniel
 *
 */
public class Stun_OLD implements CustomEffect, Updatable, Listener
{
//	static Map<UUID,>
	
	CustomEffectAPI ceapi = null;
	int duration = 20*3;
	int durationPerBoost = 1; //in ticks
	
	
	
	public Stun_OLD() 
	{
		RegisteredServiceProvider<CustomEffectAPI> rsp = Bukkit.getServicesManager().getRegistration(CustomEffectAPI.class);
		if(rsp != null)
		{
			ceapi = rsp.getProvider();
		}
	}

	@Override
	public String getEffectID() 
	{
		return "Stun_E";
	}

	@Override
	public int getEffectDuration() 
	{
		return duration;
	}

	@Override
	public boolean persistsAfterDeath() 
	{
		return false;
	}

	@Override
	public boolean isDisplayed()
	{
		return true;
	}
	
	public BuffType getBuffType()
	{
		return BuffType.Positive;
	}
	
	public int getPurgeResistence()
	{
		return 12;
	}

	public SelfStackType getSelfStackType()
	{
		return SelfStackType.KEEP_EXISTING;
	}
	
	
	@Override
	public void onUpdate(LivingEntity effectHolder, CustomEffectBundle bundle)
	{
		
	}

	@Override
	public void onInflict(LivingEntity entity, LivingEntity inflicter, CustomEffectBundle bundle) 
	{
		entity.sendMessage(ChatColor.GRAY + "[Stunned]");
		
		AreaEffectCloud aec = (AreaEffectCloud) entity.getWorld().spawnEntity(entity.getEyeLocation(), EntityType.AREA_EFFECT_CLOUD);
		aec.setDuration(this.getEffectDuration()/2);
		aec.setParticle(Particle.CLOUD);
		aec.setColor(Color.GRAY);
		aec.setDurationOnUse(0);
		aec.setSource(entity);
		aec.setRadius(1);
		aec.setRadiusOnUse(0f);
		aec.setRadiusPerTick(0);
		
	}

	@Override
	public void onRemoval(LivingEntity effectHolder, CustomEffectBundle bundle, EffectRemovalType type) 
	{
		
	}
	
	
	@EventHandler
	public void onAttack(EntityDamageByEntityEvent e)
	{
		if(!(e.getDamager() instanceof LivingEntity)) return;
		
		LivingEntity entity = (LivingEntity) e.getDamager();
					
		//See if entity has this effect.
		List<CustomEffectBundle> b = ceapi.getEffectData(entity, this);
		if(b == null ||b.size() <= 0) return;
		
		e.setCancelled(true);
	}
	
	@EventHandler
	public void onMove(PlayerMoveEvent e)
	{
		Player player = e.getPlayer();
					
		//See if entity has this effect.
		List<CustomEffectBundle> b = ceapi.getEffectData(player, this);
		if(b == null ||b.size() <= 0) return;
		
		e.setCancelled(true);
	}
	
	@EventHandler
	public void onConsume(PlayerItemConsumeEvent e)
	{
		Player player = e.getPlayer();
					
		//See if entity has this effect.
		List<CustomEffectBundle> b = ceapi.getEffectData(player, this);
		if(b == null ||b.size() <= 0) return;
		
		e.setCancelled(true);
	}
	
	@EventHandler
	public void onInteraction(PlayerInteractEvent e)
	{
		Player player = e.getPlayer();
					
		//See if entity has this effect.
		List<CustomEffectBundle> b = ceapi.getEffectData(player, this);
		if(b == null ||b.size() <= 0) return;
		
		e.setCancelled(true);
	}

	@Override
	public void onJoin(Player effectHolder, CustomEffectBundle bundle,
			PlayerJoinEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onQuit(Player effectHolder, CustomEffectBundle bundle,
			PlayerQuitEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String[] getOpusEntry()
	{
		return new String[]
				{
				"Prevents the effected from taking any actions",
				};
	}
	
}
