package com.gmail.danielreturnweb.magicapi.examples.effects;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Particle;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.AreaEffectCloud;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffect;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffectAPI;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffectBundle;
import com.gmail.danielreturnweb.magicapi.effect.api.Updatable;
import com.gmail.danielreturnweb.magicapi.effects.types.BuffType;
import com.gmail.danielreturnweb.magicapi.effects.types.EffectRemovalType;
import com.gmail.danielreturnweb.magicapi.effects.types.SelfStackType;

/**
 * A Example Effect
 * <p>
 * Upon death the entity reincarnate's, preventing there death.
 * <p>
 * The entity is healed for 25% of its max health. If it is a player their food reset to full.
 * <br> The entity will be purged of all custom and vanilla effects and gain weakness 2 for 20s along with Regeneration 3 for 10 seconds.
 * 
 * reincarnate lasts for 10m.
 * 
 * @author daniel
 *
 */
public class Reincarnation_Example implements CustomEffect, Updatable, Listener
{
//	static Map<UUID,>
	
	CustomEffectAPI ceapi = null;
	double healthSet = 0.25;
	
	int purgeStrength = 3;
	
	int weaknessDuration = 20*20; //tick*seconds
	int weaknessLevel = 1;
	
	int regenDuration = 20*12;
	int regenLevel = 2;
	
	
	
	public Reincarnation_Example() 
	{
		RegisteredServiceProvider<CustomEffectAPI> rsp = Bukkit.getServicesManager().getRegistration(CustomEffectAPI.class);
		if(rsp != null)
		{
			ceapi = rsp.getProvider();
		}
	}

	@Override
	public String getEffectID() 
	{
		return "Reincarnation_Example";
	}

	@Override
	public int getEffectDuration() 
	{
		return 20*60*10;
	}

	@Override
	public boolean persistsAfterDeath() 
	{
		return false;
	}

	@Override
	public boolean isDisplayed()
	{
		return true;
	}
	
	public BuffType getBuffType()
	{
		return BuffType.Positive;
	}
	
	public int getPurgeResistence()
	{
		return 3;
	}

	public SelfStackType getSelfStackType()
	{
		return SelfStackType.REPLACES;
	}
	
	
	@Override
	public void onUpdate(LivingEntity effectHolder, CustomEffectBundle bundle)
	{
		
	}

	@Override
	public void onInflict(LivingEntity entity, LivingEntity inflicter, CustomEffectBundle bundle) 
	{

	}

	@Override
	public void onRemoval(LivingEntity effectHolder, CustomEffectBundle bundle, EffectRemovalType type) 
	{
		
	}
	
//	@EventHandler
//	public void onDeath(EntityResurrectEvent e)
//	{
//		e.setCancelled(false);
//		LivingEntity entity = e.getEntity();
//		
//		entity.setHealth(entity.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue()*healthSet);
//		entity.sendMessage(ChatColor.MAGIC + "" + ChatColor.RED + "ABC" + ChatColor.RESET + "" + ChatColor.GOLD + 
//				"[Reincarnation]" + 
//				ChatColor.MAGIC + "" + ChatColor.RED + "ABC"
//				);
//		
//		List<CustomEffectBundle> b = ceapi.getEffectData(entity, this);
//		if(b.size() > 0)b.get(0).setEnded(true);
//		
//		entity.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS,weaknessDuration, 0));
//		entity.playEffect(EntityEffect.FIREWORK_EXPLODE);
//	}
	
	
	/*
	 * This will trigger before totem of undying's, but does not seem to always be the case.
	 * 	Is it just small amounts of damage that do not trigger it?
	 */
	@EventHandler //TODO give this a priority to be done last so my damage reduction effects do not cause it to trigger when HP is left
	public void onDeath(EntityDamageEvent e)
	{
		if(!(e.getEntity() instanceof LivingEntity)) return;
		
		LivingEntity entity = (LivingEntity) e.getEntity();
		
		if(entity.getHealth() - e.getFinalDamage() >= 0) return;
		
		//See if entity has this effect.
		List<CustomEffectBundle> b = ceapi.getEffectData(entity, this);
		if(b == null ||b.size() <= 0) return;
		
		//Set the effect to be ended.
		b.get(0).setEnded(true);
		
		
		e.setCancelled(true);
		entity.setHealth(entity.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue()*healthSet);
		entity.setFireTicks(0);
		if(entity.getType() == EntityType.PLAYER)
		{
			Player p = (Player)entity;
			
			p.setFoodLevel(20);
			p.setSaturation(5);
		}
				
		
		
		//Purge all effects (Vanilla and Custom)
		ceapi.removeAllEffectsByBuffType(entity, BuffType.Positive, EffectRemovalType.Purge,purgeStrength);
		ceapi.removeAllEffectsByBuffType(entity, BuffType.Negative, EffectRemovalType.Purge,purgeStrength);
		
		
		entity.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS,weaknessDuration, weaknessLevel));
		entity.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION,regenDuration, regenLevel));
		
		
		for(PotionEffectType t : PotionEffectType.values())
		{
			entity.removePotionEffect(t);
		}
		
		
//		entity.playEffect(EntityEffect.TOTEM_RESURRECT);
		
		int dur = 20*2;
		float shrink = -1f/10f;
		int size = 3;
		
		
		AreaEffectCloud aec = (AreaEffectCloud) entity.getWorld().spawnEntity(entity.getLocation(), EntityType.AREA_EFFECT_CLOUD);
		aec.setDuration(dur);
		aec.setParticle(Particle.HEART);
		aec.setDurationOnUse(0);
		aec.setSource(entity);
		aec.setRadius(size);
		aec.setRadiusOnUse(0f);
		aec.setRadiusPerTick((float)shrink);
		
		AreaEffectCloud aec2 = (AreaEffectCloud) entity.getWorld().spawnEntity(entity.getLocation().add(0, 0.5, 0), EntityType.AREA_EFFECT_CLOUD);
		aec2.setDuration(dur);
		aec2.setParticle(Particle.REDSTONE, new Particle.DustOptions(Color.RED,1f));
		aec2.setDurationOnUse(0);
		aec2.setSource(entity);
		aec2.setRadius(size);
		aec2.setRadiusOnUse(0f);
		aec2.setRadiusPerTick((float)shrink);
		
	
		
		
		entity.sendMessage(ChatColor.RED + "" + ChatColor.MAGIC + "ABC" + ChatColor.RESET + "" + ChatColor.GOLD + 
				"[Reincarnation]" + 
				ChatColor.RED + "" + ChatColor.MAGIC + "ABC"
				);

	}

	@Override
	public void onJoin(Player effectHolder, CustomEffectBundle bundle,
			PlayerJoinEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onQuit(Player effectHolder, CustomEffectBundle bundle,
			PlayerQuitEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String[] getOpusEntry()
	{
		return new String[]
				{
				"Upon taking faital damage the user is healed for 25% of their maximum Health",
				"Players will have there hunger set to full.",
				"The user will be purged of all effects but gain weakness II for 20s along with",
				"Regeneration III for 10s"
				};
	}
}
