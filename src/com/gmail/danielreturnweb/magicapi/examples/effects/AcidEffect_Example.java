package com.gmail.danielreturnweb.magicapi.examples.effects;

import java.util.Random;

import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeInstance;
import org.bukkit.attribute.AttributeModifier;
import org.bukkit.attribute.AttributeModifier.Operation;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.gmail.danielreturnweb.magicapi.MagicAPI;
import com.gmail.danielreturnweb.magicapi.DamageSystem.CustomDamageType;
import com.gmail.danielreturnweb.magicapi.DamageSystem.DamageManager;
import com.gmail.danielreturnweb.magicapi.DamageSystem.DamageTypes.PoisonDamageType;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffect;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffectBundle;
import com.gmail.danielreturnweb.magicapi.effect.api.Updatable;
import com.gmail.danielreturnweb.magicapi.effects.types.BuffType;
import com.gmail.danielreturnweb.magicapi.effects.types.EffectRemovalType;
import com.gmail.danielreturnweb.magicapi.effects.types.SelfStackType;

/**
 * A acid that weakens a targets armor and deals minor magic damage over time.
 * <p> Armor Reduction: 2 + 1*Level
 * <br> Toughness Reduction: 1+ 1*Level
 * <br> Damage is increased with magic boost
 * <p> Lasts for 12 seconds
 * @author daniel
 *
 */
public class AcidEffect_Example implements CustomEffect, Updatable 
{
	Random rand;
	int armorReductionBase = 2;
	int armorReductionLevel = 1;
	
	int toughReductionBase = 1;
	int toughReductionLevel = 1;

	
	
	//Magic Boost
	double magicBoostBonusDamage = 1/25; //1 damage for every 25 magic boost

	public AcidEffect_Example()
	{
		rand = new Random();
	}

	@Override
	public BuffType getBuffType() 
	{
		return BuffType.Negative;
	}

	@Override
	public int getEffectDuration() 
	{
		return 20*12;
	}

	@Override
	public String getEffectID() 
	{
		return "Acid";
	}

	@Override
	public int getPurgeResistence() 
	{
		return 1;
	}

	@Override
	public SelfStackType getSelfStackType() 
	{
		return SelfStackType.REPLACES;
	}

	@Override
	public boolean isDisplayed() 
	{
		return true;
	}
	
	
	@Override
	public boolean persistsAfterDeath() {return false;}

	
	
	@Override
	public void onInflict(LivingEntity effected, LivingEntity inflictor, CustomEffectBundle bundle) 
	{
		int level = bundle.getLevel();
		int boost = bundle.getBoost();
		
		bundle.setDataInt("atk", 0);
		
		int armorR = 0-this.armorReductionBase+this.armorReductionLevel*level;
		
		int toughR = 0-this.toughReductionBase+this.toughReductionLevel*level;
		
		AttributeInstance armor = effected.getAttribute(Attribute.GENERIC_ARMOR);
		armor.addModifier(new AttributeModifier(this.getEffectID()+"armor", armorR,Operation.ADD_NUMBER));
		
		AttributeInstance tough = effected.getAttribute(Attribute.GENERIC_ARMOR_TOUGHNESS);
		tough.addModifier(new AttributeModifier(this.getEffectID()+"tough", toughR,Operation.ADD_NUMBER));
		
	}

	@Override
	public void onRemoval(LivingEntity effected, CustomEffectBundle inflictor, EffectRemovalType type) 
	{
		AttributeInstance armorI = effected.getAttribute(Attribute.GENERIC_ARMOR);
		for(AttributeModifier mod : armorI.getModifiers())
		{
			if(mod.getName().equals(this.getEffectID()+"armor")) effected.getAttribute(Attribute.GENERIC_ARMOR).removeModifier(mod);
		}
		
		AttributeInstance toughI = effected.getAttribute(Attribute.GENERIC_ARMOR_TOUGHNESS);
		for(AttributeModifier mod : toughI.getModifiers())
		{
			if(mod.getName().equals(this.getEffectID()+"tough")) effected.getAttribute(Attribute.GENERIC_ARMOR_TOUGHNESS).removeModifier(mod);
		}
	}

	@Override
	public void onUpdate(LivingEntity entity, CustomEffectBundle bundle) 
	{
		int level = bundle.getLevel();
		int boost = bundle.getBoost();
		
//		MagicAPI.getInstance().getDamageManager().dealDamage(DamageType.Magic, entity, this.getDamage(level, boost));
		
		//Do damage
		DamageManager dMan = MagicAPI.getInstance().getDamageManager();
		dMan.dealDamage(dMan.TYPE_MAGIC, entity, this.getDamage(level, boost),false);
		
	}

	
	@Override
	public void onJoin(Player effectHolder, CustomEffectBundle bundle, PlayerJoinEvent arg2)
	{
		/*
		 *Call the onInflict method to reapply the effect.
		 *Note, this is not the ideal way of doing this and should be avoided, but im being lazy. 
		 */
		this.onInflict(effectHolder, null, bundle);
	}

	@Override
	public void onQuit(Player effectHolder, CustomEffectBundle bundle, PlayerQuitEvent arg2)
	{
		/*
		 * Call onRemoval to remove the permanent effects from the player, but do not call the
		 * it with a type of End as we do not want to end the effect.
		 * Note, this is not the ideal way of doing this and should be avoided, but im being lazy.
		 */
		this.onRemoval(effectHolder, bundle, EffectRemovalType.Unknown);
	}
	
	
	/**
	 * Gets a random base damage value between 0.4 to 0.6 damage
	 * <p>
	 * <b>Formula:</br> [rand(3+level*2)+2]/10
	 * @return Damage for the entity to take
	 */
	private double getDamage(int level, int boost)
	{
		double n = rand.nextInt(3 + level*2)+2;
		double boostDamage = (double)(this.magicBoostBonusDamage*boost);
		if(boostDamage > 3.5) boostDamage = 3.5;
		n += boostDamage;
		
		n = n/10.0;
//		System.out.println("AcidEffect Damage: " + n);
		return n;
		/*
		 * 3 = 0 to 2
		 * level*2 = 2 [Assuming lv of 1]
		 * +2
		 * Min: (0 to 2) + 2 + 2 = 4
		 * Max: (0 to 2) + 2 + 2 = 6
		 * 
		 * TO DEC
		 * 0.4 to 0.6
		 */
	}

	@Override
	public String[] getOpusEntry()
	{
		return new String[]
				{
				"Acid that weakens the effecteds armor while dealing magic damage over time",
				"This reduces armor and Toughness"
				};
	}
}
