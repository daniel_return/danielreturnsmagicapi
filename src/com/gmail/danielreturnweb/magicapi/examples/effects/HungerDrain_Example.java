package com.gmail.danielreturnweb.magicapi.examples.effects;

import org.bukkit.Bukkit;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.RegisteredServiceProvider;

import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffect;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffectAPI;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffectBundle;
import com.gmail.danielreturnweb.magicapi.effect.api.Updatable;
import com.gmail.danielreturnweb.magicapi.effects.types.BuffType;
import com.gmail.danielreturnweb.magicapi.effects.types.EffectRemovalType;
import com.gmail.danielreturnweb.magicapi.effects.types.SelfStackType;

/**
 * A Example Effect
 * <p>
 * Places an effect onto the target that drains hunger over time.
 * Duration will stack up to 25 seconds.
 * 
 * @author daniel
 *
 */
public class HungerDrain_Example implements CustomEffect, Updatable
{
//	static Map<UUID,>
	
	CustomEffectAPI ceapi = null;
	int applyTime = 3; //How often to apply the food drain. (3 = every third update)
	int drainHunger = 1;
	float drainSaturation = 1.5f;
	int maxDuration = 20*25; //ticks * seconds
	
	
	public HungerDrain_Example() 
	{
		RegisteredServiceProvider<CustomEffectAPI> rsp = Bukkit.getServicesManager().getRegistration(CustomEffectAPI.class);
		if(rsp != null)
		{
			ceapi = rsp.getProvider();
		}
	}

	@Override
	public String getEffectID() 
	{
		return "HungerDrain_Example";
	}

	@Override
	public int getEffectDuration() 
	{
		return 20*6;
	}

	@Override
	public boolean persistsAfterDeath() 
	{
		return true;
	}

	@Override
	public boolean isDisplayed()
	{
		return true;
	}
	
	public BuffType getBuffType()
	{
		return BuffType.Positive;
	}
	
	public int getPurgeResistence()
	{
		return 1;
	}

	public SelfStackType getSelfStackType()
	{
		return SelfStackType.COMBIND_DURATION;
	}
	
	
	@Override
	public void onUpdate(LivingEntity effectHolder, CustomEffectBundle bundle)
	{
		//If effect holder is not a player, end and return;
		if(!(effectHolder instanceof Player))
		{
			System.out.println("Not player update");
			bundle.setEnded(true);
			return;
		}
		
		//Get cooldown and see if effect can be done
		int k = bundle.getDataInt("cool");
		k--;
		if(k <= 0)
		{
			bundle.setDataInt("cool", this.applyTime);
			
			Player p = (Player)effectHolder;
			float sat = p.getSaturation();
			//If player has saturation, reduce hunger.
			if(sat > 0)
			{
//				System.out.println("sat " + sat);
				sat = sat - this.drainSaturation;
				if(sat < 0) sat = 0;
				p.setSaturation(sat);
			}
			//If player has no saturation, reduce hunger
			else
			{
				int hunger = p.getFoodLevel();
				hunger = hunger - drainHunger;
				if(hunger < 0) hunger = 0;
				p.setFoodLevel(hunger);
			}
			
		}
		else bundle.setDataInt("cool", k);
	}

	@Override
	public void onInflict(LivingEntity entity, LivingEntity inflicter, CustomEffectBundle bundle) 
	{
		System.out.println("Hunger");
		if(!(entity instanceof Player))
		{
			System.out.println("Not player");
			bundle.setEnded(true);
			return;
		}
				
		
		if(!bundle.hasDataInt("Cool")) bundle.setDataInt("cool", this.applyTime);
		
		if(bundle.getRemainingDuration() > this.maxDuration)
		{
			bundle.setRemainingDuration(this.maxDuration);
		}
	}

	@Override
	public void onRemoval(LivingEntity effectHolder, CustomEffectBundle bundle, EffectRemovalType type) 
	{

	}

	@Override
	public void onJoin(Player effectHolder, CustomEffectBundle bundle,
			PlayerJoinEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onQuit(Player effectHolder, CustomEffectBundle bundle,
			PlayerQuitEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String[] getOpusEntry()
	{
		return new String[]
				{
				"Drains hunger over time. ",
				"Duration will stack up to 25 seconds."
				};
	}
}
