package com.gmail.danielreturnweb.magicapi.examples.effects;

import org.bukkit.Bukkit;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeInstance;
import org.bukkit.attribute.AttributeModifier;
import org.bukkit.attribute.AttributeModifier.Operation;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.RegisteredServiceProvider;

import com.gmail.danielreturnweb.magicapi.MagicAPI;
import com.gmail.danielreturnweb.magicapi.DamageSystem.CustomDamageType;
import com.gmail.danielreturnweb.magicapi.DamageSystem.DamageManager;
import com.gmail.danielreturnweb.magicapi.DamageSystem.DamageTypes.PoisonDamageType;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffect;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffectAPI;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffectBundle;
import com.gmail.danielreturnweb.magicapi.effect.api.Updatable;
import com.gmail.danielreturnweb.magicapi.effects.types.BuffType;
import com.gmail.danielreturnweb.magicapi.effects.types.EffectRemovalType;
import com.gmail.danielreturnweb.magicapi.effects.types.SelfStackType;
import com.gmail.danielreturnweb.magicapi.utils.DoubleTruncator;

/**
 * A Example Effect
 * <p>
 * Places a poison on the target that deals 0.4*Level pure damage per second.
 * It also slows there movement by (4+[level/4])*level percent, a exponential slow.
 * LV1: 4.25% slow, LV5:26.25%
 * <br> Max Level is 5.
 * <br> Duration: 8 seconds
 * <br> Deals 0.01 more damage per point of Magic Boost
 * <br> Stack on the target increasing its level.
 * 
 * @author daniel
 *
 */
public class SlowPoison_Example implements CustomEffect, Updatable
{
//	static Map<UUID,>
	
	private CustomEffectAPI ceapi = null;
	private double baseSlow = 0.04; //As percent
	private double dps = 0.5; //Per level
	
	
	CustomDamageType damageType = new PoisonDamageType();
	
	
	public SlowPoison_Example() 
	{
		RegisteredServiceProvider<CustomEffectAPI> rsp = Bukkit.getServicesManager().getRegistration(CustomEffectAPI.class);
		if(rsp != null)
		{
			ceapi = rsp.getProvider();
		}
	}

	@Override
	public String getEffectID() 
	{
		return "SlowPoison_Example";
	}

	@Override
	public int getEffectDuration() 
	{
		return 20*8;
	}

	@Override
	public boolean persistsAfterDeath() 
	{
		return false;
	}

	@Override
	public boolean isDisplayed()
	{
		return true;
	}
	
	public BuffType getBuffType()
	{
		return BuffType.Negative;
	}
	
	public int getPurgeResistence()
	{
		return 1;
	}

	public SelfStackType getSelfStackType()
	{
		return SelfStackType.COMBIND_LEVEL;
	}
	
	private double getMoveSlow(int level)
	{
		double mod = ((baseSlow*100)+(level/3))*level; //Math done as whole numbers, not decimal. 
		mod = mod/100; //convert into percentage
		mod = DoubleTruncator.truncateDouble(mod, 3);
		
		return -mod;
	}
	
	
	@Override
	public void onUpdate(LivingEntity effectHolder, CustomEffectBundle bundle)
	{
		if(bundle.getLevel() > 5) bundle.setLevel(5);
//		effectHolder.damage(dot*bundle.getLevel());
		
		double damage = dps*bundle.getLevel();
		damage += 0.01*bundle.getBoost();
		
		
		//Do damage
		DamageManager dMan = MagicAPI.getInstance().getDamageManager();
		dMan.dealDamage(damageType, effectHolder, damage,false);
		
//		MagicAPI.getInstance().getDamageManager().dealDamage(DamageType.Pure, effectHolder, damage);
	}

	@Override
	public void onInflict(LivingEntity entity, LivingEntity inflicter, CustomEffectBundle bundle) 
	{
		int level = bundle.getLevel();
		if(level > 5) level = 5;
				
		AttributeInstance move = entity.getAttribute(Attribute.GENERIC_MOVEMENT_SPEED);
		move.addModifier(new AttributeModifier(this.getEffectID(),getMoveSlow(level),Operation.ADD_SCALAR));
	}

	@Override
	public void onRemoval(LivingEntity effectHolder, CustomEffectBundle bundle, EffectRemovalType type) 
	{
		if(bundle.getLevel() > 5) bundle.setLevel(5);
		
		//Remove the speed debuff.
		AttributeInstance ai = effectHolder.getAttribute(Attribute.GENERIC_MOVEMENT_SPEED);
		for(AttributeModifier mod : ai.getModifiers())
		{
			if(mod.getName().equals(this.getEffectID())) effectHolder.getAttribute(Attribute.GENERIC_MOVEMENT_SPEED).removeModifier(mod);
		}
	}

	@Override
	public void onJoin(Player effectHolder, CustomEffectBundle bundle, PlayerJoinEvent event) 
	{
		/*
		 *Call the onInflict method to reapply the effect.
		 *Note, this is not the ideal way of doing this and should be avoided, but im being lazy. 
		 */
		this.onInflict(effectHolder, null, bundle);
	}

	@Override
	public void onQuit(Player effectHolder, CustomEffectBundle bundle, PlayerQuitEvent event) 
	{
		/*
		 * Call onRemoval to remove the permanent effects from the player, but do not call the
		 * it with a type of End as we do not want to end the effect.
		 * Note, this is not the ideal way of doing this and should be avoided, but im being lazy.
		 */
		this.onRemoval(effectHolder, bundle, EffectRemovalType.Unknown);
	}

	@Override
	public String[] getOpusEntry()
	{
		return new String[]
				{
				"Slows the effected and deals damage over time",
				"Slow and damage increase with level",
				"Max Level is 5. Duration is 8 seconds",
				"Is effected by magic boost",
				};
	}
}
