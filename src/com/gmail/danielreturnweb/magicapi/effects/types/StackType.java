package com.gmail.danielreturnweb.magicapi.effects.types;

/**
 * <b>May replace this with a array of effect/effect id's that the effect conflicts with </b>
 * <p> A enum to see what type a effect is and if it is able to stack. <br>
 * Stackable Types will allways stack with others in there StackType. <br>
 * If a type is not stackable the first found in the effect list will be done, while others are ignored.
 * However I may have it be the strongest that is updated/goes off. <br>
 * 
 * <p> Effects of one type will allways stack with other types.
 * 
 * @author daniel
 *
 */
public enum StackType
{
	//Enum's are grouped by related types
	Unknown("Unknown", true),
	
	//Health
	Regeneration_Stack("Regeneration",true),
	Degeneration_Stack("Regeneration",true),
	LifeBoost_Stack("Life Boost",true),
	LifeReduction_Stack("Life Reduction",true),
	
	//Damage Dealt
	DamageDealtBoost_Stack("DamageBoost",true),
	DamageDealtReduction_Stack("Unknown",true),

	//Defense
	DefenseBoost_Stack("Unknown",true), 
	DefenseReduction_Stack("Unknown",true),
	
	//Attack Speed
	AttackSpeed_Boost("Attack Speed Boost", true),
	AttackSpeed_Reduction("Attack Speed Reduction", true),
	
	//Movement Speed
	MovementSpeed_Boost("Movement Speed Boost", true),
	MovementSpeed_Reduction("Movement Speed Reduction", true);
	
	
	
	boolean stacks = false;
	String name = "";
	
	StackType(String name, boolean stacks)
	{
		this.stacks = stacks;
	}
	
	public boolean canStack()
	{
		return this.stacks;
	}
	
	public String getName()
	{
		return this.name;
	}
}
