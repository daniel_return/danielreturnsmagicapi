package com.gmail.danielreturnweb.magicapi.effects.types;

/**
 * Determines how an effect stacks with its self.
 * <br> Does a effect combined with an existing instance of itself, only partially combine?
 * <br> Does it just replace the existing effect?
 * <br> the extra data from the EffectBundle will be copied over into the new effect bundle.
 * <p> TODO decide how onInflict and onRemove interact with SelfStackType.
 * I think when a effect stacks in ANY way, first its onRemoval() method must be called and then
 * its onInflict() method with the new stats. This will allow for old effects to be cleared, else
 * when the stacked effect finaly ends, it will leave lingering stat changes, and other things
 * on the entity.
 * @author daniel
 *
 */
public enum SelfStackType 
{
	/** Combines the existing effects duration & level with the one being applied.(addition)*/
	COMBIND_FULL,
	
	/** Combines the existing effects level with the one being applied.(addition)
	 * Duration is set to effect being applied. */
	COMBIND_LEVEL,
	
	/** Combines the existing effects duration with the one being applied.(addition)
	 * But its level is set to the one being applied*/
	COMBIND_DURATION,
	
	/**
	 * Refreshes the duration of the effect.
	 */
	REFRESH_DURATION,
	
	/** The effect being applied overrides the existing effect*/
	REPLACES,
	
	/** Applies the inflicted effect onto the entity without modifying any existing ones. 
	 * <br>NOTE: The scoreboard will only ever show once instance of an effect.*/
	SEPERATE,
	
	/**
	 * The effect being applied, is not applied and the existing instance on the entity stays on.
	 */
	KEEP_EXISTING;
	
	///** */
}
