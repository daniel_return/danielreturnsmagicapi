package com.gmail.danielreturnweb.magicapi.effects.types;

public enum EffectRemovalType 
{
	/** The removal type is unknown */
	Unknown,
	
	/** Called when the system is removing effects. 
	 * When this is called all effects should clean up any modifiers and other changes they have done to the entity.
	 * They should not spawn new effects or activate any on removal effects. Doing so could cause issues, inconsistencies,
	 * and introduce instability.
	 */
	System,
	
	/** The effect has ended */
	End,
	
	/** The effect was purged */
	Purge,
	
	/** The effect was dispelled*/
	Dispell,
	
	/** The effect was removed by milk - purge strength 1*/
	Milk,
	
	/** The effect was removed because the entity has immunity to that purge resistance level
	 * or immunity to that effect*/
	Immunity,
	
	/**
	 * This effect is being removed due to it stacking.
	 */
	Stack,
	
	/**
	 * When the entity dies and the effect's removedUponPlayerDeath() return's true;
	 */
	Death,
	
	/**
	 * The effect is a "passive spell" and was removed due the passive spell ending.
	 * @deprecated Passive spells will end when there associated effect duration runs out.
	 */
	PassiveRemove;
}
