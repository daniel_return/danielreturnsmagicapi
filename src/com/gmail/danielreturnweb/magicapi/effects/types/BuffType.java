package com.gmail.danielreturnweb.magicapi.effects.types;

/**
 * Dictates the intention of the Effect. Is it a positive effect or a negative effect, or a mix of both.
 * @author daniel
 *
 */
public enum BuffType 
{
	/** Represents a effect that has both Positive and Negative effects associated with it */
	Mix,
	/** Represents a effect that is positive for the entity its on */
	Positive,
	/** Represents a effect that is negative for the entity its on */
	Negative;
}
