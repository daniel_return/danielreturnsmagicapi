package com.gmail.danielreturnweb.magicapi;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

/**
 * This class is a manager for placing and removing a entities magic boost
 * 
 * 
 * <br> Its up to the effect to add and remove its magic boost when appropriate.
 * 
 * <p> Note: The player and LivingEntities have separate tracking lists.
 * This is to aid in speeding up searching, as searching for player magic boost
 * will take less time then if players and other living entities were in the same list
 * 
 * 
 * @author daniel
 *
 */
public class MagicBoostManager 
{
	public static MagicBoostManager instance;
	
	public static MagicBoostManager getInstance() {return MagicBoostManager.instance;}
	
	Map<UUID,Map<String,Integer>> entityTracker;
	Map<UUID,Map<String,Integer>> playerTracker;
	
	public MagicBoostManager() 
	{
		entityTracker = new HashMap<UUID,Map<String,Integer>>();
		playerTracker = new HashMap<UUID,Map<String,Integer>>();
		if(MagicBoostManager.instance == null) MagicBoostManager.instance = this;
	}
	
	
	/**
	 * Places the magic boost onto the player. The key allows finding and indexing of the magic boost.
	 * @param key the name of this boost
	 * @param player the player being effected
	 * @param magicBoost the value of magic boost to add
	 */
	public void addMagicBoost(String key, Player player, int magicBoost)
	{
		//If tracker does not exist, put one in.
		if(!playerTracker.containsKey(player.getUniqueId()))
		{
			Map<String,Integer> effectMap = new HashMap<String,Integer>();
			effectMap.put(key, magicBoost);
			playerTracker.put(player.getUniqueId(), effectMap);
			return;
		}
		//If it exists...
		Map<String,Integer> effectMap = playerTracker.get(player.getUniqueId());
		if(effectMap == null) //and null create a new one then
		{
			effectMap = new HashMap<String,Integer>();
		}
		//put the magic boost onto the player.
		effectMap.put(key, magicBoost);
		playerTracker.put(player.getUniqueId(), effectMap);
	}
	
	
	
	/**
	 * Places the magic boost onto the entity. The key allows finding and indexing of the magic boost.
	 * @param key the name of this boost
	 * @param entity the entity being effected
	 * @param magicBoost the value of magic boost to add
	 */
	public void addMagicBoost(String key, LivingEntity entity, int magicBoost)
	{
		//If tracker does not exist, put one in.
		if(!entityTracker.containsKey(entity.getUniqueId()))
		{
			Map<String,Integer> effectMap = new HashMap<String,Integer>();
			effectMap.put(key, magicBoost);
			entityTracker.put(entity.getUniqueId(), effectMap);
			return;
		}
		//If it exists...
		Map<String,Integer> effectMap = entityTracker.get(entity.getUniqueId());
		if(effectMap == null) //and null create a new one then
		{
			effectMap = new HashMap<String,Integer>();
		}
		//put the magic boost onto the entity.
		effectMap.put(key, magicBoost);
		entityTracker.put(entity.getUniqueId(), effectMap);
	}
	
	
	
	/**
	 * Removes a magic boost from the player by its key.
	 * @param key
	 * @param player
	 */
	public void removeMagicBoost(String key, Player player)
	{
		if(!playerTracker.containsKey(player.getUniqueId())) return;
		
		Map<String,Integer> effectMap = playerTracker.get(player.getUniqueId());
		if(effectMap == null)
		{
			effectMap = new HashMap<String,Integer>();
			return;
		}
		
		if(effectMap.containsKey(key))
		{
			effectMap.remove(key);
		}
	}
	
	
	
	
	public void removeMagicBoost(String key, LivingEntity entity)
	{
		if(!entityTracker.containsKey(entity.getUniqueId())) return;
		
		Map<String,Integer> effectMap = entityTracker.get(entity.getUniqueId());
		if(effectMap == null)
		{
			effectMap = new HashMap<String,Integer>();
			return;
		}
		
		if(effectMap.containsKey(key))
		{
			effectMap.remove(key);
		}
	}
	
	
	
	
	
	
	public int getMagicBoost(LivingEntity entity)
	{
		if(!entityTracker.containsKey(entity.getUniqueId())) return 0;
		Map<String,Integer> effectMap = entityTracker.get(entity.getUniqueId());
		if(effectMap == null) return 0;
		
		int val = 0;
		for(Entry<String,Integer> mb: effectMap.entrySet())
		{
			val +=mb.getValue();
		}
		return val;
	}
	
	public int getMagicBoost(Player player)
	{
		if(!playerTracker.containsKey(player.getUniqueId())) return 0;
		Map<String,Integer> effectMap = playerTracker.get(player.getUniqueId());
		if(effectMap == null) return 0;
		
		int val = 0;
		for(Entry<String,Integer> mb: effectMap.entrySet())
		{
			val +=mb.getValue();
		}
		return val;
	}
	
	
	/**
	 * Gets a map of all magic boosts on the Player
	 * @param player
	 * @return a map of magic boosts. Can be null.
	 */
	public Map<String,Integer> getMagicBoostData(Player player)
	{
		if(!playerTracker.containsKey(player.getUniqueId())) return null;
		return playerTracker.get(player.getUniqueId());
		
	}
	
	/**
	 * Gets a map of all magic boosts on the entity
	 * @param entity
	 * @return a map of magic boosts. Can be null.
	 */
	public Map<String,Integer> getMagicBoostData(LivingEntity entity)
	{
		if(!playerTracker.containsKey(entity.getUniqueId())) return null;
		return playerTracker.get(entity.getUniqueId());
	}
}

