package com.gmail.danielreturnweb.magicapi;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.plugin.RegisteredServiceProvider;

import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffectAPI;
import com.gmail.danielreturnweb.magicapi.effects.types.BuffType;
import com.gmail.danielreturnweb.magicapi.effects.types.EffectRemovalType;


/**
 * When milk is drunk it purges effects on the player with a purge strength of 1. <br>
 * Value can be changed in the config.
 * @author daniel
 *
 */
public class MilkDrink implements Listener
{
	int purgeStrength = 1;
	
	FileConfiguration config;
	
	public MilkDrink(FileConfiguration config) 
	{
		this.config = config;
		purgeStrength = config.getInt("Other.milk.purgeLevel");
		if(purgeStrength < 0) purgeStrength = 0;
		
	}
	
	//Gets the CEAPI. Implemented just incase the CEAPI is changed by an external plugin
	private CustomEffectAPI getCeapi()
	{
		RegisteredServiceProvider<CustomEffectAPI> rsp = Bukkit.getServicesManager().getRegistration(CustomEffectAPI.class);
		if(rsp != null)
		{
			return rsp.getProvider();
		}
		return null;
	}
	
	@EventHandler
	public void onDrink(PlayerItemConsumeEvent event)
	{
		if(event.getItem().getType() != Material.MILK_BUCKET) return;
		if(purgeStrength == 0) return;
		
		CustomEffectAPI ceapi = this.getCeapi();
		if(ceapi == null) return;
		
		Player entity =event.getPlayer();
		
		//Purge all effects (Vanilla and Custom)
		ceapi.removeAllEffectsByBuffType(entity, BuffType.Positive, EffectRemovalType.Milk,purgeStrength);
		ceapi.removeAllEffectsByBuffType(entity, BuffType.Negative, EffectRemovalType.Milk,purgeStrength);
		
		//Milk already does this.
//		for(PotionEffectType t : PotionEffectType.values())
//		{
//			entity.removePotionEffect(t);
//		}
	}
}
