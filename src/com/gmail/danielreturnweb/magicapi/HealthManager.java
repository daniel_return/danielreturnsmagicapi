package com.gmail.danielreturnweb.magicapi;

import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeModifier;
import org.bukkit.attribute.AttributeModifier.Operation;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.LivingEntity;

/**
 * This manager allows the the adjusting of Maximum health on a Living Entity. The adjustment also 
 * allows a HealthScalingType to be used.
 * @author daniel
 *
 */
public class HealthManager
{
	/**
	 * How the health being added/removed on a player will effect the players current health value.
	 * In vanilla, there is no change (None) however this Enum also offers (Scale) which keeps the
	 * current health to max health ratio (percentage) and (Heal) which heals the player by the 
	 * max health being adjusted.
	 * @author daniel
	 *
	 */
	public enum HealthScalingType
	{
		/** Current health will be changed in no way */
		None, 
		/** Current Health will be increased/decreased to keep the health percentage the same*/
		Scale, 
		/** The Current health will be increased by the max health being provided */
		Heal;
	}
	
	private MagicAPI magicAPI;
	private FileConfiguration config;
	
	private int minimumHealthCap = 6; //TODO will load from config
	
	
	
	public HealthManager(MagicAPI magicAPI)
	{
		this.magicAPI = magicAPI;
		config = magicAPI.getConfig();
	}
	
	/**
	 * Heals a entity by the specific amount. Cannot heal the entity health below 1 or above the Maximum dictated by Attribute.GENERIC_MAX_HEALTH
	 * @param entity
	 * @param amount
	 */
	public void heal(LivingEntity entity, double amount)
	{
		double max = entity.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue();
		
		double health = entity.getHealth()  + amount;
		if(health > max)
		{
			entity.setHealth(max);
		}
		else if(health < 1)
		{
			entity.setHealth(1);
		}
		else
		{
			entity.setHealth(health);
		}
	}
	
	/**
	 * Sets the entity's health. Cannot set the entity's health below 1 or above the Maximum dictated by Attribute.GENERIC_MAX_HEALTH
	 * @param entity
	 * @param amount
	 */
	private void setHealth(LivingEntity entity, double amount)
	{
		double max = entity.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue();
		
		if(amount > max) entity.setHealth(max);
		else if(amount < 1) entity.setHealth(1);
		else entity.setHealth(amount);
	}
	
	public void addMaxHealth(LivingEntity entity, HealthScalingType type, String tag, double health)
	{
		if(entity.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue() + health <= minimumHealthCap) return;
		
		double healthScale = entity.getHealth()/entity.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue();
		entity.getAttribute(Attribute.GENERIC_MAX_HEALTH).addModifier(new AttributeModifier(tag,health,Operation.ADD_NUMBER));
		
		
		if(type == HealthScalingType.None) return; //If scaling type is none, it just adds on max HP
		
		else if(type == HealthScalingType.Scale) //If type is scale, it keeps the same % scale as before the max health increase (Similar to WC3, Dota2, etc.)
		{
			double set = entity.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue()*healthScale;
			this.setHealth(entity, set);
			
		}
		else if(type == HealthScalingType.Heal) //If type is heal, it heals the player for the same amount as the max health being gained
		{
			heal(entity,health);
		}
	}
	
	/**
	 * Removes an instance of max health. (by its tag)
	 * @param entity
	 * @param tag
	 */
	public void removeMaxHealth(LivingEntity entity, String tag)
	{
		for(AttributeModifier mod : entity.getAttribute(Attribute.GENERIC_MAX_HEALTH).getModifiers())
		{
			if(mod.getName().equals(tag)) entity.getAttribute(Attribute.GENERIC_MAX_HEALTH).removeModifier(mod);
		}
	}
}
