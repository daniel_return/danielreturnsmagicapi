package com.gmail.danielreturnweb.magicapi.test;

import java.util.List;

import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.entity.AreaEffectCloud;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Listener;

import com.gmail.danielreturnweb.magicapi.spell.SpellCastData;
import com.gmail.danielreturnweb.magicapi.spell.SpellCooldownType;
import com.gmail.danielreturnweb.magicapi.spell.SpellTargeting;
import com.gmail.danielreturnweb.magicapi.spells.spell.SpellCommand;

/**
 * This class and spell exists for testing various mechanics. What it does
 * changes with what is being tested.
 * @author daniel
 * @deprecated DO NOT USE or IMPLEMENT in a PRODUCTION ENVIRONMENT
 */
public class Testing implements SpellCommand, Listener
{

	public Testing()
	{
		
	}

	@Override
	public int getSpellXPCost()
	{
		return 0;
	}

	@Override
	public int getSpellCooldown()
	{
		return 0;
	}

	@Override
	public SpellCooldownType getSpellCooldownType()
	{
		return SpellCooldownType.SPELL;
	}

	@Override
	public String getSpellID()
	{
		return "TESTING";
	}

	@Override
	public String getSpellName()
	{
		return "testing";
	}
	
	@Override
	public boolean onSpellCast(SpellCastData data, SpellTargeting targeting)
	{
		LivingEntity source = data.getCaster();
		Location loc = source.getLocation();
		
		SpellTargeting st = new SpellTargeting(source);
		
		List<LivingEntity> ents =  st.target_AOE_getWedge(6,60);
		
		for(LivingEntity e : ents)
		{
			data.getCaster().sendMessage("Entity: " + e.getType().toString());
			
			AreaEffectCloud aec3 = (AreaEffectCloud)loc.getWorld().spawnEntity(e.getLocation().add(0, 1, 0), EntityType.AREA_EFFECT_CLOUD);
			aec3.setDuration(20*10);
			aec3.setColor(Color.LIME);
			aec3.setRadius(1f);
		}
		data.getCaster().sendMessage("Size: " + ents.size());
		return true;
	}
	
//	@Override
//	public void onSpellCast(SpellCastData data, SpellTargeting targeting)
//	{
//		LivingEntity source = data.getCaster();
//		Location loc = source.getLocation();
//		Polygon pg = ShapesUtill.getTriangle(source.getEyeLocation(), 5);
//		
//		
//		
//		Location loc1 = new Location(loc.getWorld(),pg.xpoints[0],loc.getY(),pg.ypoints[0]);
//		Location loc2 = new Location(loc.getWorld(),pg.xpoints[1],loc.getY(),pg.ypoints[1]);
//		Location loc3 = new Location(loc.getWorld(),pg.xpoints[2],loc.getY(),pg.ypoints[2]);
//		
//		
//		AreaEffectCloud aec1 = (AreaEffectCloud)loc.getWorld().spawnEntity(loc1, EntityType.AREA_EFFECT_CLOUD);
//		aec1.setDuration(20*10);
//		aec1.setColor(Color.RED);
//		aec1.setRadius(1f);
//		
//		
//		AreaEffectCloud aec2 = (AreaEffectCloud)loc.getWorld().spawnEntity(loc2, EntityType.AREA_EFFECT_CLOUD);
//		aec2.setDuration(20*10);
//		aec2.setColor(Color.BLUE);
//		aec2.setRadius(1f);
//		
//		
//		AreaEffectCloud aec3 = (AreaEffectCloud)loc.getWorld().spawnEntity(loc3, EntityType.AREA_EFFECT_CLOUD);
//		aec3.setDuration(20*10);
//		aec3.setColor(Color.LIME);
//		aec3.setRadius(1f);
//	}

//	@Override
//	public void onSpellCast(SpellCastData data, SpellTargeting targeting)
//	{
//		LivingEntity source = data.getCaster();
//		
//		//90 is to offset mc to this.
//		double angle_degrees = this.normalizeDegree(source.getEyeLocation().getYaw() + 90+30);
//		double angle_degrees2 = this.normalizeDegree(source.getEyeLocation().getYaw() + 90-30);		
//		double length = 5;
//		
//		
//		Location loc = source.getLocation();
//		loc = loc.add(0, 1, 0); //I think loc is the entities feet so move it up by 1 (chest area?)		
//		
//		double x1 = loc.getX() + length * Math.cos(angle_degrees * Math.PI / 180);
//		double z1 = loc.getZ() + length * Math.sin(angle_degrees * Math.PI / 180);
//
//		double x2 = loc.getX() + length * Math.cos(angle_degrees2 * Math.PI / 180);
//		double z2 = loc.getZ() + length * Math.sin(angle_degrees2 * Math.PI / 180);
//		
//		Location loc1 = new Location(loc.getWorld(),x1,loc.getY(),z1);
//		Location loc2 = new Location(loc.getWorld(),x2,loc.getY(),z2);
//		
//		
//		AreaEffectCloud aec1 = (AreaEffectCloud)loc.getWorld().spawnEntity(loc1, EntityType.AREA_EFFECT_CLOUD);
//		aec1.setDuration(20*10);
//		aec1.setColor(Color.RED);
//		aec1.setRadius(1f);
//		
//		
//		AreaEffectCloud aec2 = (AreaEffectCloud)loc.getWorld().spawnEntity(loc2, EntityType.AREA_EFFECT_CLOUD);
//		aec2.setDuration(20*10);
//		aec2.setColor(Color.BLUE);
//		aec2.setRadius(1f);
//		
//		/*
//		 * Notes:
//		 * if ang = 00 its in the pos x direction
//		 * if ang = 90 its in the pos z direction 
//		 */
//
//	}
	
	/** @deprecated */
	protected double normalizeDegree(double degree)
	{
		if(degree >=0 && degree <360) return degree;
		if(degree >=360) return degree-360;
		if(degree <0) return degree+360;
		return 0;
	}

	@Override
	public String getPermissionToCast()
	{
		return null;
	}

	@Override
	public String getAlias()
	{
		return "test";
	}

	@Override
	public String[] getGrimoireEntry()
	{
		// TODO Auto-generated method stub
		return null;
	}

}
