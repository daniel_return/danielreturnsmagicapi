package com.gmail.danielreturnweb.magicapi;

import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.ServicePriority;
import org.bukkit.plugin.ServicesManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.danielreturnweb.magicapi.DamageSystem.DamageManager;
import com.gmail.danielreturnweb.magicapi.commands.Grimoire;
import com.gmail.danielreturnweb.magicapi.commands.Opus;
import com.gmail.danielreturnweb.magicapi.commands.StanceCommand;
import com.gmail.danielreturnweb.magicapi.effect.CustomEffectManager;
import com.gmail.danielreturnweb.magicapi.effect.CustomEffectPublicAPI;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffectAPI;
import com.gmail.danielreturnweb.magicapi.examples.Summons.SpiderRiderEffect_Example;
import com.gmail.danielreturnweb.magicapi.examples.Summons.SpiderSummon;
import com.gmail.danielreturnweb.magicapi.examples.effects.AcidEffect_Example;
import com.gmail.danielreturnweb.magicapi.examples.effects.HealingTouchEffect_Example;
import com.gmail.danielreturnweb.magicapi.examples.effects.RegenArmorEffect_Example;
import com.gmail.danielreturnweb.magicapi.examples.effects.Reincarnation_Example;
import com.gmail.danielreturnweb.magicapi.examples.effects.SlowPoisonBlade_Example;
import com.gmail.danielreturnweb.magicapi.examples.effects.SlowPoison_Example;
import com.gmail.danielreturnweb.magicapi.examples.effects.Stun_Example;
import com.gmail.danielreturnweb.magicapi.examples.spells.BreathOfAcid_Example;
import com.gmail.danielreturnweb.magicapi.examples.spells.Disk_Example;
import com.gmail.danielreturnweb.magicapi.examples.spells.FireBall_Example;
import com.gmail.danielreturnweb.magicapi.examples.spells.HealingTouch;
import com.gmail.danielreturnweb.magicapi.examples.spells.RegenArmor_Example;
import com.gmail.danielreturnweb.magicapi.implementation.EntityStanceViewDefault;
import com.gmail.danielreturnweb.magicapi.spell.SpellManager;
import com.gmail.danielreturnweb.magicapi.spell.monstercasting.MagicMonsterManager;
import com.gmail.danielreturnweb.magicapi.spells.spell.SpellItem;
import com.gmail.danielreturnweb.magicapi.summons.SummonAPI;
import com.gmail.danielreturnweb.magicapi.summons.SummonedEntityEffect;
import com.gmail.danielreturnweb.magicapi.test.Testing;
import com.gmail.danielreturnweb.magicapi.utils.SkinnedProjectile;

public class MagicAPI extends JavaPlugin
{
	private static MagicAPI instance;
	
	
	private SpellManager spellManager;
	private MagicBoostManager magicBoostManager;
	
	private DamageManager damageManager;
		
	private HealthManager healthManager;
	private AbsorptionManager absorptionManager;
	private RegenerationManager regenerationManager;
	
	private StunManager stunManager;
	private HiddenManager hiddenManager;
	private HitEvasionManager hitEvasionManager;
	
	private MagicMonsterManager magicMonsterManager;
	private SummonAPI summonAPI;
	
	
	
	public static MagicAPI getInstance()
	{
		return instance;
	}
	
	protected Logger logger;
	
	
	//===============================
	//     getManager methods
	//===============================
	
	
	
	public CustomEffectAPI getCustomEffectAPI()
	{
		RegisteredServiceProvider<CustomEffectAPI> rsp = Bukkit.getServicesManager().getRegistration(CustomEffectAPI.class);
		if(rsp != null)
		{
			return rsp.getProvider();
		}
		return null;
	}
	
	public EntityStanceView getEntityStanceView()
	{
		RegisteredServiceProvider<EntityStanceView> rsp = Bukkit.getServicesManager().getRegistration(EntityStanceView.class);
		if(rsp != null)
		{
			return rsp.getProvider();
		}
		return null;
	}
	
	public SpellManager getSpellManager()
	{
		return spellManager;
	}
	
	public AbsorptionManager getAbsorptionManager()
	{
		return absorptionManager;
	}
	
	public CustomEffectManager getCustomEffectManager()
	{
		return CustomEffectManager.getInstance();
	}
	
	public MagicBoostManager getMagicBoostManager()
	{
		return this.magicBoostManager;
	}
	
	public DamageManager getDamageManager()
	{
		return this.damageManager;
	}
	
	
	public StunManager getStunManger()
	{
		return this.stunManager;
	}
	
	public MagicMonsterManager getMagicMonsterManager()
	{
		return this.magicMonsterManager;
	}
	
	public HitEvasionManager getHitEvasionManager()
	{
		return this.hitEvasionManager;
	}
	
	public RegenerationManager getRegenerationManager()
	{
		return this.regenerationManager;
	}
	
	public SummonAPI getSummonAPI()
	{
		return this.summonAPI;
	}
	
	public HealthManager getHealthManager()
	{
		return this.healthManager;
	}
	
	public HiddenManager getHiddenManager()
	{
		return this.hiddenManager;
	}
	
	
	//===============================
	//  Magic API JavaPlugin Methods
	//===============================
	
	
	
	public void onEnable() 
	{
		super.onEnable();
		
		this.saveDefaultConfig();
		
		
		
		MagicAPI.instance = this;
		
		logger = this.getLogger();
		
		//Register Service API's
		ServicesManager sm = Bukkit.getServicesManager();
		sm.register(CustomEffectAPI.class, new CustomEffectPublicAPI(), this, ServicePriority.Low);
//		sm.register(AbsorptionManagerOld.class, new AbsorptionManagerOld(), this, ServicePriority.Low);
		
		EntityStanceView view = new EntityStanceViewDefault();
		((EntityStanceViewDefault)view).loadPlayerRelations();
		sm.register(EntityStanceView.class, view, this, ServicePriority.Low);
		Bukkit.getPluginManager().registerEvents((EntityStanceViewDefault)view, this);
		
		Bukkit.getPluginManager().registerEvents(new SkinnedProjectile(), this);
		
		StanceCommand stanceCommand = new StanceCommand();
		this.getCommand("stance").setExecutor(stanceCommand);
		
		
		
		Bukkit.getPluginManager().registerEvents(new MilkDrink(this.getConfig()), this);
		
		spellManager = new SpellManager(this.getConfig());
		Bukkit.getPluginManager().registerEvents(spellManager, this);
		
		absorptionManager = new AbsorptionManager();
		magicBoostManager = new MagicBoostManager();
		damageManager = new DamageManager(this, this.getConfig());
		
		this.stunManager = new StunManager();
		Bukkit.getPluginManager().registerEvents(stunManager, this);
		
		magicMonsterManager = new MagicMonsterManager();
		
		this.hitEvasionManager = new HitEvasionManager(this.getConfig());
		Bukkit.getPluginManager().registerEvents(this.hitEvasionManager, this);
		
		this.regenerationManager = new RegenerationManager(this);
		
		this.summonAPI = new SummonAPI(this.getConfig());
		Bukkit.getPluginManager().registerEvents(this.summonAPI, this);
		
		this.healthManager = new HealthManager(this);
		this.hiddenManager = new HiddenManager(this);
		
		
		
		this.getCommand("cast").setExecutor(this.spellManager);
		this.getCommand("castspell").setExecutor(this.spellManager);
		this.getCommand("spellcast").setExecutor(this.spellManager);
		this.getCommand("spc").setExecutor(this.spellManager);
		
		this.getCommand("grimoire").setExecutor(new Grimoire());
		this.getCommand("opus").setExecutor(new Opus());
		
		
		//Magic API technical spells and effects
		this.getCustomEffectManager().registerCustomEffect(new SummonedEntityEffect());
		
		
		//Load examples if configuration file states to do so
		if(this.getConfig().getBoolean("Examples.active")) this.loadExamples();
		
		Testing testing = new Testing();
		this.spellManager.registerSpell(testing);
		Bukkit.getPluginManager().registerEvents(testing, this);
		
		
		
		MagicAPI.instance = this;
	}
	
	
	
	@Override
	public void onDisable() 
	{
		super.onDisable();
		if(this.getEntityStanceView() instanceof EntityStanceViewDefault)
		{
			((EntityStanceViewDefault)this.getEntityStanceView()).savePlayerRelations();
		}
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command,	String label, String[] args) 
	{
		
		if(sender instanceof Player) return this.playerCommands((Player) sender, command, label, args);
		else return this.consoleCommands(sender, command, label, args);

	}
	
	
	private boolean playerCommands(Player player, Command command, String label, String[] args)
	{	
		
		if(command.getName().equalsIgnoreCase("MagicAPI"))
		{
			System.out.println("Magic API command used");
			if(args.length == 0 || (args.length == 1 && args[0].equalsIgnoreCase("info")))
			{
				//TODO help, About, & terms/mechanics (i.e. how passives work)
				player.sendMessage(ChatColor.GOLD + "MagicAPI Info, About, & Help");
			}
			if(args.length == 1 && args[0].equalsIgnoreCase("example"))
			{
				player.sendMessage("Doing example");
//				this.getSummonAPI().summonSummon(player, new SpiderSummon(), 1);				
			}
		}
		//The equivalent version of vanilla's /effect command
		if(command.getName().equalsIgnoreCase("EffectCustom")) //TODO move this to the effect manager
		{
			
		}
		if(command.getName().equalsIgnoreCase("SpellBind")) //TODO move to spell manager
		{			
			ItemStack held = player.getInventory().getItemInMainHand();
			if(held == null || held.getType() == Material.AIR)
			{
				player.sendMessage(ChatColor.RED + "You must be holding a item to bind the spell too");
				return false;
			}
			
			if(args.length >= 1)
			{
				StringBuilder input = new StringBuilder();
				for(String s : args)
				{
					input.append(s + " ");
				}
				
				for(SpellItem spell : this.getSpellManager().getSpellItems())
				{
					if(spell.getSpellName().equalsIgnoreCase(input.toString().trim()) || spell.getSpellID().equals(input.toString().trim()))
					{
						if(held.getAmount() > 1)
						{
							if(player.getGameMode() == GameMode.CREATIVE)
							{
								player.sendMessage(ChatColor.YELLOW + "[Warning]: " + ChatColor.GREEN + 
										" You are binding a spell to stacked items. Only unstacked items can be casted.");
							}
							else
							{
								player.sendMessage(ChatColor.RED + "You cannot bind spells to stacked items");
								return false;
							}
						}
						
						this.getSpellManager().bindSpellToItem(held, spell);
						return false;
					}
				}
				player.sendMessage(ChatColor.RED + "" + input + " does not exist or is not a spell that can be bound to an Item");
			}
		}
		return false;
	}
	
	
	private boolean consoleCommands(CommandSender sender, Command command, String label, String[] args)
	{
		return false;
	}
	
	
	//===============================
	//     Other methods
	//===============================
	
	
	
	private void loadExamples()
	{
		
		//****************
		//Effects
		//****************
		CustomEffectManager cem = this.getCustomEffectManager();
		cem.registerCustomEffect(new SlowPoison_Example());
		cem.registerCustomEffect(new HealingTouchEffect_Example());
		
		Reincarnation_Example res = new Reincarnation_Example();
		cem.registerCustomEffect(res);
		Bukkit.getPluginManager().registerEvents(res, this);
		
		Stun_Example stun = new Stun_Example();
		cem.registerCustomEffect(stun);
		
		SlowPoisonBlade_Example spb = new SlowPoisonBlade_Example();
		cem.registerCustomEffect(spb);
		Bukkit.getPluginManager().registerEvents(spb, this);
		
//		DiskEffect_Example diskEffect = new DiskEffect_Example();
//		cem.registerCustomEffect(diskEffect);
//		Bukkit.getPluginManager().registerEvents(diskEffect, this);
		
		AcidEffect_Example acidE = new AcidEffect_Example();
		cem.registerCustomEffect(acidE);
		
		HealingTouchEffect_Example healingTouchEffect = new HealingTouchEffect_Example();
		cem.registerCustomEffect(healingTouchEffect);
		
		RegenArmorEffect_Example regenArmorEffect = new RegenArmorEffect_Example();
		cem.registerCustomEffect(regenArmorEffect);
		Bukkit.getPluginManager().registerEvents(regenArmorEffect, this);
		
		
		SpiderRiderEffect_Example spiderRiderEffect = new SpiderRiderEffect_Example();
		cem.registerCustomEffect(spiderRiderEffect);
		Bukkit.getPluginManager().registerEvents(spiderRiderEffect, this);		
		
		
		
		
		
		
		//****************
		//Spells
		//****************
		FireBall_Example fireBallE = new FireBall_Example();
		this.spellManager.registerSpell(fireBallE);
		Bukkit.getPluginManager().registerEvents(fireBallE, this);
		
		Disk_Example diskE = new Disk_Example();
		this.spellManager.registerSpell(diskE);
		Bukkit.getPluginManager().registerEvents(diskE, this);
		
		BreathOfAcid_Example breathOfAcid = new BreathOfAcid_Example();
		this.spellManager.registerSpell(breathOfAcid);
//		Bukkit.getPluginManager().registerEvents(breathOfAcid, this);
		
		HealingTouch healingTouch = new HealingTouch();
		this.spellManager.registerSpell(healingTouch);
		
		RegenArmor_Example regenArmor = new RegenArmor_Example();
		this.spellManager.registerSpell(regenArmor);
		

		
		
		//****************
		//Monsters
		//****************
//		Bukkit.getPluginManager().registerEvents(new PigmanWarlock_Example(),this);
		
		
		
		//****************
		//Summons
		//****************
		Bukkit.getPluginManager().registerEvents(new SpiderSummon(),this);
		

	}
}
