package com.gmail.danielreturnweb.magicapi.spell.monstercasting;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;

import com.gmail.danielreturnweb.magicapi.MagicAPI;

/**
 * This is a manager for magic monsters. A API user can register a monster that can cast spell's here.
 * This will update the monster once every second reducing its cooldowns.
 * 
 * Once a monster is added, it will have persistent meta data added to it that allows the manager 
 * to keep track of the entity between chunk reloads and server reloads.
 * 
 * 
 * @author daniel
 *
 */
public class MagicMonsterManager implements Runnable
{
	/**
	 * How frequently all magic monsters are updated in ticks
	 * <br> 20 ticks = 1 second
	 */
	public static final int TICK_UPDATE = 20;
	/*
	 * I will have a list of Living Entities that get updated once per second from a bukkit runnable.
	 * Upon death they are removed.
	 * 
	 *  How do they spell cast? does it call an event the user must utilize when that monster is updated?
	 *  Do they register a list of spells they can cast?
	 *  Should I have a special object that contains a list of spells along with casting info, patterns, etc?
	 *  	would be stored in the entity list and passed into the event.
	 */
	
	List<MMHolder> mmHolderList;
	
	Map<String,Integer> behaviorCooldown;

	/*
	 * If I switch it over to the MagicMonsterBehavior instead of a event
	 * The MMHolder will be a Behavior and a list of LivingEntities that use it.
	 * 
	 * Upon being registered, the register() will search the list by behavior ID's
	 * If one is found, the monster is added to the list.
	 * If not found a new MMHolder would be created with the Behavior and a new monster list
	 * 
	 * The removeExpired would have to loop though all MMHolders and remove monsters from them.
	 * It may also remove any MMHolder objects that no longer have any monsters in there lists.
	 * If a new monster is added with the same behavior it will just have a new MMHolder created to
	 * hold them. This should help save memory.
	 * 
	 */
	
	
	public MagicMonsterManager()
	{
		mmHolderList = new ArrayList<MMHolder>();
		behaviorCooldown = new HashMap<String,Integer>();
		
		Bukkit.getScheduler().scheduleSyncRepeatingTask(MagicAPI.getInstance(), this,TICK_UPDATE,TICK_UPDATE);
	}
	
	
	/**
	 * Adds the LivingEntity to the MagicMonsterManager to provide updates to its ability to cast spells.
	 * @param entity
	 * @param data
	 * @return False if entity or data are null. False if Entity is dead.
	 */
	public boolean registerLivingEntity(LivingEntity entity, MagicMonsterBehavior behavior)
	{
		if(behavior == null || entity == null) return false;
		if(entity.isDead()) return false;
		
		
		for(MMHolder h : this.mmHolderList)
		{
			if(h.behavior.getBehaviorID().equals(behavior.getBehaviorID()))
			{
				h.addEntity(entity);
				return true;
			}
		}
		
		MMHolder h = new MMHolder(behavior);
		h.addEntity(entity);
		
		mmHolderList.add(h);
		return true;
	}
	
	
	/**
	 * Removes expired entities from the list.
	 * <br> Expired Entities are dead, null, etc.
	 * <br> if a behavior has no entities within it, then it is also removed
	 */
	protected void removeExpired()
	{
		for(int i = mmHolderList.size()-1; i >= 0; i--)
		{
			if(mmHolderList.get(i) == null) mmHolderList.remove(i);
			else if(mmHolderList.get(i).behavior == null || mmHolderList.get(i).entitys == null)  mmHolderList.remove(i);
			else if(mmHolderList.get(i).entitys.size() == 0) mmHolderList.remove(i);
			
			else for(int ii = mmHolderList.get(i).entitys.size()-1; ii >= 0; ii--)
			{
				if(mmHolderList.get(i).entitys.get(ii) == null) mmHolderList.get(i).entitys.remove(ii);
				else if(mmHolderList.get(i).entitys.get(ii).isDead()) mmHolderList.get(i).entitys.remove(ii);
			}
		}
	}
	
	
	
	/**
	 * Sees if a player is within the required distance of the magic monster
	 * @param e
	 * @param distance
	 * @return
	 */
	protected boolean hasPlayerWithinDistance(LivingEntity e, double distance)
	{
		List<Entity> ents = e.getNearbyEntities(distance, distance, distance);
		for(Entity ee: ents)
		{
			if(ee.getType() == EntityType.PLAYER) return true;
		}
		return false;
	}



	@Override
	public void run()
	{
		// TODO loop though list
		// Update all entities that can be updated
		
		for(int i = 0; i < this.mmHolderList.size(); i++)
		{
			if(mmHolderList.get(i) != null)
			{
				MagicMonsterBehavior beh = mmHolderList.get(i).behavior;
				
				//IF behavior cooldown exists, get it's value
				int cool = 0;
				if(behaviorCooldown.containsKey(beh.getBehaviorID()))
				{
					cool = behaviorCooldown.get(beh.getBehaviorID());
				}
				
				//If cooldown is <=1 do behavior
				if(cool <= 1)
				{
					behaviorCooldown.put(beh.getBehaviorID(),beh.reducedUpdates());
					this.doBehavior(mmHolderList.get(i)); //Do all behavior for all entities within the mmHolder
				}
				//If cooldown is not <=1 decrement cooldown and move on
				else
				{
					behaviorCooldown.put(beh.getBehaviorID(),cool-1);
				}
			}
			//TODO the Mob interface has getTarget()
			//This will allow me to add in an additional check for Behaviors that requier the
			//Casting mob to have a target before it starts casting spells.
		}
	}
	
	private void doBehavior(MMHolder holder)
	{
		for(LivingEntity entity : holder.entitys)
		{
			if(entity != null && !entity.isDead()) //Only non null and Living entities are updated
			{
				if(this.hasPlayerWithinDistance(entity, holder.behavior.activeDistance())) //Only entities within distance of a player are updated
				{
					holder.behavior.doBehavor(entity);
				}
			}
		}
	}
	
	
	//TODO Chunk unload persistent data storage and Chunk load 
	
	
	
	
	
	
	
	
	class MMHolder
	{
		protected MagicMonsterBehavior behavior;
		protected List<LivingEntity> entitys;
//		protected CastingMonsterData data;
		
		MMHolder(MagicMonsterBehavior behavior)
		{
			this.behavior = behavior;
			entitys = new ArrayList<LivingEntity>();
		}
		
		public void addEntity(LivingEntity entity)
		{
			entitys.add(entity);
		}
	}
}
