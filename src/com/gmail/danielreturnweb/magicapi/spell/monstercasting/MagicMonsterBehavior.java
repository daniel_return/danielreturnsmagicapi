package com.gmail.danielreturnweb.magicapi.spell.monstercasting;

import org.bukkit.entity.LivingEntity;

/**
 * The behavior that defines how a monster casts spells. <p>
 * Monsters with the same behavior will utilize the same instance of a behavior based upon its behavior ID.
 * This means each behavior passed into the register() function in MagicMonsterManager is not unique to that
 * monster.
 * 
 * 
 * <p> <b> <i> It is recommended to make one instance of your class that implements this
 * interface to pass into the MonsterMagicManger.registerMonster() method as that will aid in
 * reducing memory usage.</i> </b>
 * @author daniel
 *
 */
public interface MagicMonsterBehavior
{
	
	public String getBehaviorID();
	
	/**
	 * Called once a second by the MagicMonsterManager
	 * @param entity
	 * @param data
	 */
	public void doBehavor(LivingEntity entity);
	
	
	
	/**
	 * If this returns something that is not <= 1 then monsters with this behavior will be updated slower.
	 * They will skip this many updates before they are updated. <p>
	 * Setting this to a value > 1 aids in reducing CPU usage but may make the Magic Monster seem sluggish
	 * on casting its spells or could result in slower spell casting and delay's for spells that are off of cooldown
	 * and should be casted.
	 * <p> 
	 * For a balance between reducing CPU usage and good spell casting, this could be set to half or 1/4 of
	 * the monsters lowest cooldown spell.
	 * 
	 * <p> <b> If unsure of what this should return then return a 0 or 1. </b>
	 * @return
	 */
	public int reducedUpdates();
	
	
	/**
	 * How close does a player need to be for the entity that uses this behavior to
	 * start casting spells.
	 * @return
	 */
	public double activeDistance();
	
	
	/**
	 * States weather or not the entity will despawn when the chunk it is in unloads.
	 * If true the entity will persist, if false it will despawn;
	 * @return
	 */
	public boolean persistentWithChunks();
}
