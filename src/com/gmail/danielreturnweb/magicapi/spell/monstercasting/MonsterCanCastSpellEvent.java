package com.gmail.danielreturnweb.magicapi.spell.monstercasting;

import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;


/**
 * Called by the MagicMonsterManager when a Living Entity registered with it as a spell casting entity
 * is able to cast a spell. <p>
 * 
 * All casting logic for the entity should be placed within the listener for this event. <br>
 * @author daniel
 * @deprecated DO NOT USE. This event will never trigger. 1.0.0_Dev_004
 */
public class MonsterCanCastSpellEvent extends Event
{
	LivingEntity entity;
	CastingMonsterData data;
	
	//TODO can I do a <> on it for an Entity Type?
	//This would reduce me calling a tone of listener's every time a single entity is being updated.
	//100 entities * 5 listeners = 500 calls or whatever. Very time intensive. 
	//Providing a EntityType could allow the MagicMonsterManager to only call them by type
	//and should reduce casting calls
	//I may be able to move away from events all together by registering the monster with
	//some sort of object that handles the same code that the user would do.
	//The user would then have 1 instance they could provide and only that code would run
	//an issue with the event method is that others could "hook" in and change spell casting
	//Behavior of other monsters.
	//The class/interface could be MagicMonsterBehavior

	
	
	public MonsterCanCastSpellEvent(LivingEntity entity, CastingMonsterData data)
	{
		super();
		this.entity = entity;
		this.data = data;
	}

	



	public LivingEntity getEntity()
	{
		return entity;
	}

	public CastingMonsterData getData()
	{
		return data;
	}





	private static final HandlerList handles = new HandlerList();
	
    
    @Override
    public HandlerList getHandlers()
    {
    	return handles;
    }
    
    public static HandlerList getHandlerList() 
    {
        return handles;
    }
    
}
