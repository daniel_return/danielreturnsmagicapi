package com.gmail.danielreturnweb.magicapi.spell.monstercasting;

/**
 * This holds various information pertaining to a Living Entity that can cast spells.
 * This information helps dictate when the entity can cast its spells, the requirements for spell casting,
 * and other such data. <br>
 * This information will directly effect when the MagicMonsterManager triggers the MonsterCanCastSpell event.
 * @author daniel
 * @deprecated No longer used at this time version 1.0.0_Dev_004
 *
 */
public class CastingMonsterData
{	
	/** How close must a player be before the entity will start trying to cast spells */
	protected double playerDistance = 20;
	
	
	/** Can be set to prevent the MagicMonsterManager from updating this creature for the
	 * specified amount of updates (seconds). This allows the the entity to have a "global" cooldown for
	 * its self and could help with it not spamming spells. <br>
	 * 
	 * This also helps reduce the amount of entities that have the MonsterCanCastSpellEvent called per second
	 * reducing server impact by this system.
	 * <p> <b> Defaults to 0 </b>
	 */
	protected int entityCooldown = 0;
	
	
	/**
	 * Does the magic monster persist when a chunk unloads or should MagicMonsterManager despawn/remove it.
	 * <br>Defaults to true
	 */
	protected boolean persistsWithChunkUnloads = true;
	
	public CastingMonsterData(double playerDistance, boolean persistsWithChunkUnloads)
	{
		this.playerDistance = playerDistance;
		this.persistsWithChunkUnloads = persistsWithChunkUnloads;
	}
	
	public CastingMonsterData(double playerDistance, int cooldown, boolean persistsWithChunkUnloads)
	{
		this.playerDistance = playerDistance;
		this.entityCooldown = cooldown;
		this.persistsWithChunkUnloads = persistsWithChunkUnloads;
	}

	public int getEntityCooldown()
	{
		return entityCooldown;
	}

	public void setEntityCooldown(int entityCooldown)
	{
		this.entityCooldown = entityCooldown;
	}

	public double getPlayerDistance()
	{
		return playerDistance;
	}
	
	public boolean getPersistsWithChunkUnloads()
	{
		return this.persistsWithChunkUnloads;
	}
	
	
	
	
	/**
	 * Returns a default CastingMonsterData object with its distance set to 14
	 * and its cooldown set to 5. It will persist between chunk reloads.
	 * @return
	 */
	public static final CastingMonsterData DefaultData()
	{
		return new CastingMonsterData(14,5,true);
	}
	
	

}
