package com.gmail.danielreturnweb.magicapi.spell;

import org.bukkit.entity.LivingEntity;

/**
 * A object representing data about a casted spell
 * @author daniel
 *
 */
public class SpellCastData 
{
	/**
	 * Represents the entity casting the spell
	 */
	protected LivingEntity caster;
	
	/** The Level the spell is casted at */
	protected int level = 1;
	
	/**
	 * Represents the entity's current magic boost
	 */
	protected int magicBoost;
	
	/** If the caster is a Player and they are sneeking this will be true */
	protected boolean isSneaking = false;
	
	/**Is the caster a player */
	protected boolean isPlayer = false;
	
	//TODO castType? ITEM, COMMAND, PLUGIN, ENTITY
	
	public SpellCastData(LivingEntity caster, int magicBoost, boolean isShifting, boolean isPlayer) 
	{
		this.caster = caster;
		this.magicBoost = magicBoost;
		this.isSneaking = isShifting;
		this.isPlayer = isPlayer;
	}

	public LivingEntity getCaster() 
	{
		return caster;
	}

	public void setCaster(LivingEntity caster) 
	{
		this.caster = caster;
	}

	public int getMagicBoost() 
	{
		return magicBoost;
	}

	public void setMagicBoost(int magicBoost) 
	{
		this.magicBoost = magicBoost;
	}

	public boolean isSneaking()
	{
		return isSneaking;
	}

	public void setSneaking(boolean isShifting)
	{
		this.isSneaking = isShifting;
	}

	public boolean isPlayer()
	{
		return isPlayer;
	}

	public void setPlayer(boolean isPlayer)
	{
		this.isPlayer = isPlayer;
	}

	public int getLevel()
	{
		return level;
	}

	public void setLevel(int level)
	{
		this.level = level;
	}
	
	

}
