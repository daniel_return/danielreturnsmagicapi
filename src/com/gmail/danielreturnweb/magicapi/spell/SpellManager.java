package com.gmail.danielreturnweb.magicapi.spell;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

import com.gmail.danielreturnweb.magicapi.MagicAPI;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffect;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffectAPI;
import com.gmail.danielreturnweb.magicapi.spells.spell.ChargedSpell;
import com.gmail.danielreturnweb.magicapi.spells.spell.PassiveSpell;
import com.gmail.danielreturnweb.magicapi.spells.spell.PassiveSpell.PassiveSlot;
import com.gmail.danielreturnweb.magicapi.spells.spell.Spell;
import com.gmail.danielreturnweb.magicapi.spells.spell.SpellCommand;
import com.gmail.danielreturnweb.magicapi.spells.spell.SpellItem;
import com.gmail.danielreturnweb.magicapi.spells.spell.SpellItem.CastHand;
import com.gmail.danielreturnweb.magicapi.utils.PlayerExperience;


/**
 * The spell manager allows spells to be registered to the server. They also allow binding of spells to a item.
 * This also handles casting of spells from items and commands.
 * @author daniel
 *
 */
public class SpellManager implements Listener, CommandExecutor, TabCompleter, Runnable
{
	protected static final NamespacedKey DATA_SPELL_ID = new NamespacedKey(MagicAPI.getInstance(),"spell_id");
	protected static final NamespacedKey DATA_CHARGES_REMAINING = new NamespacedKey(MagicAPI.getInstance(),"charges_remaining");
	protected static final NamespacedKey DATA_COOLDOWN_END_DATE = new NamespacedKey(MagicAPI.getInstance(),"cooldown_end_date");
	
	protected static final String LORE_DISPLAY_CHARGES = "Charges: ";
	protected static final String LORE_DISPLAY_XP_COST = "XP Cost: ";
	protected static final String LORE_DISPLAY_HAND = "Hand: ";
	
	
	List<Spell> genericSpells;
	List<SpellItem> itemSpells;
	List<SpellCommand> commandSpells;
	
	private FileConfiguration config; 
	private int antiSpamCooldown = 0; 
	
	
	private Map<UUID,Long> antiSmapCooldown;
	private Map<String,Long> globalCooldowns;
	
	/** The cooldown tracker for spells for spells with SpellCooldownType.PLAYER <p>
	 * UUID is player, String is spell ID, Long is when its off of cooldown */
	private Map<UUID,Map<String,Long>> playerCooldown;
	
	
	private Map<UUID,Long> interactProc;
	
	
	//Load config values
	String spellItemDisplay_NameType = "PREPEND";
	String spellItemDisplay_LoreType = "APPEND";
	
	ChatColor spellItemDisplay_NameColor = ChatColor.valueOf("YELLOW");
	ChatColor spellItemDisplay_LoreColor = ChatColor.valueOf("AQUA");
	
	//Prepend and Append Names
	String spellItemDisplay_Prepend = ChatColor.translateAlternateColorCodes('&', "imbue");
	String spellItemDisplay_Append = ChatColor.translateAlternateColorCodes('&', "of");
	

	public SpellManager(FileConfiguration config) 
	{
		genericSpells = new ArrayList<Spell>();
		itemSpells = new ArrayList<SpellItem>();
		commandSpells = new ArrayList<SpellCommand>();
		
		antiSmapCooldown = new HashMap<UUID,Long>();
		playerCooldown = new HashMap<UUID,Map<String,Long>>();
		globalCooldowns = new HashMap<String,Long>();
		
		interactProc = new HashMap<UUID,Long>();
		
		
		
		/*
		 * ===============
		 * Configuration loading
		 * ===============
		 */
		
		
		this.config = config;
		this.antiSpamCooldown = config.getInt("SpellItem.antiSpam.cooldown");
		
		this.spellItemDisplay_NameType = config.getString("SpellItem.binding.name");
		this.spellItemDisplay_LoreType = config.getString("SpellItem.binding.lore");
		
		this.spellItemDisplay_NameColor = ChatColor.valueOf(config.getString("SpellItem.binding.nameColor"));
		this.spellItemDisplay_LoreColor = ChatColor.valueOf(config.getString("SpellItem.binding.loreColor"));
		
		this.spellItemDisplay_Prepend = config.getString("SpellItem.binding.nameText.prepend");
		this.spellItemDisplay_Append = config.getString("SpellItem.binding.nameText.append");
		
		
		/*
		 * ===============
		 * Runnable
		 * ===============
		 */
		Bukkit.getScheduler().scheduleSyncRepeatingTask(MagicAPI.getInstance(), this,60, 20);
	}
	
	
	/**
	 * Registers a spell.
	 * @param spell
	 */
	public void registerSpell(Spell spell)
	{
		this.genericSpells.add(spell);
		
		if(spell instanceof SpellItem)
		{
			this.itemSpells.add((SpellItem)spell);
		}
		if(spell instanceof SpellCommand)
		{
			this.commandSpells.add((SpellCommand)spell);
		}
		
		//SpellItems are added to a spell item list
		//while spell commands are added to the spell command list.
		//This will making searching for specific item/command spells easier
		//as the list "should" be smaller.
		//I.e. looking for a command spell will search the spellCommands list
		//not waisting time searching past non spellcommand like itemspells.
//		spells.add(spell);
	}
	
	
	/**
	 * Gets a spell from the spell list by its ID
	 * @param spellID
	 * @return The spell or null if a spell with that ID is not registered
	 */
	public Spell getSpellFromID(String spellID)
	{
		for(Spell s: this.genericSpells)
		{
			if(s.getSpellID().equals(spellID))
			{
				return s;
			}
		}
		return null;
	}
	
	
	/**
	 * Gets a spell by its name. Name case is ignored. <br>
	 * Returns a list of spells as multiple spells may have the same name.
	 * @param name the name of the spell to get
	 * @return
	 */
	public List<Spell> getSpellFromName(String name)
	{
		List<Spell> spells = new ArrayList<Spell>();
		
		for(Spell s: this.genericSpells)
		{
			if(s.getSpellName().equalsIgnoreCase(name))
			{
				spells.add(s);
			}
		}		
		return spells;
	}
	
	
	public List<Spell> getSpells()
	{
		return this.genericSpells;
	}
	
	public List<SpellItem> getSpellItems()
	{
		return this.itemSpells;
	}
	
	public List<SpellCommand> getSpellCommands()
	{
		return this.commandSpells;
	}
	
	
	/**
	 * Binds a spell by its ID to a ItemStack using Tags. <p>
	 * 
	 * @param stack
	 * @param spell
	 */
	public void bindSpellToItem(ItemStack stack, SpellItem spell)
	{
		this.bindSpellToItem(stack, spell, true, true);
	}
	
	/**
	 * Binds a spell by its ID to a ItemStack using Tags. <p>
	 * 
	 * @param stack
	 * @param spell
	 * @param spellName if true, the spells name will be added to the item name per config rules
	 * @param spellLore if true, the spells lore will be added to the item lore per config rules
	 */
	public void bindSpellToItem(ItemStack stack, SpellItem spell, boolean spellName, boolean spellLore)
	{
		/*
		 * Lore
		 * Cost: <xp> xp
		 * Charges: <charges>
		 * <spell lore>
		 */
		
				
		//If item is null or air return. If spell is null return
		if(stack == null || stack.getType() == Material.AIR) return;
		if(spell == null) return;
		
		
		ChargedSpell charged = null;
		if(spell instanceof ChargedSpell) charged = (ChargedSpell)spell;
		
		//Add data
		SpellItemData data = null; 
		if(charged == null) data = new SpellItemData(spell.getSpellID(),0l);
		else data = new SpellItemData(spell.getSpellID(),0l, charged.getSpellCharges());
		
		
//		System.out.println("Is ChargedSpell " + (spell instanceof ChargedSpell));
//		if(spell instanceof ChargedSpell) System.out.println("Is ChargedSpell "+ ((ChargedSpell)spell).getSpellCharges());
		
		//TODO if instance of DurabilityCastable then set the durability castable data in SpellItemData (needs to be added)
		
		
		this.setSpellItemData(stack, data);
				
		ItemMeta meta = stack.getItemMeta();

		
		//Add Name
		if(spellName)
		{
			if(spellItemDisplay_NameType.equalsIgnoreCase("PREPEND"))
			{
				if(meta.getDisplayName() == null || meta.getDisplayName().isEmpty())
				{
					meta.setDisplayName(spellItemDisplay_NameColor + spell.getSpellName() + " " + spellItemDisplay_Prepend + " " + ChatColor.RESET + "" + stack.getType().name());
				}
				else
				{
					meta.setDisplayName(spellItemDisplay_NameColor + spell.getSpellName() + " " + ChatColor.RESET + "" + meta.getDisplayName());
				}
			}
			else if(spellItemDisplay_NameType.equalsIgnoreCase("APPEND")) 
			{
				if(meta.getDisplayName() == null  || meta.getDisplayName().isEmpty())
				{
					meta.setDisplayName(stack.getType().name() + " " + spellItemDisplay_NameColor + spell.getSpellName());
				}
				else
				{
					meta.setDisplayName(meta.getDisplayName() + " " + spellItemDisplay_NameColor + spell.getSpellName());
				}
			}
			else if(spellItemDisplay_NameType.equalsIgnoreCase("OVERRIDE"))
			{
				meta.setDisplayName(spellItemDisplay_NameColor + "" + spell.getSpellName());
			}
			else {} //None and invalid values
		}
		
		
		
		
		//Add Lore
		if(spellLore)
		{
			//If its OVERRIDE or a empty/null lore just set it
			if(spellItemDisplay_LoreType.equalsIgnoreCase("OVERRIDE") || meta.getLore() == null || meta.getLore().size() == 0)
			{
				List<String> lore = new ArrayList<String>();
				
				//Note top line is lower part of lore
				if(spell.getCastHand() == null) lore.add(0, ChatColor.GRAY + SpellManager.LORE_DISPLAY_HAND + CastHand.None.getText());
				else lore.add(0, ChatColor.GRAY + SpellManager.LORE_DISPLAY_HAND + spell.getCastHand().getText());
				lore.add(0, ChatColor.GRAY + SpellManager.LORE_DISPLAY_XP_COST + spell.getSpellXPCost());
				if(charged != null) lore.add(0, ChatColor.GRAY + SpellManager.LORE_DISPLAY_CHARGES + charged.getSpellCharges()); //Comes before xp cost
				
				for(int i = 0; i < spell.getLoreDescription().length; i++)
				{
					lore.add(spellItemDisplay_LoreColor + spell.getLoreDescription()[i]);
				}
				meta.setLore(lore);
			}
			else if(spellItemDisplay_LoreType.equalsIgnoreCase("PREPEND"))
			{		
				List<String> lore = meta.getLore();
				
				for(int i = spell.getLoreDescription().length -1; i >=0; i--)
				{
					lore.add(0, spellItemDisplay_LoreColor + spell.getLoreDescription()[i]); //TODO does this work, or must I use setLore()...
				}
				
				//Note top line is lower part of lore
				if(spell.getCastHand() == null) lore.add(0, ChatColor.GRAY + SpellManager.LORE_DISPLAY_HAND + CastHand.None.getText());
				else  lore.add(0, ChatColor.GRAY + SpellManager.LORE_DISPLAY_HAND + spell.getCastHand().getText());
				lore.add(0, ChatColor.GRAY + SpellManager.LORE_DISPLAY_XP_COST + spell.getSpellXPCost());
				if(charged != null) lore.add(0, ChatColor.GRAY + SpellManager.LORE_DISPLAY_CHARGES + charged.getSpellCharges()); //Comes before xp cost
				
				meta.setLore(lore);
			}
			else if(spellItemDisplay_LoreType.equalsIgnoreCase("APPEND"))
			{
				System.out.println("APPEND");
				//Note top line is lower part of lore
				List<String> lore = meta.getLore();
				
				if(spell.getCastHand() == null) lore.add(ChatColor.GRAY + SpellManager.LORE_DISPLAY_HAND + CastHand.None.getText());
				else lore.add(ChatColor.GRAY + SpellManager.LORE_DISPLAY_HAND + spell.getCastHand().getText());
				lore.add(ChatColor.GRAY + SpellManager.LORE_DISPLAY_XP_COST + spell.getSpellXPCost());
				if(charged != null) lore.add(ChatColor.GRAY + SpellManager.LORE_DISPLAY_CHARGES + charged.getSpellCharges()); //Comes before xp cost
				
				for(String s : spell.getLoreDescription())
				{
					lore.add(spellItemDisplay_LoreColor + s);
				}
				meta.setLore(lore);
			}
			else {} //None and invalid values
		}			
		
		//Finished, set the items meta
		stack.setItemMeta(meta);
	}
	
	
	/**
	 * Get's the spell associated with the item
	 * @param stack
	 * @return The SpellItem class associated with this item or null
	 */
	public SpellItem getSpellFromItem(ItemStack stack)
	{
		if(stack == null) return null;
		if(stack.getType() == Material.AIR) return null;
		
		SpellItemData data = this.getSpellItemData(stack);
		if(data == null) return null;
		
		Spell spell = this.getSpellFromID(data.getSpellID());
		if(spell instanceof SpellItem) return (SpellItem) spell;
		return null;
	}
	
	
	/**
	 * Sets the spell item data for the selected spell casting item
	 * @param stack
	 * @param data
	 */
	public void setSpellItemData(ItemStack stack, SpellItemData data)
	{
		if(stack == null || stack.getType() == Material.AIR) return;
		if(data == null) return;
		
		//Get data container
		ItemMeta meta = stack.getItemMeta();
		PersistentDataContainer pdc = meta.getPersistentDataContainer();
		
		//Add data
		pdc.set(SpellManager.DATA_SPELL_ID, PersistentDataType.STRING, data.getSpellID());
		pdc.set(SpellManager.DATA_COOLDOWN_END_DATE, PersistentDataType.LONG, data.getCooldownEnd());
		pdc.set(SpellManager.DATA_CHARGES_REMAINING, PersistentDataType.INTEGER, 0+data.getRemainingCharges());
		

		
		stack.setItemMeta(meta);
	}
	
	/**
	 * Gets the SpellItemData from the provided item
	 * @param stack
	 * @return The SpellItemData or null if the data does not exist or is non-complete
	 */
	public SpellItemData getSpellItemData(ItemStack stack)
	{
		if(stack == null) return null;
		if(stack.getType() == Material.AIR) return null;
		
		//Get data container
		ItemMeta meta = stack.getItemMeta();
		PersistentDataContainer pdc = meta.getPersistentDataContainer();
		
		if(
				!pdc.has(SpellManager.DATA_SPELL_ID, PersistentDataType.STRING) ||
				!pdc.has(SpellManager.DATA_COOLDOWN_END_DATE, PersistentDataType.LONG)
				) {return null;}
		
		
		String id = pdc.get(SpellManager.DATA_SPELL_ID,PersistentDataType.STRING);
		long date = pdc.get(SpellManager.DATA_COOLDOWN_END_DATE,PersistentDataType.LONG);
		
		
		int charges = 0;
		boolean hasCharges = false;
		if(pdc.has(SpellManager.DATA_CHARGES_REMAINING, PersistentDataType.INTEGER))
		{
			charges = pdc.get(SpellManager.DATA_CHARGES_REMAINING, PersistentDataType.INTEGER);
			hasCharges = true;
		}
		
		
		if(hasCharges)
		{
			return new SpellItemData(id,date, charges);
		}
		else
		{
			return new SpellItemData(id,date);
		}	
	}
	
	
	/**
	 * Updates the lore display for the item. This only updates the XP cost and Item Display
	 * @param stack
	 * @param xpCost
	 * @param chargesRemaining
	 */
	public void updateSpellItemDisplay(ItemStack stack, SpellItem spell, int xpCost, int chargesRemaining)
	{
		ItemMeta meta = stack.getItemMeta();
		
		List<String> lore = meta.getLore();
		
		for(int i = 0; i < lore.size(); i++)
		{			
			if(ChatColor.stripColor(lore.get(i)).startsWith(SpellManager.LORE_DISPLAY_XP_COST))
			{
				lore.set(i,ChatColor.GRAY + SpellManager.LORE_DISPLAY_XP_COST + xpCost);
			}
			if(ChatColor.stripColor(lore.get(i)).startsWith(SpellManager.LORE_DISPLAY_CHARGES))
			{
				lore.set(i,ChatColor.GRAY + SpellManager.LORE_DISPLAY_CHARGES + chargesRemaining);
			}
			if(ChatColor.stripColor(lore.get(i)).startsWith(SpellManager.LORE_DISPLAY_HAND))
			{
				lore.set(i,ChatColor.GRAY + SpellManager.LORE_DISPLAY_HAND + spell.getCastHand().getText());
			}
		}
		meta.setLore(lore);
		stack.setItemMeta(meta);
		
		//TODO if one or more lines are not found/updated instead remove the existing ones and following
		//#PREPEND,APPEND or OVERRIDE add them back to the item (OVERRIDE may default to APPEND so users
		//don't loose there custom lore, if thats the case for the loss of the display values)
	}
	

	
	
	/*
	 * ===============================
	 * 	Spell casting Trigger's
	 * ===============================
	 */
	
	
	//*************************
	//	Plugin/Function
	//*************************
	
	/**
	 * Makes the entity cast the provided spell. This only casts the spell, it does not account for
	 * cooldowns, xp/durability costs, etc.
	 */
	public boolean castSpellFromEntity(LivingEntity caster, Spell spell)
	{
		//Get Magic Boost
		int boost = 0; 
		if(caster instanceof Player) boost = MagicAPI.getInstance().getMagicBoostManager().getMagicBoost((Player)caster);
		else boost = MagicAPI.getInstance().getMagicBoostManager().getMagicBoost(caster);
		
		//Create Spell data and targeting
		boolean shifting = false;
		boolean isPlayer = caster instanceof Player;
		if(isPlayer) shifting = ((Player)caster).isSneaking();
		
		SpellCastData d = new SpellCastData(caster, boost, shifting, isPlayer);
		SpellTargeting t = new SpellTargeting(caster);
		
		
		//Cast Spell
		return spell.onSpellCast(d, t);
	}
	
	/**
	 * Makes the entity cast the provided spell. This only casts the spell, it does not account for
	 * cooldowns, xp/durability costs, etc.
	 * @param caster
	 * @param spell
	 * @param level
	 * @return
	 */
	public boolean castSpellFromEntity(LivingEntity caster, Spell spell, int level)
	{
		//Get Magic Boost
		int boost = 0; 
		if(caster instanceof Player) boost = MagicAPI.getInstance().getMagicBoostManager().getMagicBoost((Player)caster);
		else boost = MagicAPI.getInstance().getMagicBoostManager().getMagicBoost(caster);
		
		//Create Spell data and targeting
		boolean shifting = false;
		boolean isPlayer = caster instanceof Player;
		if(isPlayer) shifting = ((Player)caster).isSneaking();
		
		SpellCastData d = new SpellCastData(caster, boost, shifting, isPlayer);
		d.setLevel(level);
		SpellTargeting t = new SpellTargeting(caster);
		
		
		//Cast Spell
		return spell.onSpellCast(d, t);
	}
	
	
	/**
	 * 
	 * @param player
	 * @param spell
	 * @param doXpCost should the player pay the xp cost, true = pay, false = do not pay
	 * @param doCooldown if true the spell will go onto cooldown for the player, if false it will not go on cooldown
	 */
	private void castSpellFromPlayer(Player player, Spell spell, boolean doXpCost, boolean doCooldown)
	{
		
	}
	
	
	
	/**
	 * Used by a plugin to make an entity cast a spell
	 */
	private void castSpellFromActor(LivingEntity caster, Spell spell)
	{
		//TODO this will take in an actor and will be the source of the spell cast.
		//TODO I may not have actor's able to cast spells
		//For now its being kept as private untill I decide If it is requiered or not
		//Currently the same as castSpellFromEntity but Actor's are special living entities???
	}
	
	
		
	//*************************
	//	Command
	//*************************
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) 
	{
		if(!(sender instanceof Player)) return false;
		
		Player caster = (Player)sender;
				
		if(args.length >=1)
		{
			StringBuilder input = new StringBuilder();
			for(String s : args)
			{
				input.append(s + " ");
			}
			
			for(SpellCommand spell : commandSpells)
			{
				if(spell.getSpellName().equalsIgnoreCase(input.toString().trim()) || spell.getAlias().equalsIgnoreCase(input.toString().trim()) ||
						spell.getSpellID().equals(input.toString().trim()) )
				{
					//Get spell permission, and see if player has it. Null/Emtpy "" means no permission required
					if(spell.getPermissionToCast() != null && !spell.getPermissionToCast().isEmpty())
					{
						if(!caster.hasPermission(spell.getPermissionToCast()))
						{
							this.sendErrorMessage(caster, spell, " has a permission requierment - You do not have permission to cast that spell");
							return false;
						}
					}
					
					
					//Get Magic Boost
					int boost = MagicAPI.getInstance().getMagicBoostManager().getMagicBoost(caster);
					
					//Create Spell data and targeting
					SpellCastData d = new SpellCastData(caster, boost, caster.isSneaking(),true);
					SpellTargeting t = new SpellTargeting(caster);
					
					//Do anti spam cooldown
					if(this.antiSpamCooldown > 0)
					{
						if(this.antiSmapCooldown.containsKey(caster.getUniqueId()))
						{
							long end = this.antiSmapCooldown.get(caster.getUniqueId());
							if(System.currentTimeMillis() < end)
							{
								this.sendErrorMessage(caster, null, "Must wait " + this.antiSpamCooldown + " seconds between casting spells");
								return false;
							}
							else
							{
								//Set new cooldown
								this.antiSmapCooldown.put(caster.getUniqueId(), System.currentTimeMillis() + 1000*this.antiSpamCooldown);
							}
						}
						else
						{
							//Set new cooldown
							this.antiSmapCooldown.put(caster.getUniqueId(), System.currentTimeMillis() + 1000*this.antiSpamCooldown);
						}
					}
				
					//Do cooldowns
					if(spell.getSpellCooldownType() == SpellCooldownType.GLOBAL)
					{
						if(this.globalCooldowns.containsKey(spell.getSpellID()))
						{
							if(this.globalCooldowns.get(spell.getSpellID()) >= System.currentTimeMillis())
							{
								this.sendErrorMessage(caster, spell, "Is on global cooldown");
								return false;
							}
							else
							{
								//Set new cooldown
								this.globalCooldowns.put(spell.getSpellID(), System.currentTimeMillis() + 1000*spell.getSpellCooldown());
							}
						}
						else
						{
							this.globalCooldowns.put(spell.getSpellID(), System.currentTimeMillis() + 1000*spell.getSpellCooldown());
						}
					}
					
					else if(spell.getSpellCooldownType() == SpellCooldownType.PLAYER || spell.getSpellCooldownType() == SpellCooldownType.SPELL)
					{
						if(this.playerCooldown.containsKey(caster.getUniqueId()))
						{
							Map<String,Long> spellCooldown = this.playerCooldown.get(caster.getUniqueId());
							
							if(spellCooldown.containsKey(spell.getSpellID()))
							{
								if(spellCooldown.get(spell.getSpellID()) >= System.currentTimeMillis())
								{
									this.sendErrorMessage(caster, spell, "Is on cooldown");
									return false;
								}
								else
								{
									//Set new cooldown
									spellCooldown.put(spell.getSpellID(), System.currentTimeMillis() + 1000*spell.getSpellCooldown());
								}
							}
							else
							{
								spellCooldown.put(spell.getSpellID(), System.currentTimeMillis() + 1000*spell.getSpellCooldown());
								this.playerCooldown.put(caster.getUniqueId(),spellCooldown);
							}							
						}
						else
							
						{
							Map<String,Long> spellCooldown = new HashMap<String,Long>();
							spellCooldown.put(spell.getSpellID(), System.currentTimeMillis() + 1000*spell.getSpellCooldown());
							this.playerCooldown.put(caster.getUniqueId(), spellCooldown);
						}
					}
					
					//Get xp cost and see if the player has the required xp
					if(spell.getSpellXPCost() > 0)
					{
						int exp = PlayerExperience.getTotalExperience(caster);
						if(exp < spell.getSpellXPCost())
						{
							this.sendErrorMessage(caster, spell, "You need more XP to cast this spell");
							return false;
						}
						PlayerExperience.setTotalExperience(caster, exp - spell.getSpellXPCost());
					}
					
					
					//Cast Spell
					spell.onSpellCast(d, t);
					return true;
				}
			}
			this.sendErrorMessage(caster, null, args[0] + " is not a spell");
			return false;
		}
		return false;
	}
	
	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] arguments)
	{
		return null;
	}
	
	//*************************
	//	Item
	//*************************
	
	
	/**
	 * This method is called when a user interacts with something.
	 * If they are holding a 
	 * @param event
	 */
	@EventHandler
	public void onItemSpellCast(PlayerInteractEvent event)
	{
		//FYI the codes a mess
		
		 
		Player player = event.getPlayer();
		Action action = event.getAction();
		
//		MagicAPI.getInstance().getMagicBoostManager().addMagicBoost("TestHT", player, 500); //TODO remove, for testing only
		
		//See if the player has interacted within the last x milliseconds.
		if(!this.interactProc.containsKey(player.getUniqueId())) this.interactProc.put(player.getUniqueId(), 0l);
		
		long dur = (System.currentTimeMillis() - this.interactProc.get(player.getUniqueId()));
//		System.out.println("Interact Event " + dur);
		this.interactProc.put(player.getUniqueId(), System.currentTimeMillis());
		if(dur <= 610) return;
		
//		System.out.println("Interact Event Item: " + event.getItem().getType());
		
		
		//********************************
		//Process the main hand
		//********************************
		ItemStack main = player.getInventory().getItemInMainHand();
		
		if(main != null && main.getType() != Material.AIR)
		{
//			System.out.println("Main is valid item");
			//Get spell and spell data
			SpellItem mainSpell = this.getSpellFromItem(main);
			SpellItemData mainData = this.getSpellItemData(main);
			
			//Has a spell
			if(mainSpell != null && mainData != null)
			{
//				System.out.println("Main is valid spell");
				//If not stacked do spell, else do no spell
				if(main.getAmount() == 1) 
				{
					//TODO Add in a SingleUseInterface. When a item with this interface is used
					//it is consumed in the use. However they can cast while stacked unlike charged spells.
					
					
					//If the casting hand is valid continue
					if(mainSpell.getCastHand() == CastHand.MainHand || mainSpell.getCastHand() == CastHand.Both)
					{
						//If the player action matches an action that can cast the spell
						if(Arrays.asList(mainSpell.getCastActions()).contains(action))
						{
							castItemSpell(player, main, mainSpell,mainData);
						}
					} //TODO should the player get an error message when trying to cast a off hand item in the main hand?
				}
				else
				{
					this.sendErrorMessage(player, mainSpell,"Spell Items cannot cast while stacked.");
				}
			}			
		}		
		

		//********************************
		//Process the off hand
		//********************************		
		ItemStack off = player.getInventory().getItemInOffHand();
		
		if(off != null && off.getType() != Material.AIR)
		{
//			System.out.println("Off is valid item");
			//Get spell and spell data
			SpellItem offSpell = this.getSpellFromItem(off);
			SpellItemData offData = this.getSpellItemData(off);
			
			//Has a spell
			if(offSpell != null && offData != null)
			{
//				System.out.println("Off is valid spell");
				//If not stacked do spell, else do no spell
				if(off.getAmount() == 1) 
				{
//					System.out.println("Off is valid Amount (1)");
					//TODO Add in a SingleUseInterface. When a item with this interface is used
					//it is consumed in the use. However they can cast while stacked unlike charged spells.
					
					
					//If the casting hand is valid continue
					if(offSpell.getCastHand() == CastHand.OffHand || offSpell.getCastHand() == CastHand.Both)
					{
//						System.out.println("Off is valid hand");
						//If the player action matches an action that can cast the spell
						if(Arrays.asList(offSpell.getCastActions()).contains(action))
						{
//							System.out.println("Off is valid Cast Action");
							castItemSpell(player, off, offSpell,offData);
						}
					} //TODO should the player get an error message when trying to cast a off hand item in the main hand?
				}
				else
				{
					this.sendErrorMessage(player, offSpell,"Spell Items cannot cast while stacked.");
				}
			}			
		}	
	}
	
	
	/**
	 * 
	 * @param player
	 * @param action
	 * @param item
	 * @param spell
	 * @param data
	 * @return true, if the spell was casted. False if it was not casted
	 */
	protected boolean castItemSpell(Player player, ItemStack item, SpellItem spell, SpellItemData data)
	{			
		boolean breakItem = false;
		//Check Cooldown
		
		//Do anti spam cooldown
		if(this.antiSpamCooldown > 0)
		{
			if(this.antiSmapCooldown.containsKey(player.getUniqueId())) //TODO change this to string and take in <hand>_PlayerUUID allowing for each hand to have there own cooldown
			{
				long end = this.antiSmapCooldown.get(player.getUniqueId());
				if(System.currentTimeMillis() < end)
				{
					this.sendErrorMessage(player, null, "Must wait " + this.antiSpamCooldown + " seconds between casting spells");
					return false;
				}
				//Set new cooldown
				else this.antiSmapCooldown.put(player.getUniqueId(), System.currentTimeMillis() + 1000*this.antiSpamCooldown);
			}
			//Set new cooldown
			else this.antiSmapCooldown.put(player.getUniqueId(), System.currentTimeMillis() + 1000*this.antiSpamCooldown);
		}
	
		//Do cooldowns
		if(spell.getSpellCooldownType() == SpellCooldownType.GLOBAL)
		{
			if(this.globalCooldowns.containsKey(spell.getSpellID()))
			{
				if(this.globalCooldowns.get(spell.getSpellID()) >= System.currentTimeMillis())
				{
					this.sendErrorMessage(player, spell, "Is on global cooldown");
					return false;
				}
			}
		}
		
		
//		if(System.currentTimeMillis() < data.getCooldownEnd())
		if(!this.spellItemOffCooldown(player, spell, data))
		{
			this.sendErrorMessage(player, spell, "is on Cooldown");
			return false;
		}
		
		//Check the players experience
		if(PlayerExperience.getTotalExperience(player) < spell.getSpellXPCost())
		{
			this.sendErrorMessage(player, spell, "not enough experience to cast!");
			return false;
		}
		
		//
		if(spell instanceof ChargedSpell) //Uses charges
		{			
			ChargedSpell charged = (ChargedSpell)spell;
			
			//If item has 0 or less charges and is not destroyed on last charge, it cannot be casted
			if(data.getRemainingCharges() <= 0 && !charged.itemDestoryedOnLastChargeCast())
			{
				this.sendErrorMessage(player, spell, "That item has no charges and cannot be casted");
				return false;
			}
			
			//Decrement remaining charges
			data.setRemainingCharges(data.getRemainingCharges()-1); //Deduct 1 charge
			
			//If remaining charges is 0 and item is destroyed on last charge, set to 0.
			if(data.getRemainingCharges() <= 0 && charged.itemDestoryedOnLastChargeCast())
			{
				//Break the Item (will still cast as its the last use)
				breakItem = true;
			} //TODO should the else check for 0 charges and not destroyed then send the player a message saying they used up the last charge?
		}
		//NOTE: No need for the non charged items to have an else as they just flow though to this area
		
		
		//TODO durability check/reduction
		
		
		
		//**************************
		//    Cast spell
		//**************************
		
		boolean wasCasted = this.castSpellFromEntity(player, spell);
		if(!wasCasted) return false; //if the spell was not casted, return
		
		
		//**************************
		//    Set spell data
		//**************************
		
		//Do XP cost and Set Cooldown
//		player.setTotalExperience(player.getTotalExperience() - spell.getSpellXPCost()); //This does nothing
		int exp = PlayerExperience.getTotalExperience(player)-spell.getSpellXPCost();
		if(exp < 0) exp = 0;
		PlayerExperience.setTotalExperience(player, exp);
		
		
		//Sset cooldown depending on cooldown type
		if(spell.getSpellCooldownType() == SpellCooldownType.GLOBAL)
		{
			this.globalCooldowns.put(spell.getSpellID(), System.currentTimeMillis() + 1000*spell.getSpellCooldown());
		}
		else if (spell.getSpellCooldownType() == SpellCooldownType.PLAYER)
		{
			//TODO SpellCooldownType.PLAYER
		}
		if (spell.getSpellCooldownType() == SpellCooldownType.SPELL)
		{
			data.setCooldownEnd(System.currentTimeMillis() + 1000*spell.getSpellCooldown());
		}
		
		
		//Update item SpellItemData and Display		
       this.setSpellItemData(item, data);
       this.updateSpellItemDisplay(item, spell, spell.getSpellXPCost(), data.getRemainingCharges());
       if(breakItem) item.setAmount(0);
       
		return true;
	}
	
	
	/**
	 * Sees if a spell is off of cooldown for a player. Does NOT check global or anti spam cooldown.
	 * @param player
	 * @param spell
	 * @return True if its off cooldown, false its not off cooldown
	 */
	private boolean spellItemOffCooldown(Player caster, Spell spell, SpellItemData data)
	{
		if(spell.getSpellCooldownType() == SpellCooldownType.PLAYER)
		{
			if(this.playerCooldown.containsKey(caster.getUniqueId()))
			{
				Map<String,Long> spellCooldown = this.playerCooldown.get(caster.getUniqueId());
				
				if(spellCooldown.containsKey(spell.getSpellID()))
				{
					if(spellCooldown.get(spell.getSpellID()) >= System.currentTimeMillis())
					{
						return false;
					}
					else
					{
						//Set new cooldown
						spellCooldown.put(spell.getSpellID(), System.currentTimeMillis() + 1000*spell.getSpellCooldown());
						return true;
					}
				}
				else
				{
					spellCooldown.put(spell.getSpellID(), System.currentTimeMillis() + 1000*spell.getSpellCooldown());
					this.playerCooldown.put(caster.getUniqueId(),spellCooldown);
					return true;
				}							
			}
			else
				
			{
				Map<String,Long> spellCooldown = new HashMap<String,Long>();
				spellCooldown.put(spell.getSpellID(), System.currentTimeMillis() + 1000*spell.getSpellCooldown());
				this.playerCooldown.put(caster.getUniqueId(), spellCooldown);
				return true;
			}
		}
		else if (spell.getSpellCooldownType() == SpellCooldownType.SPELL)
		{
			if(System.currentTimeMillis() <= data.getCooldownEnd())
			{
				return false;
			}
		}
		return true;
	}
	
	/**
	 * 
	 * @param player
	 * @param spell the spell encountering the error. Can be null if not triggered by a spell
	 * @param message
	 */
	private void sendErrorMessage(Player player, Spell spell, String message)
	{
		if(spell == null) player.sendMessage(ChatColor.GRAY + "[Spell] " + ChatColor.RED + message);
		else player.sendMessage(ChatColor.GRAY + "[Spell] " + ChatColor.LIGHT_PURPLE + spell.getSpellName() + " " + ChatColor.RED + message);
	}


	
	//TODO do inventory events to see if a player is putting a magic item in main hand, armor, or off hand
	//If the item is a SpellItem and has a passive toggle it on/off.
	//Spell.getPassive() will just return a CustomEffect. (May make a ItemPassive interface to use in conjunction with SpellItem)
	//Can command spells have passives? I can't see that really being used/working, so I think no.
	
	
	//InventoryClickEvent
	//InventoryDragEvent
	//InventoryAction
	//ClickType
	
//	@EventHandler
//	public void invClick(InventoryClickEvent event)
//	{
//		ClickType click = event.getClick();
//		InventoryAction action = event.getAction();
//		SlotType slotType = event.getSlotType();
//		InventoryType invType = event.getInventory().getType();
//		
//		HumanEntity human = event.getWhoClicked();
//		
//		ItemStack cursor = event.getCursor();
//		ItemStack current = event.getCurrentItem();
//		
////		if(invType != InventoryType.PLAYER) return;
//		
//		System.out.println("==================");
//		System.out.println("");
//		System.out.println("Click: " + click.toString());
//		System.out.println("Action: " + action.toString());
//		System.out.println("SlotType: " + slotType.toString());
//		System.out.println("InvType: " + invType.toString());
//		System.out.println("Hot Bar Button: " + event.getHotbarButton());
//		System.out.println("Raw: " + event.getRawSlot());
//		System.out.println("Cursor: " + cursor);
//		System.out.println("Current: " + current);
//		System.out.println("");
//		System.out.println("");
//		
//		
//	}
	
	
	/* 
	 * ARMOR: 0-4 using getArmorContents; Boots being 0, helm being 3
	 */	
	private void passiveArmor(Player player)
	{
		int i = 0;
		for(ItemStack stack: player.getInventory().getArmorContents())
		{
			if(stack != null)
			{
//				System.out.println("I: " + i + " -- Stack is " + stack.getType());
				CustomEffectAPI ceapi = MagicAPI.getInstance().getCustomEffectAPI();
//				CustomEffectManager cem = MagicAPI.getInstance().getCustomEffectManager();
				
				Spell spell = this.getSpellFromItem(stack);
				
				CustomEffect ce = null;
				
				if(spell != null && spell instanceof PassiveSpell)
				{
					PassiveSpell passive = (PassiveSpell) spell;
					
					//Head Slot
					if(i == 3 && (Arrays.asList(passive.getPassiveSlots()).contains(PassiveSlot.ArmorSlot) || 
							Arrays.asList(passive.getPassiveSlots()).contains(PassiveSlot.HeadSlot)))
					{
						ce = passive.getPassiveAbility();
					}
					
					//Chest Slot
					else if(i ==2 && (Arrays.asList(passive.getPassiveSlots()).contains(PassiveSlot.ArmorSlot) || 
							Arrays.asList(passive.getPassiveSlots()).contains(PassiveSlot.ChestSlot)))
					{
						ce = passive.getPassiveAbility();
					}
					
					//Leggings Slot
					else if(i == 1 && (Arrays.asList(passive.getPassiveSlots()).contains(PassiveSlot.ArmorSlot) || 
							Arrays.asList(passive.getPassiveSlots()).contains(PassiveSlot.LeggingsSlot)))
					{
						ce = passive.getPassiveAbility();
					}
					
					//Boots Slot
					else if(i == 0 && (Arrays.asList(passive.getPassiveSlots()).contains(PassiveSlot.ArmorSlot) || 
							Arrays.asList(passive.getPassiveSlots()).contains(PassiveSlot.BootsSlot)))
					{
						ce = passive.getPassiveAbility();
					}
					
					//Do effect
					if(ce != null) ceapi.placeEffect(player, ce);
				}
			}
			i++;
		}
	}
	
	
	/*
	 * MAIN HAND: (has method)
	 * OFF HAND: (has method) 
	 */
	private void passiveHands(Player player)
	{
		CustomEffectAPI ceapi = MagicAPI.getInstance().getCustomEffectAPI();
		
		//== Main Hand ==
		ItemStack mainHand = player.getInventory().getItemInMainHand();
		
		if(mainHand != null && mainHand.getType() != Material.AIR)
		{
			Spell mainSpell = this.getSpellFromItem(mainHand);
			CustomEffect ce = null;
			
			if(mainSpell != null && mainSpell instanceof PassiveSpell)
			{
				PassiveSpell passive = (PassiveSpell) mainSpell;
				
				if((Arrays.asList(passive.getPassiveSlots()).contains(PassiveSlot.MainHand)))
				{
					ce = passive.getPassiveAbility();
					if(ce != null) ceapi.placeEffect(player, ce);
				}
			}
		}
		
		//== Off Hand ==
		ItemStack offHand = player.getInventory().getItemInMainHand();
		
		if(offHand != null && offHand.getType() != Material.AIR)
		{
			Spell offSpell = this.getSpellFromItem(mainHand);
			CustomEffect ce = null;
			
			if(offSpell != null && offSpell instanceof PassiveSpell)
			{
				PassiveSpell passive = (PassiveSpell) offSpell;
				
				if((Arrays.asList(passive.getPassiveSlots()).contains(PassiveSlot.OffHand)))
				{
					ce = passive.getPassiveAbility();
					if(ce != null) ceapi.placeEffect(player, ce);
				}
			}
		}
		
		
	}
	
	/*
	 * HOT BAR: 4-12
	 * TODO merge passiveHotBar and passiveInventory?? they are within the same inventory loop and doing the entire inventory
	 * loop twice is inefficient
	 */
	private void passiveHotBar(Player player)
	{
		CustomEffectAPI ceapi = MagicAPI.getInstance().getCustomEffectAPI();
		
		int i = 0;
		for(ItemStack stack : player.getInventory().getStorageContents())
		{
			if(stack != null)
			{
				if(i >=4 && i <= 12) //HOT BAR
				{
					Spell spell = this.getSpellFromItem(stack);
					CustomEffect ce = null;
					
					if(spell != null && spell instanceof PassiveSpell)
					{
						PassiveSpell passive = (PassiveSpell) spell;
						
						if((Arrays.asList(passive.getPassiveSlots()).contains(PassiveSlot.HotBar)))
						{
							ce = passive.getPassiveAbility();
							if(ce != null) ceapi.placeEffect(player, ce);
						}
					}
				}
			}
			i++;
		}
	}
	
	
	/* 
	 * INVENTORY: 13-39
	 * TODO
	 */
	private void passiveInventory(Player player)
	{
		CustomEffectAPI ceapi = MagicAPI.getInstance().getCustomEffectAPI();
		
		int i = 0;
		for(ItemStack stack : player.getInventory().getStorageContents())
		{
			if(stack != null)
			{
				if(i >=13 && i <= 39) //HOT BAR
				{
					Spell spell = this.getSpellFromItem(stack);
					CustomEffect ce = null;
					
					if(spell != null && spell instanceof PassiveSpell)
					{
						PassiveSpell passive = (PassiveSpell) spell;
						
						if((Arrays.asList(passive.getPassiveSlots()).contains(PassiveSlot.Inventory)))
						{
							ce = passive.getPassiveAbility();
							if(ce != null) ceapi.placeEffect(player, ce);
						}
					}
				}
			}
			i++;
		}
	}

			
	

	@Override
	public void run()
	{
		for(Player player : Bukkit.getOnlinePlayers())
		{
			if(player != null && !player.isDead())
			{
				this.passiveArmor(player);
				this.passiveHands(player);
				this.passiveHotBar(player);
				this.passiveInventory(player);
			}
		}
	}	
}
