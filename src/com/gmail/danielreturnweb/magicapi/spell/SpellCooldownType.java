package com.gmail.danielreturnweb.magicapi.spell;

/**
 * Specifies the type of cooldown this spell or Spell Item Uses.
 * <p> <u>Types</u>
 * <br><b>GLOBAL</b>  The cooldown is for all players on the server
 * <br><b>PLAYER</b> The cooldown is per player. This is good for Spell Items that should have a shared cooldown.
 * <br><b>SPELL</b> The individual spell instance should contain the cooldown. This is good for items that do not have shared cooldowns.
 * @author daniel
 *
 */
public enum SpellCooldownType 
{
	GLOBAL,
	PLAYER,
	SPELL;
}
