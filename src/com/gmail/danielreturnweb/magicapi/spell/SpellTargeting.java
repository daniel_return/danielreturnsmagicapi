package com.gmail.danielreturnweb.magicapi.spell;

import java.awt.Polygon;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Animals;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import com.gmail.danielreturnweb.magicapi.EntityStanceView;
import com.gmail.danielreturnweb.magicapi.EntityStanceView.EntityStance;
import com.gmail.danielreturnweb.magicapi.MagicAPI;
import com.gmail.danielreturnweb.magicapi.utils.ShapesUtill;

/*
 *TODO todo list
 * - Make targeting's take into account hit chances and evasion.
 * - *Make targeting's take GameMode's into account
 * 
 */


/**
 * When a spell is casted This is passed into the spell. This allows for spell creators 
 * to easily get targets for there spells. <p>
 * 
 * The basic format of the methods are target_[type]_[specific Method].
 * 
 * [type] can be single (single target), AOE (area of effect target) and location (target a bukkit location)
 * 
 * @author daniel
 *
 */
public class SpellTargeting
{
	
	/**
	 * The target type of a SpellTargeting. A SpellTargeting object will default to TargetType.PLAYER_HOSTILE and TargetType.MONSTER_HOSTILE <p>
	 * When a SpellTargeting target method is called (Such as AOE_getNearbyEntities()) the values here will aid in selecting what entities
	 * are returned as a selection for the spell.
	 * @author daniel
	 *
	 */
	public enum TargetType
	{
		PLAYER,MONSTER,ANIMAL;
	}
	
	
	
	protected Entity source;
	
	/** The TargetType's this SpellTargeting object will target */
	protected TargetType[] targetTypes;
	/** The TargetStances's this SpellTargeting object will target */
	protected EntityStance[] targetStances;
	/** The GameMode's that this SpellTargeting object will target */
	protected GameMode[] targetGameModes;
	
	/** If true the spell targeting will run evasion checks on each entity.
	 * If the evasion occurs then the entity will be excluded from the targeting list
	 */
	protected boolean useEvasion = true;
	
	/**
	 * If true the spell targeting will run a hit chance based upon the casters hit chance for the
	 * Target entity. If a miss occurs the target entity will be excluded form the targeting list.
	 */
	protected boolean useHitChance = true;
	
	/**
	 * Creates a new targeting object. TargetType[] defaults to PLAYER and MONSTER.
	 * While entityStances default to HOSTILE.
	 * <p>
	 * If the SpellTargeting should target around the source, the location should be the entities currnet location.
	 * <p>
	 * @param source the entity that is the source of this Targeting Object. This is used for determining EntityStance's
	 */
	public SpellTargeting(Entity source)
	{		
		this.source = source;
		targetTypes = new TargetType[]{TargetType.PLAYER, TargetType.MONSTER};
		targetStances = new EntityStance[]{EntityStance.HOSTILE};
		targetGameModes = new GameMode[]{GameMode.SURVIVAL, GameMode.SURVIVAL};
		
		if(MagicAPI.getInstance().getConfig().getBoolean("Testing.SpellTargeting.creativeMode"))
		{
			targetGameModes = new GameMode[]{GameMode.SURVIVAL, GameMode.SURVIVAL, GameMode.CREATIVE};
		}
	}
	

	/**
	 * Creates a new targeting object.
	 * @param source 
	 * @param types The types that it targets, set to null to target all types and leave empty to target no types
	 * @param stances The stances that it targets, set to null to target all stances and leave empty to target no stances
	 */ 
	public SpellTargeting(Entity source, TargetType[] types, EntityStance[] stances)
	{
		this.source = source;
		targetTypes = types;
		targetStances = stances;
		
		if(targetTypes == null) targetTypes = TargetType.values();
		if(targetStances == null) targetStances = EntityStance.values();
	}
	
	
	
	/**
	 * Allows setting the TargetType that all targeting methods will utilized.
	 * This allows refinement of what entity classification is targeted. <p>
	 * 
	 * @see TargetType
	 * @param types The types that it targets, set to null to target all types and leave empty to target no types
	 */
	public void setTargetType(TargetType[] types)
	{
		targetTypes = types;
		if(targetTypes == null) targetTypes = TargetType.values();
	}
	
	
	/**
	 * Allows setting the EntityStance that all targeting methods will utilized.
	 * This allows refinement of what entity's is targeted. <p>
	 * 
	 * @see EntityStance
	 * @param stances The stances that it targets, set to null to target all stances and leave empty to target no stances
	 */
	public void setTargetStance(EntityStance[] stances)
	{
		targetStances = stances;
		if(targetStances == null) targetStances = EntityStance.values();
	}
	
	
	public TargetType[] getTargetTypes()
	{
		return targetTypes;
	}


	public EntityStance[] getTargetStances()
	{
		return targetStances;
	}
		

	public boolean isUseEvasion()
	{
		return useEvasion;
	}


	public void setUseEvasion(boolean useEvasion)
	{
		this.useEvasion = useEvasion;
	}


	public boolean isUseHitChance()
	{
		return useHitChance;
	}


	public void setUseHitChance(boolean useHitChance)
	{
		this.useHitChance = useHitChance;
	}
	


	
	
	/**
	 * Gets a list of all entities around the caster that are eligible for targeting. <br>
	 * The caster is not included.
	 * @param radius The radius to search for targets
	 * @return
	 */
	public List<LivingEntity> target_AOE_getNearbyEntities(double radius)
	{
		EntityStanceView esv = MagicAPI.getInstance().getEntityStanceView();
		
		List<LivingEntity> near = new ArrayList<LivingEntity>();
		if(radius <= 0) return near;
		if(source == null) return near;
		
		List<Entity> es = source.getNearbyEntities(radius, radius, radius);
		for(Entity e : es)
		{
			if(e instanceof LivingEntity)
			{
				boolean addT = false;
				boolean addS = false;
				if(e.getUniqueId() != source.getUniqueId())
				{					
					EntityStance stance = esv.getStance(source, e);
				
					//TODO Evasion and HitChance
					
					//Check to see if the target (e) is a Player, Monster, or Animal and is within targetTypes
					if(this.targetTypes == null) addT = true;
					else
					{
						if(e instanceof Monster && Arrays.asList(this.targetTypes).contains(TargetType.MONSTER)) addT = true;
						if(e instanceof Player && Arrays.asList(this.targetTypes).contains(TargetType.PLAYER)) addT = true;
						if(e instanceof Animals && Arrays.asList(this.targetTypes).contains(TargetType.ANIMAL)) addT = true;
					}
					//Check to see if the targets (e) and sources status to each other is within targetStances 
					if(this.targetStances == null) addS = true;
					else if(Arrays.asList(this.targetStances).contains(stance)) addS = true;
				}
				if(addT && addS)
				{
					near.add((LivingEntity)e);
				}
			}
		}
		return near;
	}
	
	//TODO
	/**
	 * Gets a wedge like area in front of the target. <p> This works by creating a triangle wedge
	 * with the point on the casting entity. This allows for cone like targeting but was much 
	 * Simpler to implement
	 * @param length
	 * @return
	 */
	public List<LivingEntity> target_AOE_getWedge(double length, double baseWidth)
	{
		List<LivingEntity> cone = new ArrayList<LivingEntity>();
		//TODO input size check, if cone too small return empty list
		if(length < 1) return cone;
		if(this.source == null) return cone;
		
		//
		// Create Targeting Triangle
		//		
		Location loc = source.getLocation();
		
		Polygon pg = null;
		if(source instanceof LivingEntity)
		{
			pg = ShapesUtill.getTriangle(((LivingEntity)(source)).getEyeLocation(), length,baseWidth);
		}
		else pg = ShapesUtill.getTriangle(source.getLocation(),length);
		
		//
		// Get entities within the triangle.
		//
		
		List<Entity> entities = source.getNearbyEntities(length, length, length);
		for(Entity e : entities)
		{
			if(e instanceof LivingEntity)
			{
				boolean in = ShapesUtill.isPointWithinShape(pg, new Point2D.Double(e.getLocation().getX(),e.getLocation().getZ()));
				if(in)
				{
					cone.add((LivingEntity)e);
				}
			}
		}
		
		//TODO Check entities TargetType
		
		//TODO Check entities Stances
		
		//TODO Evasion and HitChance
		
		return cone;
	}
	/*
	 * May work: //https:www.dummies.com/education/science/physics/how-to-find-a-vectors-magnitude-and-direction/
	 */
	
	
	
	
	/**
	 * 
	 * @param distance
	 * @return NULL if no entity is round else get the entity
	 */
	public LivingEntity target_Single_getEntityBeingLookedAt(double distance)
	{
		if(!(source instanceof LivingEntity)) return null;
		
		List<LivingEntity> ents = this.target_AOE_getNearbyEntities(distance);
		
		for(LivingEntity e: ents)
		{
			if(e instanceof LivingEntity)
			{
				if(getLookingAt((LivingEntity)source,(LivingEntity)e))
				{
					return (LivingEntity)e; //TODO Evasion and HitChance
				}
			}
		}		
		return null;
	}
	
	
	  private boolean getLookingAt(LivingEntity entity, LivingEntity entity1)
	  {
	    Location eye = entity.getEyeLocation();
	    Vector toEntity = entity1.getEyeLocation().toVector().subtract(eye.toVector());
	    double dot = toEntity.normalize().dot(eye.getDirection());
	   
	    return dot > 0.99D;
	  }
	
	/**
	 * Gets a location that is around the player. This is obtained by distance and degree's.
	 * This is 2D and does not take Y (height) into account. <p>
	 * This does not use targetTypes or targetStances <br>
	 * This does not use Evasion or Hit Chance <br>
	 * @param distance
	 * @param degrees
	 * @return The location or null if source is null;
	 */
	public Location target_Location_AroundEntity(double distance, double degrees)
	{
		if(source == null) return null;
		double radians = Math.toRadians(degrees);
		
		Location origin = source.getLocation();
		double originX = origin.getX();
		double originZ = origin.getZ();
		
		Location point = origin.add(0, 0, distance);
		
		double pointX = point.getX();
		double pointZ = point.getZ();
		
		double fX = originX + Math.cos(radians) * (pointX-originX) - Math.sin(radians)*(pointZ-originZ);
		double fZ = originZ + Math.sin(radians) * (pointX-originX) +  Math.cos(radians)*(pointZ-originZ);
		
		Location finL = new Location(origin.getWorld(),fX,origin.getY(),fZ);
		return finL;
	}
	/*
	 * 
	 * Note: another way to do this? (Location_AroundEntity)
	 * 
	 * ==Quote==
	 * new_x = x + distance * Math.Cos(angle_degrees * Math.Pi / 180)
	 * new_y = y + distance * Math.Sin(angle_degrees * Math.Pi / 180)
	 * Note that angle_degrees = "given direction" is measured from the positive x-axis moving toward the positive y-axis
	 * ==Quote=End==
	 * 
	 * From: https://stackoverflow.com/questions/41465581/move-point-in-cartesian-coordinate-through-distance-in-the-given-direction
	 */
	
	/**
	 * 
	 * @param entity
	 * @param distance
	 * @param degrees
	 * @return
	 */
	//What was this for?
	public Location target_Location_InDirectionFacing(LivingEntity entity, double distance, double degrees)
	{
		//TODO source and entity are the same, remove entity from args and change it to source
		if(source == null) return null;
		Location loc = entity.getEyeLocation();
		
		//90 is to offset mc to this.
		double angle_degrees = ShapesUtill.normalizeDegree(loc.getYaw() + 90+degrees);			
		
		
		double x1 = loc.getX() + distance * Math.cos(angle_degrees * Math.PI / 180);
		double z1 = loc.getZ() + distance * Math.sin(angle_degrees * Math.PI / 180);
		
		Location finL = new Location(loc.getWorld(),x1,loc.getY(),z1);
		return finL;
	}
}
