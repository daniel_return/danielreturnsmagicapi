package com.gmail.danielreturnweb.magicapi.spell;

/**
 * This represents data that is stored on a spell item.
 * @author daniel
 *
 */
public class SpellItemData
{
	protected String spellID = "";
	protected long cooldownEnd = 0;
	
	//ChargedSpell
	protected boolean isChargedSpell = false;
	protected int remainingCharges = 0;
	
	/**
	 * 
	 * @param spellID
	 * @param cooldownEnd
	 */
	public SpellItemData(String spellID, long cooldownEnd)
	{
		this.spellID = spellID;
		this.cooldownEnd = cooldownEnd;
	}
	
	/**
	 * Creates a SpellItemData for a charged spell item
	 * @param spellID
	 * @param cooldownEnd
	 * @param remainingCharges
	 */
	public SpellItemData(String spellID, long cooldownEnd,int remainingCharges)
	{
		this.spellID = spellID;
		this.cooldownEnd = cooldownEnd;
		
		this.isChargedSpell = true;
		this.remainingCharges = remainingCharges;
	}
	

	public int getRemainingCharges()
	{
		return remainingCharges;
	}

	public void setRemainingCharges(int remainingCharges)
	{
		this.remainingCharges = remainingCharges;
	}

	public long getCooldownEnd()
	{
		return cooldownEnd;
	}

	public void setCooldownEnd(long cooldownEnd)
	{
		this.cooldownEnd = cooldownEnd;
	}

	public String getSpellID()
	{
		return spellID;
	}

	public boolean isChargedSpell()
	{
		return isChargedSpell;
	}
}
