package com.gmail.danielreturnweb.magicapi.effectcomponents;

public interface EffectImmunity 
{
	public int getImmunityLevel();

}
