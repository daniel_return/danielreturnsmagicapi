package com.gmail.danielreturnweb.magicapi.effectcomponents;

/**
 * Marks the CustomEffect as regenerating Health Every update.
 * @author daniel
 * @deprecated Use RegenerationManager
 */
public interface Regeneration 
{
	public double getHealthRegeneration();
}

//TODO make a event that triggers upon HP regeneration.