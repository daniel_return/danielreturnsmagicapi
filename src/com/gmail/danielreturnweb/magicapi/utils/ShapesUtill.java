package com.gmail.danielreturnweb.magicapi.utils;

import java.awt.Polygon;
import java.awt.Shape;
import java.awt.geom.Point2D;

import org.bukkit.Location;

public class ShapesUtill
{
	
	public static boolean isPointWithinShape(Shape shape, Point2D p)
	{
		if(shape.contains(p)) return true;
		return false;
			
	}
	
	
	/**
	 * Generates a polygon with its point at the specified location. <br>
	 * This uses the locations yaw to form the triangle in the direction the
	 * the entity is facing.
	 * The polygon's x is MC x while its y is MC z.
	 * @param loc
	 * @param distance
	 * @return
	 */
	public static Polygon getTriangle(Location loc, double distance)
	{
		if(loc == null)
		{
			return new Polygon();
		}
		
		//90 is to offset mc to this.
		double angle_degrees = normalizeDegree(loc.getYaw() + 90+30);
		double angle_degrees2 = normalizeDegree(loc.getYaw() + 90-30);		
		double length = 5;		
		
		double x1 = loc.getX() + length * Math.cos(angle_degrees * Math.PI / 180);
		double z1 = loc.getZ() + length * Math.sin(angle_degrees * Math.PI / 180);

		double x2 = loc.getX() + length * Math.cos(angle_degrees2 * Math.PI / 180);
		double z2 = loc.getZ() + length * Math.sin(angle_degrees2 * Math.PI / 180);		
		
		Polygon pg = new Polygon();
		pg.addPoint(loc.getBlockX(),loc.getBlockZ());
		pg.addPoint((int)x1, (int)z1);
		pg.addPoint((int)x2, (int)z2);
		
		return pg;
	}
	
	public static Polygon getTriangle(Location loc, double distance, double angles)
	{
		if(loc == null) return new Polygon();
		
		//90 is to offset mc to this.
		double angle_degrees = normalizeDegree(loc.getYaw() + 90+(angles/2));
		double angle_degrees2 = normalizeDegree(loc.getYaw() + 90-(angles/2));		
		double length = 5;		
		
		double x1 = loc.getX() + length * Math.cos(angle_degrees * Math.PI / 180);
		double z1 = loc.getZ() + length * Math.sin(angle_degrees * Math.PI / 180);

		double x2 = loc.getX() + length * Math.cos(angle_degrees2 * Math.PI / 180);
		double z2 = loc.getZ() + length * Math.sin(angle_degrees2 * Math.PI / 180);		
		
		Polygon pg = new Polygon();
		pg.addPoint(loc.getBlockX(),loc.getBlockZ());
		pg.addPoint((int)x1, (int)z1);
		pg.addPoint((int)x2, (int)z2);
		
		return pg;
	}
	
	/**
	 * Takes in a degree and remaps it's value to a value between 0 and 359.
	 * (with 360 being mapped to 0).
	 * Example: a degree of -90 would be mapped to 270
	 * @param degree
	 * @return
	 */
	public static double normalizeDegree(double degree)
	{
		if(degree >=0 && degree <360) return degree;
		if(degree >=360) return degree-360;
		if(degree <0) return degree+360;
		return 0;
	}

}
