package com.gmail.danielreturnweb.magicapi.utils;

import java.math.BigDecimal;

/**
 * A API for truncating Doubles
 * @author daniel
 *
 */
public class DoubleTruncator 
{
	/**
	 * Truncates the double to the provided number of decimal places
	 * @param x
	 * @param numberofDecimals
	 * @return
	 */
	public static double truncateDouble(double x, int numberofDecimals)
	{
		return truncateDecimal(x,numberofDecimals).doubleValue();
	}
	
	/**
	 * Truncates the double to the provided number of decimal places but returns it as a BigDecimal
	 * @param x
	 * @param numberofDecimals
	 * @return
	 */
	public static BigDecimal truncateDecimal(double x,int numberofDecimals)
	{
	    if ( x > 0) {
	        return new BigDecimal(String.valueOf(x)).setScale(numberofDecimals, BigDecimal.ROUND_FLOOR);
	    } else {
	        return new BigDecimal(String.valueOf(x)).setScale(numberofDecimals, BigDecimal.ROUND_CEILING);
	    }
	}
}
