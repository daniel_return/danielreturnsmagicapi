package com.gmail.danielreturnweb.magicapi.utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

/**
 * A CustomConfig class based upon the Config Tutorials on Bukkits website. 
 * @author daniel
 *
 */
public class CustomizedConfiguration 
{
//	FileConfiguration data;
	
	private FileConfiguration customConfig = null;
	private File customConfigFile = null;
	
	private String fileName;
	private Plugin plugin;

//	/**
//	 * 
//	 * @param file The file to be selected normal "getDataFolder(),"file.yml"
//	 */
//	public CustomConfig(File file) 
//	{
//		this.data = YamlConfiguration.loadConfiguration(file);
//	}
	
	/**
	 * 
	 * @param plugin
	 * @param fileName The file name, not the path
	 */
	public CustomizedConfiguration(Plugin plugin, String fileName)
	{
		this.fileName = fileName;
		this.plugin = plugin;
	}
	
	
	/**
	 * Reloads the configeration file.
	 * @throws IOException
	 * @throws UnsupportedEncodingException
	 */
	public void reloadCustomConfig() throws IOException, UnsupportedEncodingException
	{
	    if (customConfigFile == null) 
	    {
	    	customConfigFile = new File(plugin.getDataFolder(), this.fileName);
	    	if(!customConfigFile.exists())
	    	{
	    		customConfigFile.createNewFile();
	    	}
	    }
	    customConfig = YamlConfiguration.loadConfiguration(customConfigFile);

	    // Look for defaults in the jar
	    Reader defConfigStream = new InputStreamReader(plugin.getResource(this.fileName), "UTF8");
	    if (defConfigStream != null) {
	        YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
	        customConfig.setDefaults(defConfig);
	    }
	}
	
	
	/**
	 * Gets the custom config file. Will reload if the config file object is null.
	 * @return
	 */
	public FileConfiguration getCustomConfig()
	{
	    if (customConfig == null) {
	        try {
				reloadCustomConfig();
			} catch (UnsupportedEncodingException e) 
			{
				e.printStackTrace();
				return null;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }
	    return customConfig;
	}
	
	/**
	 * Saves the custom config to file
	 */
	public void saveCustomConfig() 
	{
	    if (customConfig == null || customConfigFile == null) {
	        return;
	    }
	    try {
	        getCustomConfig().save(customConfigFile);
	    } catch (IOException ex) {
	        plugin.getLogger().log(Level.SEVERE, "Could not save config to " + customConfigFile, ex);
	    }
	}
	
	/**
	 * Saves the default configuration file from the plugin jar file to the disk
	 */
	public void saveDefaultConfig() 
	{
	    if (customConfigFile == null) {
	        customConfigFile = new File(plugin.getDataFolder(), fileName);
	    }
	    if (!customConfigFile.exists()) {            
	         plugin.saveResource(fileName, false);
	     }
	}
	
	/**
	 * Sees if the configuration file exists.
	 * @return
	 */
	public boolean exists()
	{
		return customConfigFile.exists();
	}

}
