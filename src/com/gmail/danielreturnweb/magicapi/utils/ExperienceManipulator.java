package com.gmail.danielreturnweb.magicapi.utils;

/**
 * A class to manipulate experience values <br>
 * Useful for manipulating bottle o' enchanting xp and other sources of xp.
 * 
 * <p> This is not used to get and set a players experience or level. This is used for calculating the experience and level of a player or xp source.
 * <p> This also contains some common experience values. Though they will be expanded to include all values between 1 and 40 in the future
 * <p> Note: i am NOT good at math, the formulas should be checked for correctness.
 * @author daniel
 *
 */
public class ExperienceManipulator 
{
	/** the xp required to have 5 levels */
	public static final int LEVEL_5 = 55;
	/** the xp required to have 20 levels */
	public static final int LEVEL_20 = 550;
	/** the xp required to have 40 levels */
	public static final int LEVEL_40 = 2920;
	//TODO flush this out to contain values 1-40
	
	public ExperienceManipulator(){}
	
	/**
	 * Converts a level value to xp.
	 * Formula obtained from Minecraft Wiki.
	 * @param level the level of experience to obtain.
	 * @return the amount of xp that makes up this level.
	 */
	public static int convertLevelToXP(int level)
	{
		if(level < 0) return 0;
		else if (level >= 0 && level <= 16)
		{
			return (int) (Math.pow(level, 2) + 6*level);
		}
		else if(level >= 17 && level <= 31)
		{
			return (int) (2.5*Math.pow(level, 2)-40.5*level+360);
		}
		else
		{
			return (int)(4.5*Math.pow(level, 2)-162.5*level+2220);
		}
	}
	
	/**
	 * Returns the xp required to obtain the next level.
	 * @param currentLevel
	 * @return
	 */
	public static int xpRequiredForNextLevel(int currentLevel)
	{
		int currentXP = ExperienceManipulator.convertLevelToXP(currentLevel);
		int nextLevelXP = ExperienceManipulator.convertLevelToXP(currentLevel + 1);
		return nextLevelXP - currentXP;
	}
	
	/**
	 * Calculates how much experience is needed to reach targetLevel from currentLevel
	 * @param currentLevel
	 * @param targetLevel
	 * @return returns 0 if levels are the same or targetLevel is less then currentLevel.
	 */
	public static int xpRequierdToAttainLevel(int currentLevel, int targetLevel)
	{
		if(currentLevel == targetLevel) return 0;
		if(currentLevel < targetLevel) return 0;
		
		int currentXP = ExperienceManipulator.convertLevelToXP(currentLevel);
		int targetXP = ExperienceManipulator.convertLevelToXP(targetLevel);
		return targetXP - currentXP;
	}

}
