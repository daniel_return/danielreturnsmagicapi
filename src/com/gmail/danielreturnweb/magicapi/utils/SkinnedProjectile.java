package com.gmail.danielreturnweb.magicapi.utils;

import java.util.List;

import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Snowball;
import org.bukkit.entity.ThrowableProjectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.util.Vector;

import com.gmail.danielreturnweb.magicapi.MagicAPI;
import com.gmail.danielreturnweb.magicapi.DamageSystem.CustomDamageType;
import com.gmail.danielreturnweb.magicapi.DamageSystem.DamageManager;

/**
 * This class offers very basic utilities for launching a projectile that is skinned as a item stack.
 * <p>
 * This works by launching a snow ball and setting its visual item to a provided item.
 * @author daniel
 *
 */
public class SkinnedProjectile implements Listener
{
	/**
	 * Launches a projectile from entity in the direction and power of vector.
	 * The projectile has its custom name set to name, and it is skinned to look like skin.
	 * <p>
	 * The projectile has bounce and gravity set to false by default.
	 * @param entity
	 * @param skin
	 * @param name
	 * @param vector
	 * @return
	 */
	public static ThrowableProjectile launchProjectile(LivingEntity entity, ItemStack skin, String name, Vector vector)
	{	
		ThrowableProjectile tProjectile = entity.launchProjectile(Snowball.class,vector);
		if(entity != null) tProjectile.setShooter(entity);
		tProjectile.setCustomName(name);
		tProjectile.setItem(skin);
		
		tProjectile.setBounce(false);
		tProjectile.setGravity(false);
		
		return tProjectile;
	}
	
	/**
	 * Launches a projectile from entity in the direction and power of vector.
	 * The projectile has its custom name set to name, and it is skinned to look like skin.
	 * <p>
	 * The projectile will deal damage upon contact with an entity. SkinnedProjectile Utility will handle the 
	 * damage using Bukkit Listeners and meta data.
	 * <p>
	 * The projectile has bounce and gravity set to false by default.
	 * @param entity
	 * @param skin
	 * @param name
	 * @param vector
	 * @param damage The damage the projectile will deal
	 * @return
	 */
	public static ThrowableProjectile launchAttackProjectile(LivingEntity entity, ItemStack skin, String name, Vector vector, double damage)
	{	
		ThrowableProjectile tProjectile = entity.launchProjectile(Snowball.class,vector);
		if(entity != null) tProjectile.setShooter(entity);
		tProjectile.setCustomName(name);
		tProjectile.setItem(skin);
		
		tProjectile.setBounce(false);
		tProjectile.setGravity(false);
		
		tProjectile.setMetadata("SkinnedProjectile_ProjectileDamage" , new FixedMetadataValue(MagicAPI.getInstance(),damage));
		
		return tProjectile;
	}
	
	/**
	 * Launches a projectile from entity in the direction and power of vector.
	 * The projectile has its custom name set to name, and it is skinned to look like skin.
	 * <p>
	 * The projectile will deal damage of a specific CustomDamageType upon contact with an entity.
	 * SkinnedProjectile Utility will handle the 
	 * damage using Bukkit Listeners and meta data.
	 * <p>
	 * The projectile has bounce and gravity set to false by default.
	 * @param entity
	 * @param skin
	 * @param name
	 * @param vector
	 * @param damage The damage the projectile will deal
	 * @param type A CustomDamageType or null for vanilla damage
	 * @return
	 */
	public static ThrowableProjectile launchAttackProjectile(LivingEntity entity, ItemStack skin, String name, Vector vector, double damage, CustomDamageType type)
	{	
		ThrowableProjectile tProjectile = entity.launchProjectile(Snowball.class,vector);
		if(entity != null) tProjectile.setShooter(entity);
		tProjectile.setCustomName(name);
		tProjectile.setItem(skin);
		
		tProjectile.setBounce(false);
		tProjectile.setGravity(false);
		
		tProjectile.setMetadata("SkinnedProjectile_ProjectileDamage" , new FixedMetadataValue(MagicAPI.getInstance(),damage));
		if(type != null) tProjectile.setMetadata("SkinnedProjectile_DamageType" , new FixedMetadataValue(MagicAPI.getInstance(),type.typeName()));
		
		return tProjectile;
	}
	
	
	
	
	
	
	@EventHandler
	public void projectileHit(ProjectileHitEvent event)
	{
		if(event.getEntityType() == EntityType.SNOWBALL)
		{
			ThrowableProjectile projectile = (ThrowableProjectile) event.getEntity();
			
			DamageManager dMan = MagicAPI.getInstance().getDamageManager();
			
			double dmg = 0;
			CustomDamageType type = null;
			LivingEntity target = null;
			
			Entity targ = event.getHitEntity();
			if(targ == null) return;
			if(targ instanceof LivingEntity)
			{
				target = (LivingEntity) targ;
			}
			if(target == null) return;
			
			
			if(projectile.hasMetadata("SkinnedProjectile_ProjectileDamage"))
			{
				List<MetadataValue> metaList = event.getEntity().getMetadata("SkinnedProjectile_ProjectileDamage");
				
				for(MetadataValue value : metaList)
				{
					dmg += value.asDouble();
				}
			}
			
			if(projectile.hasMetadata("SkinnedProjectile_DamageType"))
			{
				
				String typeName = event.getEntity().getMetadata("SkinnedProjectile_DamageType").get(0).asString();
				type = dMan.getDamageType(typeName);
			}
			
			if(dmg > 0)
			{
				if(type == null) //Vanilla Damage
				{
					if(projectile.getShooter() == null) target.damage(dmg);
					else 
					{
						if(projectile.getShooter() instanceof LivingEntity)	target.damage(dmg,(LivingEntity)projectile.getShooter());
						else target.damage(dmg);
					}
				}
				else //Custom Damage
				{
					if(projectile.getShooter() == null) dMan.dealDamage(type, target, dmg, false);
					else 
					{
						if(projectile.getShooter() instanceof LivingEntity)	dMan.dealDamage(type, target, (LivingEntity)projectile.getShooter(), dmg, false);
						else dMan.dealDamage(type, target, dmg, false);
					}
				}
			}
		}
	}
	
}
