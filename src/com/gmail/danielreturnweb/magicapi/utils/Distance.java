package com.gmail.danielreturnweb.magicapi.utils;

import org.bukkit.Location;

/**
 * A simple class that provides two methods for getting distance in a 2D plane
 * or a 3D cube
 * @author daniel
 */
public class Distance 
{
	/**
	 * Gets the distance between 2 location height (y) is ignored for this as its 2D
	 * @param a
	 * @param b
	 * @return
	 */
	public static double getDistence2D(Location a, Location b)
	{
		double x = Math.pow(b.getBlockX() - a.getBlockX(),2);
		double z = Math.pow(b.getBlockZ() - a.getBlockZ(),2);
		return Math.sqrt(x+z);
	}
	
	/**
	 * Gets the distance between two locations.
	 * @param a
	 * @param b
	 * @return
	 */
	public static double getDistence3D(Location a, Location b)
	{
		double x = Math.pow((b.getBlockX() - a.getBlockX()),2);
		double y = Math.pow((b.getBlockY() - a.getBlockY()),2);
		double z = Math.pow((b.getBlockZ() - a.getBlockZ()),2);
		return Math.sqrt(x+y+z);
	}
}
