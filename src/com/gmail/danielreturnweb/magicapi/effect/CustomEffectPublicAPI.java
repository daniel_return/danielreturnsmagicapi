package com.gmail.danielreturnweb.magicapi.effect;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import com.gmail.danielreturnweb.magicapi.MagicAPI;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffect;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffectAPI;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffectBundle;
import com.gmail.danielreturnweb.magicapi.effects.types.BuffType;
import com.gmail.danielreturnweb.magicapi.effects.types.EffectRemovalType;
import com.gmail.danielreturnweb.magicapi.effects.types.SelfStackType;

/*
 * TODO
 * Issues:
 * 	Bundles:
 * 		When getting bundle data it seems to be unmutable/changeable this prevents effects from using
 * 		events, runnables, or other external triggers to change the effects bundle data. (i.e. when making a effect
 * 		that provides regen when out of combat by setting it to 0 on EntityDamageByEntityEvent).
 * 		
 * 		It also seems when placing an effect while that effect already exists, the bundle data is not copied properly.
 * 		(For StackTypes that need to copy the data)
 * 
 * TODO
 * What still needs to be done
 *  Events
 *  	OnInflictEvent
 *  	OnRemoveEvent
 *  	OnUpdateEvent ??
 *  	OnTickEvent ??
 */

/**
 * A simple implementation of the CustomEffectAPI interface
 * @author daniel
 *
 */

public class CustomEffectPublicAPI implements CustomEffectAPI
{	
//	Map<String,CustomEffect> customEffects;

	Map<UUID,List<CustomEffectBundle>> effectTracker;
	
	public CustomEffectPublicAPI() 
	{
//		customEffects = new HashMap<String,CustomEffect>();
		effectTracker = new HashMap<UUID,List<CustomEffectBundle>>();
	}

	/**
	 * @deprecated use the Custom Effect Manager
	 * @param effect
	 * @return
	 */
	public boolean registerCustomEffect(CustomEffect effect) 
	{
		return CustomEffectManager.getInstance().registerCustomEffect(effect);
	}
	
	/**
	 * @deprecated use the Custom Effect Manager
	 * @param id
	 * @return
	 */
	protected CustomEffect getRegisteredEffect(String id) 
	{
		return CustomEffectManager.getInstance().getRegisteredEffect(id);
	}
	
	@Override
	public CustomEffectBundle createCustomEffectBundle(CustomEffect e)
	{
		CustomEffectBundle b = new EffectBundle();
		
		b.setRemainingDuration(e.getEffectDuration());
		b.setEffectID(e.getEffectID());
		b.setLevel(1);
		b.setBoost(0);
		
		return b;
	}
	
	@Override
	public CustomEffectBundle createCustomEffectBundle(String string)
	{
		CustomEffect effect = this.getRegisteredEffect(string);
		if(effect != null) return this.createCustomEffectBundle(effect);
		return null;
	}
	
	
	private boolean internalPlaceEffect(LivingEntity target, LivingEntity inflictor, CustomEffect effect, int level, CustomEffectBundle bundle)
	{
		boolean placed = false;
//		if(entity instanceof Player) System.out.println("UUID: " + entity.getUniqueId());
			
		//Create Bundle
		CustomEffectBundle newBundle = bundle;
		
		if(newBundle == null)
		{
			newBundle = new EffectBundle();
			newBundle.setLevel(level);
			newBundle.setRemainingDuration(effect.getEffectDuration());
			newBundle.setEffectID(effect.getEffectID());
			if(inflictor != null)
			{
				if(inflictor instanceof Player) newBundle.setBoost(MagicAPI.getInstance().getMagicBoostManager().getMagicBoost((Player)inflictor));
				else newBundle.setBoost(MagicAPI.getInstance().getMagicBoostManager().getMagicBoost(inflictor));	
			}
		}		
		
		
		//Get custom effects on entity.
		List<CustomEffectBundle> efs = new ArrayList<CustomEffectBundle>();
		if(this.effectTracker.containsKey(target.getUniqueId())) efs = this.effectTracker.get(target.getUniqueId());
		
		//If effect already is inflicted and do stacking procedures.
		for(int i = efs.size() - 1; i >=0; i--)
		{
			//See if same effect
			if(efs.get(i).getEffectID().equals(effect.getEffectID()))
			{
				//Do stack types
				SelfStackType sst = effect.getSelfStackType();
				if(sst == SelfStackType.REPLACES)
				{
					effect.onRemoval(target, efs.get(i), EffectRemovalType.Stack);
					//Copies the effect bundle data over to new effect.
					//Does type check in case effect user implemented there own CustomEffectBundle
					
					//TODO if a effect is being replaced, its old data should be thrown out not kept (as it is now)
//					if(efs.get(i) instanceof EffectBundle && newBundle instanceof EffectBundle)
//					{((EffectBundle)newBundle).data = ((EffectBundle)efs.get(i)).data;} 
					efs.remove(i);
					break;
				}
				else if(sst == SelfStackType.SEPERATE){} //place holder. Nothing needs to happen here
				else if(sst == SelfStackType.COMBIND_LEVEL)
				{
					newBundle.setLevel(newBundle.getLevel() + efs.get(i).getLevel());
					
					//Copies the effect bundle data over to new effect.
					//Does type check in case effect user implemented there own CustomEffectBundle
					if(efs.get(i) instanceof EffectBundle && newBundle instanceof EffectBundle)
					{((EffectBundle)newBundle).data = ((EffectBundle)efs.get(i)).data;}
					
					effect.onRemoval(target, efs.get(i), EffectRemovalType.Stack);
					efs.remove(i);
					break;
				}
				else if(sst == SelfStackType.COMBIND_DURATION)
				{
					newBundle.setRemainingDuration(newBundle.getRemainingDuration() + efs.get(i).getRemainingDuration());
					
					//Copies the effect bundle data over to new effect.
					//Does type check in case effect user implemented there own CustomEffectBundle
					if(efs.get(i) instanceof EffectBundle && newBundle instanceof EffectBundle)
					{((EffectBundle)newBundle).data = ((EffectBundle)efs.get(i)).data;}
					
					effect.onRemoval(target, efs.get(i), EffectRemovalType.Stack);
					efs.remove(i);
					break;
				}
				else if(sst == SelfStackType.COMBIND_FULL)
				{
					newBundle.setLevel(newBundle.getLevel() + efs.get(i).getLevel());
					newBundle.setRemainingDuration(newBundle.getRemainingDuration() + efs.get(i).getRemainingDuration());
					
					//Copies the effect bundle data over to new effect.
					//Does type check in case effect user implemented there own CustomEffectBundle
					if(efs.get(i) instanceof EffectBundle && newBundle instanceof EffectBundle)
					{((EffectBundle)newBundle).data = ((EffectBundle)efs.get(i)).data;}
					
					effect.onRemoval(target, efs.get(i), EffectRemovalType.Stack);
					efs.remove(i);
					break;
				}
				else if(sst == SelfStackType.REFRESH_DURATION)
				{
					//This just uses the bundle provided but copies over
					//the old bundles data.
					
					//Copies the effect bundle data over to new effect.
					//Does type check in case effect user implemented there own CustomEffectBundle
					if(efs.get(i) instanceof EffectBundle && newBundle instanceof EffectBundle)
						{((EffectBundle)newBundle).data = ((EffectBundle)efs.get(i)).data;}
					
					effect.onRemoval(target, efs.get(i), EffectRemovalType.Stack);
					efs.remove(i);
					break;
				}
				else if(sst == SelfStackType.KEEP_EXISTING) //The already existing effect is kept discarding the one being placed.
				{
					return false;
				}
			}
		}
		
		//Call the effects onInflict
		effect.onInflict(target, inflictor, newBundle);
		if(!newBundle.getEnded()) //If ended, do not add it to the tracker.
		{
			efs.add(newBundle);
			this.effectTracker.put(target.getUniqueId(), efs);
			placed = true;
		}
		
		return placed;
	}
	
	

	@Override
	public boolean placeEffect(LivingEntity entity, CustomEffect effect) 
	{
		return this.internalPlaceEffect(entity, null, effect, 1, null);
	}

	@Override
	public boolean placeEffect(LivingEntity entity, CustomEffect effect, int level) 
	{
		return this.internalPlaceEffect(entity, null, effect, level, null);
	}

	@Override
	public boolean placeEffect(LivingEntity entity, CustomEffect effect, CustomEffectBundle bundle) 
	{
		return this.internalPlaceEffect(entity, null, effect, 1, bundle);
	}
	
	
	
	public boolean placeEffect(LivingEntity entity,LivingEntity inflicter, CustomEffect effect)
	{
		return this.internalPlaceEffect(entity, inflicter, effect, 1, null);
	}
	
	public boolean placeEffect(LivingEntity entity,LivingEntity inflicter, CustomEffect effect,int level)
	{
		return this.internalPlaceEffect(entity, inflicter, effect, level, null);
	}
	
	public boolean placeEffect(LivingEntity entity,LivingEntity inflicter, CustomEffect effect,CustomEffectBundle bundle)
	{
		return this.internalPlaceEffect(entity, inflicter, effect, 1, bundle);
	}

	
	
	@Override
	public boolean placeEffect(LivingEntity entity, String id) 
	{
		CustomEffect effect = this.getRegisteredEffect(id);
		if(effect == null) return false;
		return this.internalPlaceEffect(entity, null, effect, 1, null);
	}

	@Override
	public boolean placeEffect(LivingEntity entity, String id, int level) 
	{
		CustomEffect effect = this.getRegisteredEffect(id);
		if(effect == null) return false;
		return this.internalPlaceEffect(entity, null, effect, level, null);
	}

	@Override
	public boolean placeEffect(LivingEntity entity, String id, CustomEffectBundle bundle) 
	{
		CustomEffect effect = this.getRegisteredEffect(id);
		if(effect == null) return false;
		return this.internalPlaceEffect(entity, null, effect, 1, bundle);
	}

	@Override
	public void removeEffect(LivingEntity entity, CustomEffect effect) 
	{
		List<CustomEffectBundle> effs = this.effectTracker.get(entity.getUniqueId());
		if(effs == null || effs.size() <= 0) return;
		
		for(int i = effs.size()-1; i >= 0; i--)
		{
			if(effs.get(i).getEffectID().equals(effect.getEffectID()))
			{
				CustomEffect ef = this.getRegisteredEffect(effs.get(i).getEffectID());
				ef.onRemoval(entity, effs.get(i), EffectRemovalType.System);
				effs.remove(i);
			}
		}
		this.effectTracker.put(entity.getUniqueId(), effs);
	}

	@Override
	public void removeEffect(LivingEntity entity, String id) 
	{
		List<CustomEffectBundle> effs = this.effectTracker.get(entity.getUniqueId());
		if(effs == null || effs.size() <= 0) return;
		
		for(int i = effs.size()-1; i >= 0; i--)
		{
			if(effs.get(i).getEffectID().equals(id))
			{
				CustomEffect ef = this.getRegisteredEffect(effs.get(i).getEffectID());
				ef.onRemoval(entity, effs.get(i), EffectRemovalType.System);
				effs.remove(i);
			}
		}
		this.effectTracker.put(entity.getUniqueId(), effs);
	}

	@Override
	public void removeEffect(LivingEntity entity, CustomEffect effect, EffectRemovalType type)
	{
		List<CustomEffectBundle> effs = this.effectTracker.get(entity.getUniqueId());
		if(effs == null || effs.size() <= 0) return;
		
		for(int i = effs.size()-1; i >= 0; i--)
		{
			if(effs.get(i).getEffectID().equals(effect.getEffectID()))
			{
				CustomEffect ef = this.getRegisteredEffect(effs.get(i).getEffectID());
				ef.onRemoval(entity, effs.get(i), type);
				effs.remove(i);
			}
		}
		this.effectTracker.put(entity.getUniqueId(), effs);
	}

	@Override
	public void removeEffect(LivingEntity entity, String id, EffectRemovalType type) 
	{
		List<CustomEffectBundle> effs = this.effectTracker.get(entity.getUniqueId());
		if(effs == null || effs.size() <= 0) return;
		
		for(int i = effs.size()-1; i >= 0; i--)
		{
			if(effs.get(i).getEffectID().equals(id))
			{
				CustomEffect ef = this.getRegisteredEffect(effs.get(i).getEffectID());
				ef.onRemoval(entity, effs.get(i), type);
				effs.remove(i);
			}
		}
		this.effectTracker.put(entity.getUniqueId(), effs);
		
	}

	@Override
	public List<CustomEffectBundle> getEffectData(LivingEntity entity, CustomEffect effect)
	{
		List<CustomEffectBundle> list = new ArrayList<CustomEffectBundle>();
		
		if(!this.effectTracker.containsKey(entity.getUniqueId())) return list;
		if(this.effectTracker.get(entity.getUniqueId()) == null) return list;
		
//		for(CustomEffectBundle b: this.effectTracker.get(entity.getUniqueId()))
//		{
//			if(b.getEffectID().equals(effect.getEffectID()))
//			{
//				list.add(b);
//			}
//		}
		//TODO this is a test
		//It seems bundles obtained with this method are not references? at least they don't seem to be mutatable
		//They should be. More research requiered
		List<CustomEffectBundle> k = this.effectTracker.get(entity.getUniqueId());
		for(int i = 0; i < k.size(); i++)
		{
			if(k.get(i).getEffectID().equals(effect.getEffectID()))
				{
					list.add(k.get(i));
				}
		}
		return list;
	}

	@Override
	public List<CustomEffectBundle> getEffectData(LivingEntity entity, String id)
	{
		List<CustomEffectBundle> list = new ArrayList<CustomEffectBundle>();
		
		if(!this.effectTracker.containsKey(entity.getUniqueId())) return list;
		if(this.effectTracker.get(entity.getUniqueId()) == null) return list;
		
		for(CustomEffectBundle b: this.effectTracker.get(entity.getUniqueId()))
		{
			if(b.getEffectID().equals(id))
			{
				list.add(b);
			}
		}
		return list;
	}

	@Override
	public Map<String, CustomEffectBundle> getAllEffectData(LivingEntity entity) 
	{
//		if(entity instanceof Player) System.out.println("UUID: " + entity.getUniqueId());
		
		Map<String,CustomEffectBundle> maps = new HashMap<String,CustomEffectBundle>();
		if(!this.effectTracker.containsKey(entity.getUniqueId())) return maps;
		
		List<CustomEffectBundle> effs = this.effectTracker.get(entity.getUniqueId());
//		System.out.println("EffectTracker effect is Null?: " + (effs == null));
		
		for(CustomEffectBundle effect : effs)
		{
			maps.put(effect.getEffectID(), effect);
		}
		return maps;
	}
	
	public void removeExpiredEffects(LivingEntity entity)
	{
		List<CustomEffectBundle> effs = this.effectTracker.get(entity.getUniqueId());
		if(effs == null || effs.size() <= 0) return;
		
		
		for(int i = effs.size()-1; i >= 0; i--)
		{
			if(effs.get(i).getEnded() || effs.get(i).getRemainingDuration() <= 0)
			{
				CustomEffect ef = this.getRegisteredEffect(effs.get(i).getEffectID());
				if(ef != null)
				{
					ef.onRemoval(entity, effs.get(i), EffectRemovalType.End); //TODO copy the null check to other removal methods
					effs.remove(i);
				}
			}
		}
		this.effectTracker.put(entity.getUniqueId(), effs);
	}

	@Override
	public void removeAllEffects(LivingEntity entity) 
	{
		if(this.effectTracker.containsKey(entity.getUniqueId()))
		{
			List<CustomEffectBundle> effs = this.effectTracker.get(entity.getUniqueId());
			for(CustomEffectBundle ceb : effs)
			{
				CustomEffect ef = this.getRegisteredEffect(ceb.getEffectID());
				ef.onRemoval(entity, ceb, EffectRemovalType.System);
			}
			
			this.effectTracker.remove(entity.getUniqueId());
			this.effectTracker.put(entity.getUniqueId(), new ArrayList<CustomEffectBundle>());
		}
	}
	
	public void removeNonDeathPersistentEffects(LivingEntity entity)
	{
		List<CustomEffectBundle> effs = this.effectTracker.get(entity.getUniqueId());
		if(effs == null || effs.size() <= 0) return;
		
		for(int i = effs.size()-1; i >= 0; i--)
		{
			CustomEffect ef = this.getRegisteredEffect(effs.get(i).getEffectID());
			if(ef != null)
			{
				if(!ef.persistsAfterDeath())
				{
					ef.onRemoval(entity, effs.get(i), EffectRemovalType.Death);
					
					effs.get(i).setEnded(true);
				}
			}
		}
		this.effectTracker.put(entity.getUniqueId(), effs);
	}

	@Override
	public void removeAllEffectsByBuffType(LivingEntity entity, BuffType type) 
	{
		this.removeAllEffectsByBuffType(entity, type,EffectRemovalType.Dispell,0);
	}

	
	@Override
	public void removeAllEffectsByBuffType(LivingEntity entity, BuffType bt, EffectRemovalType ert, int strength)
	{
		List<CustomEffectBundle> effs = this.effectTracker.get(entity.getUniqueId());
		if(effs == null || effs.size() <= 0) return;
		
		
		for(int i = effs.size()-1; i >= 0; i--)
		{
			CustomEffect ef = this.getRegisteredEffect(effs.get(i).getEffectID());
			if(ef.getBuffType() == bt)
			{
				if(ef.getPurgeResistence() <= strength && ef.getPurgeResistence() != 0)
				{
					ef.onRemoval(entity, effs.get(i), ert);
//					effs.remove(i);
					effs.get(i).setEnded(true);
				}
			}
		}
		this.effectTracker.put(entity.getUniqueId(), effs);
	}
}
