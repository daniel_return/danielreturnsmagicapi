package com.gmail.danielreturnweb.magicapi.effect;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.RegisteredServiceProvider;

import com.gmail.danielreturnweb.magicapi.MagicAPI;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffect;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffectAPI;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffectBundle;
import com.gmail.danielreturnweb.magicapi.effect.api.Tickable;
import com.gmail.danielreturnweb.magicapi.effect.api.Updatable;

/**
 * This manages all custom effects. This runs there onUpdate, onPlayerJoin, onPlayerLeave, and tracks player's deaths for
 * managing effects that are removed upon death. <p>
 *  
 * Calls onUpdate() once every 20 ticks as a SyncRepeatingTask.
 *  
 * @author daniel
 *
 */
public class CustomEffectManager implements Listener, Runnable 
{
	private static CustomEffectManager instance = null;	
	public static final CustomEffectManager getInstance()
	{
		if(instance == null) CustomEffectManager.instance = new CustomEffectManager();
		return instance;
	}
	
	private final long START_DELAY = 20*15;
	private final long REPEAT = 1;
	private long lastUpdate = 0;
	private int debugCount = 0;
	
	Map<String, CustomEffect> customEffects;
	CustomEffectAPI ceapi = null;
	EffectScoreboard board;

	public CustomEffectManager() 
	{
		this.customEffects = new HashMap<String, CustomEffect>();
		
		RegisteredServiceProvider<CustomEffectAPI> rsp = Bukkit.getServicesManager().getRegistration(CustomEffectAPI.class);
		if(rsp != null) ceapi = rsp.getProvider();
		
		board = new EffectScoreboard();
		Bukkit.getScheduler().scheduleSyncRepeatingTask(MagicAPI.getInstance(), this,START_DELAY, REPEAT);
		
		Bukkit.getPluginManager().registerEvents(this, MagicAPI.getInstance());
		
		CustomEffectManager.instance = this;
	}
	
	
	/**
	 * Registers a custom effect.
	 * @param effect
	 * @return
	 */
	public boolean registerCustomEffect(CustomEffect effect) 
	{
		if(customEffects.containsKey(effect.getEffectID())) return false;
		customEffects.put(effect.getEffectID(), effect);
		return true;
	}
	
	/**
	 * Gets a custom effect that has been registered.
	 * @param id
	 * @return
	 */
	public CustomEffect getRegisteredEffect(String id) 
	{
		for(Entry<String,CustomEffect> e : customEffects.entrySet())
		{
			if(e.getKey().equals(id))
			{
				return e.getValue();
			}
		}
		return null;
	}
	
	
	
	/*
	 * Example
	 * entitydamageentityevent...
	 * getbundle (public api)
	 * 	pass into effect
	 * setBundle (public api) - put modified bundle back onto the entity.
	 */
	
//	@EventHandler
//	public void onEntityAttack(EntityDamageByEntityEvent event)
//	{
//		Entity attacker = event.getDamager();
//		Entity defender = event.getEntity();
//		
//		if(!(attacker instanceof LivingEntity)) return;
//		if(!(defender instanceof LivingEntity)) return;
//		
//		if(ceapi == null) return;
//		
//		
//		
//		
//	}
	
	
	@EventHandler
	public void onEntityDeath(EntityDeathEvent event)
	{
		if(!(event.getEntity()instanceof LivingEntity)) return;
		ceapi.removeNonDeathPersistentEffects(event.getEntity());
		
	}
	
	@EventHandler 
	public void onPlayerLogIn(PlayerJoinEvent event)
	{
		event.getPlayer().setScoreboard(this.board.getPlayersScoreboard(event.getPlayer()));
		
		for(Entry<String,CustomEffectBundle> b : ceapi.getAllEffectData(event.getPlayer()).entrySet())
		{
			CustomEffect e = this.getRegisteredEffect(b.getValue().getEffectID());
			e.onJoin(event.getPlayer(), b.getValue(), event);
		}
	}
	
	@EventHandler 
	public void onPlayerQuit(PlayerQuitEvent event)
	{
		for(Entry<String,CustomEffectBundle> b : ceapi.getAllEffectData(event.getPlayer()).entrySet())
		{
			CustomEffect e = this.getRegisteredEffect(b.getValue().getEffectID());
			e.onQuit(event.getPlayer(), b.getValue(), event);
		}
	}
	
	
	@Override
	public void run()
	{
		if(ceapi == null) return;

		//TODO change the bukkit runnable to update every tick.
		if(System.currentTimeMillis() >= this.lastUpdate)
		{
			onUpdate();
			this.lastUpdate = System.currentTimeMillis() + 1000;
		}
		onTick();
		
		//TODO (WIP) #2 will have the loop trigger two different update methods.
		//The first is the standard update() which is triggered every second
		//and the second updates every tick. While this second one is more
		//CPU intensive, it will allow for better cosmetics for effects
		//In alignment with this the update() method for effects is now in the interface
		//Updatable while tick() is in Tickable()
		//NOTE: right now the enitre run method is the update(). I will make two sub methods
		//one for update and one for tick that run() will call

	}
	
	private void onUpdate()
	{
//		debugCount++;
		long st = System.currentTimeMillis();
		int efC = 0;
		int enC = 0;
		int enN = 0;
		int plN = 0;
		
		//TODO since i now storing effect data in a list/map and not in each living entity
		//I can just loop though the list instead of every world.
		//However differing implementations of the CustomEffectAPI will do this differently,
		//So this why while inefficient, is the most reliable. 
		
		for(World world : Bukkit.getWorlds())
		{
			/*
			 * ====================
			 * 		Players
			 * ====================
			 */
			for(Player p : world.getPlayers())
			{
				Map<String,CustomEffectBundle> entityEffects = ceapi.getAllEffectData(p);
				
//				p.sendMessage("effs is Null?: " + (entityEffects == null));
//				p.sendMessage("effs size: " + entityEffects.size());
				
				//Update Effects
				if(entityEffects != null && entityEffects.size() > 0)
				{
					plN++;
					for(Entry<String,CustomEffectBundle> ent : entityEffects.entrySet())
					{
						efC++;
						CustomEffect ef = this.getRegisteredEffect(ent.getKey());
						if(ef != null && ef instanceof Updatable) ((Updatable)ef).onUpdate(p, ent.getValue());
						
						//Set isEnded if current duration is <= 0
						if(ent.getValue().getRemainingDuration() <= 0)
						{
							ent.getValue().setEnded(true);
						}
					}
				}
				
				//Update Players board
				board.updateScoreboard(p);
				
				//Remove expired (ended) effects	
				ceapi.removeExpiredEffects(p);
			}
			
			/*
			 * ====================
			 * 		Monsters
			 * ====================
			 */
			for(LivingEntity e: world.getLivingEntities())
			{
				Map<String,CustomEffectBundle> entityEffects = ceapi.getAllEffectData(e);
				
				//TODO may make it effect all but animals and players
				//This would allow villagers to be inflicted, and it would be up to the spell/ability/placer of the effect
				//to not target villagers (as they are friendly)
				//Then positive effects could be used on villagers
				if(e instanceof Monster || e.getType() == EntityType.ARMOR_STAND) //TODO what entities are ticked
				{
					//Update Effects
					if(entityEffects != null && entityEffects.size() > 0)
					{
						plN++;
						for(Entry<String,CustomEffectBundle> ent : entityEffects.entrySet())
						{
							efC++;
							CustomEffect ef = this.getRegisteredEffect(ent.getKey());
							if(ef != null && ef instanceof Updatable) ((Updatable)ef).onUpdate(e, ent.getValue());
							
							//Set isEnded if current duration is <= 0
							if(ent.getValue().getRemainingDuration() <= 0)
							{
								ent.getValue().setEnded(true);
							}
						}
					}
					
					//Remove expired (ended) effects	
					ceapi.removeExpiredEffects(e);
				}
			}
		}
		
		long stn = System.currentTimeMillis();
		if(debugCount >= 10)
		{
			System.out.println("================================================================");
			System.out.println("DanielsLibrary-EffectAPI-Update | Effected Players:  " + plN);
			System.out.println("DanielsLibrary-EffectAPI-Update | Skipped  Entities: " + enN);
			System.out.println("DanielsLibrary-EffectAPI-Update | Effected Entities: " + enC);
			System.out.println("DanielsLibrary-EffectAPI-Update | Number of Effects: " + efC);
			System.out.println("DanielsLibrary-EffectAPI-Update | Millis Taken:      " + (stn-st));
			debugCount = 0;
		}
	}
	
	private void onTick()
	{
		for(World world : Bukkit.getWorlds())
		{
			/*
			 * ====================
			 * 		Players
			 * ====================
			 */
			for(Player p : world.getPlayers())
			{
				Map<String,CustomEffectBundle> entityEffects = ceapi.getAllEffectData(p);
				
//				p.sendMessage("effs is Null?: " + (entityEffects == null));
//				p.sendMessage("effs size: " + entityEffects.size());
				
				//Update Effects
				if(entityEffects != null && entityEffects.size() > 0)
				{
					for(Entry<String,CustomEffectBundle> ent : entityEffects.entrySet())
					{
						CustomEffect ef = this.getRegisteredEffect(ent.getKey());
						if(ef != null && ef instanceof Tickable) ((Tickable)ef).onTick(p, ent.getValue());
						
						//Set isEnded if current duration is <= 0
						if(ent.getValue().getRemainingDuration() <= 0)
						{
							ent.getValue().setEnded(true);
						}
					}
				}
				
				//Update Players board
				board.updateScoreboard(p);
				
				//Remove expired (ended) effects	
				ceapi.removeExpiredEffects(p);
			}
			
			/*
			 * ====================
			 * 		Monsters
			 * ====================
			 */
			for(LivingEntity e: world.getLivingEntities())
			{
				Map<String,CustomEffectBundle> entityEffects = ceapi.getAllEffectData(e);
				
				//TODO may make it effect all but animals and players
				//This would allow villagers to be inflicted, and it would be up to the spell/ability/placer of the effect
				//to not target villagers (as they are friendly)
				//Then positive effects could be used on villagers
				if(e instanceof Monster || e.getType() == EntityType.ARMOR_STAND) //TODO what entities are ticked
				{
					//Update Effects
					if(entityEffects != null && entityEffects.size() > 0)
					{
						for(Entry<String,CustomEffectBundle> ent : entityEffects.entrySet())
						{
							CustomEffect ef = this.getRegisteredEffect(ent.getKey());
							if(ef != null && ef instanceof Tickable) ((Tickable)ef).onTick(e, ent.getValue());
							
							//Set isEnded if current duration is <= 0
							if(ent.getValue().getRemainingDuration() <= 0)
							{
								ent.getValue().setEnded(true);
							}
						}
					}
					//Remove expired (ended) effects	
					ceapi.removeExpiredEffects(e);
				}
			}
		}
	}
}
