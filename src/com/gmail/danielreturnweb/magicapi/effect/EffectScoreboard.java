package com.gmail.danielreturnweb.magicapi.effect;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffect;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffectAPI;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffectBundle;


public class EffectScoreboard 
{
	CustomEffectAPI ceapi = null;
	
	Scoreboard board;
	Objective objective;
	
	private Map<UUID,Scoreboard> playerBoards;
	
	
	public EffectScoreboard() 
	{
		RegisteredServiceProvider<CustomEffectAPI> rsp = Bukkit.getServicesManager().getRegistration(CustomEffectAPI.class);
		if(rsp != null)
		{
			ceapi = rsp.getProvider();
		}
		
		
		board = Bukkit.getScoreboardManager().getNewScoreboard();
		objective = board.registerNewObjective("CustomEffects", "dummy", "Custom Effects");
		objective.setDisplaySlot(DisplaySlot.SIDEBAR);
		
		
		playerBoards = new HashMap<UUID,Scoreboard>();
		
	
	}
	
	private CustomEffectAPI getCEAPI()
	{
		if(ceapi == null)
		{
			RegisteredServiceProvider<CustomEffectAPI> rsp = Bukkit.getServicesManager().getRegistration(CustomEffectAPI.class);
			if(rsp != null)
			{
				ceapi = rsp.getProvider();
				
			}
		}
		return ceapi;
	}
	
	
	private boolean createPlayerScoreboard(OfflinePlayer player)
	{
		if(this.playerBoards.containsKey(player.getUniqueId())) return false;
		
		Scoreboard b = Bukkit.getScoreboardManager().getNewScoreboard();
		b.registerNewObjective("CustomEffects", "dummy", "Custom Effects");
		b.getObjective("CustomEffects").setDisplaySlot(DisplaySlot.SIDEBAR);
		
		playerBoards.put(player.getUniqueId(), b);
		return true;
	}
	
	public Scoreboard getPlayersScoreboard(OfflinePlayer player)
	{
		if(!this.playerBoards.containsKey(player.getUniqueId())) this.createPlayerScoreboard(player);
		
		return this.playerBoards.get(player.getUniqueId());
	}
	
	/**
	 * Removes a effect from the player's scoreboard.
	 * @param player
	 * @param e
	 */
	public void removeDisplayedEffect(OfflinePlayer player, String effectID)
	{
		Scoreboard b = this.getPlayersScoreboard(player);
		b.resetScores(effectID);
	}
	
	public void clearScoreboard(OfflinePlayer player)
	{
		Scoreboard b = this.getPlayersScoreboard(player);
		for(String s : b.getEntries())
		{
			b.resetScores(s);
		}
	}
	
	public void destoryScoreboard(Player player)
	{
		if(!this.playerBoards.containsKey(player.getUniqueId())) return;
		
		this.clearScoreboard(player);
		player.setScoreboard(Bukkit.getScoreboardManager().getMainScoreboard());
		this.playerBoards.remove(player.getUniqueId());
	}

	
	/**
	 * Updates the scoreboard for the selected player
	 * @param p
	 */
	public void updateScoreboard(Player player)
	{
		if(getCEAPI() == null) return;
		
		
		if(!this.playerBoards.containsKey(player.getUniqueId()))
		{
			this.createPlayerScoreboard(player);
			player.setScoreboard(this.getPlayersScoreboard(player));
		}
		
		Scoreboard pb = this.getPlayersScoreboard(player);
		
		int i = 0;
		Map<String,CustomEffectBundle> em = ceapi.getAllEffectData(player);
		
		for(Entry<String,CustomEffectBundle> vals : em.entrySet())
		{
			//Get the custom effect object
			CustomEffect ce = CustomEffectManager.getInstance().getRegisteredEffect(vals.getKey());
			if(ce != null && ce.isDisplayed()) //If it display's, then display it
			{
//				Score ss = objective.getScore("<Effect ID here>: " /* + duration*/);
//				ss.setScore(i); 
					
				//If not ended, update
				Score s = pb.getObjective("CustomEffects").getScore(vals.getKey());
				s.setScore(vals.getValue().getRemainingDuration()/20);
				
				if(vals.getValue().getEnded())
				{
					pb.resetScores(vals.getKey());
				}
				
				/*
				 * its "score" this just allows me to order effects, There info will be in the name.
				 * Higher numbers display higher on the board...
				 */
				i++;
			}
		}
	}
}
