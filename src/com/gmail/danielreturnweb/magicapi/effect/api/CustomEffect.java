package com.gmail.danielreturnweb.magicapi.effect.api;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.gmail.danielreturnweb.magicapi.effects.types.BuffType;
import com.gmail.danielreturnweb.magicapi.effects.types.EffectRemovalType;
import com.gmail.danielreturnweb.magicapi.effects.types.SelfStackType;


/**
 * This is the definition of an effect. The effects data is kept as a CustomEffectBundle. This bundle stores the data
 * of an active effect.
 * 
 * <p> This is a bare bones CustomEffect.
 * <br> functionality can be flushed out by listening for events. (note you must implement listener and register your listener) 
 * 
 * <p> To make a instant effect (one that activates and is then removed (smiler to minecrafts instant damage/heal),
 * set its associated bundle's ended to true and the effect will activate, but not linger. Setting duration to 0 may achieve a similar effect.
 * 
 * <p> Lastly custom effects <b> DO NOT BY <i> DEFAULT </i> UPDATE </b>. To have a effect that updates
 * once per second use the interface Updatable. To have an effect that updates per tick use the interface Tickable.
 * <br> Lastly effects that provide a gain over time should use managers such as the Regeneration Manager which handles
 * applying regeneration to the target every second instead of implementing Updatable and healing in that method.
 * @see Updatable
 * @see Tickable 
 * @author daniel
 *
 */
public interface CustomEffect
{
	/**Gets the effects ID*/
	public String getEffectID();
	
	/** does the effect display on the scoreboard */
	public boolean isDisplayed();
	
	/**Gets the effects duration in ticks*/
	public int getEffectDuration();
	
	
	/**
	 * If the entity dies, does the effect persist after they die
	 * @return true if it persists, false if it is removed (onRemoval is called with EffectRemovalType.Death)
	 */
	public boolean persistsAfterDeath();
	
	/** Gets the BuffType of this effect */
	public BuffType getBuffType();
	
//	/** Gets the Stacking Type of this effect*/
//	public StackType getStackType();
	//Have yet to decide how I want to do stacking.
	/** The purge resistance of this effect.
	 * A level of 0 means it cannot be purged.
	 *  <br> Note: Milk has a purge strength of 1.
	 */
	public int getPurgeResistence();
	
	/** */
	public SelfStackType getSelfStackType();
	
	/*
	 * How would this work? If any of the effects exist does it not get applied? 
	 * If it exists and a conflicting one is applied does it not get applied?
	 * Do they co-exist, but the first one in the effect list, effect's are ran, but then not the rest?
	 * 		Requiring more looping in the onUpdate Method's and onInflict. Effect maker would need to then
	 * 		Manually check for conflicts on any Listeners they have.
	 */
//	/**
//	 * A list of effects that this effect will not stack with.
//	 * @return
//	 */
//	public CustomEffect[] doesNotStackWith();
	
	
	/**
	 * Gets the Effects entry for the Opus command. <p>
	 * Can use ChatColor codes
	 * @return
	 */
	public String[] getOpusEntry();
	
	/**
	 * Called when the effect is placed upon a entity
	 * @param effectHolder the entity with the effect on them.
	 * @param inflicter the Living Entity that inflicted the effectHolder with this effect, else null.
	 * @param bundle
	 */
	public void onInflict(LivingEntity effectHolder, LivingEntity inflicter, CustomEffectBundle bundle);
	
	/**
	 * Called when a effect is removed from the entity.
	 * @param effectHolder
	 * @param bundle
	 * @param type
	 */
	public void onRemoval(LivingEntity effectHolder,CustomEffectBundle bundle, EffectRemovalType type);
	
	
	/**
	 * Called when the player joins the server and still has effects on them.
	 *  <br>
	 * Useful to reply any permanent stat changes to the player.
	 * @param effectHolder
	 * @param bundle
	 * @param event
	 */
	public void onJoin(Player effectHolder, CustomEffectBundle bundle, PlayerJoinEvent event);
	
	/**
	 * Called when the player leaves the server and still has effects on them
	 * The effect is not removed from the player.
	 * <br>
	 * Useful to remove any permanent stat changes from the player
	 * @param effectHolder
	 * @param bundle
	 * @param event
	 */
	public void onQuit(Player effectHolder, CustomEffectBundle bundle, PlayerQuitEvent event);
	
//	/**
//	 * Called when a effect is inflicted while another one is active.
//	 */
//	public void onStack(...)
}
