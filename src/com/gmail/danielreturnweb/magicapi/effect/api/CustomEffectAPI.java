package com.gmail.danielreturnweb.magicapi.effect.api;

import java.util.List;
import java.util.Map;

import org.bukkit.entity.LivingEntity;

import com.gmail.danielreturnweb.magicapi.effects.types.BuffType;
import com.gmail.danielreturnweb.magicapi.effects.types.EffectRemovalType;


/**
 * The API that users will interact with when placing and removing effects on an entity.
 * This translates between the CustomEffectBundle and the stored form of the effect.
 * @author daniel
 *
 */
public interface CustomEffectAPI
{
//	/**Registers a custom effect.**/
//	public boolean registerCustomEffect(CustomEffect effect);
//	
//	/**Gets a Custom Effect Object from the Registered Custom Effects */
//	public CustomEffect getRegisteredEffect(String id);
	
	/**A factory method to create a custom effect bundle */
	CustomEffectBundle createCustomEffectBundle(CustomEffect e);
	CustomEffectBundle createCustomEffectBundle(String string);
	
	
	boolean placeEffect(LivingEntity entity,CustomEffect effect);
	boolean placeEffect(LivingEntity entity,CustomEffect effect,int level);
	boolean placeEffect(LivingEntity entity,CustomEffect effect,CustomEffectBundle bundle);
	
	boolean placeEffect(LivingEntity entity,LivingEntity inflicter, CustomEffect effect);
	boolean placeEffect(LivingEntity entity,LivingEntity inflicter, CustomEffect effect,int level);
	boolean placeEffect(LivingEntity entity,LivingEntity inflicter, CustomEffect effect,CustomEffectBundle bundle);
	
	boolean placeEffect(LivingEntity entity, String id);
	boolean placeEffect(LivingEntity entity, String id, int level);
	boolean placeEffect(LivingEntity entity, String id, CustomEffectBundle bundle);
	
	//TODO make my implementation remove the effect from the scoreboard
	void removeEffect(LivingEntity entity, CustomEffect effect);
	void removeEffect(LivingEntity entity,String id);
	void removeEffect(LivingEntity entity,CustomEffect effect,EffectRemovalType type);
	void removeEffect(LivingEntity entity,String id,EffectRemovalType type);
	
	
	List<CustomEffectBundle> getEffectData(LivingEntity entity, CustomEffect effect);                
	List<CustomEffectBundle> getEffectData(LivingEntity entity, String id);
	
	
	/** Gets all effect's on this entity. Returns a map<EffectID-String,EffectData-CustomEffectBundle>
	 * @param entity
	 * @return
	 */
	Map<String,CustomEffectBundle> getAllEffectData(LivingEntity entity);
	
	void removeExpiredEffects(LivingEntity entity);
	
	void removeAllEffects(LivingEntity entity);
	
	/**
	 * Removes all effects that do not persist after death. <br>
	 * 
	 * @param entity
	 */
	void removeNonDeathPersistentEffects(LivingEntity entity);
	
	void removeAllEffectsByBuffType(LivingEntity entity, BuffType type);
	
	/**
	 * 
	 * @param entity
	 * @param bt
	 * @param ert
	 * @param strength the strength of the removal. Will not purge any effect with a resistance of 0.
	 */
	void removeAllEffectsByBuffType(LivingEntity entity, BuffType bt, EffectRemovalType ert, int strength);
	
	
	//TODO provide a standard way to copy the bundle data from 1 bundle to another
	//This will aid in preventing the CustomEffectAPI from being strongly bound to
	//CustomEffectBundle implementations (as the default one currently is)
	//This will have a toGeneric() and fromGeneric(generic) methods
	//Converting the bundle into some generic data type possibly a map
	//This would also allow Bundles to be converted between different bundle implementations
	//TODO (See above todo)
}
