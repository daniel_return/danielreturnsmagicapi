package com.gmail.danielreturnweb.magicapi.effect.api;

import org.bukkit.entity.LivingEntity;

/**
 * Marks a custom effect as being updated once per second.
 * If the effect needs to be updated faster consider using
 * Tickable.
 * @see Tickable
 * @author daniel
 *
 */
public interface Updatable
{
	/**
	 * Is called periodically. Must be registered to function.
	 * @param entity the entity that is effected by the effect
	 * @param CustomEffectBundle
	 * @param updateDelay the time spent in between each update. Provided incase CustomEffectAPI implementations have differing delays. [Should allways be 1 second/20 ticks]
	 */
	public void onUpdate(LivingEntity effectHolder,CustomEffectBundle bundle);
}
