package com.gmail.danielreturnweb.magicapi.effect.api;

import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Listener;

import com.gmail.danielreturnweb.magicapi.effects.types.EffectRemovalType;
import com.gmail.danielreturnweb.magicapi.effects.types.StackType;


/**
 * This is the definition of a effect. Effect instances will not be there own object instead it will be metadata
 * that is saved with the LivingEntity using Persistent Meta Data. When a effect is ticked on a entity its effect
 * id is grabbed and its data is then put though the effects appropriate methods.
 * 
 * The effect has methods for triggering parts of the effect when its placed, removed, and updated. It also includes a few methods
 * for triggering the effect when the entity deals damage or takes damage. May also provide other methods in the future. For any other
 * triggers such as triggering on healing, events will need to be used.
 * @author daniel
 *@deprecated
 */
public abstract class CustomEffectOld implements Listener
{
	protected String ID = "";
	protected int duration = 0;
	protected StackType stackType = StackType.Unknown;
	
	
	
	/**If the effect should be removed upon death of the player*/
	protected boolean removedOnDeath = true;
	
	/** If this effect is displayed in the scoreboard viewer */
	protected boolean displayed = true;
	
	
	
	public CustomEffectOld() 
	{
		
	}
	
	public String getID()
	{
		return ID;
	}
	
	
	/*
	 * ===========================================
	 * NON-OPTIONAL OVERRIDES
	 * ===========================================
	 */
	
	
	
	//Updates once every 20 ticks
	/**
	 * 
	 * @param entity the entity that is effected by the effect
	 */
	public abstract void onUpdate(LivingEntity effectHolder,LivingEntity inflicter,CustomEffectBundle bundle);
	
	
	/**
	 * Called when the effect is placed upon a entity
	 */
	public abstract void onInflict(LivingEntity entity, LivingEntity inflicter, CustomEffectBundle bundle);
	
	/**
	 * Called when a effect is removed from the entity
	 * @param entity
	 */
	public abstract void onRemoval(LivingEntity effectHolder,CustomEffectBundle bundle, EffectRemovalType type);
	
	
	/*
	 * ===========================================
	 * OPTIONAL OVERRIDES
	 * ===========================================
	 */
	
	
	
//	/**
//	 * Called by the effect manager when the effected entity takes damage
//	 */
//	public void onDamageTaken(){};
//	
//	/**
//	 * Called by the effect manager when the effected entity deals damage
//	 */
//	public void onDamageDealt(){};
	
}
