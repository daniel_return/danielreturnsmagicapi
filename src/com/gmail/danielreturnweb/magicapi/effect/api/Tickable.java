package com.gmail.danielreturnweb.magicapi.effect.api;

import org.bukkit.entity.LivingEntity;

/**
 * Marks a custom effect as being ticked once per second. <p>
 * If an effect only needs to be updated once per second, consider using
 * Updatable instead.
 * @see Updatable
 * @author daniel
 *
 */
public interface Tickable
{
	/**
	 * Is called once per tick. Must be registered to function.
	 * @param entity the entity that is effected by the effect
	 * @param CustomEffectBundle
	 */
	public void onTick(LivingEntity effectHolder,CustomEffectBundle bundle);
}
