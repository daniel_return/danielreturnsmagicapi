package com.gmail.danielreturnweb.magicapi.effect.api;

import java.util.List;
import java.util.Map.Entry;
import java.util.UUID;

/**
 * CustomEffectBundle is the state of a custom effect on an entity.
 * <p>
 * It has the basic state of the effect and allows the effect to place additional data
 * within the bundle.
 * @author daniel
 *
 */
public interface CustomEffectBundle 
{
	public void setEffectID(String id);
	public String getEffectID();
	
	/**Sets the remaining duration of the effect*/
	public void setRemainingDuration(int duration);
	/**Adds the provided value to the remaining duration
	 * @param duration the value to add
	 */
	public void addRemainingDuration(int duration);
	public int getRemainingDuration();
	
	/**Sets the level of the Effect*/
	public void setLevel(int level);
	public int getLevel();
	
	/** Sets if the effect has ended. Ended effects are no longer active and will be removed.*/
	public void setEnded(boolean ended);
	public boolean getEnded();
	
	/**Sets the boost level this effect has
	 * Normally set upon being placed on a entity, and is based off of the players magic boost.*/
	public void setBoost(int boost);
	public int getBoost();
	
	
	public void setDataInt(String key,int value);
	public void setDataDouble(String key,double value);
	public void setDataLong(String key,long value);
	//public void setDataOther(String key, Object value);
	public void setDataUUID(String key, UUID uuid);
	
	
	public int getDataInt(String key);
	public double getDataDouble(String key);
	public long getDataLong(String key);
	//public Object getDataOther(String key);
	public UUID getDataUUID(String key);
	
	
	public boolean hasDataInt(String key);
	public boolean hasDataDouble(String key);
	public boolean hasDataLong(String key);
	//public Object getDataOther(String key);
	public boolean hasDataUUID(String key);
	
	
	
	/** Returns a list containing all key/value pairs as a Entry. */
	public List<Entry<String,Object>> getAllData();
	
	
}
