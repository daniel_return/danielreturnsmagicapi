package com.gmail.danielreturnweb.magicapi.effect;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffectBundle;


public class EffectBundle implements CustomEffectBundle 
{	
	Map<String, Object> data;
	
	String id = "";
	int level = 1;
	int duration = 20*10;
	boolean ended = false;
	int magicBoost = 0;
	
	long ends;

	
	
	public EffectBundle() 
	{
		data = new HashMap<String,Object>();
		ends = System.currentTimeMillis()+1000; //default is 1 second
	}
	
	public void setEffectID(String id)
	{
		this.id = id;
	}
	public String getEffectID()
	{
		return this.id;
	}

	@Override
	public void setRemainingDuration(int duration) 
	{
		this.ends = System.currentTimeMillis() + (duration/20)*1000;
		this.duration = duration;
	}
	
	public void addRemainingDuration(int duration)
	{
		this.ends = this.ends + (duration/20)*1000;
	}

	@Override
	public int getRemainingDuration() 
	{
		long dur = 0;
		int rem = 0;
		
		dur = this.ends - System.currentTimeMillis();
		if(dur <= 0) return 0;
		
		rem = (int) Math.round((dur/1000)*20);
		
		return rem;
	}

	@Override
	public void setLevel(int level) 
	{
		this.level = level;

	}

	@Override
	public int getLevel() 
	{
		return this.level;
	}

	@Override
	public void setEnded(boolean ended) 
	{
		this.ended = ended;

	}

	@Override
	public boolean getEnded() 
	{
		return this.ended;
	}

	@Override
	public void setBoost(int boost) 
	{
		this.magicBoost = boost;

	}

	@Override
	public int getBoost() 
	{
		return this.magicBoost;
	}

	@Override
	public void setDataInt(String key, int value) 
	{
		this.data.put(key, value);

	}

	@Override
	public void setDataDouble(String key, double value) 
	{
		this.data.put(key, value);

	}

	@Override
	public void setDataLong(String key, long value) 
	{
		 this.data.put(key, value);

	}
	public void setDataUUID(String key, UUID uuid)
	{
		this.data.put(key, uuid);
	}
	

	@Override
	public int getDataInt(String key) 
	{
		if(this.data.containsKey(key))
		{
			int d = (int) this.data.get(key);
			return d;
		}
		return 0;
	}

	@Override
	public double getDataDouble(String key) 
	{
		if(this.data.containsKey(key))
		{
			double d = (double) this.data.get(key);
			return d;
		}
		return 0;
	}

	@Override
	public long getDataLong(String key) 
	{
		if(this.data.containsKey(key))
		{
			long d = (long) this.data.get(key);
			return d;
		}
		return 0;
	}
	
	@Override
	public UUID getDataUUID(String key) 
	{
		if(this.data.containsKey(key))
		{
			UUID d = (UUID) this.data.get(key);
			return d;
		}
		return null;
	}


	@Override
	public List<Entry<String, Object>> getAllData() 
	{
		List<Entry<String,Object>> ld = new ArrayList<Entry<String,Object>>();
		for(Entry<String,Object> en : data.entrySet())
		{
			ld.add(en);
		}
		return ld;
	}

	@Override
	public boolean hasDataInt(String key) 
	{
		if(this.data.containsKey(key)) return true;
		return false;
	}

	@Override
	public boolean hasDataDouble(String key) 
	{
		if(this.data.containsKey(key)) return true;
		return false;
	}

	@Override
	public boolean hasDataLong(String key) 
	{
		if(this.data.containsKey(key)) return true;
		return false;
	}

	@Override
	public boolean hasDataUUID(String key) 
	{
		if(this.data.containsKey(key)) return true;
		return false;
	}	
}
