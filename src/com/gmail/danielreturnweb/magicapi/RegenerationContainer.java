package com.gmail.danielreturnweb.magicapi;

import com.gmail.danielreturnweb.magicapi.RegenerationManager.RegenerationType;

public class RegenerationContainer
{
	private double value;
	private RegenerationType type;
	
	
	public RegenerationContainer(double value, RegenerationType type)
	{
		super();
		this.value = value;
		this.type = type;
	}


	public double getValue()
	{
		return value;
	}


	public void setValue(double value)
	{
		this.value = value;
	}


	public RegenerationType getType()
	{
		return type;
	}


	public void setType(RegenerationType type)
	{
		this.type = type;
	}

	


}
