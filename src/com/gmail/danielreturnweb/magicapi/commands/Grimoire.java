package com.gmail.danielreturnweb.magicapi.commands;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.NullArgumentException;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.event.block.Action;

import com.gmail.danielreturnweb.magicapi.MagicAPI;
import com.gmail.danielreturnweb.magicapi.spell.SpellManager;
import com.gmail.danielreturnweb.magicapi.spells.spell.ChargedSpell;
import com.gmail.danielreturnweb.magicapi.spells.spell.DurabilitySpell;
import com.gmail.danielreturnweb.magicapi.spells.spell.PassiveSpell;
import com.gmail.danielreturnweb.magicapi.spells.spell.PassiveSpell.PassiveSlot;
import com.gmail.danielreturnweb.magicapi.spells.spell.Spell;
import com.gmail.danielreturnweb.magicapi.spells.spell.SpellCommand;
import com.gmail.danielreturnweb.magicapi.spells.spell.SpellItem;
import com.gmail.danielreturnweb.magicapi.spells.spell.SpellItem.CastHand;

/**
 * A command that allows players to list all spells that are registered with the spell manager.
 * Also allows viewing of each spells description and other associated information.
 * <p> Currently this class, methods, and the command is untested
 * @author daniel
 *
 */
public class Grimoire implements CommandExecutor, TabCompleter
{
	public static final ChatColor TITLE_CHATCOLOR = ChatColor.YELLOW;
	public static final ChatColor FIELD_CHATCOLOR = ChatColor.AQUA;
	public static final ChatColor TEXT_CHATCOLOR = ChatColor.GRAY;
	public static final ChatColor LIST_CHATCOLOR = ChatColor.GREEN;
	
	public static final ChatColor ENTRY_CHATCOLOR = ChatColor.GRAY;
	public static final ChatColor ERROR_CHATCOLOR = ChatColor.RED;
	
	public Grimoire()
	{
		// TODO Auto-generated constructor stub
	}
	
	
	/**
	 * Displays the spell to the selected CommandSender
	 * @param sender
	 * @param spell
	 */
	private void displaySpell(CommandSender sender, Spell spell)
	{
//		if(sender == null) throw new IllegalArgumentException("CommandSender cannot be null when using Grimoire.displaySpell(sender,spell)");
		if(sender == null) throw new NullArgumentException("CommandSender cannot be null when using Grimoire.displaySpell(sender,spell)");
		if(spell == null) throw new NullArgumentException("Spell cannot be null when using Grimoire.displaySpell(sender,spell)");

		String name = spell.getSpellName();
		int cooldown = spell.getSpellCooldown();
		int xpCost = spell.getSpellXPCost();
		String[] entry = spell.getGrimoireEntry();
		
		sender.sendMessage("  ");
		sender.sendMessage(TITLE_CHATCOLOR + "Grimoire Entry - " + ChatColor.WHITE + name);
		
		sender.sendMessage(FIELD_CHATCOLOR + "Coodlown: " + TEXT_CHATCOLOR + cooldown + " seconds");
		sender.sendMessage(FIELD_CHATCOLOR + "XP Cost: " + TEXT_CHATCOLOR + xpCost + "");
		
				
		if(spell instanceof ChargedSpell)
		{
			ChargedSpell cs = (ChargedSpell) spell;
			int charges = cs.getSpellCharges();
			sender.sendMessage(FIELD_CHATCOLOR + "Charges: " + TEXT_CHATCOLOR + charges);
		}
		
		if(spell instanceof DurabilitySpell)
		{
			DurabilitySpell ds = (DurabilitySpell) spell;
			int cost = ds.getCastDurabilityCost();
			sender.sendMessage(FIELD_CHATCOLOR + "Durability Cost: " + TEXT_CHATCOLOR + cost);
		}
		
		if(spell instanceof PassiveSpell)
		{
			sender.sendMessage(FIELD_CHATCOLOR + "Cast Type: " + TEXT_CHATCOLOR +  "PASSIVE"); //What should <field?> be called?
			PassiveSpell ps = (PassiveSpell) spell;
			PassiveSlot[] slots = ps.getPassiveSlots();
//			CustomEffect effect = ps.getPassiveAbility();

			StringBuilder builder = new StringBuilder();
			for(PassiveSlot slot : slots)
			{
				builder.append(slot.toString() + ", ");
			}
			
			sender.sendMessage(FIELD_CHATCOLOR + "Passive Slots: " + TEXT_CHATCOLOR + builder.toString());
		}
		else 
		{
			sender.sendMessage(FIELD_CHATCOLOR + "Cast Type: " + TEXT_CHATCOLOR +  "ACTIVE");
		}
		
		if(spell instanceof SpellItem)
		{
			SpellItem si = (SpellItem) spell;
			CastHand hand = si.getCastHand();
			Action[] actions = si.getCastActions();
			
			sender.sendMessage(FIELD_CHATCOLOR + "Cast Hand: " + TEXT_CHATCOLOR + hand);
			
			StringBuilder builder = new StringBuilder();
			if(actions != null)
			{
				for(Action act : actions)
				{
					builder.append(act.toString() + ", ");
				}
			}
			sender.sendMessage(FIELD_CHATCOLOR + "Cast Action(s): " + TEXT_CHATCOLOR + builder.toString());
		}
		if(spell instanceof SpellCommand)
		{
			SpellCommand sc = (SpellCommand) spell;
			String alias = sc.getAlias();
			
			sender.sendMessage(FIELD_CHATCOLOR + "Command Alias: " + TEXT_CHATCOLOR + alias);
		}
		
		sender.sendMessage(TITLE_CHATCOLOR + "Entry: ");
		if(entry != null)
		{
			for(String s : entry)
			{
				sender.sendMessage("  " + Grimoire.ENTRY_CHATCOLOR + s);
			}
			sender.sendMessage("  ");
		}
		else
		{
			sender.sendMessage("  " + Grimoire.ENTRY_CHATCOLOR + "No Entry");
		}
	}
	
	
	/**
	 * Provides a list of Spells to the CommandSender
	 * @param sender
	 */
	private void displaySpellList(CommandSender sender)
	{
		if(sender == null) throw new NullArgumentException("CommandSender cannot be null when using Grimoire.displaySpellList(sender)");		
		
		SpellManager sm = MagicAPI.getInstance().getSpellManager();
		List<Spell> spells = sm.getSpells();
		
		sender.sendMessage("  ");
		sender.sendMessage(TITLE_CHATCOLOR + "Grimoire Spell List");
		for(Spell spell : spells)
		{
			if(!(spell instanceof GrimoireHideable)) sender.sendMessage(LIST_CHATCOLOR + "" + spell.getSpellName());
		}
	}

	/*
	 * /grimoire list
	 * /grimoire view <spellName>
	 * 
	 * 
	 */
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) 
	{
		SpellManager manager = MagicAPI.getInstance().getSpellManager();
		
		if(args.length == 0 || (args.length == 1 && args[0].equalsIgnoreCase("help")))
		{
			sender.sendMessage(TITLE_CHATCOLOR + "Grimoire Help");
			sender.sendMessage(FIELD_CHATCOLOR + "/Grimoire list  " + TEXT_CHATCOLOR + " Lists all spells");
			sender.sendMessage(FIELD_CHATCOLOR + "/Grimoire view [spell]  " + TEXT_CHATCOLOR + "Allows the viewing of a spells entry and infomation");
		}
		
		if(args.length == 1 && args[0].equalsIgnoreCase("list"))
		{
			this.displaySpellList(sender);
		}
		
		if(args.length >= 2 && args[0].equalsIgnoreCase("view"))
		{
			StringBuilder builder = new StringBuilder();
			for(int i = 1; i < args.length; i++)
			{
				builder.append(args[i] + " ");
			}
			
			Spell spell = null;
			
			List<Spell> spells = manager.getSpellFromName(builder.toString().trim());
			if(spells == null || spells.size() == 0)
			{
				sender.sendMessage(ERROR_CHATCOLOR + "No Spell Error:  " + TEXT_CHATCOLOR + " the spell cannot be found");
				return false;
			}
			
			if(spells.size() > 1)
			{
				sender.sendMessage(FIELD_CHATCOLOR + "Multiple spells with this name were found, displaying the first one only");
			}
			
			spell = spells.get(0);
			if(spell == null)
			{
				sender.sendMessage(ERROR_CHATCOLOR + "Null Error:  " + TEXT_CHATCOLOR + " item 0 in spell list is NULL");
				if(spell == null) throw new NullArgumentException("Spell in spell list is null. Grimoire.onCommand(...) view");
			}
			/*
			 * TODO how to handle multiple spells w/ the same name when displaying?
			 * Should this feature be removed and not allow spells with the same name?
			 * Should it be handled like a mail plugin with multiple pages?
			 * 	If so it would not display the spell info and instead tell the sender that there are multiple spells
			 * 	with the same name and how many there are. It would then say to do "/grimoire view <spell> <#>"
			 */
			
			this.displaySpell(sender, spell);
		}
		return false;
	}
	
	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args)
	{
		//TODO when showing the spell list to the user it needs to respect GrimoireHideable
		
		List<String> words = new ArrayList<String>();
		
		if(args.length == 0)
		{
			words.add("list");
			words.add("view");
		}
		if(args.length == 1)
		{
			if("list".startsWith(args[0])) words.add("list");
			if("view".startsWith(args[0])) words.add("view");
		}
		
		if(args.length >= 1 && args[0].equalsIgnoreCase("view"))
		{
			SpellManager sm = MagicAPI.getInstance().getSpellManager();
			List<Spell> spells = sm.getSpells();
			
			for(Spell spell : spells)
			{
				if(!(spell instanceof GrimoireHideable))
				{
					words.add(spell.getSpellName());
				}
			}
		}
		
		if(args.length >= 2 && args[0].equalsIgnoreCase("view"))
		{
			StringBuilder builder = new StringBuilder();
			for(int i = 1; i < args.length; i++)
			{
				builder.append(args[i] + " ");
			}
			
			
			SpellManager sm = MagicAPI.getInstance().getSpellManager();
			List<Spell> spells = sm.getSpells();
			
			for(Spell spell : spells)
			{
				if(!(spell instanceof GrimoireHideable))
				{
					if(spell.getSpellName().toLowerCase().startsWith(builder.toString().toLowerCase()))
					words.add(spell.getSpellName());
				}
			}
		}
		
		return words;
	}
}
