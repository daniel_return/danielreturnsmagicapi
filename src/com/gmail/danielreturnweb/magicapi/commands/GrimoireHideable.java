package com.gmail.danielreturnweb.magicapi.commands;

/**
 * If a spell has this interface, it will be hidden from the Grimoire list sub command
 * @author daniel
 *
 */
public interface GrimoireHideable
{

}
