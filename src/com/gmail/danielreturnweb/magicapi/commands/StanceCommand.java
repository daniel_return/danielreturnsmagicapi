package com.gmail.danielreturnweb.magicapi.commands;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import com.gmail.danielreturnweb.magicapi.EntityStanceView;
import com.gmail.danielreturnweb.magicapi.MagicAPI;
import com.gmail.danielreturnweb.magicapi.EntityStanceView.EntityStance;
import com.gmail.danielreturnweb.magicapi.RegenerationManager.RegenerationType;
import com.gmail.danielreturnweb.magicapi.implementation.Relation;

public class StanceCommand implements  CommandExecutor, TabCompleter
{
	
	/**
	 * A map of pending relations. The UUID is the requesting player.
	 */
	Map<UUID,Relation> pendingRelations;
	
	

	public StanceCommand()
	{
		pendingRelations = new HashMap<UUID,Relation>();
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) 
	{
		EntityStanceView stanceView = MagicAPI.getInstance().getEntityStanceView();
		
		
		//*****************
		//Server Console
		//*****************
		if(!(sender instanceof Player))
		{
			//TODO a command so console can set two players stance
			return false;
		}
		
		//*****************
		//Player
		//*****************
		Player player = (Player) sender;
		/*
		 * Commands
		 *   /stance help
		 *   /stance view <player>   Views the stance of the <player> and the command using player
		 *   /stance set <player> <stance>  Sets this players stance with the other player.
		 *   /stance accept <player>   Accepts the stance change that <player> requested
		 *   /stance adminset <player1> <player2> <stance> sets the stance between two player's
		 */
		
		if(args.length == 0 || (args.length == 1 && args[0].equalsIgnoreCase("help")))
		{
			ChatColor cmdC = ChatColor.AQUA;
			ChatColor desC = ChatColor.LIGHT_PURPLE;
			ChatColor otherC = ChatColor.GRAY;
			
//			player.sendMessage("");
			player.sendMessage(ChatColor.GREEN + "Stance Help & Infomation");
			player.sendMessage(cmdC + "/stance help " + desC + "  Brings up this Help & Infomation");
			player.sendMessage(cmdC + "/stance view <player>" + desC + "  Views the stance between the <player> and yourself");
			player.sendMessage(cmdC + "/stance set <player>" + desC + "  Sets your stance with <player>");
			player.sendMessage(cmdC + "/stance accept <player>" + desC + "  Accepts the stance change initiated by <player>");
			
			if(player.hasPermission("magicapi.stance.admin"))
			{
				player.sendMessage(cmdC + "/stance adminset <player1> <player2>" + desC + "  Sets the stance between two players");
			}
			
			player.sendMessage(otherC + "If a stance is set higher then the current stance, then both players must agree to the change");
			player.sendMessage(otherC + "If a stance is set lower, it will set without both players agreeing.");
			player.sendMessage(otherC + "Valid Stances: " + 
			EntityStance.HOSTILE + ", " + EntityStance.NEUTRAL + ", " + EntityStance.FRIENDLY);
		}
		
		if(args.length == 2 && args[0].equalsIgnoreCase("view"))
		{
			OfflinePlayer target = Bukkit.getOfflinePlayer(args[1]);
			if(target == null || !target.hasPlayedBefore())
			{
				player.sendMessage(ChatColor.GREEN + "[Stance] " + ChatColor.RED + "Error: Player does not exist");
				return false;
			}
			
			EntityStance stance = stanceView.getStance(player, target);
			player.sendMessage(ChatColor.GREEN + "[Stance] " + ChatColor.YELLOW + "Your stance with " + args[1] + " is " + stance.coloredString());
			
		}
		else if(args.length == 3 && args[0].equalsIgnoreCase("set"))
		{
			OfflinePlayer target = Bukkit.getOfflinePlayer(args[1]);
			
			if(target == null || !target.hasPlayedBefore())
			{
				player.sendMessage(ChatColor.GREEN + "[Stance] " + ChatColor.RED + "Error: Player does not exist");
				return false;
			}
			
			if(player.getUniqueId() == target.getUniqueId())
			{
				player.sendMessage(ChatColor.GREEN + "[Stance] " + ChatColor.RED + "Error: Cannot set stance with yourself");
				return false;
			}
			
			
			EntityStance current = stanceView.getStance(player.getPlayer(), target);
			EntityStance stance = EntityStance.valueOf(args[2].toUpperCase());
			
			
			//If the new stance is more friendly then the current stance, the other player must agree to the new stance.
			if(stance.getValue() > current.getValue())
			{
				Calendar now = Calendar.getInstance();
				Calendar last = Calendar.getInstance();
				last.setTimeInMillis(target.getLastPlayed());
				
				int days = now.get(Calendar.DAY_OF_YEAR) - last.get(Calendar.DAY_OF_YEAR);
				//If the player has been offline for 30 days or more, change the status freely
				if(days >= 30) //TODO make a configuration value?
				{
					stanceView.setStance(player, target, stance);
					player.sendMessage(ChatColor.GREEN + "[Stance] " + ChatColor.YELLOW + "Your stance with " + args[1] + 
							" has been changed to " + stance.coloredString());
				}
				else //If the player has been on within the last 30 days, they must approve the stance change (must be online to do this)
				{
					if(!target.isOnline())
					{
						player.sendMessage(ChatColor.GREEN + "[Stance] " + ChatColor.RED + "Error: Cannot set a stance with a player who is offline" +
								" and has been active within the last 30 days.");
						return false;
					}
					
					player.sendMessage(ChatColor.GREEN + "[Stance] " + ChatColor.YELLOW + "You have requested your stance with " + args[1] + 
							" be changed to " + stance.coloredString());
					player.sendMessage(ChatColor.GREEN + "[Stance] " + ChatColor.YELLOW + "They must accept this change");
					
					
					Player targ = Bukkit.getPlayer(target.getUniqueId());
					targ.sendMessage(ChatColor.GREEN + "[Stance] " + ChatColor.YELLOW + player.getName() + " has requested to change your stance with them to" +
							" " + stance.coloredString() + ChatColor.YELLOW + ". To agree do /stance accept " + player.getName());
					
					
					Relation re = new Relation(player.getUniqueId(),target.getUniqueId(),stance);
					this.pendingRelations.put(player.getUniqueId(),re);
					
					return false;
				}
			}
			else //If the new stance is more hostile then the current stance, it will change with no input from the other player
			{
				stanceView.setStance(player, target, stance);
				player.sendMessage(ChatColor.GREEN + "[Stance] " + ChatColor.YELLOW + "Your stance with " + args[1] + 
						" has been changed to " + stance.coloredString());
				if(target.isOnline())
				{
					Player targetP = (Player) target;
					targetP.sendMessage(ChatColor.GREEN + "[Stance] " + ChatColor.YELLOW + player.getName() + " has changed their stance with you");
					targetP.sendMessage(ChatColor.GREEN + "[Stance] " + ChatColor.YELLOW + player.getName() + " The new stance is " + stance.coloredString());
				}
			}
			
		}
		else if(args.length == 4 && args[0].equalsIgnoreCase("adminset"))
		{
			if(!player.hasPermission("magicapi.stance.admin"))
			{
				player.sendMessage(ChatColor.GREEN + "[Stance] " + ChatColor.RED + "Error: You do not have permission to use this command");
				return false;
			}
			
			OfflinePlayer target1 = Bukkit.getOfflinePlayer(args[1]);
			OfflinePlayer target2 = Bukkit.getOfflinePlayer(args[2]);
			
			if(target1 == null || !target1.hasPlayedBefore())
			{
				player.sendMessage(ChatColor.GREEN + "[Stance] " + ChatColor.RED + "Error: "+args[1]+" does not exist");
				return false;
			}
			if(target2 == null || !target2.hasPlayedBefore())
			{
				player.sendMessage(ChatColor.GREEN + "[Stance] " + ChatColor.RED + "Error: "+args[2]+" does not exist");
				return false;
			}
			
			if(target1.getUniqueId() == target2.getUniqueId())
			{
				player.sendMessage(ChatColor.GREEN + "[Stance] " + ChatColor.RED + "Error: Cannot set a players stance with themself");
				return false;
			}
			
			stanceView.setStance(target1, target2, EntityStance.valueOf(args[3].toUpperCase()));
			player.sendMessage(ChatColor.GREEN + "[Stance] " + ChatColor.YELLOW + "Stance Set");
		}
		
		else if(args.length == 2 && args[0].equalsIgnoreCase("accept"))
		{
			Player targ = Bukkit.getPlayer(args[1]);
			if(targ == null)
			{
				player.sendMessage(ChatColor.GREEN + "[Stance] " + ChatColor.RED + "Error: "+args[1]+" does not exist");
				return false;
			}
			
			if(pendingRelations.containsKey(targ.getUniqueId()))
			{
				Relation r = pendingRelations.get(targ.getUniqueId());
				stanceView.setStance(player.getPlayer(),targ.getPlayer(),r.stance);
				
				targ.sendMessage(ChatColor.GREEN + "[Stance] " + ChatColor.YELLOW + "Stance has been updated!");
				player.sendMessage(ChatColor.GREEN + "[Stance] " + ChatColor.YELLOW + "Stance has been accepted!");
			}			
		}
		// /stance accept <player>   Accepts the stance change that <player> requested
		
		
		return false;
		/*
		 * Notes
		 * When a stance is upgraded from a more hostile stance to a more friendly one the other player must accept this change.
		 * i.e. PlayerA and playerB have a stance of Hostile. If playerA wants to be neutral they set the status to neutral but 
		 * playerB must accept. However if the selected player has been offline for 30 days or more, the stance change will go though.
		 * 
		 * Example: if a player is hostile with another player and they change there stance to a non hostile stance
		 * the other player must accept the new stance.
		 * 
		 * 
		 */
	}
	
	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args)
	{
		if(!(sender instanceof Player)) return null;
		Player player = (Player)sender;
		
		List<String> commands = new ArrayList<String>();
		commands.add("help");
		commands.add("view");
		commands.add("set");
		commands.add("accept");
//		commands.add("");
		
		if(args.length == 0)
		{
			return commands;
		}
		else if(args.length == 1)
		{
			List<String> toReturn = new ArrayList<String>();
			for(String s : commands)
			{
				if(s.startsWith(args[0])) toReturn.add(s);
			}
			return toReturn;
		}
		else if (args.length == 2)
		{
			List<String> toReturn = new ArrayList<String>();
			for(Player p: Bukkit.getOnlinePlayers())
			{
				if(p.getName().toLowerCase().startsWith(args[1].toLowerCase()))
				{
					if(!p.getName().equalsIgnoreCase(player.getName())) toReturn.add(p.getName());
				}
			}
			return toReturn;
		}
		else if (args.length == 3)
		{
			if(args[0].equalsIgnoreCase("set"))
			{
				List<String> toReturn = new ArrayList<String>();
				toReturn.add(EntityStance.FRIENDLY.toString());
				toReturn.add(EntityStance.NEUTRAL.toString());
				toReturn.add(EntityStance.HOSTILE.toString());
				return toReturn;
			}
			else return null;
		}
		else
		{
			return null;
		}
	}
}
