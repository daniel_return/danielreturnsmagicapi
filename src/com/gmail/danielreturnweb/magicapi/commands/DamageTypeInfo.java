package com.gmail.danielreturnweb.magicapi.commands;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.NullArgumentException;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

import com.gmail.danielreturnweb.magicapi.MagicAPI;
import com.gmail.danielreturnweb.magicapi.DamageSystem.CustomDamageType;
import com.gmail.danielreturnweb.magicapi.spell.SpellManager;

/**
 * A command that allows players to list all custom damage types
 * TODO
 *
 */
public class DamageTypeInfo implements CommandExecutor, TabCompleter
{
	public static final ChatColor TITLE_CHATCOLOR = ChatColor.YELLOW;
	public static final ChatColor FIELD_CHATCOLOR = ChatColor.AQUA;
	public static final ChatColor TEXT_CHATCOLOR = ChatColor.GRAY;
	public static final ChatColor LIST_CHATCOLOR = ChatColor.GREEN;
	
	public static final ChatColor ENTRY_CHATCOLOR = ChatColor.GRAY;
	public static final ChatColor ERROR_CHATCOLOR = ChatColor.RED;
	
	public DamageTypeInfo()
	{
		// TODO Auto-generated constructor stub
	}
	
	
	/**
	 * Displays the damageType to the selected CommandSender
	 * @param sender
	 * @param type
	 */
	private void displayDamageType(CommandSender sender, CustomDamageType type)
	{
//		if(sender == null) throw new IllegalArgumentException("CommandSender cannot be null when using Grimoire.displaySpell(sender,spell)");
		if(sender == null) throw new NullArgumentException("CommandSender cannot be null when using DamageTypeInfo.displayDamageType(sender,type)");
		if(type == null) throw new NullArgumentException("Spell cannot be null when using DamageTypeInfo.displayDamageType(sender,type)");


	}
	
	
	/**
	 * Provides a list of Spells to the CommandSender
	 * @param sender
	 */
	private void displayDamageTypeList(CommandSender sender)
	{
		if(sender == null) throw new NullArgumentException("CommandSender cannot be null when using DamageTypeInfo.displayTypeList(sender)");		
		
		SpellManager sm = MagicAPI.getInstance().getSpellManager();
		List<CustomDamageType> types = null;
		
		sender.sendMessage("  ");
		sender.sendMessage(TITLE_CHATCOLOR + "Damage Type List");
		for(CustomDamageType type : types)
		{
		}
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) 
	{
		return false;
	}
	
	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args)
	{
		List<String> words = new ArrayList<String>();
		

		return words;
	}
}
