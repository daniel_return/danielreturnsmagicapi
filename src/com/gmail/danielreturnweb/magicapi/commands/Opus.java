package com.gmail.danielreturnweb.magicapi.commands;

import java.util.List;

import org.apache.commons.lang.NullArgumentException;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

import com.gmail.danielreturnweb.magicapi.MagicAPI;
import com.gmail.danielreturnweb.magicapi.effect.CustomEffectManager;
import com.gmail.danielreturnweb.magicapi.effect.api.CustomEffect;
import com.gmail.danielreturnweb.magicapi.effects.types.BuffType;
import com.gmail.danielreturnweb.magicapi.effects.types.SelfStackType;
import com.gmail.danielreturnweb.magicapi.spells.spell.Spell;

/**
 * A command that allows players to list all effects that are registered with the effect manager.
 * Also allows viewing of each effect description and other associated information.
 * By default effects that do not show up on the effect scoreboard will not be listable or 
 * viewable by players unless they have the associated permission. 
 * @author daniel
 *
 */
public class Opus implements CommandExecutor, TabCompleter
{
	
	public static final ChatColor TITLE_CHATCOLOR = ChatColor.YELLOW;
	public static final ChatColor FIELD_CHATCOLOR = ChatColor.AQUA;
	public static final ChatColor TEXT_CHATCOLOR = ChatColor.GRAY;
	public static final ChatColor LIST_CHATCOLOR = ChatColor.GREEN;
	
	public static final ChatColor ENTRY_CHATCOLOR = ChatColor.GRAY;
	public static final ChatColor ERROR_CHATCOLOR = ChatColor.RED;

	public Opus()
	{
		
	}
	

	/**
	 * 
	 * @param sender
	 * @param effect
	 */
	private void displayEffect(CommandSender sender, CustomEffect effect)
	{
//		if(sender == null) throw new IllegalArgumentException("CommandSender cannot be null when using Grimoire.displaySpell(sender,spell)");
		if(sender == null) throw new NullArgumentException("CommandSender cannot be null when using Grimoire.displaySpell(sender,spell)");
		if(effect == null) throw new NullArgumentException("Spell cannot be null when using Opus.displayEffect(sender,effect)");

		String id = effect.getEffectID();
		
		int duration = effect.getEffectDuration();
		duration = duration/20;
		BuffType buffType = effect.getBuffType();
		SelfStackType stackType = effect.getSelfStackType();
		int purge = effect.getPurgeResistence();
		
		String[] entry = effect.getOpusEntry();
		
		sender.sendMessage("  ");
		sender.sendMessage(TITLE_CHATCOLOR + "Opus Entry - " + ChatColor.WHITE + id);
		
		sender.sendMessage(FIELD_CHATCOLOR + "Duration: " + TEXT_CHATCOLOR + duration);
		sender.sendMessage(FIELD_CHATCOLOR + "Buff Type: " + TEXT_CHATCOLOR + buffType.toString());
		sender.sendMessage(FIELD_CHATCOLOR + "Stack Type: " + TEXT_CHATCOLOR + stackType.toString());
		sender.sendMessage(FIELD_CHATCOLOR + "Purge Resistance: " + TEXT_CHATCOLOR + purge);
		
		sender.sendMessage(TITLE_CHATCOLOR + "Entry: ");
		if(entry != null)
		{
			for(String s : entry)
			{
				sender.sendMessage("  " + ENTRY_CHATCOLOR + s);
			}
			sender.sendMessage("  ");
		}
		else
		{
			sender.sendMessage("  " + ENTRY_CHATCOLOR + "No Entry");
		}
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) 
	{
		CustomEffectManager manager = MagicAPI.getInstance().getCustomEffectManager();
		
		if(args.length == 0 || (args.length == 1 && args[0].equalsIgnoreCase("help")))
		{
			sender.sendMessage(TITLE_CHATCOLOR + "Opus Help");
//			sender.sendMessage(FIELD_CHATCOLOR + "/Opus list  " + TEXT_CHATCOLOR + " Lists all effects");
			sender.sendMessage(FIELD_CHATCOLOR + "/Opus view [effect]  " + TEXT_CHATCOLOR + "Allows the viewing of a effects entry and infomation");
		}
		
		if(args.length == 1 && args[0].equalsIgnoreCase("list"))
		{
		
		}
		
		if(args.length >= 2 && args[0].equalsIgnoreCase("view"))
		{
			StringBuilder builder = new StringBuilder();
			for(int i = 1; i < args.length; i++)
			{
				builder.append(args[i] + " ");
			}
			
			CustomEffect effect = manager.getRegisteredEffect(builder.toString().trim());
			
			
			if(effect == null)
			{
				sender.sendMessage(ERROR_CHATCOLOR + "Null Error:  " + TEXT_CHATCOLOR + " that effect does not exist");
				return false;
			}
			/*
			 * TODO how to handle multiple spells w/ the same name when displaying?
			 * Should this feature be removed and not allow spells with the same name?
			 * Should it be handled like a mail plugin with multiple pages?
			 * 	If so it would not display the spell info and instead tell the sender that there are multiple spells
			 * 	with the same name and how many there are. It would then say to do "/grimoire view <spell> <#>"
			 */
			
			this.displayEffect(sender, effect);
		}
		return false;
	}
	
	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args)
	{
		return null;
	}
}
