# DanielReturnsMagicAPI 
#### Release Version 1.3.0
#### Development Version: ---

Minecraft Target Version: 1.15.2

Other Supported Versions: none


### THIS API IS STILL IN HEAVY DEVELOPMENT AND NO GARENTEES ARE MADE TO KEEP THE API
### BACKWARDS COMPATABLE. UPDATES MAY AND WILL BREAK EXISTING FUNCTIONALITY.
	

## Overview


DanielReturns Magic API is a api that allows creation of Magic Spells and Effects.
This is an API and does not add any functionality excluding some default behaviors for various API implementations and a few technical spells and effects
for internal use and examples spells that can be enabled in the configuration file.
The API also adds in supporting API's, classes, functions, and managers to make the creation of Spells, effects, etc. easier and more manageable. 


This Readme file consists of what the API does along with ideas and notes of what I have planned for it, want it to do along with various notes
and design ideas.


## Magic Design Guidelines
With this magic API I have designed a concise set of guidelines regarding how magic should be implemented and used.
However the guidelines are not strictly enforced so developer may do what ever they want with there magic design when using this as a API plugin.
Following the guidelines however ensures a similar and concise experience between magic implementations.

Examples would be:

	The Hidden status should prevent single entity targeting spells from targeting the entity with the hidden status,
	while AOE spells can still target the hidden entity.
	
	True Damage not being negated in any way (See DamageManager class Java Doc)
	
	Spells and Effects taking into account a entities MagicResistance when calculating the spells damage, duration, and other effects.

## Using DanielReturnsMagicAPI as a server owner & Developer
At this time DanielReturnsMagicAPI is NOT RELEASED for bukkit or spigot.

DanielReturnsMagicAPI is not yet ready to be ran on a production server, however if you are still wanting to this is what you will need to do.
You will need to go to the Exports folder on the repo and navagate to Release folder. Next you select the version of the Plugin you are wanting to
utilize. Lastly copy it into your servers plugin folder.

Developers would go though the same process, but would additionaly import the API into there IDE and project.  


## Custom Effect API

The Custom Effect API consists of 3 main components. The classes and methods for creating a Custom Effect, the API/Service for placing and removing effects
onto a living entity along with a CustomEffectManager for registering and managing all custom effects, along with running/ticking them.

The API for placing and removing effects is a Service so it can be easily reimplemented by API users if they do not like its setup. This is
because this API controls the conversion between the stored state of the effect and the active effect/bundle. Removing the need for Custom Effect
creators to worry about how an effect is stored on a monster.

When a effect is placed upon a player a scoreboard will appear displaying the effect and its duration. The level will be included in the scoreboard
display at a later date.

The Custom Effect API also includes many interfaces for making standard unified effects. This is found in the effect components.
Components allow for easier creation of standard effects such as stuns, roots, regeneration etc. 
While these components and others can be made without them (such as making a custom effect and increasing hp during onUpdate() to make a regeneration)
this system will allow effects to work closer together. Such as making a degeneration effect that lowers regeneration could extend the Regeneration
component allow it to reduce regeneration instead of having to damage the player during the onUpdate().

Lastly there are multiple managers that effects can used for certain active components of effects that should stack/work with each other.

An example of this is life regeneration. Two sources of life regeneration can stack w/ eachother and this would work if each effect manualy
increased the HP of the target, but a Manager is provided to unify Health regneration allowing for some minor functionality that would be
very difficult to implement if each effect ran its own regeneration healing. Furthermore this allows the effect to have no update/tick component
reducing its load on the server. It would just need to add and remove the regeneration from the manager. 





## Spell and Magic Item API
This will allow for the creation of spells. There are two spell varients; item spells (SpellItem) and command spells (SpellCommand).
The first can be bound to a item using tags and triggered by using the item, while the other is triggered by using the spells command.
Lastly both types can be used together. 
There are also passive spells but these are an extention of item spells.

A Magic item is a ItemStack with a spell embeded into it.



## Entity Stance API
The Stance API allows for defining of stances between entities and players, players and players, and entities and entities.
This allows players and entities to have relations/stances with each other equal to HOSTILE, NEUTRAL, and FRIENDLY. 
Spells and effects can have there behavior changed based upon the casters stance with the target entity.

[as an example a AOE damage spell may exclude friendly players from being targeted to prevent friendly fire]

The API consists of an interface that defines how stances are determined. This API can be overriden by API users to redefine how it functions.
A very basic default implementation is provided by this API.
 
DanielReturns Magic API also includes a basic command to allow players to set there stances with each other.
This should work regardless of the stance implementation, unless the implementation prevents player from having stance with eachother,
or only allows one stance.





## Managers	
DanielReturnsMagicAPI has a few managers within it to better aid the user in some aspects. They can be used with spells, effects, and other systems.


### AbsorptionManager 
Allows placing and removing of absorption health upon a player.
	

### MagicBoostManager
Allows placing and removing of magic boost on a user. Spells may use magic boost when being casted to increase there overall power.
	

### DamageManager
The Damage Manager allows for custom damage to be dealt to entities.
	

### (Max) Health Manager
Allows maximum health to be placed and removed from the player.
This will provide a method to do vanilla HP gain/removal along with a method that
keeps the current percentage of HP. 
Example: a player has 10/20 HP. There Max HP is boosted to 40. Vanilla they would have 10/40.
but if the percentage is kept, they would then be 20/40.


### Regeneration Manager
The regeneration manager allows for an API user to place natural and magical health regeneration/degeneration upon an entity.
This allows for spells and effects manipulate the HP regeneration of an entity.
This does not effect Vanilla Natural or Magical Health regeneration. (Such as having full hunger or using a regeneration potion)

There is a Regeneration Event that can be listened to for manipulating the two types of regeneration and degeneration.

Lastly there are options in the configuration file that allow for players to have a base (always on) natural regeneration without 
the need for custom effects. Server owners can also configure if negative regeneration values can damage entities and if entities
can be killed by negative values. (These all default to false in the configuration file)


### HitEvasionManager
Allows the placement of Evasion and HitChance onto entities. Evasion is the chance to evade a attack while
Hit Chance is the chance to land a attack.
A entities "true" evasion is their evasion + the hit chance of the attacking entity.
Melee attack's will take into account "true" evasion while projectiles will only take into
account the shooting entities hit chance.

#### Magic and Evasion
Magic spells should respect evasion excluding AOE spells. 
Projectile magic spells should treat themselves as a normal Projectile (See projectile above).
Spells that directly target an entity should take the targets evasion into account to see if the 
spell misses but not the caster's Hit Chance.

Of course this is just a guideline for how Evasion and Hitchance should work.
Most of the above is not set in stone and can be changed to fit the API users needs.


### Hidden Manager
The hidden manager is to mark players as hidden or not. This uses Bukkit/Spigot's player.hide() method to hid players
from other players. In addition it provides a true sight API allowing players with true sight to see hidden players within
a certain distance from them. It provides a permission so admin/staff can see hidden players. Lastly monsters will not
target hidden player's to simulate the player being hidden from the entity. (This part will break if they attack monsters)
Multiple sources of Hidden can stack.

A general rule of thumb is that direct target spells should ignore hidden players but AOE's can target hidden players.
Essensialy the Hidden status is a more powerful invisability. 

	

### StatusManager [WIP]

NOTE: the StatusManager may not be created along with Effect Statuses. The originaly idea would cause all effects on an entity to be looped over when checking for a Status caused byu an effect. Moving forward most "statuses" will be implemented as there own manager that effects can "register" a player has having that status and then "unregister" when the effect ends. (Similar to how the MagicBoostManager and RegenerationManager work.) The managers would have any run() and/or event associated code if applicable. 

Manages active statuses. CustomEffects must implement a status interface for the active statuses to work.
Other status interfaces provide methods allowing status information to be passed appropriately. 
	
## Statuses
	
#### Stunned
Prevents the user from taking any actions.

#### Weak_Root
Prevents the user from moving and being effected by forced movement effects (such as being pushed around)

#### Rooted
Prevents the user from moving, being effected by forced movement effects and using moving items (such as ender pearls).

#### Ethereal
The unit takes 50% less normal and pure damage, but takes 50% more magical damage. True damage not effected.

#### MagicImmunity
Marks the entity immune to all spells, effects and magic damage.

#### EffectImmunity
Marks the entity immune to effects of a certain level and lower. Does not purge or dispell existing effects
The Effect will provide a level that it blocks.

#### MagicDamageImmunity
Marks the entity immune to magic damage.

#### MagicResistance
Provides a percentage based magic immunity to the unit Spells and Effects should take this value into account when calculating there effect values.

#### StatusImmunity 
Provides the entity with immunity to the Statuses this provides. (Set by effect maker) does no purge any statuses.
	
#### Invulnerability
Marks the LivingEntity immune to Normal and Pure Damage. Does not effect Magic and True Damage.
	
#### Disarmed
The entity cannot attack with normal attacks.
	
#### Silenced
The entity cannot use magic items or cast spells
	
#### Sleep  
Disables the unit compleatly in the same manner as a stun. However taking damage will remove sleep.

		
### Status Notes
	
	When a effect is applied to a LivingEntity the onInflict() should check for statuses and if the status list contains a value that blocks it,
	then the effect should exit and end its self. This would allow for effects/abilities that can pierce status effects.

	
	This splits statuses into two groups.
	Normal and Functional. Functional statuses would be stun and Rooted as they prevent certain functions while normal is fully up to spells and effects
	to change there effects upon. That or I have a onMove listener that see's if the effected entity has Stun or Rooted on them.
	
	I also need to have a purgeEffectByStatus() method
	
	
	
	
	
	
# Credits

### ExperienceManager (renamed PlayerExperience)

Credit: Dev_Richard

Git Hub Link: https://gist.github.com/RichardB122/8958201b54d90afbc6f0